Setup:
You will be creating a new Android Studio project named LastNameFirstName_CE05 using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top of all .java files using the below format.


// First Name Last Name

// AID - Term Number

// Java File Name


Grading:
Please refer to the CE:05 tab of this rubric to understand the grading procedures for this assignment.


Deliverables:
You will compress your project and upload a file named LastNameFirstName_CE05.zip.  Please see the class information activity about the penalties around not following the naming convention for turn-ins.

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
You will be building an application that utilizes multiple fragments to present data that is entered in a form. Minimally, your app must do the following things to be considered complete:


Follow the guidelines/requirements described below in the video, text, and/or images.


UI:

Application should only work in landscape mode
In portrait mode it should direct a user to rotate the phone
In landscape it should contain 2 side-by-side fragments. 1 ListFragment, 1 (displayed) form Fragment
Application contains a spinner, with 3 options, in the main activity (not in a fragment).
Selecting an option from the spinner loads a form that correlates with the selected item.
A refresh button is available to reload the selected category, including refreshing the list.  

Data source:

All items should be saved from the forms as custom objects
Each option will represent a different type of object.
The three object types listed in the spinner will all inherit from a common base class. For example, you might have a base class named "Person" and the three child classes could be "Student", "Teacher", and "Administrator".
All object type subclasses will have data members that are unique to their type but share a common set of data members in the base class.
Name, University, ID - Person
Grade - Student, Course - Teacher, Program - Administrator
All data must be saved and loaded from a single collection and persist through app rotation.
Collection should be mutable.
Collection should NOT store items as key/value pairs

Form fragments (Must inherit from the Fragment class):

Must load 3 different form fragments, 1 for each data type.
Form layout should accommodate each data types properties.
Each form Fragment should be a separate class.
Fragments should be loaded into the form fragment container.
Each form fragment should showcase all requisite data field for the custom data type selected and contain a button to submit the form values.
The fragment must not contain any member variables.**
 

ListFragment (Must inherit from the ListFragment class):

If the application does not contain data the container should display a stand-in fragment or empty state.
If the application contains data the container should display a list of stored items.
The fragment must not contain any member variables.**
Each object should show a line of text that is custom to the type.*
Joshua Donlan | Administrator
Michael Celey | Teacher
John Doe | Student

Format:

All static strings stored should be stored as string resources (including the static string array)
Styles are utilized for reusable view assets
The application uses proper units for sizing views and text

Extra Information


* To accomplish this, you may override the toString() method for the object classes.

** This excludes private static final variables such as TAGs as well as any Interfaces used.


This is a silent demonstration of the completed application. Please read the instructions to learn the required specifications of the project.


Code Exercise 05: Data Display Fragments Example

