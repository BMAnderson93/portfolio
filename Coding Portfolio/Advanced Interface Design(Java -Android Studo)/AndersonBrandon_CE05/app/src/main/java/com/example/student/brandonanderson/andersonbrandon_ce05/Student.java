package com.example.student.brandonanderson.andersonbrandon_ce05;

class Student extends Person {
    private String mGrade;

    public Student(String mName, String mID, String mUni, String _grade) {
        super(mName, mID, mUni);
        mGrade = _grade;
    }




    @Override
    public String toString() {

        return super.getmName() +" | " +this.getClass().getSimpleName();
    }
}
