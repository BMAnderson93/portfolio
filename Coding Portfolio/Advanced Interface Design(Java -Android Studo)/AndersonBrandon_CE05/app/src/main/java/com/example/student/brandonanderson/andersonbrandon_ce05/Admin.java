package com.example.student.brandonanderson.andersonbrandon_ce05;

class Admin extends Person{

    private String mProgram;

    public Admin(String mName, String mID, String mUni, String mProgram) {
        super(mName, mID, mUni);
        this.mProgram = mProgram;
    }


    @Override
    public String toString() {

        return super.getmName() +" | " +this.getClass().getSimpleName();
    }
}
