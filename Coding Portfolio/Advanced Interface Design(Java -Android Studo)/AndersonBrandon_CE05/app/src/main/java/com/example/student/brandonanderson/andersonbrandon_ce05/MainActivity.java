package com.example.student.brandonanderson.andersonbrandon_ce05;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements StudentFormFragment.AddStudentListener
        , TeacherFormFragment.AddTeacherListener, AdminFormFragment.AddAdminListener{


    private Spinner spinner;
    private Button refresh;
    private static final ArrayList<Person> people = new ArrayList<>();
    private final ArrayList<String> selections = new ArrayList<>();
    private static final ArrayList<Person>peopleToAdd = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main);

        } else {
            setContentView(R.layout.activity_rotated);
            setup();
        }

    }
    private void setup() {
        selections.add(getResources().getString(R.string.student));
        selections.add(getResources().getString(R.string.teacher));
        selections.add(getResources().getString(R.string.admin));
        spinner = (Spinner) findViewById(R.id.spinner);
        refresh =(Button) findViewById(R.id.button);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reload();
            }
        });
        ArrayAdapter<String> a = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                selections

        );

        spinner.setAdapter(a);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.FormFragmentContainer, StudentFormFragment.newInstance()).commit();
                } else if (i == 1) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.FormFragmentContainer, TeacherFormFragment.newInstance()).commit();
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.FormFragmentContainer, AdminFormFragment.newInstance()).commit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.listviewFragmentContainer, ListViewFragment.newInstance(people)).commit();
    }

    @Override
    public void addStudent(String name, String id, String uni, String grade) {

        peopleToAdd.add(new Student(name,id,uni,grade));

    }

    @Override
    public void addTeacher(String name, String id, String uni, String course) {
        peopleToAdd.add(new Teacher(name,id,uni,course));


    }
    @Override
    public void addAdmin(String name, String id, String uni, String program) {
        peopleToAdd.add(new Teacher(name,id,uni,program));

    }

    private void reload(){
        people.addAll(peopleToAdd);
        peopleToAdd.clear();
        getSupportFragmentManager().beginTransaction().replace(R.id.listviewFragmentContainer, ListViewFragment.newInstance(people)).commit();
    }

}
