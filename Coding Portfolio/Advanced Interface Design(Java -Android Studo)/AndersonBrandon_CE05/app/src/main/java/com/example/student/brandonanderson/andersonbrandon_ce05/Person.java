package com.example.student.brandonanderson.andersonbrandon_ce05;

import java.io.Serializable;

class Person implements Serializable {
    private String mName;
    private String mID;
    private String mUni;

    Person(String mName, String mID, String mUni) {
        this.mName = mName;
        this.mID = mID;
        this.mUni = mUni;
    }

    String getmName() {
        return mName;
    }




}
