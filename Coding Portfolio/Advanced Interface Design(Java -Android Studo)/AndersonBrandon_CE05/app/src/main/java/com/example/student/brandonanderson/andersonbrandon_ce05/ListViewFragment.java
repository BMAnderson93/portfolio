package com.example.student.brandonanderson.andersonbrandon_ce05;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class ListViewFragment extends ListFragment {
    private static final String ARG_PEOPLE = "ARG_PEOPLE";
    public ListViewFragment() {
    }

    public static ListViewFragment newInstance(ArrayList<Person> people) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_PEOPLE, people);
        ListViewFragment fragment = new ListViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getArguments() != null){
            ArrayList<Person> people = (ArrayList<Person>) getArguments().getSerializable(ARG_PEOPLE);


            ArrayAdapter<Person> adapter = new ArrayAdapter<>(
                    getActivity(), android.R.layout.simple_list_item_1, people
            );
            setListAdapter(adapter);
        }
    }
}
