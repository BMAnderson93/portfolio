package com.example.student.brandonanderson.andersonbrandon_ce05;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class TeacherFormFragment extends Fragment  {
    public TeacherFormFragment() {
        // Empty Default

    }

    public static TeacherFormFragment newInstance() {

        Bundle args = new Bundle();

        TeacherFormFragment fragment = new TeacherFormFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public interface AddTeacherListener{
        void addTeacher(String name, String id, String uni, String course);
    }
    private AddTeacherListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacherform, container, false);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof AddTeacherListener){
            mListener = (AddTeacherListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getView() != null){
            final EditText editTextName = (EditText) getView().findViewById(R.id.name);
            final EditText editTextUNI = (EditText) getView().findViewById(R.id.uni);
            final EditText editTextID = (EditText) getView().findViewById(R.id.id);
            final EditText editTextCourse = (EditText) getView().findViewById(R.id.course);
            Button log = (Button) getView().findViewById(R.id.addTeacher);

            log.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.addTeacher(editTextName.getText().toString()
                            , editTextID.getText().toString(), editTextUNI.getText().toString()
                            ,editTextCourse.getText().toString());

                    editTextID.setText("");
                    editTextName.setText("");
                    editTextCourse.setText("");
                    editTextUNI.setText("");
                }
            });
        }
    }
}