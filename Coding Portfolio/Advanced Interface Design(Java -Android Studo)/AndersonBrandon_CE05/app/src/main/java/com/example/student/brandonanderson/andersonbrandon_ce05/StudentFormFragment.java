package com.example.student.brandonanderson.andersonbrandon_ce05;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class StudentFormFragment extends Fragment  {
    public StudentFormFragment() {
        // Empty Default

    }

    public static StudentFormFragment newInstance() {

        Bundle args = new Bundle();

        StudentFormFragment fragment = new StudentFormFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public interface AddStudentListener{
        void addStudent(String name, String id, String uni, String grade);
    }
    private AddStudentListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_studentform, container, false);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof AddStudentListener){
            mListener = (AddStudentListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getView() != null){
            final EditText editTextName = (EditText) getView().findViewById(R.id.name);
            final EditText editTextUNI = (EditText) getView().findViewById(R.id.uni);
            final EditText editTextID = (EditText) getView().findViewById(R.id.id);
            final EditText editTextGrade = (EditText) getView().findViewById(R.id.grade);
            Button log = (Button) getView().findViewById(R.id.addStudent);

            log.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   mListener.addStudent(editTextName.getText().toString()
                           , editTextID.getText().toString(), editTextUNI.getText().toString()
                           ,editTextGrade.getText().toString());

                    editTextID.setText("");
                    editTextName.setText("");
                    editTextGrade.setText("");
                    editTextUNI.setText("");
                }
            });
        }
    }
}