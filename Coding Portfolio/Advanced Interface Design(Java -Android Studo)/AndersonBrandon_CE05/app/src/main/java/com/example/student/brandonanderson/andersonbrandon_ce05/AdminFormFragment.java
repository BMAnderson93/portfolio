package com.example.student.brandonanderson.andersonbrandon_ce05;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class AdminFormFragment extends Fragment  {
    public AdminFormFragment() {
        // Empty Default

    }

    public static AdminFormFragment newInstance() {

        Bundle args = new Bundle();

        AdminFormFragment fragment = new AdminFormFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public interface AddAdminListener{
        void addAdmin(String name, String id, String uni, String program);
    }
    private AddAdminListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_adminform, container, false);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof AddAdminListener){
            mListener = (AddAdminListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getView() != null){
            final EditText editTextName = (EditText) getView().findViewById(R.id.name);
            final EditText editTextUNI = (EditText) getView().findViewById(R.id.uni);
            final EditText editTextID = (EditText) getView().findViewById(R.id.id);
            final EditText editTextProgram = (EditText) getView().findViewById(R.id.program);
            Button log = (Button) getView().findViewById(R.id.addAdmin);

            log.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.addAdmin(editTextName.getText().toString()
                            , editTextID.getText().toString(), editTextUNI.getText().toString()
                            ,editTextProgram.getText().toString());

                    editTextID.setText("");
                    editTextName.setText("");
                    editTextProgram.setText("");
                    editTextUNI.setText("");

                }
            });
        }
    }
}