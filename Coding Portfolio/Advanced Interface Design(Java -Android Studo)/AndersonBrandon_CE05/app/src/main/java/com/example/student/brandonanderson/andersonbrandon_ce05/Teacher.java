package com.example.student.brandonanderson.andersonbrandon_ce05;

class Teacher extends Person {
    private String mCourse;

    public Teacher(String mName, String mID, String mUni, String mCourse) {
        super(mName, mID, mUni);
        this.mCourse = mCourse;
    }

    @Override
    public String toString() {
        return super.getmName() +" | " +this.getClass().getSimpleName();
    }
}
