 Setup
You will be creating a new Android Studio project named LastNameFirstName_CE06 using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top of all .java files using the below format.


// First Name Last Name

// AID - Term Number

// Java File Name


Grading
Please refer to the CE:06 tab of this rubric to understand the grading procedures for this assignment.


Deliverables
You will compress your project and upload a file named LastNameFirstName_CE06.zip.  Please see the class information activity about the penalties around not following the naming convention for turn-ins.

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions
You will be building an application that utilizes Java interfaces to pass data. Minimally, your app must do the following things to be considered complete:

Build an application that mimics a contacts application.
Contain three fragments:
ListFragment displaying any contacts saved. This must also contain an empty state when no contact data is available.
Details Fragment which loads when the user clicks on a list item. Data from that item is passed to the Activity via an interface method which loads in the Details Fragment.
Form Fragment that is loaded via a Floating Action Button located in the main activity.
The Form Fragment should contain, at minimum the following:
2 fields for the user to enter a first name and last name separately.
1 field for the user to enter a phone number
This field must use the appropriate input method.
The user must not be able to submit the form unless all fields are filled, and the user must be notified of this if the form is not complete.
If the form is complete the application should pass the data to be stored in a collection in the Activity.
Both the Details Fragment and Form Fragment must open and close using a custom animation.
The user must not be able to get trapped within an application screen. The user should be able to return to the List or Stand-in Fragment by popping the back stack.
The Floating Action Button should also be hidden if a fragment other than the List or Stand-in Fragment is visible.

Extra Information

You may start this application using a �Blank Activity� which contains a Floating Action Button and coordinator layout however you must remove the options menu if it is unused.


This is a silent demonstration of the completed application. Please read the instructions to learn the required specifications of the project.


Code Exercise 06: Interface Callbacks & Animations

