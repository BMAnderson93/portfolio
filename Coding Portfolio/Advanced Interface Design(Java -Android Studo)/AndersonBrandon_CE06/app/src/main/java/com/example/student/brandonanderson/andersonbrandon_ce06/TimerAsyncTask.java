// Brandon Anderson

// JAV1 - MDV3810

// MainActivity.java
package com.example.student.brandonanderson.andersonbrandon_ce06;


import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class TimerAsyncTask extends AsyncTask<String, Integer, Float> {

    static final private String TAG = "JAV1_DAY7_TASK";


    static final private long SLEEP = 100;
    private Long startTime;
    private Long compareTime;
    final private Context mHostContext;
    final private OnFinished mFinishedInterface;

    // An inner-interface for the user of this class to be notified when the task is done
    interface OnFinished {
        void onFinished();
        void onCancelled(Float timePassed);
        void onUpdate(Integer mins, Integer secs);
    }

    TimerAsyncTask(Context _context, OnFinished _finished) {
        mHostContext = _context;
        mFinishedInterface = _finished;
    }

    // TODO:
    // implement pre execute

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


        // Show toast


        Toast.makeText(mHostContext, R.string.onPreExecute, Toast.LENGTH_SHORT).show();
    }
    // TODO:
    // implement do in background

    @Override
    protected Float doInBackground( String... strings) {
        // process parameter passed to the execute method
        if(strings == null || strings.length <= 0  || strings[0].trim().isEmpty()){
            return 0.0F;

        }

        startTime = System.currentTimeMillis();
        compareTime = startTime;
        Integer pcount = Integer.parseInt(strings[0]);
        Integer count = Integer.parseInt(strings[0]);

        // block until count is satisfied
        while(count > 0 && !isCancelled()){


            count = pcount - ((int)((System.currentTimeMillis() - startTime) / 1000));
            publishProgress(count);


          try{
                Thread.sleep(SLEEP);
            }catch (InterruptedException e){
                e.printStackTrace();
            }



        }

        return ((System.currentTimeMillis() - startTime) / 1000.0f);

    }



    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);


        Integer mins = values[0] / 60;
        Integer secs = values[0] % 60;
        mFinishedInterface.onUpdate(mins, secs);

    }


    @Override
    protected void onPostExecute(Float aFloat) {

        if(mFinishedInterface != null){
            mFinishedInterface.onFinished();
        }


    }



    @Override
    protected void onCancelled(Float aFloat) {



        if(mFinishedInterface != null){

            mFinishedInterface.onCancelled(aFloat);
        }
    }


}
