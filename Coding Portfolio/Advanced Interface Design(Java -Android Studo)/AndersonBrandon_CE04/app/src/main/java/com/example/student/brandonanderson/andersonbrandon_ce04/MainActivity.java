package com.example.student.brandonanderson.andersonbrandon_ce04;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Person> allPeople = new ArrayList<>();
    private ListView listView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createReviews();
        listView = (ListView) findViewById(R.id.listView);
        CustomBaseAdapter ca = new CustomBaseAdapter(this, allPeople);
        listView.setAdapter(ca);


    }


    private void createReviews(){

        allPeople.add(new Person("Brandon Anderson", "08/10/2018", 5,
                R.drawable.round1, "Best app ive used!", "Totally fun to play, endless hours of fun"));
        allPeople.add(new Person("Kevin Anderson", "12/12/2018", 1,
                R.drawable.round1, "Worst app ive used!", "Awful experiance full of microtransactions meant to nickle and dime you"));
        allPeople.add(new Person("Dave Anderson", "03/19/2008", 4,
                R.drawable.round1, "Its decent", "Could be a lot better but still fun"));
        allPeople.add(new Person("Kairi Anderson", "10/30/2011", 2,
                R.drawable.round1, "This app is broken!", "Cant even log in, keeps crashing and deleted all my progress!"));
        allPeople.add(new Person("Robert Anderson", "12/19/2010", 1,
                R.drawable.round1, "Please fix!", "Really was looking forward to this game, too bad i cant play it."));

    }
}
