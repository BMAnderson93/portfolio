// Brandon Anderson

// JAV1 - MDV3810

// Person.java

package com.example.student.brandonanderson.andersonbrandon_ce04;



public class Person {

    private final String mFullName;
    private final String mDate;
    private final String mTitle;
    private final String mReview;
    private final Integer mRating;

    private final Integer mIcon;


    Person(String _FN, String _Desc,Integer _Rating, Integer _P, String _Title, String _Review ){

        mFullName =_FN;
        mTitle = _Title;
        mReview = _Review;
        mDate = _Desc;
        mRating = _Rating;
        mIcon = _P;

    }

    String getName(){return mFullName; }
    Integer getRating(){return mRating;}
    String getDate(){return  mDate;}
    Integer getPic(){return mIcon;}
    String getReview(){return mReview;}
    String getTitle(){return mTitle;}



    @Override
    public String toString() {
        return  getName();
    }
}
