// Brandon Anderson

// JAV1 - MDV3810

// CustomBaseAdapter.java

package com.example.student.brandonanderson.andersonbrandon_ce04;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomBaseAdapter extends BaseAdapter  {


    private static final long BASE_ID = 0x01011;

    private final Context mContext;


    private final  List<Person> mAccounts;




    public CustomBaseAdapter(Context _context, List<Person> _account){

        mContext = _context;
        mAccounts = _account;

    }



    @Override
    public int getCount(){

        if(mAccounts != null){

            return mAccounts.size();
        }

        return 0;
    }


    @Override
    public Object getItem(int position){

        if(mAccounts != null && position >= 0 || position < mAccounts.size()){

            return mAccounts.get(position);
        }

        return null;
    }



    @Override
    public long getItemId(int position) {

        return BASE_ID + position;

    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder vh;

        if (convertView == null){

            convertView = LayoutInflater.from(mContext).inflate(R.layout.customadapter, parent, false);

            vh = new ViewHolder(convertView);
            convertView.setTag(vh);

        }else{

            vh = (ViewHolder)convertView.getTag();
        }

        Person a = (Person)getItem(position);

        if (a != null){

            vh.tv_fn.setText(a.getName());
            vh.tv_date.setText(a.getDate());
            vh.iv_icon.setImageResource(a.getPic());
            vh.tv_title.setText(a.getTitle());
            vh.tv_review.setText(a.getReview());

            Integer rating = a.getRating();

            if(rating == 5){
                vh.iv_s1.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s2.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s3.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s4.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s5.setImageResource(R.drawable.round_star_black_18dp);

            }else if(rating == 4){
                vh.iv_s1.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s2.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s3.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s4.setImageResource(R.drawable.round_star_black_18dp);
            }else if(rating == 3){
                vh.iv_s1.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s2.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s3.setImageResource(R.drawable.round_star_black_18dp);
            }else if (rating == 2){
                vh.iv_s1.setImageResource(R.drawable.round_star_black_18dp);
                vh.iv_s2.setImageResource(R.drawable.round_star_black_18dp);

            }else if (rating == 1){
                vh.iv_s1.setImageResource(R.drawable.round_star_black_18dp);
            }




        }


        return convertView;
    }




    static class ViewHolder{

        private final TextView tv_fn;
        private final  TextView tv_date;
        private final TextView tv_title;
        private final TextView tv_review;
        private final ImageView iv_icon;
        private final ImageView iv_s1;
        private final ImageView iv_s2;
        private final ImageView iv_s3;
        private final ImageView iv_s4;
        private final ImageView iv_s5;



        private ViewHolder(View _layout) {

            tv_title = (TextView) _layout.findViewById(R.id.title);
            tv_review = (TextView) _layout.findViewById(R.id.review);
            tv_fn = (TextView) _layout.findViewById(R.id.listview_item_fullName);
            tv_date =(TextView) _layout.findViewById(R.id.date);
            iv_s1 = (ImageView) _layout.findViewById(R.id.s1);
            iv_s2 = (ImageView) _layout.findViewById(R.id.s2);
            iv_s3 = (ImageView) _layout.findViewById(R.id.s3);
            iv_s4 = (ImageView) _layout.findViewById(R.id.s4);
            iv_s5 = (ImageView) _layout.findViewById(R.id.s5);
            iv_icon =(ImageView) _layout.findViewById(R.id.icon);


        }

    }
}