Setup:
You will be creating a new Android Studio project named "LastNameFirstName_CE04" using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top of all .java files using the below format.


// First Name Last Name

// AID - Term Number

// File Name


Grading:
Please refer to the CE:04 tab of this rubric to understand the grading procedures for this assignment.


Deliverables:
You will compress and upload a file named "LastNameFirstName_CE04.zip" with the following contents:

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.


For this synthesis assignment, you will be building a mockup of a single page in the Google Play Store. Since this is a synthesis assignment, you will be given a more general list of requirements than other assignments and will be expected to build the application using your knowledge of Android instead of following a detailed list of tasks.


The application should mimic the look and feel of a Google Play Store App page.
It should contain the following elements:
A large image at the top of the screen acting as a masthead.
The page should scroll and the foreground should scroll over the header image.
An on-screen app icon paired with the app�s title and developer.
2 buttons (one to open, one to uninstalled) styled appropriately.
A �What�s New� section, styled appropriately, with a bulleted list of �new features�
A �Read More� link, styled appropriately (but non-functional)
A section with 2-3 app screenshots (Does not need to scroll horizontally)
A list of 5 unique reviews, containing:
User avatar
User name
Date
1-5 star rating
Title
A brief summary
A �read all reviews� link, styled appropriate (but non-functional)
A share icon with text (non-functional)
A footer with a list of links to:
Email the developer
View app permission reviews
Flag as inappropriate
The application should contain all appropriate system icons (see example below)

Extra Information:


All system icon resources needed for this assignment can be downloaded from the Material Design icons page.
The �What�s New� icon can be downloaded here.
The reviews section should use a custom adapter view to ensure a consistent style for each review.
Dividers can be created by making �line� shape drawables.

This is a silent demonstration of the completed application. Please read the instructions to learn the required specifications of the project.


Code Exercise 04: Play Store Page


Please review the following wireframes for an example of how the application should look.


