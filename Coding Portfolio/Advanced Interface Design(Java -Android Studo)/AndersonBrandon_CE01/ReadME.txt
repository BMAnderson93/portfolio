Setup:
You will be creating a new Android Studio project named "LastNameFirstName_CE01" using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top of all .java files using the below format.


// First Name Last Name

// AID - Term Number

// File Name


Grading:
Please refer to the CE:01 tab of this rubric to understand the grading procedures for this assignment.


Deliverables:
You will compress and upload a file named "LastNameFirstName_CE01.zip" with the following contents:

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.


Application UI:

Application should contain the following elements:
A header area near the top of the viewport that contains a left-aligned picture* of yourself, and large right-aligned text with your full name.
Below the header should be 4 different sections (birthday, hometown, hobbies, and a short �about me� section)**.
Each section should stack vertically 1 after another (not side-by-side)
Each section should include a title above it.
Application must contain both Linear and Constraint layouts***.
The linear layout should be shown by default.
The constraint layout should be hidden and not take up space until shown.
When clicked, the button near the bottom center of the screen should switch the visible layout between the linear layout and the constraint layout.
The components should be positioned identically on both the linear and constraint layouts.

UI Standards:

All view size and position properties should use appropriate DP units.
All text size properties should use appropriate SP units.
Image resources used should be properly resized before implementation and should not rely heavily on Android�s native image scaling.
Application should fit Android�s 48dp row standards.

Format:

All class names should begin with an uppercase letter.
All method names should begin with a lowercase letter.
All views should have an ID that is named to represent what the view will be used for. (e.g. naming the addition button "button_addition")

Extra Information


* These pictures should be drawable resources and should not use mipmap resources. Mipmaps are reserved for app icons only.

** All views used should be uneditable TextViews. EditText widgets are not allowed.

*** Both the Linear and Constraint layouts should be children of a single parent layout. The parent may be any type of layout.


Hint: You may use multiple, nested, linear layouts to complete the assignment, however, the constraint layout must not contain any linear layouts.


This is a silent demonstration of the completed application. Please read the instructions to learn the required specifications of the project.


Code Exercise 01: Layouts