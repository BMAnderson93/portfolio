// Brandon Anderson

// AID - MDV 3325

// AndersonBrandon_CE01


package com.example.student.brandonanderson.andersonbrandon_ce01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {

    String TAG = "TESTING";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.button).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
    }
    @Override
    public  void onClick(View v) {

        String buttonTag = v.getTag().toString();
        if (v.getTag() == findViewById(R.id.button2).getTag()){
            findViewById(R.id.linear1).setVisibility(View.GONE);
            findViewById(R.id.linear2).setVisibility(View.GONE);
            findViewById(R.id.constraint).setVisibility(View.VISIBLE);
        }
        else{
            findViewById(R.id.linear1).setVisibility(View.VISIBLE);
            findViewById(R.id.linear2).setVisibility(View.VISIBLE);
            findViewById(R.id.constraint).setVisibility(View.GONE);
        }
        Log.d(TAG, buttonTag);
    }
}
