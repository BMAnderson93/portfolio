Setup:
You will be creating a new Android Studio project named "LastNameFirstName_CE03" using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top of all .java files using the below format.


// First Name Last Name

// AID - Term Number

// File Name


Grading:
Please refer to the CE:03 tab of this rubric to understand the grading procedures for this assignment.


Deliverables:
You will compress and upload a file named "LastNameFirstName_CE03.zip" with the following contents:

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.


UI:

The application should contain both a portrait and landscape layout.
The application should contain a Frame Layout with a solid background color.
In portrait, the Frame Layout should be near the top center and ~200dp square.
In landscape, the Frame Layout should be left aligned and ~275dp square.
An image or drawable with a transparent background should be centered in the middle of the Frame Layout.
The Frame Layout should also contain a TextView with placeholder text near the top center.
Under the Frame Layout (portrait), or right of the Frame Layout (Landscape) should be a form with the following items:
A radio group with a choice of 3 color options, with a text title or label.
An EditText that allows the user to fill in text.
An additional radio group with the choice of 3 text color options, with a title.
A Switch control, with a title, that toggles bold text.

Styles and Theme:

The app should have a custom theme with an appropriate accent, primary, and primary dark colors.
All non-shared properties of the Frame Layout should be loaded from separate styles for both portrait and landscape views.
All Frame Layout background colors should be loaded from color resources.
All TextView colors and the bold option should be loaded from separate child styles.
Each title should load from 1 consistent style, regardless of orientation.

Functionality:

Selecting a color from the first radio group should change the background color of the Frame Layout. *
Entering text into the EditText field should change the TextView within the Frame Layout using the addTextChangedListener() method.
Selecting a color from the second radio group should change the text color of the text within the Frame Layout. *
Toggling the Switch control should enable or disable bold font weight of the TextView within the Frame Layout.
All text changes must be applied by changing only the view�s style using the setTextAppearance() method.

Format:

All strings and colors should be loaded from the appropriate resource files.
All style names should represent what the style will be used for. (e.g. Naming the title style �TextTitle�)
All accessed views should have an ID that is named to represent what the view will be used for. (e.g. naming the radio group "backgroundColorGroup")
The application should have proper white space as described by the Material Design grid system.

Extra Information


* RadioGroups should correctly use onCheckChangedListener() and not rely on a general onClickListeners.


This is a silent demonstration of the completed application. Please read the instructions to learn the required specifications of the project.


Code Exercise 03: Styles & Themes