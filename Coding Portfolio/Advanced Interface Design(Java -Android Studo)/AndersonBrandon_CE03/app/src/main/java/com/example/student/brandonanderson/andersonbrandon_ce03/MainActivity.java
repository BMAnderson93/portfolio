package com.example.student.brandonanderson.andersonbrandon_ce03;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private RadioGroup bgColor;
    private RadioGroup txtColor;
    private FrameLayout frameLayout;
    private TextView txtDisplayed;
    private EditText txtInput;
    private Switch boldSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == 1 ){
            setContentView(R.layout.activity_main);
        }else {setContentView(R.layout.activity_rotated);}

        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        boldSwitch = (Switch) findViewById(R.id.switch1);
        bgColor = (RadioGroup) findViewById(R.id.radioGroup);
        txtDisplayed =(TextView) findViewById(R.id.textView2);
        txtInput = (EditText) findViewById(R.id.editText);
        boldSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    txtDisplayed.setTypeface(Typeface.DEFAULT_BOLD);
                }else{txtDisplayed.setTypeface(Typeface.DEFAULT);}
            }
        });
        txtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtDisplayed.setText(txtInput.getText());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        bgColor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int rdo1 =  findViewById(R.id.radioButton1).getId();
                int rdo2 =  findViewById(R.id.radioButton2).getId();
                int rdo3 =  findViewById(R.id.radioButton3).getId();
                if(i == rdo1){frameLayout.setBackgroundColor(getResources().getColor(R.color.colorOptionPink));}
                else if(i == rdo2){ frameLayout.setBackgroundColor(getResources().getColor(R.color.colorOptionYellow));}
                else if(i == rdo3){ frameLayout.setBackgroundColor(getResources().getColor(R.color.colorOptionBlue));}


            }
        });
        txtColor = (RadioGroup) findViewById(R.id.radioGroup2);
        txtColor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int rdo1 =  findViewById(R.id.radioButton4).getId();
                int rdo2 =  findViewById(R.id.radioButton5).getId();
                int rdo3 =  findViewById(R.id.radioButton6).getId();
                if(i == rdo1){txtDisplayed.setTextColor(getResources().getColor(R.color.colorOptionPink));}
                else if(i == rdo2){ txtDisplayed.setTextColor(getResources().getColor(R.color.colorOptionYellow));}
                else if(i == rdo3){ txtDisplayed.setTextColor(getResources().getColor(R.color.colorOptionBlue));}

            }
        });
    }
}
