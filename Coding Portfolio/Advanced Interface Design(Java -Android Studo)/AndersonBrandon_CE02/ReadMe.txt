Setup:
You will be creating a new Android Studio project named "LastNameFirstName_CE02" using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top of all .java files using the below format.


// First Name Last Name

// AID - Term Number

// File Name


Grading:
Please refer to the CE:02 tab of this rubric to understand the grading procedures for this assignment.


Deliverables:
You will compress and upload a file named "LastNameFirstName_CE02.zip" with the following contents:

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.


Application UI:

The application should contain 3 sections discussing a particular topic.
Each section should contain a text title (Introduction, Summary, Conclusion) followed by a brief paragraph. All text must be uneditable.
All titles should have a different text style than the body text, have a different text color* than the body text, and should be styled consistently.
The application should contain a header image that relates to the text content.
In portrait the header image should:
Be positioned at the top of the viewport.
Fill the viewport end-to-end with no padding or margin on the sides or above.
Not exceed a height of 20% of the viewport vertically.
In landscape the header image should:
Be positioned left on the left side of the view port.
Fill the viewport vertically with no padding or margin above, below, or on the left side.
Not exceed a width of 25% of the viewport horizontally.
In both portrait and landscape, the image should remain static while the content should scroll if it extends past the viewport.
The app default theme must be changed by editing only the default theme colors in the colors XML resource file.

Resources:

The app must contain a separate, landscape, Main Activity XML layout.
All static string values must be referenced from the strings XML resource.
All color values must be referenced via the colors XML resource.
Image resources used should be properly resized before implementation and should not rely heavily on Android�s native image scaling.

Format:

All class names should begin with an uppercase letter.
All method names should begin with a lowercase letter.
All accessed views should have an ID that is named to represent what the view will be used for. (e.g. naming the addition button "button_addition")

Extra Information


* The colors used must not be black or grey, and must not be one of the predefined theme colors (colorPrimary, colorPrimaryDark, or colorAccent).


The application content can be about whatever you�d like so long as it is clear, consistent, and maintains professionalism.


This is a silent demonstration of the completed application. Please read the instructions to learn the required specifications of the project.


Code Exercise 02: Resources