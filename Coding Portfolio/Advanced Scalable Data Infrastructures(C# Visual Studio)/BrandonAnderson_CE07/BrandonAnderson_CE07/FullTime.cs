﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE07
{
    class FullTime : Hourly
    {
        public FullTime(string name, string address, decimal payPerHour)
            :base (payPerHour, 40, name, address)
        {

        }
        public override decimal CalculatePay()
        {

            decimal pay = ((_hoursPerWeek * _payPerHour) * 4) * 12;
            return pay;
        }
    }
}
