﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE07
{
    class Salaried : Employee
    {
       protected decimal _salary;
        public decimal GetSalary()
        {
            return _salary;
        }
        public Salaried(string name, string address, decimal salary)
            :base(name, address)
        {
            _salary = salary;
        }
        public override decimal CalculatePay()
        {
            return _salary;
        }

    }
}
