﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE07
{
    class Employee : IComparable
    {
        string _name;
        string _address;

        public string GetName()
        {
            return _name;
        }
        public string GetAddress()
        {
            return _address;
        }
        public int CompareTo(object p)
        {
            Employee n = p as Employee;
            string name2 = n.GetName();
            char[] nameArray = _name.ToCharArray();
            char[] nameArray2 = name2.ToCharArray();
            for (int i = 0; i < _name.Length; i++)
            {
                if (nameArray[i] < nameArray2[i])
                {
                    return 0;
                }
                else if (nameArray[i] > nameArray2[i])
                {
                    return 1;
                }
                else if (nameArray[i] == nameArray2[i])
                {

                }
                else
                    return 0;

            } 
            
            return 0;
        }
     
       
        public Employee(string name, string address)
        {
            _name = name;
            _address = address;

        }
        public virtual decimal CalculatePay()
        {
            decimal pay = .0m;
            return pay;
        }
    }
}
