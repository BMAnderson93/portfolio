﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE07
{
    class Hourly : Employee
    {
        protected decimal _payPerHour;
        protected decimal _hoursPerWeek;
       
        public decimal GetPayPerHour()
        {
            return _payPerHour;
        }
        public decimal GetHoursPerWeek()
        {
            return _hoursPerWeek;
        }
        
        public Hourly( decimal payPerHour, decimal hoursPerWeek, string name, string address)
            :base (name, address)
        {
            _payPerHour = payPerHour;
            _hoursPerWeek = hoursPerWeek;
            
          

        }
        
    }
}
