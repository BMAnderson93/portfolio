﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE07
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>();
            bool running = true;
            while (running)
            {
                Console.Clear();
               
                Console.Write("Please select a option from the following menu." + Environment.NewLine +
                    "1.) Add Employee" +Environment.NewLine+ "2.) Remove Employee" + Environment.NewLine +
                    "3.) Display Payroll" + Environment.NewLine + "4.) Exit" + Environment.NewLine);
                string input = Console.ReadLine().ToLower();
                switch (input)
                {
                    case "1":
                    case "add employee":
                        {
                            Console.WriteLine("What type of employee would you like to create?" + Environment.NewLine +
                                "1.) FullTime 2.) PartTime 3.) Contractor 4.) Salaried 5.) Manager");
                            input = Console.ReadLine().ToLower();
                            switch (input)
                            {
                                case "1":
                                case "fulltime":
                                    {
                                        Console.WriteLine("Please enter the employees full name.");
                                        string fullName = NoNumbers(Console.ReadLine());
                                        Console.WriteLine("Please enter the address of the employee.");
                                        string address = Console.ReadLine();
                                        Console.WriteLine("Please enter the employees hourly pay.");
                                        decimal hourlyPay = IsDecimal(Console.ReadLine());
                                        FullTime newEmployee = new FullTime(fullName, address, hourlyPay);
                                        employees.Add(newEmployee);
                                        employees.Sort();

                                    }
                                    break;
                                case "2":
                                case "parttime":
                                    {
                                        Console.WriteLine("Please enter the employees full name.");
                                        string fullName = NoNumbers(Console.ReadLine());
                                        Console.WriteLine("Please enter the address of the employee.");
                                        string address = Console.ReadLine();
                                        Console.WriteLine("Please enter the employees hourly pay.");
                                        decimal hourlyPay = IsDecimal(Console.ReadLine());
                                        Console.WriteLine("Please enter the amount of hours the employee works weekly.");
                                        decimal hoursPerWeek = IsDecimal(Console.ReadLine());
                                        PartTime newEmployee = new PartTime(fullName, address, hourlyPay, hoursPerWeek);
                                        employees.Add(newEmployee);
                                        employees.Sort();

                                    }
                                    break;

                                case "3":
                                case "contractor":
                                    {
                                        Console.WriteLine("Please enter the contractors full name.");
                                        string fullName = NoNumbers(Console.ReadLine());
                                        Console.WriteLine("Please enter the address of the contractor.");
                                        string address = Console.ReadLine();
                                        Console.WriteLine("Please enter the contractor hourly pay.");
                                        decimal hourlyPay = IsDecimal(Console.ReadLine());
                                        Console.WriteLine("Please enter the amount of hours the contractor works weekly.");
                                        decimal hoursPerWeek = IsDecimal(Console.ReadLine());
                                        Console.WriteLine("Please enter the bonus the contractor recieves");
                                        decimal noBenBonus = IsDecimal(Console.ReadLine());
                                        Contractor newContractor = new Contractor(fullName, address, hourlyPay, hoursPerWeek, noBenBonus);
                                        employees.Add(newContractor);
                                        employees.Sort();
                                    }
                                    break;

                                case "4":
                                case "salaried":
                                    {
                                        Console.WriteLine("Please enter the employees full name.");
                                        string fullName = NoNumbers(Console.ReadLine());
                                        Console.WriteLine("Please enter the address of the employee.");
                                        string address = Console.ReadLine();
                                        Console.WriteLine("Please enter the salary of this employee.");
                                        decimal salary = IsDecimal(Console.ReadLine());
                                        Salaried newEmployee = new Salaried(fullName, address, salary);
                                        employees.Add(newEmployee);
                                        employees.Sort();



                                    }
                                    break;
                                case "5":
                                case "manager":
                                    {
                                        Console.WriteLine("Please enter the manager full name.");
                                        string fullName = NoNumbers(Console.ReadLine());
                                        Console.WriteLine("Please enter the address of the manager.");
                                        string address = Console.ReadLine();
                                        Console.WriteLine("Please enter the salary of this manager.");
                                        decimal salary = IsDecimal(Console.ReadLine());
                                        Console.WriteLine("Please enter the bonus for the manager.");
                                        decimal bonus = IsDecimal(Console.ReadLine());

                                        Manager newEmployee = new Manager(fullName, address, salary, bonus);
                                        employees.Add(newEmployee);
                                        employees.Sort();

                                    }
                                    break;
                                default:
                                    {
                                        Console.WriteLine("Invalid input selected, returning to the main menu");
                                        Console.ReadKey();
                                    }
                                    break;
                            }

                        }
                        break;

                    case "2":
                    case "remove employee":
                        {
                           
                            int userInputNumber;
                            if (employees.Count == 0)
                            {
                                Console.WriteLine("There are no employees to remove, do some hiring!");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("Please select an employees number to remove.");
                                for (int i = 0; i < employees.Count; i++)
                                {
                                    string count = (i + 1).ToString() + ".) ";
                                    Console.WriteLine(count + employees[i].GetName());




                                }
                                input = Console.ReadLine();
                                while (!int.TryParse(input, out userInputNumber) || userInputNumber > employees.Count || userInputNumber < 0 || input == "")
                                {
                                    Console.WriteLine("Please enter a valid input!");
                                    input = Console.ReadLine();
                                }
                                Console.Clear();
                                employees.RemoveAt(userInputNumber - 1);
                                Console.WriteLine("Employee removed!");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "3":
                    case "display payroll":
                        {
                            if (employees.Count() == 0)
                            {
                                Console.WriteLine("You must have atleast 1 employee before doing this! Get to hiring!");
                                Console.ReadKey();
                            }
                            else
                            {
                                for (int i = 0; i < employees.Count; i++)
                                {
                                    string fullName = employees[i].GetName();
                                    string address = employees[i].GetAddress();
                                    string pay = employees[i].CalculatePay().ToString();
                                    Console.WriteLine(fullName + " " + address + " $" + pay);
                                }
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            running = false;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Please enter a valid choice!");
                            Console.ReadKey();
                        }
                        break;
                }
            }

         
        }
        public static string NoNumbers(string input)
        {
            while (input.Any(char.IsDigit))
            {
                Console.WriteLine("Please enter a valid name.");
                input = Console.ReadLine();
            }
            string fullName = input;
            return fullName;
        }
        public static decimal IsDecimal(string input)
        {
            decimal payPerHour;
            while(!decimal.TryParse(input, out payPerHour) || payPerHour <= 0)
            {
                Console.WriteLine("Please enter a correct input");
                input = Console.ReadLine();
            }
            return payPerHour;

        }
    }
}
