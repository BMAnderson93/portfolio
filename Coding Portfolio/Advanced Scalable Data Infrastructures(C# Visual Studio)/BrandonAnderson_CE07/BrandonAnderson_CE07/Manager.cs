﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE07
{
    class Manager : Salaried
    {
        decimal _bonus;

        public Manager(string name, string address, decimal salary, decimal bonus)
            :base (name, address, salary)
        {
            _bonus = bonus;

        }
        public override decimal CalculatePay()
        {
            decimal pay = _salary + _bonus;
            return pay;
        }
    }
}
