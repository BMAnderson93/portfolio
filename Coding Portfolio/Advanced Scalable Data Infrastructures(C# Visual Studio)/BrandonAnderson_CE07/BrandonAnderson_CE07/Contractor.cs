﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE07
{
    class Contractor : Hourly
    {
        decimal _noBenefitsBonus;

        public decimal GetBonus()
        {
            return _noBenefitsBonus;
        }

        public Contractor(string name, string address, decimal payPerHour, decimal hoursPerWeek, decimal noBenefitsBonus)
            :base (payPerHour, hoursPerWeek, name, address)
        {
            _noBenefitsBonus = noBenefitsBonus;
        }
        public override decimal CalculatePay()
        {
            decimal basePay = ((_hoursPerWeek * _payPerHour)* 4) * 12;
            decimal pay = basePay +( basePay * _noBenefitsBonus);
            return pay;

        }
    }
}
