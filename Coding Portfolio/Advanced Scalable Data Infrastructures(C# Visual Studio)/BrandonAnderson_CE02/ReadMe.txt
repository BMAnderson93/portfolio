Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE02" using the C# Console Application template.
Grading:
Please refer to the CE:02 tab of this rubric to understand the grading procedures for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE02.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Be sure to use this rubric (It is the same link as the doc for your first assignment. All class rubrics are there.)
-------------------------------------------------------------------------------------------------------------------------------
For this activity you will be creating the start of a terminal application that might be used by a bank teller.  You will create a CheckingAccount class that contains a balance and a Customer class that has a CheckingAccount.  The program will feature a menu that has the option of creating a customer, creating a checking account, setting the account balance, displaying the account balance, and exiting the program.  The program only has to keep track of a single Customer and the Customer only has to have a single account.
Use the following guidelines to complete this application:
Classes
Create a CheckingAccount class
CheckingAccount must have the following fields
a Decimal field for the account balance.
a int field for the account number.
Create a Customer class
Customer must have the following fields
a CheckingAccount field for the customer�s account
a string field for the customer�s name
Constructors
CheckingAccount needs a constructor that takes an int parameter and a decimal parameter
These parameters should be assigned to the appropriate fields.
Customer needs a constructor that takes a string parameter
This parameter should be assigned to the appropriate field.
Main
In main before anything else you will need a Customer variable to use for the currentCustomer.
Program runs until the user chooses to exit.
Menu
The menu must have the following options:
Create customer - this option needs to prompt the user for a customer name, use that input to instantiate a new Customer object, and  assign that object to the currentCustomer variable.
Create account - this option should only run if a isn�t null, it needs to prompt the user for values to create a CheckingAccount with, use those values to instantiate a new CheckingAccount object and assign it to the currentCustomer.
Set account balance - this option should only run if currentCustomer isn�t null and currentCustomer�s account isn�t null, it needs to prompt the user for a new account balance and then apply it to currentCustomer�s account.
Display account balance - this option needs to display the current customer�s account balance and the name associated with the account
Exit - stop the program.

Input Validation
User�s input must be validated.
The user must not be able to crash your program by entering invalid values.


Extra Information
Go back through your code and check for the following:
All variables and methods are named appropriately.
Any information being output to the user should be clear and concise.
The user should be clearly informed of what is occurring throughout the application. 
Make sure nothing accesses an object that doesn�t exist.

Additional Challenge
(For anyone that wants to take their programming skills to the next level)
Change the currentCustomer variable to an array of customers and modify the menu options to allow the user to select which customer is being created etc.
You will have to either create a hardcoded array of customers or prompt the user for the number of customers the array should hold and enforce that limit.
