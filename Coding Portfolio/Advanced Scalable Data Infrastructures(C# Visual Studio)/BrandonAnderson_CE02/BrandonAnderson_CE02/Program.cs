﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE02
{
    class Program
    {
        static void Main(string[] args)
        {

            Customer currentCustomer = null;

            DisplayMenu();
            string userInput = InputChecker(Console.ReadLine());

            while (userInput == "1" || userInput == "2" || userInput == "3" || userInput == "4" || userInput == "5")
            {
                if (userInput == "1")
                {
                    currentCustomer = new Customer();
                    currentCustomer.CreateName();
                    DisplayMenu();
                    userInput = InputChecker(Console.ReadLine());
                }
                else if (userInput == "2" && currentCustomer != null)
                {
                    currentCustomer.CreateAccountInfo();
                    DisplayMenu();
                    userInput = InputChecker(Console.ReadLine());
                }
                else if (userInput == "2" && currentCustomer == null)
                {
                    Console.WriteLine("You must first create a customer!");
                    DisplayMenu();
                    userInput = InputChecker(Console.ReadLine());
                }
                else if (userInput == "3")
                {
                    if (currentCustomer != null && currentCustomer.GetCustomerAccount() != null)
                    {
                        currentCustomer.ChangeAccountBalance();
                        DisplayMenu();
                        userInput = InputChecker(Console.ReadLine());
                    }
                    else if (currentCustomer == null)
                    {
                        Console.WriteLine("You must create a customer AND account before accessing this feature!");
                        DisplayMenu();
                        userInput = InputChecker(Console.ReadLine());
                    }
                    else if (currentCustomer.GetCustomerAccount() == null)
                    {
                        Console.WriteLine("You must create a customer AND account before accessing this feature!");
                        DisplayMenu();
                        userInput = InputChecker(Console.ReadLine());
                    }
                }
                else if (userInput == "4")
                {
                    if (currentCustomer != null && currentCustomer.GetCustomerAccount() != null)
                    {
                        currentCustomer.PrintAccountBalance();
                        DisplayMenu();
                        userInput = InputChecker(Console.ReadLine());
                    }
                    else if (currentCustomer == null)
                    {
                        Console.WriteLine("You must create a customer AND account before accessing this feature!");
                        DisplayMenu();
                        userInput = InputChecker(Console.ReadLine());
                    }
                    else if (currentCustomer.GetCustomerAccount() == null)
                    {
                        Console.WriteLine("You must create a customer AND account before accessing this feature!");
                        DisplayMenu();
                        userInput = InputChecker(Console.ReadLine());
                    }
                }
                else if (userInput == "5")
                {
                    Environment.Exit(0);
                }




            }

        }
        static void DisplayMenu()
        {

            Console.WriteLine("1. Create Customer" + System.Environment.NewLine +
                "2. Create Account" + System.Environment.NewLine + "3. Change Account Balance" + System.Environment.NewLine + "4. View Current Account Balance"
                + System.Environment.NewLine + "5. Exit");

        }
        static string InputChecker(string userInput)
        {
            while (userInput != "1" && userInput != "2" && userInput != "3" && userInput != "4" && userInput != "5")
            {
                Console.WriteLine("Please enter a valid responce!");
                userInput = Console.ReadLine();
            }
            return userInput;
        }

    }
}
