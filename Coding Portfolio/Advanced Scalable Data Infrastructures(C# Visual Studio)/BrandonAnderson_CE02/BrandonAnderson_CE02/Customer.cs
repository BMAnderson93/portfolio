﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE02
{
    class Customer
    {
        CheckingAccount customerAccount = null;
        string customerName;

        public void CreateAccountInfo()
        {
            
            Console.WriteLine("Please create an account number for the new customer!");
            string newAccountNumber = AccountNumberValidation(Console.ReadLine());
            Console.WriteLine("Please enter the balance of this new customer!");
            string newAccountBalance = BalanceNumberValidation(Console.ReadLine());
            customerAccount = new CheckingAccount();
            customerAccount.CreateAccount(newAccountBalance, newAccountNumber);
            

        }
        public void CreateName()
        {
            Console.WriteLine("Please enter the first name of the customer!");
            string firstName = NameValidation(Console.ReadLine());
            Console.WriteLine("Please enter the last name of the customer!");
            string lastName = NameValidation(Console.ReadLine());
            customerName = firstName + " " + lastName;
        }
        public static string NameValidation(string name)
        {
            while (name.Any(char.IsDigit))
            {
                Console.WriteLine("Please enter a valid name!");
                name = Console.ReadLine();
                Console.Clear();
            }
            return name;
        }
        public void ChangeAccountBalance()
        {
            Console.WriteLine("Please enter the new account balance!");
            customerAccount.UpdateAccountBalance(Console.ReadLine());
        }
        public void PrintAccountBalance()
        {
            customerAccount.PrintAccountBalance();
            Console.WriteLine("Press enter when done viewing!");
            Console.ReadLine();
        }

        public string AccountNumberValidation(string userInput)
        {
            int i;
            while(!int.TryParse(userInput, out i))
            {
                Console.WriteLine("Please enter a valid account number!");
                userInput = Console.ReadLine();
                
            }
            return userInput;
        }

        public string BalanceNumberValidation(string userInput)
        {
            decimal d;
            while (!decimal.TryParse(userInput, out d))
            {
                Console.WriteLine("Please enter a valid balance ammount!");
                userInput = Console.ReadLine();
            }
            return userInput;
        }
        public CheckingAccount GetCustomerAccount()
        {
            return customerAccount;
        }


    }

   
}
