﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE02
{
    class CheckingAccount
    {
       private decimal accountBalance;
       private  int accountNumber;

        private static void One()
        {
            Console.WriteLine("Account Created");

        }

        public void CreateAccount(string createAccountBalance, string createAccountNumber)
        {
            Console.WriteLine("Account created!");
            decimal d;
            while (!decimal.TryParse(createAccountBalance, out d))
            {
                Console.WriteLine("Please enter a valid balance!");
                createAccountBalance = Console.ReadLine();
                Console.Clear();
            }
            accountBalance = decimal.Parse(createAccountBalance);

            int i;
            while (!int.TryParse(createAccountNumber, out i))
            {
                Console.WriteLine("Please enter a valid account number!");
                createAccountNumber = Console.ReadLine();
                Console.Clear();
            }
            accountNumber = int.Parse(createAccountNumber);
            
            
            
        }
        public void UpdateAccountBalance(string newBalance)
        {
            decimal d;
            while(!decimal.TryParse(newBalance, out d))
            {
                Console.WriteLine("Please enter a correct balance input!");
                newBalance = Console.ReadLine();
            }
            accountBalance = decimal.Parse(newBalance);
        }
        public void PrintAccountBalance()
        {
            Console.WriteLine("The account balance for the current user is $" +accountBalance);
        }

    }
    

    
}
