Setup: 
You will be creating a new Visual Studio project named "FirstnameLastname_CE10" using the C# Console Application template.
Grading:
Please refer to the CE:10 tab of this rubric to understand the grading procedures for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE10.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
For today's lab you will be creating a store inventory management system.  Your program will have to have the following elements.  It should allow any number of customers to shop at the store using names to identify each one.  The user should be able to select which one is the current customer.  The system must contain a list of at least 50 items (repeats are allowed) that can be purchased by the customers.  
For a customer to make a purchase they must transfer the items they wish to buy to their own shopping cart. (A Shopping cart list should be maintained for each customer).   The user should be able to switch between customers without losing the contents of each customer's cart.  The user can select complete purchase and will be presented with a total for that user�s purchases.  Customers should be able to remove items from their cart and return them to the stores inventory. .  A customer�s cart should be removed after her/his purchase is complete.  
A log class should be set up like CE04 except this version should write the messages to a file instead of the console window.  Every time the current customer is changed the name of the new customer should be logged.  Every time an item moves to or from a shopping cart the item name and new location should be logged.  When a purchase is completed the customer name and total should be logged.
NOTE: The code structure and guidelines are light because this exercise is designed to test your critical thinking skills and see how you apply the skills you�ve learned throughout the duration of this class.
Use the following guidelines to complete this application:
Classes
Classes should be used to represent
Inventory Items
Customers
List(s)
Lists should be used to represent
The stores inventory
Customers shopping carts
Dictionary
A Dictionary should be used to 
Track all of the customers - identified by their name
User Options
The user should have the following options
Select current shopper - list of all shoppers and option to create another shopper
View store inventory - list everything the store is selling
View cart - list of everything in the current Customers cart
Add item to cart - allow the user to select an item to add to the current Customer�s cart and remove it from the store�s inventory. (Can be combined with the View store option if you wish)
Remove item from cart - allow the user to select an item to add to the stores inventory and remove it from the current Customer�s cart. (can be combined with the View cart option if you wish)
Complete purchase - Total up the cost of all of the items in the user�s cart and display that value.  Remove the customer from the dictionary of customers
Exit - Exit the program
Logging
A logging class is used to log the required info to file �log.txt�
It is your choice on which method to log with. (log, logW, logD)
Required info
Current Customer�s name every time it changes
Item name and location (cart/store inventory) every time it changes
Customer name and purchase total when a purchase is completed
Input
All input should be validated and limited to the options presented to the user
The user should not be able to crash the program
Program should continue to run until the user chooses to exit



Extra Information
Go back through your code and check for the following:
All variables and methods are named appropriately.
Any information being output to the user should be clear and concise.
The user should be clearly informed of what is occurring throughout the application. When values change or objects are instantiated information about this occurrence should be displayed.
Make sure nothing accesses an object that doesn�t exist.

