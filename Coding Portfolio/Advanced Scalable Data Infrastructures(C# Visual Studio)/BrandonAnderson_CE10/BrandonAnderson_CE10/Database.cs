﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql;
using System.Data;
using MySql.Data.MySqlClient;
using System.IO;

namespace BrandonAnderson_CE10
{ 
    class Database
    {
        private MySqlConnection _connection = null;

        public Database()
        {
            _connection = new MySqlConnection();
        }
        public bool Connect()
        {

            BuildConnectionString();
            bool success = false;

            try
            {
                _connection.Open();
                Console.WriteLine("Connection successful!");
                success = true;
            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    default:
                    case 0:
                        {
                            msg = e.ToString();
                        }
                        break;
                    case 1042:
                        {
                            msg = "Can't resolve host address. \n" + _connection.ConnectionString;
                        }
                        break;

                    case 1045:
                        {
                            msg = "Invalid username / password";
                        }
                        break;
                }
                Console.WriteLine(msg);
            }
            return success;
        }

        private void BuildConnectionString()
        {
            StringBuilder conString = new StringBuilder();
            conString.Append("Server=");
            using (StreamReader sr = new StreamReader("C:/VFW/connect.txt"))
            {
                string ip = sr.ReadLine();
                conString.Append($"{ip};");
            }
            conString.Append("uid=admin;");
            conString.Append("pwd=password;");
            conString.Append("database=exampleDatabase;");
            conString.Append("port=8889;");

            _connection.ConnectionString = conString.ToString();

        }
        public DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapt = new MySqlDataAdapter(query, _connection);
            DataTable data = new DataTable();
            adapt.SelectCommand.CommandType = CommandType.Text;
            adapt.Fill(data);

            return data;
        }
    }
}
