﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BrandonAnderson_CE10
{
    class Logger 
    {
        string filePath = "../FileIOExtraFiles/";
        protected static int lineNumber = 1;
        public void UserChange(string name)
        {
            try
            {
                StreamWriter sr;
                {
                    if (!File.Exists( filePath+"logfile.txt"))
                    {
                        sr = new StreamWriter(filePath + "logfile.txt");
                        sr.WriteLine(lineNumber+"). "+ name + " is now the current user");
                        lineNumber++;
                        sr.Close();

                    }
                    else
                    {
                        sr = File.AppendText(filePath + "logfile.txt");
                        sr.WriteLine(lineNumber + "). " + name + " is now the current user");
                        lineNumber++;
                        sr.Close();
                    }
                }
            }
            catch
            {

            }
        }
        public void ItemChangeOfLocation (string name, string location)
        {
            try
            {
                StreamWriter sr;
                {
                    if (!File.Exists(filePath + "logfile.txt"))
                    {
                        sr = new StreamWriter(filePath + "logfile.txt");
                        sr.WriteLine(lineNumber + "). " + name + " has been moved to " + location );
                        lineNumber++;
                        sr.Close();
                    }
                    else
                    {
                        sr = File.AppendText(filePath + "logfile.txt");
                        sr.WriteLine(lineNumber + "). " + name + " has been moved to " + location);
                        lineNumber++;
                        sr.Close();
                    }
                }
            }
            catch
            {

            }

        }
        public void CheckOutLog(string name, decimal total)
        {
            try
            {
                StreamWriter sr;
                {
                    if (!File.Exists(filePath + "logfile.txt"))
                    {
                        sr = new StreamWriter(filePath + "logfile.txt");
                        sr.WriteLine(lineNumber + "). " + name + " has checked out with a total of " + total);
                        lineNumber++;
                        sr.Close();
                    }
                    else
                    {
                        sr = File.AppendText(filePath + "logfile.txt");
                        sr.WriteLine(lineNumber + "). " + name + " has checked out with a total of " + total);
                        lineNumber++;
                        sr.Close();
                    }
                }
            }
            catch
            {

            }
        }
    }
}