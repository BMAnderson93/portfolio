﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql;
using System.Data;
using MySql.Data.MySqlClient;
using System.IO;

namespace BrandonAnderson_CE10
{
    class Program
    {
        static void Main(string[] args)
        {
            bool running = true;
            Logger nl = new Logger();
            Database db = new Database();
            Store currentStore = new Store();
            Customer currentCustomer = null;
            if (db.Connect())
            {
                DataTable data = db.QueryDB("SELECT title, retailPrice  FROM item LIMIT 50");
                DataRowCollection rows = data.Rows;

                foreach (DataRow r in rows)
                {
                    string title = r["title"].ToString();

                    decimal price = Convert.ToDecimal(r["retailPrice"].ToString());
                    


                    Item currentItem = new Item(title, price);
                    currentStore.AddToStore(currentItem);
                   



                }
               

            }
            while (running)
            {
                Console.Clear();
                Console.WriteLine("Please select an option from the following menu.");
                Console.WriteLine("1.) Select the current shopper." +Environment.NewLine+ "2.) View store inventory." + Environment.NewLine +
                    "3.) View cart." + Environment.NewLine + "4.) Add item to cart." + Environment.NewLine + "5.) Remove item from cart." + Environment.NewLine +
                    "6.) Complete purchase." + Environment.NewLine + "7.) Exit." + Environment.NewLine );
                string input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "1":
                    case " select the current shopper":
                        {
                            if (currentStore.GetCustomerCount() == 0)
                            {
                                currentCustomer = new Customer();
                                Console.WriteLine("There is currently no customerss, press any key to create one!");
                                Console.ReadKey();
                                Console.WriteLine("Please enter the full name of the new customer!");
                                string customerName = NoNumbers(Console.ReadLine());
                                currentCustomer.SetName(customerName);
                                currentStore.AddCustomer(currentCustomer);
                                nl.UserChange(currentCustomer.GetName());
                            }
                            else
                            {
                                currentCustomer = new Customer();
                                currentStore.DisplayAllCustomers();
                                Console.WriteLine("Please enter the name of the customer you would like to use, if you would like to add a new customer enter their name now.");
                                input = NoNumbers(Console.ReadLine());
                                currentCustomer = currentStore.SelectCurrentCustomer(input);
                                nl.UserChange(currentCustomer.GetName());

                            }

                        }
                        break;
                    case "2":
                    case "view store inventory":
                        {
                            if (currentStore.GetStoreCount() == 0)
                            {
                                Console.WriteLine("The store is out of items! ");
                            }
                            else
                            {
                                currentStore.DisplayStore();
                                Console.WriteLine("Press any key to continue.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "3":
                    case "view cart":
                        {
                            if (currentCustomer == null || currentStore.GetCustomerCount() == 0 || currentCustomer.GetShoppingCartCount() == 0)
                            {
                                Console.WriteLine("You must have atleast 1 customer with atleast 1 item in their cart before" +
                                    " accessing this option.");
                                Console.ReadKey();
                            }
                            else
                            {
                                currentCustomer.DisplayCartContents();
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "4":
                    case "add item to cart":
                        {
                            if (currentCustomer == null || currentStore.StoreItemsLeft() == 0 || currentStore.GetCustomerCount() == 0 )
                            {
                                Console.WriteLine("There are no more items left in the store, or you need to create atleast 1 customer.");
                                Console.ReadKey();
                            }
                            else
                            {
                                currentStore.DisplayStore();
                                Console.WriteLine("Please select the item number you would like to have added to your kart.");
                                int itemNumber = CorrectNumberChosen(Console.ReadLine(), currentStore.GetStoreCount());
                                currentStore.AddToCustomer(itemNumber, currentCustomer.GetName());
                               
                            }
                        }
                        break;
                    case "5":
                    case "remove item from cart":
                        {
                            if ( currentCustomer == null || currentCustomer.GetShoppingCartCount() == 0 )
                            {
                                Console.WriteLine("There is currently no items in this customers cart.");
                                Console.ReadKey();
                            }
                            else
                            {
                                currentCustomer.DisplayCartContents();
                                Console.WriteLine("Please select the item number you would like to have removed from your kart.");
                                int itemNumber = CorrectNumberChosen(Console.ReadLine(), currentCustomer.GetShoppingCartCount());
                                currentStore.RemoveFromCustomer(itemNumber, currentCustomer.GetName());
                            }
                        }
                        break;

                    case "6":
                    case "complete purchase":
                        {
                            if (currentCustomer == null ||currentStore.GetCustomerCount() == 0 || currentCustomer.GetShoppingCartCount() == 0)
                            {
                                Console.WriteLine("You need atleast 1 customer with atleast 1 item in their cart before preforming this action.");
                                Console.ReadKey();
                            }
                            else
                            {
                                currentStore.CheckOut(currentCustomer.GetName());
                                currentCustomer = null;
                            }
                        }
                        break;

                    case "7":
                    case "exit":
                        {
                            running = false;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Please enter a correct input!");
                            Console.ReadKey();
                        }
                        break;
                }
            }
            
        }
        public static string NoNumbers(string input)
        {
            while (input.Any(Char.IsDigit))
            {
                Console.WriteLine("Please enter a valid input!");
                input = Console.ReadLine();
            }
            return input;
        }
        public static int CorrectNumberChosen(string input, int count)
        {
            int output;
            while(!int.TryParse(input, out output) || output > count || output == 0)
            {
                Console.WriteLine("Please enter a valid input!");
                input = Console.ReadLine();
            }
            return output;
        }
    }
}
