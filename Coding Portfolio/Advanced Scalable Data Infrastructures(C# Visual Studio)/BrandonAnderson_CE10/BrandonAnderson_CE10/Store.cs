﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE10
{
    class Store
    {
        List<Item> _storeInventory = new List<Item>();
        Dictionary<string, Customer> _customers = new Dictionary<string, Customer>();
       public Logger nl = new Logger();

        public void AddToStore(Item addMe)
        {
            _storeInventory.Add(addMe);
        }
        public void AddCustomer(Customer newCustomer)
        {
            _customers.Add(newCustomer.GetName(), newCustomer);
        }
        public int GetStoreCount()
        {
            return _storeInventory.Count();
        }
        public int GetCustomerCount()
        {
            return _customers.Count();
        }
        public void DisplayStore()
        {
            for (int i = 0; i < _storeInventory.Count; i++)
            {
                int counter = i + 1;
                Console.WriteLine(counter + "). " + _storeInventory[i].GetName() + " $" + _storeInventory[i].GetPrice());
            }
        }
        public void DisplayAllCustomers()
        {
            int counter = 1;
            foreach (string key in _customers.Keys)
            {
                
                Console.WriteLine(counter + "). "+key);
                counter++;
            }
           
        }
        public int StoreItemsLeft()
        {
            return _storeInventory.Count();
        }

        public Customer SelectCurrentCustomer(string input)
        {
            string[] myKeys = _customers.Keys.ToArray();
            if (myKeys.Contains(input))
            {
                return _customers[input];
            }
            else
            {
                Customer addCustomer = new Customer();
                addCustomer.SetName(input);
                _customers.Add(input ,addCustomer);
                return addCustomer;
            }
        }
        public void AddToCustomer (int input, string keyName)
        {
            input--;
            string nameOfCart = keyName + "'s shopping cart";
            nl.ItemChangeOfLocation(_storeInventory[input].GetName(), nameOfCart);
            _customers[keyName].AddToCart(_storeInventory[input]);
            _storeInventory.RemoveAt(input);
            

        }
        public void RemoveFromCustomer(int input, string keyName)
        {
            input--;
            nl.ItemChangeOfLocation(_customers[keyName].GetItemName(input), " the store");
            _storeInventory.Add( _customers[keyName].RemoveFromCart(input));
         

        }
        public void CheckOut(string keyName)
        {
            decimal totalPrice = _customers[keyName].GetTotal();
            nl.CheckOutLog(keyName, totalPrice);
            Console.WriteLine("The total price for this customers cart was $" + totalPrice + ".");
            _customers.Remove(keyName);
            Console.ReadKey();
            
        }
    }
}
