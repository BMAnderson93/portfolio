﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE10
{
    class Customer
    {
        string _customerName;
        List<Item> _shoppingCart = new List<Item>();
        decimal total = 0;
        
        public string GetItemName(int input)
        {
            return _shoppingCart[input].GetName();
        }
        public decimal GetTotal()
        {
            foreach(Item Item in _shoppingCart)
            {
                total += Item.GetPrice();
            }
            return total;
        }
        public void SetName (string customerName)
        {
            _customerName = customerName;

        }
        public string GetName()
        {
            return _customerName;
        }
        public int GetShoppingCartCount()
        {
            return _shoppingCart.Count();
        }
        public void DisplayCartContents()
        {
            int counter = 1;
            foreach(Item Item in _shoppingCart)
            {
                Console.WriteLine(counter + "). "+Item.GetName() + " $" + Item.GetPrice());
                counter++;
            }
        }
        public void AddToCart(Item newAdd)
        {
            _shoppingCart.Add(newAdd);
        }
        public Item RemoveFromCart(int input)
        {
             Item temp = _shoppingCart[input];
            _shoppingCart.RemoveAt(input);
            return temp;
        }
    }
}
