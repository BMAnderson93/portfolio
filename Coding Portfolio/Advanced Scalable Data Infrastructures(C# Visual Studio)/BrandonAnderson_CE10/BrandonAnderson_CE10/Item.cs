﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE10
{
    class Item
    {
        string _name;
        decimal _price;

        public Item(string name ,decimal price)
        {
            _name = name;
            _price = price;

        }

        public string GetName()
        {
            return _name;
        }
        public decimal GetPrice()
        {
            return _price;
        }

    }
}
