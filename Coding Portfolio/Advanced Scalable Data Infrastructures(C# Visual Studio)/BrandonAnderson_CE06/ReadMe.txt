Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE06" using the C# Console Application template.
Grading:
Please refer to the CE:06 tab of this rubric to understand the grading procedures for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE06.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
For today's lab you will be creating a collections manager class that handles multiple collections of cards using a dictionary to pair collection names (string) with lists of cards (List).  You�ll have to create a Card class that has fields for a name (string), description (string), and a value (decimal).  The main part of the program will consist of a menu that gives the user the access to the following functionality: Create collection, Create a card, Add a card to a collection, Remove a card from a collection, Display a collection, Display all collections, and Exit.  
Once a card is created it should not be destroyed.  Created cards should be added into a list of available cards in the manager.  Cards added to a collection are selected from the list of available cards.  When the card is added to the collection it is removed from the available list.  When a card is removed from a collection it is added to the list of available cards.  The only exception to this is if the user creates a new collection manager at which point all of the information in the previous manager is lost.
Use the following guidelines to complete this application:
Classes
Create a Card class 
Card must have the following fields
a string for the card�s name
a string for the cards description
a decimal for the cards value
Create a CollectionManager class
CollectionManager must have the following fields
a List<Card> for the list of available cards
a Dictionary<string, List<Card>> to hold multiple collections
each collection (List of cards) is accessed with a string


Dictionary and List Usage
List
Created cards are added to the available cards list in collection manager
When a card is added to a collection it is removed from the list of available cards
When a card is removed from a collection it is added back to the list of available cards
Dictionary 
Holds Lists of Cards
Strings are used as the key for each collection
Menu
The menu must have the following options:
Create collection manager- this option creates a new CollectionManager object and assigns it to the currentManager variable.  This is the one situation where created items can be lost.
Create empty collection - At least one empty collection list needs to be created before a card can be added to it.
Create a card - this option prompts the user for the input necessary to create a new Card and adds it to the available cards list in the currentManager.
Add a card to a collection - this option prompts the user for a collection to add the card to, presents the user with a list of the available cards, the user selects a card, the selected card is removed from the available cards list, and is added to the specified collection.  If the collection does not exist it should be created and the card added to it.
Remove a card from a collection - this option prompts the user for a collection to remove the card from, presents the user with a list of cards in that collection, the user selects a card, the selected card is removed from the collection and is added to the list of available cards.
Display a collection - this option prompts the user for a collection to display and then either displays all of the cards in the collection or displays a message if the collection does not exist.
Display all collections - this option loops through all of the collections in the dictionary and displays all of the cards in each collection.
Exit - stop the program.
Main
In main before anything else you will need a CollectionManager variable to use for the currentManager.
Program runs until the user chooses to exit.
Input Validation

User�s input must be validated
The user must not be able to crash your program by entering invalid input

Extra Information
Go back through your code and check for the following:
All variables and methods are named appropriately.
Any information being output to the user should be clear and concise.
The user should be clearly informed of what is occurring throughout the application. When values change or objects are instantiated information about this occurrence should be displayed.
Make sure nothing accesses an object that doesn�t exist.

Additional Challenge
(For anyone that wants to take their programming skills to the next level)
Add a menu option that allows the user to move a card directly from one collection to another without having to move it to the available cards list first.
