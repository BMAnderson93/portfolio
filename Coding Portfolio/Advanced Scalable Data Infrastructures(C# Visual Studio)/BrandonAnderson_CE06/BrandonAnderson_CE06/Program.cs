﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE06
{
    class Program
    {
        static void Main(string[] args)
        {
            CollectionManager currentManager = null;

            bool running = true;
            string input;
            while (running)
            {
                Console.Clear();
                Console.WriteLine("Please select an option from the following menu!" + Environment.NewLine + "(1). Create collection manager" + Environment.NewLine + "(2). Create a card" + Environment.NewLine + "(3). Add a card to a collection" + Environment.NewLine +
                    "(4.) Remove a card from a collection" + Environment.NewLine + "(5.) Display a collection" + Environment.NewLine + "(6.) Display all collections" + Environment.NewLine + "(7). Exit");
                input = Console.ReadLine().ToLower();
                Console.WriteLine();
                switch(input)
                {
                    case "1":
                    case "create collection manager":
                        {
                            currentManager = new CollectionManager();

                        }
                        break;
                    case "2":
                    case "create a card":
                        {
                            Console.WriteLine("Please enter the cards name.");
                            string cardName = Console.ReadLine();
                            Console.WriteLine("Please enter a description of the card.");
                            string cardDescription = Console.ReadLine();
                            Console.WriteLine("Please enter the value of the card.");
                            input = Console.ReadLine();
                            decimal cardValue;
                            while(!decimal.TryParse(input, out cardValue))
                            {
                                Console.WriteLine("Please enter a valid responce!");
                                input = Console.ReadLine();
                            }
                            Card currentCard = new Card();
                            currentCard.CreateCard(cardName, cardDescription, cardValue);
                            currentManager.AddAvailableCards(currentCard);


                        }
                        break;
                    case "3":
                    case "add a card to a collection":
                        {
                            Console.WriteLine("Please enter the name of the collection you would like to add a card to.");
                            string collectionName = Console.ReadLine();
                            currentManager.DisplayAvailableCard(collectionName);
                           
                        }
                        break;
                    case "4":
                    case "remove a card from a collection":
                        {
                            currentManager.RemoveCard();
                        }
                        break;
                    case "5":
                    case "display a collection":
                        {
                            Console.WriteLine("Please enter the name of the collection you would like to display!");
                            string collectionName = Console.ReadLine();
                            currentManager.DisplayCollection(collectionName);
                        }
                        break;
                    case "6":
                    case "display all collections":
                        {
                            currentManager.DisplayAllCollections();
                        }
                        break;
                    case "7":
                    case "exit":
                        {
                            Environment.Exit(0);
                        }
                        break;
                     default:
                        {
                            Console.WriteLine("Please enter a valid input!");
                        }
                        break;
                }
            }
        }
    }
}
