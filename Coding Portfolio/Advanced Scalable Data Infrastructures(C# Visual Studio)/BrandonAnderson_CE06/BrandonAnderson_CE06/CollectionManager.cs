﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE06
{
    class CollectionManager
    {
        List<Card> availableCards = new List<Card>();
        Dictionary<string, List<Card>> container = new Dictionary<string, List<Card>>();
        List<Card> cards = new List<Card>();


        public void AddAvailableCards( Card newCard)
        {
            availableCards.Add(newCard);
         
        }

        public void DisplayAvailableCard(string collectionName)
        {
            Console.WriteLine("Please choose a card number from the following options to add.");
            for(int i = 0; i < availableCards.Count(); i++)
            {
                int counter = i + 1;
                Console.WriteLine(counter.ToString() );
                availableCards[i].DisplayCard();
            }
            string input = Console.ReadLine();
            int validInput;
            while (!int.TryParse(input, out validInput))
            {
                Console.WriteLine("Please enter a valid input!");
                input = Console.ReadLine();
            }
            while (validInput < 1 || validInput > availableCards.Count())
            {
                Console.WriteLine("Please enter a valid input");
                input = Console.ReadLine();
                while (!int.TryParse(input, out validInput))
                {
                    Console.WriteLine("Please enter a valid input!");
                    input = Console.ReadLine();
                }
            }
            int userChoice = validInput - 1;

            string[] arrayOfAllKeys = container.Keys.ToArray();
            bool containsName = arrayOfAllKeys.Contains(collectionName);
            if (!containsName)
            {
                Console.WriteLine("bool was false");
                Console.ReadKey();
                container.Add(collectionName, cards);
                container[collectionName].Add(availableCards[userChoice]);
                availableCards.RemoveAt(userChoice);
            }
            else
            {
                Console.WriteLine("bool was true");
                Console.ReadKey();
                container[collectionName].Add(availableCards[userChoice]);
                availableCards.RemoveAt(userChoice);
            }
           
        }
        public void RemoveCard()
        {
            Console.WriteLine("Please select the name of the collection you would like to remove from.");
            string userChoice =  DisplayKeys();
            Console.WriteLine("Please enter the number of the card you would like to remove!");
            for(int i = 0; i< container[userChoice].Count(); i++)
            {
                string conter = (i + 1).ToString();
                container[userChoice][i].DisplayCard();
            }
            string input = Console.ReadLine();
            int validInput;
            while (!int.TryParse(input, out validInput))
            {
                Console.WriteLine("Please enter a valid input!");
                input = Console.ReadLine();
            }
            while (validInput < 1 || validInput > container[userChoice].Count())
            {
                Console.WriteLine("Please enter a valid input");
                input = Console.ReadLine();
                while (!int.TryParse(input, out validInput))
                {
                    Console.WriteLine("Please enter a valid input!");
                    input = Console.ReadLine();
                }
            }
            int cardToRemove = validInput - 1;

            availableCards.Add(container[userChoice][cardToRemove]);
            container[userChoice].RemoveAt(cardToRemove);


        }
        
        public string DisplayKeys()
        {
            string[] arrayOfAllKeys = container.Keys.ToArray();
            for(int i = 0; i < arrayOfAllKeys.Count(); i++)
            {
                string counter = (i+1).ToString();
                Console.WriteLine(counter + " "+ arrayOfAllKeys[i]);
            }
            string userChoice = Console.ReadLine();
            bool correctInput = arrayOfAllKeys.Any(s => arrayOfAllKeys.Contains(userChoice));
            while (!correctInput)
            {
                Console.WriteLine("Please enter a correct input!");
                userChoice = Console.ReadLine();
                correctInput = arrayOfAllKeys.Any(s => arrayOfAllKeys.Contains(userChoice));
            }
            return userChoice;
        }
        public void DisplayCollection(string collectionName)
        {
            string[] arrayOfAllKeys = container.Keys.ToArray();
            bool correctInput = arrayOfAllKeys.Any(s => arrayOfAllKeys.Contains(collectionName));
            if (correctInput)
            {
                for (int i = 0; i < container[collectionName].Count; i++)
                {
                    container[collectionName][i].DisplayCard();
                   
                }
                Console.ReadKey();
            }
            else Console.WriteLine("Sorry, that collection does not exist!");
            Console.ReadKey();
        }
        public void DisplayAllCollections()
        {

            string[] arrayOfAllKeys = container.Keys.ToArray();
            for(int i = 0; i < arrayOfAllKeys.Count(); i++)
            {
                container[arrayOfAllKeys[i]][i].DisplayCard();
            }
            Console.ReadKey();
        }
        
    }
}
