﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE06
{
    class Card
    {
        string _cardName;
        string _cardDescription;
        decimal _cardValue;

        public void CreateCard(string cardName, string cardDescription, decimal cardValue)
        {
            _cardName = cardName;
            _cardDescription = cardDescription;
            _cardValue = cardValue;
        }
        public void DisplayCard()
        {
            Console.WriteLine(_cardName, _cardDescription, _cardValue);
        }
    }
   
}
