﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon__CE01
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] fishColor = { "Red", "Blue", "Blue", "Green", "Purple", "Red", "Yellow", "Blue", "Red", "Purple", "Blue", "Red" };
            float[] fishLength = { 2.4f, 3.1f, 6.6f, 1.2f, 23.8f, 43.21f, 12.43f, 25.8f, 9.23f, 8.45f, 102.23f, 1000.54f };
            DisplayColors();
            string userInputColor = Console.ReadLine().ToUpper();
            string userInputColorValidated = FirstChecker(userInputColor);
            Console.WriteLine("Would you like to see the smallest, or largest fish of that color? (Enter 1 for smallest, or 2 for largest)");
            string userInputLength = Console.ReadLine();
            int userInputLengthValidated = SecondChecker(userInputLength);
            FishDisplay(fishLength, fishColor, userInputLengthValidated, userInputColorValidated);
        }
        static void DisplayColors()

        {
            var originalColor = Console.ForegroundColor;
            Console.WriteLine("Please select a fish color");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Red, ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("Blue, ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Green, ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.Write("Purple, ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Yellow.");
            Console.WriteLine("");
            Console.ForegroundColor = originalColor;

        }
        static string FirstChecker(string userInput)
        {
            while (userInput != "RED" ^ userInput != "BLUE" ^ userInput != "GREEN" ^ userInput != "PURPLE" ^ userInput != "YELLOW")
            {
                Console.Clear();
                Console.WriteLine("Please enter a valid choice!");
                DisplayColors();
                userInput = Console.ReadLine().ToUpper();
            }
            Console.Clear();
            return userInput;

        }
        static int SecondChecker(string input)
        {
            while (input != "1" && input != "2")
            {
                Console.Clear();
                Console.WriteLine("Please enter a valid choice! (1 for smallest, 2 for largest.)");
                input = Console.ReadLine();
            }
            return int.Parse(input);
            

        }
        static void FishDisplay(float[] fishLengths, string[]fishColors, int smallOrLong, string color )
        {
            float fishCount = 0;
            if (smallOrLong == 1)
            {
                for (int i = 0; i < fishColors.Length; i++)
                {
                    
                    if (fishColors[i].ToUpper() == color )
                    {
                        if(fishCount == 0 || fishCount > fishLengths[i])
                        fishCount = fishLengths[i];

                    }
                }
                Console.WriteLine("The smallest " +color.ToLower()+ " fish, is " +fishCount+ "." );
            }
            else
            {
                for (int i = 0; i < fishColors.Length; i++)
                {
                    
                    if (fishColors[i].ToUpper() == color)
                    {
                        if (fishCount == 0 || fishCount < fishLengths[i])
                            fishCount = fishLengths[i];

                    }
                }
                Console.WriteLine("The largest " + color.ToLower() + " fish, is " + fishCount + ".");
            }
        }
    }
}
