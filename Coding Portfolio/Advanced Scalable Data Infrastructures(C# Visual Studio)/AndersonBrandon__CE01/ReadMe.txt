Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE01" using the C# Console Application template.
Grading:
Please refer to the CE:01 tab of this rubric to understand the grading procedures for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE01.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
For today's lab you will be stepping through 2 arrays that represent the color and length of several fish.  Each array index can be thought of a representing a single fish. (ex: Index 0 represents a fish's color in the color array and its length in the length array.)  You will prompt the user with a menu of fish colors to select from, after setting up the color and length arrays.  You will then look for the biggest fish that matches the color selected by the user (ex: The user selects �Blue� so then you have to find the longest blue fish.)  You will have to work with both arrays to match color and find the length of the fish.

Array Setup
Create 2 arrays
Array of strings to hold colors
Array of floats to hold lengths
Each array needs a minimum of 12 entries and both must have the same length.
Fill the arrays.  The values can be hard coded - no user input required for the data in the arrays.
The color array has to contain at least 4 different colors.
The color array must contain repeat colors.
The length array should have all different lengths (no repeats.)
The length array values should not be ascending or descending.

User Menu
Display a menu of colors for the user to select from
The menu should allow color selection by number
example menu: (your menu does not have to match.)
Select a color of fish
1. Red
2. Blue
3. Yellow
4. Green
Selection: _
Input Validation
You must validate the user�s input to ensure that a number is entered
You must limit the user�s selection to the colors listed in the menu. (ex: No numbers beyond the last value in the list.)
The user must not be able to crash your program by entering invalid input.
Search
You must use the selected color and search through the array for the biggest fish that matches the selected color.
This means that you need to look at both color and length for each fish.
Final Output
After finding the biggest fish you should display the result to the user using string interpolation.  (ex: The displayed message should list that it is the biggest fish of {selectedColor} with a length of {fishLength}.)
Just displaying a number is not sufficient (ex: 5.)
Example:
Given the following data:
ColorArray {blue, blue, green, red, blue, yellow}
SizeArray {2.6, 4.7, 6.87, 9.2, 5, 1.7}
If searching for the largest blue fish the final message would be something like this:
�The biggest blue fish has a length of 5.�  since the fish at index 4 is blue and has length 5


Extra Information
Go back through your code and check for the following:
All variables and methods are named appropriately.
Any information being output to the user should be clear and concise.
The user should be clearly informed of what is occurring throughout the application. 
Be sure to test your input code with valid and invalid options. (User�s will try to crash your code.)

Additional Challenge
(For anyone that wants to take their programming skills to the next level)
Add input to allow the user to search for the biggest or the smallest fish.  Let the user decide which fish to search for.
