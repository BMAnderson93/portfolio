﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BrandonAnderson_CE08
{
    class Program
    {
        static void Main(string[] args)
        {
            
            List<string> keys = new List<string>();
            List<string[]> file1 = new List<string[]>();
            
            string filePath = "../FileIOExtraFiles/";
            bool running = true;
            while (running) {
                Console.Clear();
            Console.WriteLine("Which data file would you like to read out? (1) (2) (3)");
            string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        {
                            running = false;
                            try
                            {
                                using (StreamReader sr = new StreamReader(filePath + "DataFile1.txt"))
                                {
                                    

                                    while (sr.EndOfStream == false)
                                    {

                                        string txt = sr.ReadLine();
                                        string[] txtArray = txt.Split('|');
                                        file1.Add(txtArray);


                                    }
                                    



                                }

                            }



                            catch
                            {
                            }
                        }
                        break;
                    case "2":
                        {
                            running = false;
                            try
                            {
                                using (StreamReader sr = new StreamReader(filePath + "DataFile2.txt"))
                                {

                                    while (sr.EndOfStream == false)
                                    {

                                        string txt = sr.ReadLine();
                                        string[] txtArray = txt.Split('|');
                                        file1.Add(txtArray);


                                    }
                                   



                                }

                            }



                            catch
                            {
                            }
                        }
                        break;
                    case "3":
                        {
                            running = false;
                            try
                            {
                                using (StreamReader sr = new StreamReader(filePath + "DataFile3.txt"))
                                {

                                    while (sr.EndOfStream == false)
                                    {

                                        string txt = sr.ReadLine();
                                        string[] txtArray = txt.Split('|');
                                        file1.Add(txtArray);


                                    }
                                    




                                }

                            }



                            catch
                            {
                            }
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Please enter a valid choice.");
                            Console.ReadKey();
                        }
                        break;
                }

            }
            try
            {
                using (StreamReader sr = new StreamReader(filePath + "DataFieldsLayout.txt"))
                {
                    while (sr.EndOfStream == false)
                    {
                        string word = sr.ReadLine();
                        keys.Add(word);
                    }

                }


            }
            catch
            {

            }
            try
            {

               
                using (StreamWriter sr = new StreamWriter(filePath + "test.json"))
                {
                    
                   
                    file1.RemoveAt(0);
                    file1.RemoveAt(file1.Count - 1);
                    
                    sr.Write("[");

                    for (int i = 0; i < file1.Count; ++i)
                    {
                        for (int j = 0; j < file1[i].Count(); j++)
                        {


                            sr.Write("{");
                            sr.WriteLine($"\"{keys[j]}\" : \"{file1[i][j]}\"");


                         
                            if ( i == file1.Count -1 && j == keys.Count -1)
                            {
                                sr.Write("}");
                            }
                            else
                            { 
                                sr.Write("},");
                            }
                        }

                    }

                    sr.Write("]");
                   

                

            }

            }
            catch
            {

            }
            Console.WriteLine("File Created.");
            Console.ReadKey();
            
            
           

        }

    }
}
