Setup:
You will be working with a premade project provided to you that can be downloaded here.
Grading:
Please refer to the CE:05 tab of this rubric to understand the grading procedures for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE05.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
For today's lab you will be given a project that has been thoroughly wrecked with various syntax and logic errors and you are tasked with repairing this project. Utilize the following guidelines for fixing this project.
The first step is to get the project to compile by eliminating the syntax errors. The following are several key pieces of information to help you through this.
There are a total of 20 individual syntax errors.
The syntax errors are scattered throughout every code file.
Go through each error in the compiler�s error list and focus on one at a time, most of these errors are not overly complex so the error list will provide fairly concise information about them.
The project�s components are similar to items you�ve had to implement in previous exercises.  When fixing these errors make sure to restore the expected functionality, do NOT use Visual Studio�s quick actions to fix the syntax errors. 
Once the project is compiling successfully you can now look towards fixing the logic errors. The following guidelines cover what to expect from these errors.
The logic errors have nothing to do with the phone number or email fields - correct formatting is not enforced on these items that is known and not included in the logic errors you are expected to find and fix.
There are a total of 10 logic errors in the project.
There is one logic error in Program outside of the switch statement.
The other 9 logic errors are evenly split 1 to 1 among each of the 9 possible cases in the switch statement. So each case has one logic error.
Most of the logic errors do not cause crashes so you have to check for broken features that simply don�t do what they are supposed to.
First fix the logic error in Main that isn�t in the switch statement.
After that first error is fixed focus on one case at a time. The errors were made in such a way that they only impact one case each.
Start with the cases that do the least complex tasks such as exiting or only printing text.
The error could be in any code that is called from the case. Any object that gets created in the case or any method that is called by it.
First identify exactly what is broken about the case before attempting to dig through the code to find the issue.
Breakpoints will be extremely helpful.

