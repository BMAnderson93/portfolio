

Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE03" using the C# Console Application template.
Grading:
Please refer to the CE:03 tab of this rubric to understand the grading procedures for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE03.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
For this activity you will be creating the start of a terminal application that might be used by a school to keep track of their courses, who is teaching them, and the students in the class.  You will create a Person class that serves as the parent class for a Teacher class and a Student class.  You will create a Course class that contains a Teacher and an array of Students.  The program will feature a menu that has the option of creating a Course, creating a Teacher that will be assigned to the current course, creating students to fill the course�s array of students, display the course, and exit the program.  The program only has to keep track of a single Course.

Classes
Create a Person class
Person must have the following fields
a string for the person�s name.
a string for the person�s description.
an int for the person�s age.
Create a Student class that inherits from Person
Student must have the following fields
an int for the Student�s grade
Create a Teacher class that inherits from Person
Teacher must have the following field
a string array for the Teacher�s knowledge
Create a Course class
Course must have the following fields
a string for the course title
a string for the course description
a Teacher for the teacher assigned to the course
a Student array for the students in the course
Constructors
Person needs a constructor that takes parameters to fill out all of its fields with
Teacher needs a constructor that takes in a parameter to fill its knowledge array
This constructor can take other parameters as well.
Student needs a constructor that takes in a parameter to fill its grade field
This constructor can take other parameters as well.
Course needs a constructor that takes in parameters to fill out the title, description, and number of students.
The number of students should be used to create the array to be held in the Student array field.
Menu
The menu must have the following options:
Create course - this option needs to prompt the user for the information needed to create a new Course object and assign that object to the currentCourse variable.
Create teacher - this option needs to prompt the user for the information needed to create a new Teacher object and assign that object to be the currentCourse�s teacher.
Add students - this option needs to prompt the user for the information needed to create enough students to fill the currentCourse�s Student array, create those students, and assign them to the currentCourse�s Student array. 
Display - this option needs to display all of the information about the course, the teacher, and each of the students.
Exit - stop the program.
Main
In main before anything else you will need a Course variable to use for the currentCourse.
Program runs until the user chooses to exit.
Input Validation

The menu must handle the user typing the text of a selection or the number
Input should be handled in a case insensitive format
Ex: �1. Create course�
The user should be able to choose the option �1�, �create course�, or even �CrEaTe CoUrSe�.
User�s input must be validated
The user must not be able to crash your program by entering invalid input

	

Extra Information
Go back through your code and check for the following:
All variables and methods are named appropriately.
Any information being output to the user should be clear and concise.
The user should be clearly informed of what is occurring throughout the application. When values change or objects are instantiated information about this occurrence should be displayed.
Make sure nothing accesses an object that doesn�t exist.

Additional Challenge
(For anyone that wants to take their programming skills to the next level)
Add a menu option that allows the user to select a student and change her/his grade
