﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE03
{
    class Program
    {
        static void Main(string[] args)
        {

            Course currentCourse = null;

            string userInput = DisplayMenu();
            Teacher currentTeacher = null;



            while (userInput == "1" || userInput.ToLower() == "create course" || userInput.ToLower() == "create teacher"
              || userInput.ToLower() == "create student" || userInput.ToLower() == "display" || userInput.ToLower() == "exit"
              || userInput == "2" || userInput == "3" || userInput == "4" || userInput == "5")
            {
                if (userInput == "1" || userInput.ToLower() == "create course")
                {
                    currentCourse = new Course();
                    Console.WriteLine("Please enter the name of the new course!");
                    string cName = Console.ReadLine();
                    Console.WriteLine("Please enter a brief description of the class!");
                    string cDescription = Console.ReadLine();
                    Console.WriteLine("Please enter the total number of students in this class!");
                    int cSize = CourseSizeValidation(Console.ReadLine());
                    currentCourse.NameNewCourse(cName);
                    currentCourse.DescribeNewCourse(cDescription);
                    currentCourse.AssignClassSize(cSize);
                    userInput = DisplayMenu();

                }
                else if (userInput == "2" || userInput.ToLower() == "create teacher")
                {
                    currentTeacher = new Teacher();

                    Console.WriteLine("Please enter the name of the teacher!");
                    string cTeacherName = NoNumbers(Console.ReadLine());
                    Console.WriteLine("Please enter the age of the teacher!");
                    int cTeacherAge = OnlyNumbers(Console.ReadLine());
                    Console.WriteLine("Please give a short discription of this teacher!");
                    string cTeacherDescription = Console.ReadLine();
                    Console.WriteLine("Please enter all the course subjects the teacher has knowledge of seperated by a comma!");
                    string[] cTeacherKnowledge = StringToArray(Console.ReadLine());
                    currentTeacher.AssignName(cTeacherName);
                    currentTeacher.AssignDescription(cTeacherDescription);
                    currentTeacher.AssignAge(cTeacherAge);
                    currentTeacher.AssignKnowledge(cTeacherKnowledge);
                    userInput = DisplayMenu();

                }
                else if (userInput == "3" || userInput.ToLower() == "create student")
                {
                    if (currentCourse == null)
                    {
                        Console.WriteLine("You must first create a class!");
                        userInput = DisplayMenu();
                    }
                    else
                    {
                        currentCourse.AddInStudents();
                        userInput = DisplayMenu();
                    }
                }
                else if (userInput == "4" || userInput.ToLower() == "display")
                {
                    if (currentCourse == null || currentTeacher == null )
                    {
                        Console.WriteLine("You must first run and populate all other areas before getting the info of the class!");
                        userInput = DisplayMenu();
                    }
                  
                    else
                    {

                        currentCourse.DisplayInfo(currentCourse, currentTeacher);
                        userInput = DisplayMenu();
                    }
                }
                else if (userInput == "5" || userInput.ToLower() == "exit")
                {
                    Environment.Exit(0);
                }
            }

        }

        static string DisplayMenu()
        {
            string userInput;
            Console.WriteLine("1. Create Course" + System.Environment.NewLine + "2. Create Teacher"
                + System.Environment.NewLine + "3. Add Students" + System.Environment.NewLine +
                "4. Display" + System.Environment.NewLine + "5. Exit");
            userInput = Console.ReadLine();
            while (userInput != "1" && userInput.ToLower() != "create course" && userInput.ToLower() != "create teacher"
              && userInput.ToLower() != "create student" && userInput.ToLower() != "display" && userInput.ToLower() != "exit"
              && userInput != "2" && userInput != "3" && userInput != "4" && userInput != "5")
            {
                Console.WriteLine("Please enter a valid selection!");
                userInput = Console.ReadLine();
            }
            return userInput;
        }
        static int CourseSizeValidation(string userInput)
        {
            int i;
            while (!int.TryParse(userInput, out i) || int.Parse(userInput) < 0)
            {
                Console.WriteLine("Please enter a valid class size!");
                userInput = Console.ReadLine();
            }
            return int.Parse(userInput);
        }
        static string[] StringToArray(string userInput)
        {
            string[] teacherKnowledgeAwrray = userInput.Split(',').ToArray();
            return teacherKnowledgeAwrray;
        }
        public static string NoNumbers(string userInput)
        {
            bool onlyLetters = userInput.All(Char.IsLetter) && userInput != " ";
            while (onlyLetters == false)
            {
                Console.WriteLine("Please enter a correct input!");
                userInput = Console.ReadLine();
                onlyLetters = userInput.All(Char.IsLetter) && userInput != " ";

            }
            return userInput;
        }
        public static int OnlyNumbers(string userInput)
        {
            int i;
            while (!int.TryParse(userInput, out i) || int.Parse(userInput) < 0)
            {
                Console.WriteLine("Please enter a correct input!");
                userInput = Console.ReadLine();
            }
            return int.Parse(userInput);
        }
        public static bool IsArrayEmpty(Student[] userInput)
        {
            if (userInput.Length == 0)
            {
                return true;
            }
            //if (userInput[0] == null)
            //{
            //    return true;
            //}
            else
            {
                return false;
            }
        }
    }
}
