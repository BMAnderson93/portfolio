﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE03
{
    class Person
    {
        string pName;
        string pDescription;
        int pAge;
        public string AssignName (string userInput)
        {
            pName = userInput;
            return pName;
        }
        public string AssignDescription(string userInput)
        {
            pDescription = userInput;
            return pDescription;
        }
        public int AssignAge(int userInput)
        {
            pAge = userInput;
            return pAge;
        }
        public string PrintName()
        {
            return pName;
        }
        public string PrintDescription()
        {
            return pDescription;
        }
        public int PrintAge()
        {
            return pAge;
        }
    }

}
