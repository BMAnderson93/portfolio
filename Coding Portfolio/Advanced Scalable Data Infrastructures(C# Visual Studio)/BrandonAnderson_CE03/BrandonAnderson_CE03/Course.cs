﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE03
{
    class Course
    {
        string cTitle;
        string cDescription;
        Teacher cTeacher;
        Student[] cStudents = null;
        public string NameNewCourse(string userInput)
        {
            return cTitle = userInput;

        }
        public string DescribeNewCourse(string userInput)
        {
            return cDescription = userInput;

        }
        public Teacher AssignNewTeacher(Teacher userInput)
        {
            return cTeacher = userInput;
        }
        public Student[] AssignClassSize(int userInput)
        {
            return cStudents = new Student[userInput];
        }
        public Student[] ReturnClassSize()
        {
            return cStudents;
        }
        public Student[] AddInStudents()
        {
            int i = 0;
            while (i < cStudents.Length )
            {
                cStudents[i] = new Student();
                Console.WriteLine("Please enter the name of the student!");
                string cStudentName = NoNumbers(Console.ReadLine());
                Console.WriteLine("Please enter the age of the student!");
                int cStudentAge = OnlyNumbers(Console.ReadLine());
                Console.WriteLine("Please give a short discription of this student!");
                string cStudentDescription = Console.ReadLine();
                Console.WriteLine("Please enter the current grade of this student in %!");
                int cStudentGrade = OnlyNumbers(Console.ReadLine());
                cStudents[i].AssignName(cStudentName);
                cStudents[i].AssignDescription(cStudentDescription);
                cStudents[i].AssignAge(cStudentAge);
                cStudents[i].AssignGrade(cStudentGrade);
                i++;
            }
            return cStudents;
        }

        public static string NoNumbers(string userInput)
        {
            bool onlyLetters = userInput.All(Char.IsLetter);
            while (onlyLetters == false)
            {
                Console.WriteLine("Please enter a correct input!");
                userInput = Console.ReadLine();
                onlyLetters = userInput.All(Char.IsLetter);

            }
            return userInput;
        }
        public static int OnlyNumbers(string userInput)
        {
            int i;
            while (!int.TryParse(userInput, out i))
            {
                Console.WriteLine("Please enter a correct input!");
                userInput = Console.ReadLine();
            }
            return int.Parse(userInput);
        }
        public  void DisplayInfo(Course currentCourse, Teacher currentTeacher)
        {
            Console.WriteLine("This course's name is " +currentCourse.cTitle + ".");
            Console.WriteLine("Course info - (" + currentCourse.cDescription + ") .");
            Console.WriteLine("This course is taught by " + currentTeacher.PrintName() + " age " +currentTeacher.PrintAge() + ".");
            Console.WriteLine("Teacher info - (" + currentTeacher.PrintDescription() + ") and has experiance in the following areas - ("
                + string.Join(",", currentTeacher.GetKnowledge())+ ") .");
            if (cStudents.Length > 0 && cStudents[0] !=null)
            {
                int i = 0;
                while (i < cStudents.Length)
                {
                    Console.WriteLine("Name -" + cStudents[i].PrintName() + Environment.NewLine
                        + "Age -" + cStudents[i].PrintAge() + Environment.NewLine + "Student Info -"
                        + Environment.NewLine + cStudents[i].PrintDescription() + Environment.NewLine + "Grade - "
                        + cStudents[i].ReturnGrade());
                    i++;
                }
            }
            else if (cStudents.Length > 0 && cStudents[0] == null)
            {
                Console.WriteLine("You have room for " +cStudents.Length + " more students, none have been assigned yet.");
            }
        }
        public string PrintArray (string[] userInput)
        {
            int i = 0;
           
            while (i < userInput.Length)
            {
                 Console.WriteLine(userInput[i]);
                i++;
            }
            return "";
        
        } 
        public Student[] GetStudent()
        {
            return cStudents;
        }
        
    }
}
