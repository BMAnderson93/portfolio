﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql;
using System.Data;
namespace lecturetest
{
    class Program
    {
        static void Main(string[] args)
        {
            Database db = new Database();
            DVD _currentStore = new DVD();
            bool running = true;

            
           if(db.Connect())
            {
                DataTable data = db.QueryDB("SELECT DVD_Title, Price, publicRating FROM dvd LIMIT 20");
                DataRowCollection rows = data.Rows;

                foreach(DataRow r in rows)
                {
                    string title = r["DVD_Title"].ToString();

                    decimal price = Convert.ToDecimal(r["Price"].ToString());

                    float rating = Convert.ToSingle((r["publicRating"].ToString()));
                   
                   
                    DVD _currentDvd = new DVD();
                    _currentDvd.AssignValues(title, price, rating);
                    _currentStore.AddToStore(_currentDvd);


                    
                }
               
            }

            while (running)
            {
                Console.Clear();
                Console.WriteLine("Please select an option from the following menu");
                Console.WriteLine("1). View inventory" + Environment.NewLine+ "2). View shopping cart " + Environment.NewLine+
                    "3). Add DVD to cart " + Environment.NewLine + "4). Remove DVD from cart" + Environment.NewLine + "5). Exit");
                string input = Console.ReadLine().ToLower();
                switch (input)
                {
                    case "1":
                    case "view inventory":
                        {
                            _currentStore.ViewInventory();
                            Console.WriteLine("Press any key to continue.");
                            Console.ReadKey();
                        }
                        break;
                    case "2":
                    case "view shopping cart":
                        {
                            _currentStore.ViewCart();
                            Console.WriteLine("Press any key to continue.");
                            Console.ReadKey();

                        }
                        break;
                    case "3":
                    case "add dvd to cart":
                        {
                            if (_currentStore.getCount() == 0)
                            {
                                Console.WriteLine("The store is out of movies! Please return some to continue!");
                                Console.ReadKey();
                            }
                            else
                            {
                                _currentStore.ViewInventory();
                                Console.WriteLine("Please enter the number of the DVD you would like to add to your cart!");
                                input = Console.ReadLine();
                                _currentStore.AddToCart(CorrectInputSize2(input));
                            }
                        }
                        break;

                    case "4":
                    case "remove dvd from cart":
                        {
                            if (_currentStore.getShoppingCount() == 0)
                            {
                                Console.WriteLine("Your cart is empty, you must have atleast one item in your cart!");
                                Console.ReadKey();
                            }
                            else
                            {
                                _currentStore.ViewCart();
                                Console.WriteLine("Please enter the number of the DVD you would like to remove from your cart!");
                                input = Console.ReadLine();
                                _currentStore.RemoveFromCart(CorrectInputSize1(input));
                            }
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            Environment.Exit(0);

                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Please enter a valid input!");
                            Console.ReadKey();
                        }
                        break;

                }
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

             int CorrectInputSize1(string input)
            {
                int t;
                while (!int.TryParse(input, out t) || int.Parse(input) == 0 ||  int.Parse(input) > _currentStore.getShoppingCount())
                {
                    Console.WriteLine("Please enter a valid input!");
                    input = Console.ReadLine();
                    
            }
                return t;
                

            }
            int CorrectInputSize2(string input)
            {
                int t;
                while (!int.TryParse(input, out t) || int.Parse(input) == 0 || int.Parse(input) > _currentStore.getCount())
                {
                    Console.WriteLine("Please enter a valid input!");
                    input = Console.ReadLine();

                }
                return t;


            }

        }
    }
}
