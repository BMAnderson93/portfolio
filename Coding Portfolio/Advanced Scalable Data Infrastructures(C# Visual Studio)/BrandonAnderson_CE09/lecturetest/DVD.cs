﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecturetest
{
    class DVD
    {
        string _title;
        decimal _price;
        float _rating;
        List<DVD> inventory = new List<DVD>();
        List<DVD> shoppingCart = new List<DVD>();

        

        public void AssignValues(string title, decimal price, float rating)
        {
            _title = title;
            _price = price;
            _rating = rating;
        }
        public void AddToStore(DVD newDvd)
        {
            inventory.Add(newDvd);
        }
        public int getCount()
        {
            return inventory.Count;
        }
        public int getShoppingCount()
        {
            return shoppingCart.Count;
        }

        public void ViewInventory()
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                string counter = (i + 1).ToString();
                Console.WriteLine(counter+"). Movie Title - " + inventory[i]._title);
                Console.WriteLine(" Movie Price - " + inventory[i]._price);
                Console.WriteLine(" Movie Rating - " + inventory[i]._rating);
            }
            
        }
        public void ViewCart()
        {
            if (shoppingCart.Count == 0)
            {
                Console.WriteLine("Your cart is currently empty!");
               
            }
            else
            {
                for (int i = 0; i < shoppingCart.Count; i++)
                {
                    string counter = (i + 1).ToString();
                    Console.WriteLine(counter + "). Movie Title - " + shoppingCart[i]._title);
                    Console.WriteLine(" Movie Price - " + shoppingCart[i]._price);
                    Console.WriteLine(" Movie Rating - " + shoppingCart[i]._rating);
                }
               
            }
        }
        public void AddToCart(int input)
        {
            shoppingCart.Add(inventory[input-1]);
            inventory.RemoveAt(input-1);

        }
        public void RemoveFromCart(int input)
        {
            inventory.Add(shoppingCart[input - 1]);
            shoppingCart.RemoveAt(input - 1);

        }
    }
}
