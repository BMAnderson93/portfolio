﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_C04
{
    class Car
    {
        string _make;
        string _model;
        string _color;
        float _milesOnCar;
        int _year;

        public void CreateCar(string make, string model, string color, float milesOnCar, int year)
        {
            _make = make;
            _model = model;
            _color = color;
            _milesOnCar = milesOnCar;
            _year = year;
        }
        public void Drive(float input)
        {
            _milesOnCar += input;
            Program.GetLogger().LogW(_milesOnCar.ToString());
        }
    }
}
