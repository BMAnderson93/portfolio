﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_C04
{
    class Program
    {
        private static Logger log = null;
        static void Main(string[] args)
        {
            Car currentCar = null;
            DisplayMenu();
            string userInput = MenuValidation(Console.ReadLine().ToLower());
            for (int i = 0; i < 1;)
            {
                if (userInput == "1" || userInput == "disable logging")
                {
                    log = new DoNotLog();
                    Console.WriteLine("Logging has been disabled");
                    DisplayMenu();
                    userInput = MenuValidation(Console.ReadLine());
                }
                else if (userInput == "2" || userInput == "enable logging")
                {
                    log = new LogToConsole();
                    Console.WriteLine("Logging has been enabled");
                    DisplayMenu();
                    userInput = MenuValidation(Console.ReadLine());
                }
                else if (userInput == "3" || userInput == "create a car")
                {
                    if (log == null)
                    {
                        Console.WriteLine("You must first declare if you want to log information or not!");
                        DisplayMenu();
                        userInput = MenuValidation(Console.ReadLine());
                    }
                    else if (log != null)
                    {
                        currentCar = new Car();
                        SetCarInfo(currentCar);
                        Console.WriteLine("New car created!");
                        DisplayMenu();
                        userInput = MenuValidation(Console.ReadLine());
                    }
                }
                else if (userInput == "4" || userInput == "drive the car")
                {
                    if (currentCar == null)
                    {
                        Console.WriteLine("You must first create a car before you can drive!");
                        DisplayMenu();
                        userInput = MenuValidation(Console.ReadLine());
                    }
                    else if (currentCar != null)
                    {
                        Console.WriteLine("Please enter the miles you want to drive!");
                        float milesDriven = IsFloat(Console.ReadLine());
                        currentCar.Drive(milesDriven);
                        DisplayMenu();
                        userInput = MenuValidation(Console.ReadLine());
                    }
                }
                else if (userInput == "5" || userInput == "destroy the car")
                {
                    currentCar = null;
                    Console.WriteLine("Car was destroyed.");
                    DisplayMenu();
                    userInput = MenuValidation(Console.ReadLine());
                }
                else if (userInput == "6" || userInput == "exit")
                {
                    Environment.Exit(0);
                }
            }
            
            
        }
        public static Logger GetLogger()
        {
            return log;
        }
       
        public static void DisplayMenu()
        {
               Console.WriteLine("Please choose which option you'd like to do!" + Environment.NewLine +
                "1. Disable logging " + Environment.NewLine + "2. Enable logging " + Environment.NewLine
                + "3. Create a car " + Environment.NewLine + "4. Drive the car" + Environment.NewLine
                + "5. Destroy the car" + Environment.NewLine + "6. Exit");
            
        }
        public static string MenuValidation(string userInput)
        {
            
            while (userInput != "disable logging" && userInput != "enable logging" && userInput != "create a car"
                && userInput != "drive the car" && userInput != "destroy the car" && userInput != "exit"
                && userInput != "1" && userInput != "2" && userInput != "3" && userInput != "4" && userInput != "5"
                 && userInput != "6")
            {
                Console.WriteLine("Please choose a correct input!");
                DisplayMenu();
                userInput = Console.ReadLine();
            }
            return userInput;
        }

        public static void SetCarInfo(Car currentCar)
        {
            Console.WriteLine("Please enter the make of the car.");
            string make = NoNumbers(Console.ReadLine());
            Console.WriteLine("Please enter the model of the car");
            string model = NoNumbers(Console.ReadLine());
            Console.WriteLine("Please enter the color of the car");
            string color = NoNumbers(Console.ReadLine());
            Console.WriteLine("Please enter the current miles on the car");
            float miles = IsFloat(Console.ReadLine());
            Console.WriteLine("Please enter the year of the car.");
            int year = IsInt(Console.ReadLine());
            log.LogD(make);
            log.LogD(model);
            log.LogD(color);
            log.LogD(miles.ToString());
            log.LogD(year.ToString());

            currentCar.CreateCar(make,model,color,miles,year);

            

            
            
            
            
        }
        public static string NoNumbers (string input)
        {
            while (input.Any(char.IsDigit))
            {
                Console.WriteLine("Please enter the correct information!");
                input = Console.ReadLine();
            }
            return input;

        }
        public static float IsFloat(string input)
        {
            float f;
            while (!float.TryParse(input, out f))
            {
                Console.WriteLine("Please enter the correct information!");
                input = Console.ReadLine();

            }
            return f;

        }
        public static int IsInt(string input)
        {
            int i;
            while (!int.TryParse(input, out i))
            {
                Console.WriteLine("Please enter the correct information!");
                input = Console.ReadLine();

            }
            return i;

        }
    }
}
