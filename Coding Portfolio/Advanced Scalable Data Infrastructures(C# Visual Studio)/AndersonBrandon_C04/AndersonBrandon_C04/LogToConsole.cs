﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_C04
{
    class LogToConsole : Logger
    {
        public override void Log(string userInput)
        {
            Console.WriteLine(lineNumber + userInput);
            lineNumber++;

        }
        public override void LogD(string userInput)
        {
            Console.WriteLine(lineNumber + " DEBUG " +userInput);
            lineNumber++;

        }
        public override void LogW(string userInput)
        {
            Console.WriteLine(lineNumber + " WARNING " + userInput);
            lineNumber++;
        }

    }
}
