 
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE04" using the C# Console Application template.
Grading:
Please refer to the CE:04 tab of this rubric to understand the grading procedures for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE04.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
For this activity you will be creating a basic logging framework which is a helpful item to have when developing and debugging larger applications.  The framework will be set up using an interface and an abstract base class with two child logging classes: DoNotLog and LogToConsole.  The rest of the program will consist of a car class and a menu in main with functionality that will allow the user to enable logging, disable logging, create a car, drive the car, Destroy the car, and exit the program.

Classes
Create an interface called ILog
The ILog interface must contain the following methods
Log - returns nothing and takes a string parameter
LogD - returns nothing and takes a string parameter
LogW - returns nothing and takes a string parameter
Create an abstract class called Logger that implements ILog
logger needs a protected static int field called lineNumber
this field will track the number of lines logged which will be used when data is logged.
Logger does not have to provide a body for the ILog methods it should declare them as abstract so that its children have to implement them.
Create a class called DoNotLog that inherits from Logger
Create a class called LogToConsole that inherits from Logger
Create a class called Car
Car should have the following fields
make of type string
model of type string
color of type string
mileage of type float
year of type int

Log Implementation
DoNotLog
Log - should do nothing
LogD - should do nothing
LogW - should do nothing
LogToConsole
Log - should use Console.Writeline to output the lineNumber and the string that was passed in.  It should also increase the lineNumber.
LogD - should use Console.Writeline to output the lineNumber, the word DEBUG, and the string that was passed in.  It should also increase the lineNumber.
LogW - should use Console.Writeline to output the lineNumber, the word WARNING, and the string that was passed in.  It should also increase the lineNumber.
Program class
Before Main, create a private static field of type Logger
this field should be initialized to null
Needs a public static method GetLogger that returns the value held by the above field.

Car Implementation
Needs a constructor that takes in parameters for each of its fields and initializes them
The new values of the car�s fields should be logged using the LogD method of the static Logger object in the Program class.  Use GetLogger().
Needs a Drive method that returns nothing and takes a parameter of type float
The value passed in should be used to update the car�s mileage.
The new mileage should be logged using the LogW method of the static Logger object in the Program class.  Use GetLogger().

Input Validation
Must be validated - prevent bad input from crashing the program.
Must be case insensitive - typing Exit, exit, and EXiT should all work.
Program must not crash if the user selects an option out of the expected order
ex. selecting drive car before one is created.

Main
In main before anything else you will need a Car variable to use for the currentCar.
Program runs until the user chooses to exit.

Menu
The menu must have the following options:
Disable logging - create a new DoNotLog object and store it in Program�s static Logger field.
Enable logging - create a new LogToConsole object and store it in Program�s static Logger field.
Create a car - prompt the user for values to create a car object and store the object in currentCar.
Drive the car - prompt the user for how far they are driving the car and call the Drive method on currentCar.
Destroy the car - set currentCar to null.
Exit - stop the program
Menu must handle numeric selection �1, 2, 3, etc� as well as typed out options like �disable logging�.

Extra Information
Go back through your code and check for the following:
All variables and methods are named appropriately.
Any information being output to the user should be clear and concise.
The user should be clearly informed of what is occurring throughout the application. When values change or objects are instantiated information about this occurrence should be displayed.
Make sure nothing accesses an object that doesn�t exist.

Additional Challenge
(For anyone that wants to take their programming skills to the next level)
Add color to the logged messages so they stand out from regular program input/output.
