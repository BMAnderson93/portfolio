﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarCardGame
{
    class Card
    {
        int cardValue;
        string cardType;



        public void SetCardValues(int number, string type)
        {

            cardValue = number;
            cardType = type;
        }
        public string DisplayCard()
        {
            string output = cardValue + " of " + cardType;
            return output;
        }
        public int GetCardValue()
        {
            return cardValue;
        }
    }
}
