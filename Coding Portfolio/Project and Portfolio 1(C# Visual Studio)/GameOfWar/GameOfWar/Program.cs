﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarCardGame
{
    class Program
    {
        public static void Main(string[] args)
        {
            Start:
            ArrayList playerOneDeck = new ArrayList();
            ArrayList playerTwoDeck = new ArrayList();
            Console.WriteLine("Player 1, please enter your name!");
            string player1 = Console.ReadLine();
            Console.WriteLine("Player 2, please enter your name!");
            string player2 = Console.ReadLine();
            int deckChoice = CoinFlip(player1, player2);
            ArrayList newDeck = CreateDeck();
            ArrayList randomDeck = ShuffleDeck(newDeck);
            ArrayList randomDeck1 = ShuffleDeck(randomDeck);
            ArrayList randomDeck2 = ShuffleDeck(randomDeck1);
            ArrayList randomDeck3 = ShuffleDeck(randomDeck2);
            ArrayList randomDeck4 = ShuffleDeck(randomDeck3);
            ArrayList playerOneStock = new ArrayList();
            ArrayList playerTwoStock = new ArrayList();
            Console.WriteLine("Decks created!");
            Console.ReadKey();
            if (deckChoice == 1)
            {
                playerOneDeck = SortDeck(randomDeck4, 1);
                playerTwoDeck = SortDeck(randomDeck4, 2);
            }
            else if (deckChoice == 2)
            {
                 playerOneDeck = SortDeck(randomDeck4, 2);
                 playerTwoDeck = SortDeck(randomDeck4, 1);
            }
            else if (deckChoice == 3)
            {
                 playerOneDeck = SortDeck(randomDeck4, 2);
                 playerTwoDeck = SortDeck(randomDeck4, 1);
            }
            else if (deckChoice == 4)
            {
                 playerOneDeck = SortDeck(randomDeck4, 1);
                 playerTwoDeck = SortDeck(randomDeck4, 2);
            }


            while ((playerOneDeck.Count != 0 || playerOneStock.Count != 0) && (playerTwoDeck.Count != 0 || playerTwoStock.Count != 0))
            {
                Console.Clear();
                if (playerOneDeck.Count == 0)
                {
                    Console.WriteLine("Shuffeling " +player1+ " cards!");
                    Console.ReadKey();
                    playerOneDeck = new ArrayList();
                    playerOneDeck = ShuffleDeck(playerOneStock);
                    playerOneStock = new ArrayList();
                }
                if (playerTwoDeck.Count == 0)
                {
                    Console.WriteLine("Shuffeling " + player2 + " cards!");
                    Console.ReadKey();
                    playerTwoDeck = new ArrayList();
                    playerTwoDeck = ShuffleDeck(playerTwoStock);
                    playerTwoStock = new ArrayList();
                }
                int whatToDo = PlayWar(playerOneDeck[0] as Card, playerTwoDeck[0] as Card, player1, player2);
                if (whatToDo == 1)
                {
                    playerOneStock.Add(playerTwoDeck[0]);
                    playerOneStock.Add(playerOneDeck[0]);
                    playerOneDeck.RemoveAt(0);
                    playerTwoDeck.RemoveAt(0);
                }
                else if (whatToDo == 2)
                {
                    playerTwoStock.Add(playerOneDeck[0]);
                    playerTwoStock.Add(playerTwoDeck[0]);
                    playerOneDeck.RemoveAt(0);
                    playerTwoDeck.RemoveAt(0);
                }
                else
                {
                    if (playerOneDeck.Count < 6)
                    {
                        Console.WriteLine(player1 + " does not have enough cards to play war! Player 2 wins the draw.");
                        playerTwoStock.Add(playerOneDeck[0]);
                        playerTwoStock.Add(playerTwoDeck[0]);
                        playerOneDeck.RemoveAt(0);
                        playerTwoDeck.RemoveAt(0);
                        Console.ReadKey();

                    }
                    else if (playerTwoDeck.Count < 6)
                    {
                        Console.WriteLine(player2 + " does not have enough cards to play war! Player 1 wins the draw.");
                        playerTwoStock.Add(playerOneDeck[0]);
                        playerTwoStock.Add(playerTwoDeck[0]);
                        playerOneDeck.RemoveAt(0);
                        playerTwoDeck.RemoveAt(0);
                        Console.ReadKey();

                    }
                    else
                    {
                        int tieBreaker = TieBreaker(playerOneDeck[1] as Card, playerOneDeck[2] as Card, playerOneDeck[3] as Card,
                            playerTwoDeck[1] as Card, playerTwoDeck[2] as Card, playerTwoDeck[3] as Card);
                        if (tieBreaker == 1)
                        {
                            playerOneStock.Add(playerTwoDeck[0]);
                            playerOneStock.Add(playerTwoDeck[1]);
                            playerOneStock.Add(playerTwoDeck[2]);
                            playerOneStock.Add(playerTwoDeck[3]);
                            playerOneStock.Add(playerOneDeck[0]);
                            playerOneStock.Add(playerOneDeck[1]);
                            playerOneStock.Add(playerOneDeck[2]);
                            playerOneStock.Add(playerOneDeck[3]);
                            playerTwoDeck.RemoveAt(0);
                            playerTwoDeck.RemoveAt(1);
                            playerTwoDeck.RemoveAt(2);
                            playerTwoDeck.RemoveAt(3);
                            playerOneDeck.RemoveAt(0);
                            playerOneDeck.RemoveAt(1);
                            playerOneDeck.RemoveAt(2);
                            playerOneDeck.RemoveAt(3);
                        }
                        else if (tieBreaker == 2)
                        {
                            playerTwoStock.Add(playerOneDeck[0]);
                            playerTwoStock.Add(playerOneDeck[1]);
                            playerTwoStock.Add(playerOneDeck[2]);
                            playerTwoStock.Add(playerOneDeck[3]);
                            playerTwoStock.Add(playerTwoDeck[0]);
                            playerTwoStock.Add(playerTwoDeck[1]);
                            playerTwoStock.Add(playerTwoDeck[2]);
                            playerTwoStock.Add(playerTwoDeck[3]);
                            playerOneDeck.RemoveAt(0);
                            playerOneDeck.RemoveAt(1);
                            playerOneDeck.RemoveAt(2);
                            playerOneDeck.RemoveAt(3);
                            playerTwoDeck.RemoveAt(0);
                            playerTwoDeck.RemoveAt(1);
                            playerTwoDeck.RemoveAt(2);
                            playerTwoDeck.RemoveAt(3);


                        }
                        else
                        {
                            Console.WriteLine("You have drawn, no one wins this round!");
                            playerOneStock.Add(playerOneDeck[0]);
                            playerTwoStock.Add(playerTwoDeck[0]);
                            playerOneDeck.RemoveAt(0);
                            playerTwoDeck.RemoveAt(0);
                            Console.ReadKey();
                        }
                    }


                }
            }
            if (playerOneDeck.Count == 0 && playerOneStock.Count == 0)
            {
                Console.WriteLine(player2 + " wins!");
                Console.WriteLine("Would you like to play again? enter 1 to play again, enter 2 to exit.");
                string input = Console.ReadLine();
                if(input != "1")
                {
                    Environment.Exit(0);
                }
                else
                {
                    goto Start;
                }
            }
            else if (playerTwoDeck.Count == 0 && playerTwoStock.Count == 0)
            {
                Console.WriteLine(player1 + " wins!");
                Console.WriteLine("Would you like to play again? enter 1 to play again, enter 2 to exit.");
                string input = Console.ReadLine();
                if (input != "1")
                {
                    Environment.Exit(0);
                }
                else
                {
                    goto Start;
                }
            }




        }

        public static ArrayList CreateDeck()
        {
            ArrayList deckOfCards;
            deckOfCards = new ArrayList();


            for (int i = 0; i < 52; i++)
            {

                Card assignCard = new Card();
                int cardNumber = i % 13;
                string cardType;

                if (i < 13)
                {
                    cardType = "Diamonds";
                    assignCard.SetCardValues(cardNumber, cardType);
                    deckOfCards.Add(assignCard);
                }
                else if (i < 26)
                {
                    cardType = "Spades";
                    assignCard.SetCardValues(cardNumber, cardType);
                    deckOfCards.Add(assignCard);
                }
                else if (i < 39)
                {
                    cardType = "Clubs";
                    assignCard.SetCardValues(cardNumber, cardType);
                    deckOfCards.Add(assignCard);
                }
                else if (i <= 52)
                {
                    cardType = "Hearts";
                    assignCard.SetCardValues(cardNumber, cardType);
                    deckOfCards.Add(assignCard);
                }



            }
            return deckOfCards;


        }
        public static ArrayList ShuffleDeck(ArrayList deck)
        {
            ArrayList randomDeck = new ArrayList();
            int i = 0;
            while (deck.Count >= 1)
            {

                Random rnd = new Random();
                int randomNumber = rnd.Next(0, deck.Count);
                randomDeck.Add(deck[randomNumber]);
                deck.RemoveAt(randomNumber);
                i++;

            }
            return randomDeck;
        }
        public static ArrayList SortDeck(ArrayList deckOfCards, int player)
        {
            ArrayList shuffDeck = new ArrayList();
            if (player == 1)
            {
                for (int i = 0; i <= 25; i++)
                {
                    shuffDeck.Add(deckOfCards[i]);
                }

            }
            else
            {
                for (int i = 26; i <= 51; i++)
                {
                    shuffDeck.Add(deckOfCards[i]);
                }

            }
            return shuffDeck;

        }

        public static int PlayWar(Card playerOne, Card playerTwo, string playerOneName, string playerTwoName)
        {
            int playerOneCard = playerOne.GetCardValue();
            int playerTwoCard = playerTwo.GetCardValue();
            int i;
            Console.WriteLine(playerOneName +" has the " + playerOne.DisplayCard() + " " +playerTwoName+ " has the " + playerTwo.DisplayCard());
            if (playerOneCard > playerTwoCard)
            {
                Console.WriteLine( playerOneName +" wins this round!");
                Console.ReadKey();
                return i = 1;
            }
            else if (playerOneCard < playerTwoCard)
            {
                Console.WriteLine(playerTwoName +" wins this round!");
                Console.ReadKey();
                return i = 2;
            }
            else
                return i = 3;

        }
        public static int TieBreaker(Card p1c1, Card p1c2, Card p1c3, Card p2c1, Card p2c2, Card p2c3)
        {
            Card[] playerOneCards = new Card[3];
            Card[] playerTwoCards = new Card[3];
            playerOneCards[0] = p1c1;
            playerOneCards[1] = p1c2;
            playerOneCards[2] = p1c3;
            playerTwoCards[0] = p2c1;
            playerTwoCards[1] = p2c2;
            playerTwoCards[2] = p2c3;


            Console.WriteLine("Player one, please choose which card you want! (1)(2)(3)");
            int player1Input;
            string input = Console.ReadLine();
            while(!int.TryParse(input, out player1Input))
            {
                Console.WriteLine("Please enter a valid responce!");
                input = Console.ReadLine();
            }
            player1Input--;
            Console.WriteLine("Player two, please choose which card you want! (1)(2)(3)");
            int player2Input;
            input = Console.ReadLine();
            while (!int.TryParse(input, out player2Input))
            {
                Console.WriteLine("Please enter a valid responce!");
                input = Console.ReadLine();
            }
            player2Input--;

            Console.WriteLine("Player one has picked the " + playerOneCards[player1Input].DisplayCard() + " Player two has picked the " + playerTwoCards[player2Input].DisplayCard());
            if (playerOneCards[player1Input].GetCardValue() > playerTwoCards[player2Input].GetCardValue())
            {
                Console.WriteLine("Player one wins all the cards!");
                Console.ReadKey();
                return 1;
            }
            else if (playerOneCards[player1Input].GetCardValue() < playerTwoCards[player2Input].GetCardValue())
            {
                Console.WriteLine("Player two wins all the cards!");
                Console.ReadKey();
                return 2;
            }
            else
                return 3;
        }

        public static int CoinFlip(string player1, string player2)
        {
            
            Console.WriteLine(player1 +" please choose 1. heads or  2.tails");
            string input = Console.ReadLine();
            while (input != "1" && input != "2")
            {
                Console.WriteLine("Please enter a correct input!");
                input = Console.ReadLine();
            }
            int number = int.Parse(input);
            Random headsOrTails = new Random();
            int random =  headsOrTails.Next(1, 3);

            if (number == random)
            {
                Console.WriteLine(player1 + " wins the flip, please choose if you want the first deck or the second. (enter 1 or 2)");
                input = Console.ReadLine();
                while (input != "1" && input != "2")
                {
                    Console.WriteLine("Please enter a correct input!");
                    input = Console.ReadLine();
                }
                int inputDeck = int.Parse(input);
                if (inputDeck == 1)
                {
                    return 1;
                }
                else if (inputDeck == 2)
                {
                    return 2;
                }
            }
            else if (number != random)
            {
                Console.WriteLine(player2 + " wins the flip, please choose if you want the first deck or the second. (enter 1 or 2)");
                input = Console.ReadLine();
                while (input != "1" && input != "2")
                {
                    Console.WriteLine("Please enter a correct input!");
                    input = Console.ReadLine();

                }
                int inputDeck = int.Parse(input);
                if (inputDeck == 1)
                {
                    return 3;
                }
                else

                    return 4;

            }
            
            return 7;
           
        }
    }
}
