//
//  Store.swift
//  AndersonBrandonCE07
//
//  Created by Brandon Anderson on 5/15/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class Store {
    //Vars needed to build a store object
    let name: String
    var itemsNeed : [String]
    var itemsHave : [String]
    
    //Init for store object
    init(name: String, itemsHave: [String], itemsNeed : [String]) {
        self.name = name
        self.itemsHave = itemsHave
        self.itemsNeed = itemsNeed
    }
}
