//
//  StoreTVExt.swift
//  AndersonBrandonCE07
//
//  Created by Brandon Anderson on 5/15/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation
import UIKit

extension StoresTableViewController{
    func CreateJson(){
        
        if let path = Bundle.main.path(forResource: "storeShopping", ofType: ".json"){
            // Creates the URL using the path we just created
            let url = URL(fileURLWithPath: path)
            
            do {
                // This creates a data object from the url we created
                let data = try Data.init(contentsOf: url)
                // creates the jsonObj from the datafile we created
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)as? [Any]
                // runs the parse function which will handle creating our objects
                Parse(jsonObject: jsonObj)
                
            }
            catch{
                // Prints the error that occured
                print(error.localizedDescription)
            }
        }
    }
    func Parse(jsonObject: [Any]?){
        // Safely binds jsonObject to jsonObj
        if let jsonObj = jsonObject{
            //Loop through the first level itms in the json obj
            for firstLevelItm in jsonObj {
                
                // Validates that we can unwrap these and if so we will continue to creating a object
                guard let object = firstLevelItm as? [String: Any],
                    let store = object["store"] as? String,
                    let itemsNeeded = object["itemsNeeded"] as? [String]
                    
                    // gets rid of the current obj that failed to be unwrapped and moves to the next
                    else{return}
                
                // Create a new store with all the json data
                self.stores.append(Store.init(name: store, itemsHave: [], itemsNeed: itemsNeeded))
            }
        }
        
    }
    //Function for adding the new store to our list of stores
    func AddNewStore(){
        //Displays an alert for geting the new stores name from the user
        let alert = UIAlertController(title: "Add Store", message: "Please enter the name of the new store!", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if textField?.text != ""{
                //Updates the list with new store
            self.UpdateList(string: (textField?.text)!)
            }
        }))
        //Presents the alert
        self.present(alert, animated: true, completion: nil)
        
    }
    //Adds the new store and updates the TV
    func UpdateList(string: String){
        self.stores.append(Store.init(name: string, itemsHave: [], itemsNeed: []))
        self.tableView.reloadData()
    }
}
