//
//  StoreTableViewCell.swift
//  AndersonBrandonCE07
//
//  Created by Brandon Anderson on 5/15/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class StoreTableViewCell: UITableViewCell {

    //References for the storetable view custom cell
    @IBOutlet weak var storeLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
