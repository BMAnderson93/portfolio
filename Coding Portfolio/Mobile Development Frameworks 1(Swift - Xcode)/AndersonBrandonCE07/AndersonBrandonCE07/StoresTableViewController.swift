//
//  StoresTableViewController.swift
//  AndersonBrandonCE07
//
//  Created by Brandon Anderson on 5/15/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class StoresTableViewController: UITableViewController {

    //Array of stores to be used in our table view
    var stores: [Store] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        //Creates and parses our json
        CreateJson()

        //Allows us to add an identifier to the custom header to call upon it
        let headerNib = UINib.init(nibName: "CustomHeader", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "header_ID1")
        //This allows the user to select multiple stores during editing
        tableView.allowsMultipleSelectionDuringEditing = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //If we are editing we will change the left bar button to a trash, and run our trash all method
    @IBAction func editTapped(_ sender: UIBarButtonItem){
    tableView.setEditing(!tableView.isEditing, animated: true)
    
    if tableView.isEditing{
   navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(StoresTableViewController.trashAllSelected))
    }
    else {
        //Else we will change it back to nil
   navigationItem.leftBarButtonItem = nil
    }
    }
    
    @objc func trashAllSelected(){
      //Goes through all the selected Index paths and sorts them so we can properly get rid of them
        if var selectedIPs = tableView.indexPathsForSelectedRows{
            //Sort from largest to smallest
            selectedIPs.sort { (a, b) -> Bool in
                a.row > b.row
            }
            // Gets the users final okay to delete every selected store
            let alert = UIAlertController(title: "Warning", message: "Are you sure you want to delete this store? You can not undo this!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
                //run your function here
                for indexpath in selectedIPs{
                    //Delete the comic first
                    self.stores.remove(at: indexpath.row)
                    self.tableView.reloadData()
                }
            }))
            //Presents the alert
            self.present(alert, animated: true, completion: nil)
            
          
            
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return stores.count
    }
    

    //Configures the cell to match each stores name and items needed from it
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! StoreTableViewCell

        // Configure the cell...
        cell.countLbl.text = stores[indexPath.row].itemsNeed.count.description
        cell.storeLbl.text = stores[indexPath.row].name

        return cell
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    //Sets up our custom header and applies it to the needed section
    //Called once for every visible section header when the table loads
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //Create view
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header_ID1") as? CustomHeaderView
        //Configure that view
        header?.count.text = stores.count.description
        
        //Finally return the view
        return header
    }
    //Adds the new store
    @IBAction func buttonPressed(){
    AddNewStore()
    }
    

   //Alows us to swipe and delete at the row
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
   
    
    //Used for setting up our swipe to delete
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //Final warning to the user to stop the deletion
        let delete = UIContextualAction(style: .destructive, title: "Remove") { (action, sourceView, completionHandler) in
            
            let alert = UIAlertController(title: "Warning", message: "Are you sure you want to delete this store? You can not undo this!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
                //Removes the swiped index path
                
                self.stores.remove(at: indexPath.row)
                tableView.reloadData()
                
            }))
            //Presents the alert
            self.present(alert, animated: true, completion: nil)
        }
        
       //Returns the swipe config
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
    
    
    


    
    // MARK: - Navigation
    //Wont allow a segue to happen if we're editing
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if !tableView.isEditing{
            return true
        }
        else {
            return false}
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //If we're not editing, then we will send over the selected store
        if let indexPath = tableView.indexPathForSelectedRow{
        if let sVC = segue.destination as? StoreDetailsTableViewController{
            sVC.store = stores[indexPath.row]
            }}
        }
    //Reloads the data when this view is going to appear
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

}
