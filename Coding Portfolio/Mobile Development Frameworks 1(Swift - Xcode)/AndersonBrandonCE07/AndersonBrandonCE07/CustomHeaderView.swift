//
//  CustomHeaderView.swift
//  AndersonBrandonCE07
//
//  Created by Brandon Anderson on 5/15/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class CustomHeaderView: UITableViewHeaderFooterView {
//References needed for the custom header
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var subTitleCount: UILabel!
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var button: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
