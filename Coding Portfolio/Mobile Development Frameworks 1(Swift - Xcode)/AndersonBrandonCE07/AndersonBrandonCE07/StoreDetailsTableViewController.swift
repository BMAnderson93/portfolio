//
//  StoreDetailsTableViewController.swift
//  AndersonBrandonCE07
//
//  Created by Brandon Anderson on 5/15/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class StoreDetailsTableViewController: UITableViewController {
    
    //Catches the sent store
    var store: Store!
    var header: UITableViewHeaderFooterView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //Sets our title to the store name
        title = store.name
        //Creates our rightbar item and assigns it to the addnewitem func
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(StoreDetailsTableViewController.addNewItem))
        
        //Allows us to give the customheader a reference
        let headerNib = UINib.init(nibName: "CustomHeader", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "header_ID1")
        //Allows multiple selection during editing
        tableView.allowsMultipleSelectionDuringEditing = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
       
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 2
    }

    //Seperates how many rows each section has based on what we need in that section
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return store.itemsNeed.count
        }
        else {
            return store.itemsHave.count
            
        }
    }
    //Adds the new item, just like how we added a new store
    @objc func addNewItem(){
        let alert = UIAlertController(title: "Add Item", message: "Please enter the name of the new item!", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if textField?.text != ""{
                self.store.itemsNeed.append((textField?.text)!)
                self.tableView.reloadData()
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    //Lets us edit specific rows
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    
    //Sets each cell based on what section it is for need and have items
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? TableViewCell

        // Configure the cell...
        if indexPath.section == 0 {
            cell?.itemLbl.text = store.itemsNeed[indexPath.row]
        }
        else {
            cell?.itemLbl.text = store.itemsHave[indexPath.row]
            
        }

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    //Sets up our custom header
    override func tableView(_: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        //Gets rid of the custom image we used and changes up the title
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header_ID1") as! CustomHeaderView
        header.button.setImage(nil, for: .normal)
        header.button.setTitle("Edit", for: .normal)
        //Configure that view
        //Custom message for each section, have and need
        if section == 0 {
            header.count.text = store.itemsNeed.count.description
            header.Title.text = "Items Needed"
            
            //Finally return the view
        }
        else {
            header.count.text = store.itemsHave.count.description
            header.Title.text = "Purchased Items"
        }
        return header
    }
    //Sets up our swipe to delete
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Remove") { (action, sourceView, completionHandler) in
            
            //Confirms the user wants to delete
            let alert = UIAlertController(title: "Warning", message: "Are you sure you want to delete this store? You can not undo this!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
                //Removes from the sections and rows
                if indexPath.section == 0{
                    self.store.itemsNeed.remove(at: indexPath.row)}
                else {
                    self.store.itemsHave.remove(at: indexPath.row)
                }
                tableView.reloadData()
                
            }))
            //Presents the alert
            self.present(alert, animated: true, completion: nil)
        }
        
        //Returns our swipe config
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
    //Begins the editing of the items
    @IBAction func editPressed(){
   //Sets the editing state to be the opposite of is editing
        tableView.setEditing(!tableView.isEditing, animated: true)
     //Changes the bar items to match what we need and ties the needed function to them
        if tableView.isEditing{
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(StoreDetailsTableViewController.trashAllSelected))
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(StoreDetailsTableViewController.stopEditing))
        }
        else {
            //Reverts the barbuttons back to defaults
            navigationItem.leftBarButtonItem = nil
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(StoreDetailsTableViewController.addNewItem))
            
        }
    }
    //Stops the editing and resets the barbuttons
    @objc func stopEditing(){
     tableView.setEditing(false, animated: true)
        navigationItem.leftBarButtonItem = nil
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(StoreDetailsTableViewController.addNewItem))
    }
    //Gets rid of the selected index paths
    @objc func trashAllSelected(){
        if var selectedIPs = tableView.indexPathsForSelectedRows{
            //Sort from largest to smallest
            selectedIPs.sort { (a, b) -> Bool in
                a.row > b.row
            }
            //Confirms the user wants to delete
            let alert = UIAlertController(title: "Warning", message: "Are you sure you want to delete this store? You can not undo this!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
                //Goes through each section and row deleting what was selected and updates
                for indexpath in selectedIPs{
                    //Delete the comic first
                    if indexpath.section == 0{
                        self.store.itemsNeed.remove(at: indexpath.row)
                    }
                    else {
                         self.store.itemsHave.remove(at: indexpath.row)
                    }
                    self.tableView.reloadData()
                }
            }))
            //Presents the form
            self.present(alert, animated: true, completion: nil)
            
            
            
        }
    }
   
    //Sends the items up and down based on where we selected it from
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !tableView.isEditing{
            if  indexPath.section == 0 {
                store.itemsHave.append(store.itemsNeed[(indexPath.row)])
                store.itemsNeed.remove(at: indexPath.row)
            }
            else {
                store.itemsNeed.append(store.itemsHave[(indexPath.row)])
                store.itemsHave.remove(at: indexPath.row)
            }
            tableView.reloadData()
        }
    }
    

}
