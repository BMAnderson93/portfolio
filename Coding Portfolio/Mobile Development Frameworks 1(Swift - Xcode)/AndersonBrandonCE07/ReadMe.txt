Create a JSON file with:

3 default shopping lists
Each containing 5 grocery items
 

Shopping List Table View with 1 section

Section will have a Custom header with an Add Button to allow the user to add new lists.
When add button is tapped a UIAlertController populates with a textField to allow the user to name the list.
Custom cells will display the name of the list as well as the number of items that is currently in the list.
Swiping a cell will allow the user to delete the list
Deleting a list will populate an alert, if the user confirms the list should be removed, if the user cancels the list should remain.
 

Tapping a shopping list will bring up a Table View with 2 sections:

First section will contain the selected shopping list items
User should be able to add a new item by tapping a UIBarButtonItem.
Add button populates an alert with a Text Field allowing the user to add a new item to the list.
Selecting an item in the Shopping List will move it to the Purchased section.
Swiping the cell will allow the user to delete the item.
Deleting an item will populate an alert, if the user confirms, the item should be removed, if the user cancels, the item should remain.
Custom header will display the section title as well as the current count of items in that section.
Counts should always update to display the current correct count.
Second section contains a list of items already purchased
Selecting an item will move it back the section of shopping list items.
Deleting an item in the purchased section will populate an alert, if the user confirms, the item should be removed, if the user cancels the item should remain.
Custom header will display the section title as well as the current count of items in that section.
Counts should always update to display the current correct count.
Final turn-in for this coding exercise should be named (lastname)(firstname)CE07.zip
Example �DoeJaneCE07.zip�
 

Keep in mind that in this exercise there is no data persistence so anything added will be gone once the app is closed.