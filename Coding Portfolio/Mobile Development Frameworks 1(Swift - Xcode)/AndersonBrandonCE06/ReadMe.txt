For this assignment, you will be pulling and parsing related JSON data from NewsAPI
It will be up to you to read their documentation and figure out how to get the information that you need. Remember, it�s your job to solve problems!
For this project since it deals primarily with aggregated news sources, we have no choice but to disable App Transport Security.
This should always be a last resort, but in this case for this particular project we will allow it.
This is not a long term solution as Apple will begin to ban apps that disable ATS from the app store.
The original ban date was set for January 2017 but has so far, not been enacted.
Overview Video
Final turn-in for this coding exercise should be named (lastname)(firstname)CE06.zip
Example �DoeJaneCE06.zip�