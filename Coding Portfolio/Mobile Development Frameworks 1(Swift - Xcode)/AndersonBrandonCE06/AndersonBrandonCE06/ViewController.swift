//
//  ViewController.swift
//  AndersonBrandonCE06
//
//  Created by Brandon Anderson on 5/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
var sources: [Sources] = []
    
    @IBOutlet weak var loadingIcon: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        DownloadJson()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Allows me to preform set up work prior to transitioning to the navigation controller
        let navVC = segue.destination as! UINavigationController
        
        let tableVC = navVC.viewControllers.first as! SourcesTableViewController
        // Sends all the sources we loaded to the first table view
        tableVC.sources = sources
        //Stops the icon froma animating
        loadingIcon.stopAnimating()
    }


}

