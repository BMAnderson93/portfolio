//
//  ArticleDetailsViewController.swift
//  AndersonBrandonCE06
//
//  Created by Brandon Anderson on 5/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ArticleDetailsViewController: UIViewController {

    // Vars and references used for the details controller
    @IBOutlet weak var articleImage: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    
    @IBOutlet weak var detailsTxt: UITextView!
    
    @IBOutlet weak var webLinkButton: UIButton!
    
    var currentArticle: NewsArticle!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Validates that we have a article and updates the form with its information
        if currentArticle != nil{
            articleImage.image = currentArticle.image
            titleLbl.text = currentArticle.title
            detailsTxt.text = currentArticle.details
            
        }
        // Makes sure that we do infact have a URL before allowing the user to click the button
        if currentArticle.url == nil || currentArticle.url ==  ""{
            webLinkButton.isHidden = true
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        if let sVC = segue.destination as? WebViewController{
            sVC.url = currentArticle.url
        }
        
        // Pass the selected object to the new view controller.
    }
    //Checks to see wether were entering this form from the parent or not, and if were coming from the web view skip loading this form by poping it
    override func viewWillAppear(_ animated: Bool) {
        if self.isMovingToParentViewController == false{
            
            navigationController?.popViewController(animated: true)
            
        }
        
    }
    

}
