//
//  ArticlesTableViewController.swift
//  AndersonBrandonCE06
//
//  Created by Brandon Anderson on 5/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ArticlesTableViewController: UITableViewController {
    
    //Variables and referece we'll use for the story board
    @IBOutlet weak var loadingIcon: UIActivityIndicatorView!
    var id: String!
    var articles: [NewsArticle] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        //Downloads the json data based on the selected ID
        DownloadJson(id: id)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    //Once again we only need 1 section
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    //For rows we need the same amount of articles we have
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
            return articles.count
       
    }

    //Sets and configs the cell, then returns it
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? ArticleTableViewCell
            else{return tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)}

        // Configure the cell...
        
        cell.articleIcon.image = articles[indexPath.row].image
        cell.titleLbl.text = articles[indexPath.row].title
        
        return cell
        
    }
    


    
    // MARK: - Navigation
    // Sends the selected article to the details controller to display the information from it
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let indexPath = tableView.indexPathForSelectedRow{
        if let sVC = segue.destination as? ArticleDetailsViewController {
            sVC.currentArticle = articles[indexPath.row]
            
        }
        }
    }
    

}
