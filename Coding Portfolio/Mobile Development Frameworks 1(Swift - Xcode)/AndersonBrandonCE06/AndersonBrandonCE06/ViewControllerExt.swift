//
//  ViewControllerExt.swift
//  AndersonBrandonCE06
//
//  Created by Brandon Anderson on 5/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

extension ViewController {
    
    func DownloadJson(){
        
        // Creates the session config and URL used in this task
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "https://newsapi.org/v1/sources?language=en")
        
        let task = session.dataTask(with: url!, completionHandler: { (opt_data, opt_response, opt_error) in
            
            //Bail Out on error
            if opt_error != nil { return }
            
            //Check the response, statusCode, and data
            guard let response = opt_response as? HTTPURLResponse,
                response.statusCode == 200,
                let data = opt_data
                else { return }
            
            do {
                //De-Serialize data object
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : AnyObject]{
                   // Drills to get the values of the news source we need
                    for j in json{
                        if let data = j.value as? [[String : Any]]{
                            for d in data{
                                //Adds the new source to the array of sources
                                guard let id = d["id"] as? String,
                                let name = d["name"] as? String
                                
                               
                                    else{return}
                                // Creates the source and adds it to the array
                                let temp = Sources.init(id: id, name: name)
                                self.sources.append(temp)
                            }
                        }
                    }
                    //Once we've got all the information we'll segue to the navigation controller
                    self.performSegue(withIdentifier: "toSourcesTV", sender: nil)
                    
                }            }
                // catchs and prints any errors
            catch {
                print(error.localizedDescription)
            }
        })
        // Starts the task
        task.resume()
        
    }
}
