//
//  WebViewController.swift
//  AndersonBrandonCE06
//
//  Created by Brandon Anderson on 5/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit
//We need this to use to use the web controller
import WebKit

class WebViewController: UIViewController {

    //Var and references used in the webcontroller
    @IBOutlet weak var webview: WKWebView!
    
    var url : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        if url != nil {
            // makes sure we have a valid url and then turns it into a URL request
            let validURL = URL(string: url)
            let request = URLRequest(url:validURL!)
            //Loads the webview with the request we made
            webview.load(request)
        }
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    
   
}
