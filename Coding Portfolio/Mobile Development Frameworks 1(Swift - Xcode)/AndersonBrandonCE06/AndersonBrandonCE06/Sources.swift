//
//  Sources.swift
//  AndersonBrandonCE06
//
//  Created by Brandon Anderson on 5/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class Sources {
    //Vars needed to build a source object
    var id: String
    var name : String
    
    
    //Inits the source obj
    init(id : String, name : String){
        self.id = id
        self.name = name
       
   
    }
}
