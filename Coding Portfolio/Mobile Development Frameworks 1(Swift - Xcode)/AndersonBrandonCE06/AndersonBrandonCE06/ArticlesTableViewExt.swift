//
//  ArticlesTableViewExt.swift
//  AndersonBrandonCE06
//
//  Created by Brandon Anderson on 5/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation
//Extension of the articles table view controller
extension ArticlesTableViewController {
    func DownloadJson (id : String){
        //Sets the config of the session for the task
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        //Validates we have a good URL
        if let validURL = URL(string:"https://newsapi.org/v1/articles?source=\(id)&apiKey=300af61e7d604794a55b1ef01f611e65"){
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail Out on error
                if opt_error != nil { return }
                
                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }
                
                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : AnyObject]{
                        //Loops through the json object and gets the information we will need to create a news article object
                        for j in json {
                            if let data = j.value as? [[String : Any?]]{
                                for d in data {
                                    
                                    let title = d["title"] as? String?
                                    let description = d["description"] as? String?
                                    let url = d["url"] as? String?
                                    let imageUrl = d["urlToImage"] as? String?
                                    
                                    //No guard here to make sure we dont lose any information, instead we pass everything into the init and itll decide what information we are missing
                                    let temp = NewsArticle.init(image: imageUrl!, url: url! , title: title!, details: description!)
                                   //Adds the new article to our list of articles
                                    self.articles.append(temp)
                                    
                                    
                                }
                            }
                        }
                       
                       
                    }
                    //Updates the main thread when done parsing
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.loadingIcon.stopAnimating()
                    }
                }
                    // catchs and prints any errors
                catch {
                    print(error.localizedDescription)
                }
            })
            // Starts the task
            task.resume()
        }
        
    }
}
