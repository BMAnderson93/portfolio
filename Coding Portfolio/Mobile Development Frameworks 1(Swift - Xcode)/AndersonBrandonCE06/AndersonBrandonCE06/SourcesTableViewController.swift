//
//  SourcesTableViewController.swift
//  AndersonBrandonCE06
//
//  Created by Brandon Anderson on 5/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class SourcesTableViewController: UITableViewController {

    var sources: [Sources]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    // We only need 1 section, so thats what we will use
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    // Setes the number of rows equal to how many sources we have
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sources.count
    }

    //Sets and configures the custom cell then returns it
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? SourceTableViewCell
         else{return tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)}

        // Configure the cell...
        cell.sourceLbl.text = sources[indexPath.row].name
        

        return cell
    }
    

   
    // MARK: - Navigation

    //Prepares for the articles control to be opened
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        if let indexPath = tableView.indexPathForSelectedRow {
            //Sends over the selected news source
        if let destination = segue.destination as? ArticlesTableViewController{
            destination.id = sources[indexPath.row].id
            destination.title = sources[indexPath.row].name
        }
        }
        
        
    }
    
    

}
