//
//  NewsArticle.swift
//  AndersonBrandonCE06
//
//  Created by Brandon Anderson on 5/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation
import UIKit

class NewsArticle{
    //Variables needed to build a news article obj
    var image: UIImage!
    let url: String!
    let title: String!
    let details: String!
    
    //Inits the custom object
    init(image: String?, url : String?, title : String?, details: String?) {
        self.url = url
        // Checks the inputed variables to check if the article is missing anything
        // If it is missing anything we will use default placeholder data
        if title != nil{
            self.title = title}
        else{
            self.title = "Unavailable"
        }
        if details != nil  && details != ""{
            self.details = details}
        else {
            self.details = "Details are unavailable for this article, click to view the entire article for more informaton!"
        }
        if image != nil {
            //Checks wether or not we can use the image URL provided, if not we will just give it a default image
        if let imageurl = URL(string: image!){
            do {
                //Creates a UIImage from the URL created
                let data = try Data(contentsOf: imageurl)
                self.image = UIImage(data: data)!
            }
            catch{
                self.image = #imageLiteral(resourceName: "news")
                print(error.localizedDescription)
            }
        }
        }
        else {
            self.image = #imageLiteral(resourceName: "news")
        }
    }
    
}
