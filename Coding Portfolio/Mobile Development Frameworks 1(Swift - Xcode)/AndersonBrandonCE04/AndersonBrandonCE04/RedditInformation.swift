//
//  RedditInformation.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/7/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation
import UIKit

class RedditInformation{
    //Variables used to build the object
    let postName : String
    let autherName : String
    var image : UIImage!
   
    //Initializer
    init(postName : String, autherName: String, url: String) {
        self.postName = postName
        self.autherName =  autherName
        //Creates a url from the string passed in
        if let url = URL(string: url){
            do {
                //Creates a UIImage from the URL created
                let data = try Data(contentsOf: url)
                self.image = UIImage(data: data)!
            }
            catch{
                print(error.localizedDescription)
            }
        }
        
    }
}
