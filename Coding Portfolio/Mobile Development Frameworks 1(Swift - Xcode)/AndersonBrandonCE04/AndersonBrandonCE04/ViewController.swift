//
//  ViewController.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/7/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //Creates a reference to the tableview
    @IBOutlet weak var tableView: UITableView!
    var redditInfoHolder = [RedditInformation] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        downloadThenPop(jsonAtUrl: "https://www.reddit.com/r/disney/.json")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - UITableViewDataSource Protocal Callbacks
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return redditInfoHolder.count
    }
    
    //Row Display. Implementers should always try to reuse cells by setting each cells reuseIdentifier and querying for available resuable cells with dequeueResuableCellWIthIdentifier
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Resuse an existing cell or create a new one if needed
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_ID_1", for: indexPath)
        
        //configure the cell
        cell.textLabel?.text = redditInfoHolder[indexPath.row].postName
        cell.detailTextLabel?.text = redditInfoHolder[indexPath.row].autherName
        cell.imageView?.image = redditInfoHolder[indexPath.row].image
        
        //Return the config cell
        return cell
    }

}

