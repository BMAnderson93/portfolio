For this assignment, you will be pulling and parsing related JSON data from Reddit.
Only Posts that contain a thumbnail will be used, discard the rest.
Display Parsed data into a TableView of cells.
Example url for JSON data https://www.reddit.com/r/iphone/.json (this is just for reference, you may choose your own subreddit)
Final turn-in for this coding exercise should be named (lastname)(firstname)CE04.zip
Example �DoeJaneCE04.zip�