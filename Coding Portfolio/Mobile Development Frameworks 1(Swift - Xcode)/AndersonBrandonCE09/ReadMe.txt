Reddit Revisit

For this assignment, you will be modifying and adding to your previous reddit project from the TableView Introduction assignment.
We suggest that you create a copy of the project to work from so as not to alter the original.
If your project was not in correct working order, you will first need to fix the assignment based on the grading feedback received and the rubric for that assignment.
 

To your working reddit project you are to add the following:
Toolbar with a �Theme� button and a �Subreddits� button.
Corresponding Views/ViewControllers for the theme and subreddits views.
Settings and options screens (in general) should not be part of a navigation stack due to their nature.
Be sure and segue appropriately.
ToolBar
A Toolbar is active and available on all screens with appropriate controls
Main Page: Theme and Subreddits
Each settings/options page should contain �save� and a �reset� button
Theme Options View:
The purpose of the theme view is to allow your user to alter the visual theme of the main reddit view.
Users should be able to easily pick any color for:
The TableView cells of the main view
The TableView itself
The cell�s text
Users will then be able to save their choices.
The act of Saving:
Alters the main reddit view appropriately
Saves those choices to the device�s persistent storage
Exits the Theme Option View
Upon closing and opening the application, the user�s last theme choices are retrieved from storage and used.
Subreddits Options View
This Editable TableView will allow the user to add and remove subreddits from the list.
Each subreddit listed here will be pulled and displayed in it�s own section in the main view
Final turn-in for this coding exercise should be named (lastname)(firstname)CE09.zip
Example �DoeJaneCE09.zip�