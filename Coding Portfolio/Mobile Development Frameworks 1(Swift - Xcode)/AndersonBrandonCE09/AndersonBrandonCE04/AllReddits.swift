//
//  AllReddits.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/19/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

// Source for holding all my reddit posts for passing them back and forth
class AllReddits : NSObject, NSCoding {
 
    
   
    
    var allReddits: [Int : [RedditInformation]]
    
    init(allReddits: [Int : [RedditInformation]]){
        self.allReddits = allReddits
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(allReddits, forKey: "allReddits")
    }
    required init?(coder aDecoder: NSCoder) {
        self.allReddits = aDecoder.decodeObject(forKey:"allReddits") as! [Int : [RedditInformation]]
    }
    
}
