//
//  CellColors.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/19/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class CellColors: NSObject, NSCoding {
    
    //Used to hold all my color variables for the cell
    var labelSliderR: Float = 0
    var labelSliderG: Float = 0
    var labelSliderB: Float = 0
    var detailLabelSlideR: Float = 0
    var detailLabelSlideG: Float = 0
    var detailLabelSlideB: Float = 0
    var backgroundSliderR: Float = 1
    var backgroundSliderG: Float = 1
    var backgroundSliderB: Float = 1
    
    init(labelSliderR:Float,labelSliderG:Float,labelSliderB:Float,detailLabelSlideR: Float,detailLabelSlideG: Float,detailLabelSlideB: Float, backgroundSliderR : Float, backgroundSliderG : Float, backgroundSliderB : Float) {
        
        self.labelSliderR = labelSliderR
        self.labelSliderB = labelSliderB
        self.labelSliderG = labelSliderG
        self.detailLabelSlideR = detailLabelSlideR
        self.detailLabelSlideG = detailLabelSlideG
        self.detailLabelSlideB = detailLabelSlideB
        self.backgroundSliderR = backgroundSliderR
        self.backgroundSliderG = backgroundSliderG
        self.backgroundSliderB = backgroundSliderB
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(labelSliderR, forKey: "labelSliderR")
        aCoder.encode(labelSliderG, forKey: "labelSliderG")
        aCoder.encode(labelSliderB, forKey: "labelSliderB")
        aCoder.encode(detailLabelSlideR, forKey: "detailLabelSlideR")
        aCoder.encode(detailLabelSlideG, forKey: "detailLabelSlideG")
        aCoder.encode(detailLabelSlideB, forKey: "detailLabelSlideB")
        aCoder.encode(backgroundSliderR, forKey: "backgroundSliderR")
        aCoder.encode(backgroundSliderG, forKey: "backgroundSliderG")
        aCoder.encode(backgroundSliderB, forKey: "backgroundSliderB")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.labelSliderR = aDecoder.decodeFloat(forKey: "labelSliderR" )
        self.labelSliderG = aDecoder.decodeFloat(forKey: "labelSliderG" )
        self.labelSliderB = aDecoder.decodeFloat(forKey: "labelSliderB" )
        self.detailLabelSlideR = aDecoder.decodeFloat(forKey: "detailLabelSlideR")
        self.detailLabelSlideG = aDecoder.decodeFloat(forKey: "detailLabelSlideG")
        self.detailLabelSlideB = aDecoder.decodeFloat(forKey: "detailLabelSlideB")
        self.backgroundSliderR = aDecoder.decodeFloat(forKey: "backgroundSliderR")
         self.backgroundSliderG = aDecoder.decodeFloat(forKey: "backgroundSliderG")
         self.backgroundSliderB = aDecoder.decodeFloat(forKey: "backgroundSliderB")
        
    
    }
}
