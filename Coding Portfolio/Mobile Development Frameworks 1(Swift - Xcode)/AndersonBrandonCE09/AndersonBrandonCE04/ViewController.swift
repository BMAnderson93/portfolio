//
//  ViewController.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/7/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //Creates a reference to the tableview
    @IBOutlet weak var tableView: UITableView!
    var allReddits : AllReddits = AllReddits.init(allReddits: [:])
    var redditInfoHolder = [RedditInformation] ()
    var subRedit = "disney"
    var backUp : [RedditInformation]!
    var cellColors = CellColors.init(labelSliderR: 0, labelSliderG: 0, labelSliderB: 0, detailLabelSlideR: 0, detailLabelSlideG: 0, detailLabelSlideB: 0, backgroundSliderR: 1, backgroundSliderG: 1, backgroundSliderB: 1)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let savedReddits = UserDefaults.standard.savedReddits(forKey: "savedReddits"), let savedDefault = UserDefaults.standard.defaultReddit(forKey: "savedDefault"){
            allReddits = savedReddits
            //I use this backup for reloading the original reddit
            backUp = savedDefault
        }
        else {
            //Downloads the original subreddit then populates the table view
        downloadThenPop(jsonAtUrl: getURL(subReddit: subRedit))
        }
        //Trys to get the saved colors if they exist and update the table view based on it
        if let savedColors = UserDefaults.standard.savedColors(forKey: "savedColors"){
            cellColors = savedColors
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return allReddits.allReddits.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return allReddits.allReddits[section]?[0].redditName
    }
    //MARK: - UITableViewDataSource Protocal Callbacks
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("This is section\(section)")
        return (allReddits.allReddits[section]?.count)!
    }
    
    //Row Display. Implementers should always try to reuse cells by setting each cells reuseIdentifier and querying for available resuable cells with dequeueResuableCellWIthIdentifier
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Resuse an existing cell or create a new one if needed
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_ID_1", for: indexPath)
        
        //configure the cell
        
        // Sets the optional value to a default choice if its nil
        cell.textLabel?.text = allReddits.allReddits[indexPath.section]?[indexPath.row].postName ?? "unnamed"
        cell.detailTextLabel?.text = allReddits.allReddits[indexPath.section]?[indexPath.row].autherName ?? "anonymous"
        cell.imageView?.image = allReddits.allReddits[indexPath.section]?[indexPath.row].image ?? #imageLiteral(resourceName: "q")
        //Sets up the table view color scheme
        cell.textLabel?.textColor = UIColor(displayP3Red: CGFloat(cellColors.labelSliderR), green: CGFloat(cellColors.labelSliderG), blue: CGFloat(cellColors.labelSliderB), alpha: 1)
        cell.detailTextLabel?.textColor = UIColor(displayP3Red: CGFloat(cellColors.detailLabelSlideR), green: CGFloat(cellColors.detailLabelSlideG), blue: CGFloat(cellColors.detailLabelSlideB), alpha: 1)
        cell.backgroundColor = UIColor(displayP3Red: CGFloat(cellColors.backgroundSliderR), green: CGFloat(cellColors.backgroundSliderG), blue: CGFloat(cellColors.backgroundSliderB), alpha: 1)
        //Return the config cell
        return cell
    }
    
    //Returns a url based on the subreddit
    func getURL (subReddit: String) -> String{
        let URLMain : String = "https://www.reddit.com/r/"
        let URLJson = "/.json"
        return URLMain + subReddit + URLJson
    }
    //Prepares for either segue by sending all the subreddits or the color scheme
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sVC = segue.destination as? RedditViewController{
            sVC.allRedditsSent = allReddits
            sVC.backUpSent = backUp
        }
        if let sVC = segue.destination as? ColorViewController{
            sVC.colors = cellColors
        }
    }
    
    //Reloads the data when its about to appear
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
        print()
    }
    //Passes back all the reddits to the main form again
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? RedditViewController{
            allReddits = sourceViewController.allRedditsSent
        }
    }
    

}

