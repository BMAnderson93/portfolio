//
//  RedditInformation.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/7/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation
import UIKit

class RedditInformation : NSObject, NSCoding {
   
    
    
    
    //Variables used to build the object
    let postName : String
    let autherName : String
    var image : UIImage!
    var redditName : String
   
    //Initializer
    init(postName : String, autherName: String, url: String!, redditName : String) {
        self.postName = postName
        self.autherName =  autherName
        self.redditName = redditName
        //Creates a url from the string passed in
        if url == nil {
            self.image = #imageLiteral(resourceName: "q")
        }
        else {
            if let url = URL(string: url){
                do {
                    //Creates a UIImage from the URL created
                    let data = try Data(contentsOf: url)
                    self.image = UIImage(data: data)!
                }
                catch{
                    print(error.localizedDescription)
                }
            }

        }
        
   
    
            
            
        
    }
    
    //Encoders and decoders to use the Userdefaults
    func encode(with aCoder: NSCoder) {
        aCoder.encode(postName, forKey: "postName")
        aCoder.encode(autherName, forKey: "autherName")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(redditName, forKey: "redditName")

        
    }
    required init?(coder aDecoder: NSCoder) {
        self.postName = aDecoder.decodeObject(forKey:"postName") as! String
        self.autherName = aDecoder.decodeObject(forKey:"autherName") as! String
        self.redditName = aDecoder.decodeObject(forKey:"redditName") as! String
        self.image = aDecoder.decodeObject(forKey:"image") as! UIImage
        
    }
    
    
}
