//
//  RedditViewController.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/19/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class RedditViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    //Back up file for reseting the subreddits
    var backUpSent : [RedditInformation]!
    var allRedditsSent : AllReddits = AllReddits.init(allReddits: [:])
    //Temp reddits to hold while we reorder the main sub reddits during deletion
    var tempAllReddits: AllReddits = AllReddits.init(allReddits: [:])
    override func viewDidLoad() {
        super.viewDidLoad()
      

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Alows us to swipe and delete at the row
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    //Used for setting up our swipe to delete
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //Final warning to the user to stop the deletion
        let delete = UIContextualAction(style: .destructive, title: "Remove") { (action, sourceView, completionHandler) in
            
            let tempAllReddits: AllReddits = AllReddits.init(allReddits: [:])
            
            //Goes through and reorders the keys and their values by -1 if theyre greater than the one removed
            self.allRedditsSent.allReddits.removeValue(forKey:indexPath.row)
                for r in self.allRedditsSent.allReddits{
                    if r.key > indexPath.row {
                    tempAllReddits.allReddits.updateValue(r.value, forKey: r.key - 1)
                    }
                    else {
                        tempAllReddits.allReddits.updateValue(r.value, forKey: r.key)
                    }
            }
            self.allRedditsSent = tempAllReddits
          
                tableView.reloadData()
                
            }
        
        
        
        //Returns the swipe config
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allRedditsSent.allReddits.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Resuse an existing cell or create a new one if needed
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_ID_1", for: indexPath)
        
        //configure the cell
        
       
        
      
        if let count = allRedditsSent.allReddits[indexPath.row]?.count {
            if count != 0 {
                cell.textLabel?.text = allRedditsSent.allReddits[indexPath.row]?[indexPath.section].redditName}}
        
        
        //Return the config cell
        return cell
    }
    
    @IBAction func Save(){
        //Saves the userdefaults for the saved reddit posts
        UserDefaults.standard.set(savedReddits: allRedditsSent, forKey: "savedReddits")
    }
    @IBAction func Reset(){
        //resets the reddit posts back to the original
        allRedditsSent.allReddits = [:]
        allRedditsSent.allReddits.updateValue(backUpSent, forKey: 0)
        tableView.reloadData()
        
    }
    //Adds a new post based on what the user types in, or retrusn nothing if they didnt enter a valid subreddit
    @IBAction func Add(){
        print("This Ran")
        if !(txtField.text?.isEmpty)!{
            downloadThenPop(jsonAtUrl: getURL(subReddit: txtField.text!), redditTitle: txtField.text!)
        }
        
        
        
    }
    
    
    //Gets a url from the users typed sub reddit
    func getURL (subReddit: String) -> String{
        let URLMain : String = "https://www.reddit.com/r/"
        let URLJson = "/.json"
        return URLMain + subReddit + URLJson
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
