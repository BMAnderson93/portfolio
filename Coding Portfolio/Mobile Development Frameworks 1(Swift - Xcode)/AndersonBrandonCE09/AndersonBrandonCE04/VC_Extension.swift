//
//  VC_Extension.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/8/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit
import Foundation

// Extension used to help keep my code clean
extension ViewController {
    func downloadThenPop(jsonAtUrl urlString: String){
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        
        //Creates the url I need from the string passed in
        if let validURL = URL(string: urlString){
            print("We have a valid URL")
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail Out on error
                if opt_error != nil { return }
                
                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }
                
                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                        //Parse Data
                        //Drills into the data, children, data to get the information I need from the json
                        let data1 =  json["data"]
                        var tempRedditContainer : [RedditInformation] = []
                        
                        if let children = data1!["children"] as? [[String : Any]]{
                            for i in children{
                                if let data = i["data"] as? [String : Any]{
                                    guard let autherName = data["author"] as? String,
                                    let redditName = data["title"] as? String,
                                    let url = data["thumbnail"] as? String
                                        else{return}
                                    
                                    // Validates that the URL for the image is valid and if so creates a new reddit post
                                    if url.contains("http"){
                                        let tempRedit = RedditInformation.init(postName: redditName, autherName: autherName, url: url, redditName : self.subRedit)
                                        tempRedditContainer.append(tempRedit)
                                        
                                    }
                                    else {
                                        let tempRedit = RedditInformation.init(postName: redditName, autherName: autherName, url: nil, redditName : self.subRedit)
                                        tempRedditContainer.append(tempRedit)
                                    }
                                    
                                        
                                    
                                }
                            }
                            
                            
                            self.allReddits.allReddits.updateValue(tempRedditContainer, forKey: 0)
                            self.backUp = tempRedditContainer
                            UserDefaults.standard.set(defaultReddit: tempRedditContainer, forKey: "savedDefault")
                            //Reloads the data to the table
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                           
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
            })
            //All tasks must use this
            task.resume()
        }
        
    }
}
