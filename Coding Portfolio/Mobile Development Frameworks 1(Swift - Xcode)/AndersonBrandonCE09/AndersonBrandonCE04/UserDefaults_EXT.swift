//
//  UserDefaults_EXT.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/19/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

extension UserDefaults{
    func set(savedReddits: AllReddits, forKey key: String){
        //Converts all the reddits to a data object by archiving
        let binaryData = NSKeyedArchiver.archivedData(withRootObject: savedReddits)
        
        //Save that data we created to the defaults
        
        self.set(binaryData, forKey: key)
    }
    func set(defaultReddit: [RedditInformation], forKey key: String){
        //Converts all the reddits to a data object by archiving
        let binaryData = NSKeyedArchiver.archivedData(withRootObject: defaultReddit)
        
        //Save that data we created to the defaults
        
        self.set(binaryData, forKey: key)
    }
    func set(savedColors: CellColors, forKey key: String){
        let binaryData = NSKeyedArchiver.archivedData(withRootObject: savedColors)
        
        //Save that data we created to the defaults
        
        self.set(binaryData, forKey: key)
    }
    
    
    //Get saved Reddits from saved defaults with its key
    func savedReddits(forKey key : String ) -> AllReddits?{
        if let binaryData = data(forKey: key){
            if let savedReddits = NSKeyedUnarchiver.unarchiveObject(with: binaryData) as? AllReddits{
                return savedReddits
            }
            
            //Else something is wrong with our data
            
        }
        return nil
    }
    
    func defaultReddit(forKey key : String ) -> [RedditInformation]?{
        if let binaryData = data(forKey: key){
            if let savedReddits = NSKeyedUnarchiver.unarchiveObject(with: binaryData) as? [RedditInformation]{
                return savedReddits
            }
            
            //Else something is wrong with our data
            
        }
        return nil
    }
    func savedColors(forKey key : String) -> CellColors? {
        if let binaryData = data(forKey: key){
            if let savedColors = NSKeyedUnarchiver.unarchiveObject(with: binaryData) as? CellColors{
                return savedColors
            }
            
            //Else something is wrong with our data
            
        }
        return nil
    }
}
