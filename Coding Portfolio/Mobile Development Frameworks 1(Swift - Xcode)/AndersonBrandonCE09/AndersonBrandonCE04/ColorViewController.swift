//
//  ColorViewController.swift
//  AndersonBrandonCE04
//
//  Created by Brandon Anderson on 5/19/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ColorViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editingLbl: UILabel!
    
    var colors : CellColors!
    
    var whichEdit = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        redSlider.value = colors.labelSliderR
        greenSlider.value  = colors.labelSliderG
        blueSlider.value = colors.labelSliderB
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func sliderDidChange(_ sender: UISlider) {
        
        //Updates the sliders and color information based on which slider is being moved and which part of the cell is being edited
        
        switch whichEdit {
        case 0:
            
                colors.labelSliderR = redSlider.value
                colors.labelSliderG = greenSlider.value
                colors.labelSliderB = blueSlider.value
             tableView.visibleCells.first?.textLabel?.textColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value), blue: CGFloat(blueSlider.value), alpha: 1)
        case 1:
            
            colors.detailLabelSlideR = redSlider.value
            colors.detailLabelSlideG = greenSlider.value
            colors.detailLabelSlideB = blueSlider.value
            tableView.visibleCells.first?.detailTextLabel?.textColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value), blue: CGFloat(blueSlider.value), alpha: 1)
        case 2:
            
            colors.backgroundSliderR = redSlider.value
            colors.backgroundSliderG = greenSlider.value
            colors.backgroundSliderB = blueSlider.value
            tableView.visibleCells.first?.backgroundColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value), blue: CGFloat(blueSlider.value), alpha: 1)
        default:
            print("I've got a bad feeling about this....")
        }
        
        
        
        
        
    }
    //Changes the selected cell the users editing and moves the sliders to match its colors
    @IBAction func changeSelected (sender: UIButton){
        whichEdit = sender.tag
        switch whichEdit {
        case 0:
            editingLbl.text = "Editing Title..."
            redSlider.value  = colors.labelSliderR
            greenSlider.value = colors.labelSliderG
            blueSlider.value = colors.labelSliderB
        case 1:
            editingLbl.text = "Editing Author..."
            redSlider.value  = colors.detailLabelSlideR
            greenSlider.value = colors.detailLabelSlideG
            blueSlider.value = colors.detailLabelSlideB
        case 2:
            editingLbl.text = "Editing Cell Background..."
            redSlider.value = colors.backgroundSliderR
            greenSlider.value = colors.backgroundSliderG
            blueSlider.value = colors.backgroundSliderB
        default:
            print("It's a trap!")
        }
        
    }
    //Saves the color scheme for the users cells to be loaded when the app starts
    @IBAction func save(){
        UserDefaults.standard.set(savedColors: colors, forKey: "savedColors")
    }
    // resets all the color values and sliders back to their original values
    @IBAction func reset(){
        tableView.visibleCells.first?.textLabel?.textColor = UIColor.black
        tableView.visibleCells.first?.detailTextLabel?.textColor = UIColor.black
        tableView.visibleCells.first?.backgroundColor = UIColor.white
       
            redSlider.value = 1
            blueSlider.value = 1
            greenSlider.value = 1
            colors.backgroundSliderR = 1
            colors.backgroundSliderG = 1
            colors.backgroundSliderB = 1
  
            redSlider.value = 0
            blueSlider.value = 0
            greenSlider.value = 0
            colors.labelSliderR = 0
            colors.labelSliderG = 0
            colors.labelSliderB = 0
            colors.detailLabelSlideR = 0
            colors.detailLabelSlideG = 0
            colors.detailLabelSlideB = 0
        
    }
    //Goes back to the main view
    @IBAction func back (){
    self.dismiss(animated: true, completion: nil)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Resuse an existing cell or create a new one if needed
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        //configure the cell
        
        
        cell.textLabel?.text = "Reddit Title"
        cell.detailTextLabel?.text = "Author"
        cell.textLabel?.textColor = UIColor(displayP3Red: CGFloat(colors.labelSliderR), green: CGFloat(colors.labelSliderG), blue: CGFloat(colors.labelSliderB), alpha: 1)
        cell.detailTextLabel?.textColor = UIColor(displayP3Red: CGFloat(colors.detailLabelSlideR), green: CGFloat(colors.detailLabelSlideG), blue: CGFloat(colors.detailLabelSlideB), alpha: 1)
        cell.backgroundColor = UIColor(displayP3Red: CGFloat(colors.backgroundSliderR), green: CGFloat(colors.backgroundSliderG), blue: CGFloat(colors.backgroundSliderB), alpha: 1)
        
        //Return the config cell
        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
