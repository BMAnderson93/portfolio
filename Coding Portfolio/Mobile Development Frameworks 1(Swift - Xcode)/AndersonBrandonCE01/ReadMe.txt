JSON Introduction Assignment

For this assignment, you will first create your own JSON data file according to the specifications on the rubric.

You will then use your JSON file to populate a simple object browser application.

The application will step through your list of model objects and correctly populate all UI fields with relevant info.

Final turn-in for this coding exercise should be named (lastname)(firstname)CE01.zip