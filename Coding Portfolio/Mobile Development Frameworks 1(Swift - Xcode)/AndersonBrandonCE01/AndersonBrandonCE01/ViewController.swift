//
//  ViewController.swift
//  AndersonBrandonCE01
//
//  Created by Brandon Anderson on 4/30/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // games will hold each object created from our json
    var games : [Game] = []
    // gameIndex is used for us to know which game to display on screen
    var gameIndex: Int = 0
    
    // Creates references to each label for editing
    @IBOutlet weak var gameTitle: UILabel!
    @IBOutlet weak var releaseYear: UILabel!
    @IBOutlet weak var multiplayer: UILabel!
    @IBOutlet weak var platforms: UILabel!
    @IBOutlet weak var age: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Creates the path ill need to access the videoGames.json file
            if let path = Bundle.main.path(forResource: "videoGames", ofType: ".json"){
        // Creates the URL using the path we just created
            let url = URL(fileURLWithPath: path)
            
            do {
                // This creates a data object from the url we created
                let data = try Data.init(contentsOf: url)
                // creates the jsonObj from the datafile we created
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)as? [Any]
                // runs the parse function which will handle creating our objects
                Parse(jsonObject: jsonObj)
                
            }
            catch{
                // Prints the error that occured
                print(error.localizedDescription)
            }
        }
        // Update function used for changing the labels to the correct information
        update()
        
        
        
        
    }
    func Parse(jsonObject: [Any]?){
        
        // Safely binds jsonObject to jsonObj
        if let jsonObj = jsonObject{
            //Loop through the first level itms in the json obj
            for firstLevelItm in jsonObj {
                
                // Validates that we can unwrap these and if so we will continue to creating a object
                guard let object = firstLevelItm as? [String: Any],
                    let title = object["title"] as? String,
                    let yReleased = object["year_released"] as? Int,
                    let hasMultiP = object["multiplayer"] as? Bool,
                    let platforms = object["consoles"] as? [String]
                    
                    // gets rid of the current obj that failed to be unwrapped and moves to the next
                    else{return}
                
                // Create a new game with all the json data
                games.append(Game(title: title, releaseYear: yReleased, multiplayer: hasMultiP, platforms: platforms))
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Used for deciding which game should be displayed
    @IBAction func showNew(sender: UIButton){
        switch sender.tag {
            // if the next button was pressed and we are already at the end, we will loop back to the beginning.
            // else we go to the next game
        case 0:
            if gameIndex == games.count - 1{
                gameIndex = 0
            }
            else{ gameIndex += 1}
            // If the previous button is pressed and we are already at the begining we will go to the end
            // else we go back 1 game
        default:
            if gameIndex == 0{
                gameIndex = games.count - 1
            }
            else{ gameIndex -= 1}
        }
        // updates the labels
        update()
    }
    
    // Sets the labels to match the selected game
    func update(){
        gameTitle.text = games[gameIndex].title
        releaseYear.text = String(games[gameIndex].releaseYear)
        multiplayer.text = games[gameIndex].multiplayerString
        platforms.text = games[gameIndex].eachConsole
        age.text = String(games[gameIndex].yearsOut)
    }


}

