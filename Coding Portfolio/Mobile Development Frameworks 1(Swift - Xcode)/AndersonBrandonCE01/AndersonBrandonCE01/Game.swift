//
//  Game.swift
//  AndersonBrandonCE01
//
//  Created by Brandon Anderson on 4/30/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class Game{
   
    // Class Properties
    let title : String
    let releaseYear : Int
    let multiplayer : Bool
    let platforms : [String]
    
    // Computed Properties
    
    // Gets the years that the game has been out for
    var yearsOut : Int {
        get{return 2018 - releaseYear}
    }
    
    // Displays each consol the game is out on in a more readable form
    var eachConsole : String{
        get{var platString = ""
            for p in platforms {
                platString += " \(p),"
            }
            return String(platString.dropLast())
        }
    }
    
    // Displays the bool in a more readable form
    var multiplayerString: String {
        get{ var boolString = ""
            if multiplayer == true{
                boolString = "Available"
            }
            else {
                boolString = "N/A"
            }
            return boolString
        }
    }
    
    
    
    
    
    // Inits
    
    // Initializes the game class
    init(title: String, releaseYear: Int, multiplayer : Bool, platforms : [String]){
        self.title = title
        self.releaseYear = releaseYear
        self.multiplayer = multiplayer
        self.platforms = platforms
    }
    
    
    
}
