//
//  ViewController.swift
//  AndersonBrandonCE03
//
//  Created by Brandon Anderson on 5/4/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // variables and references i will need for the story board
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblCatchPhrase: UILabel!
    
    @IBOutlet weak var lblc1Name: UILabel!
    @IBOutlet weak var lblc2Name: UILabel!
    @IBOutlet weak var lblc3Name: UILabel!
    @IBOutlet weak var lblc4Name: UILabel!
    @IBOutlet weak var lblc1: UILabel!
    @IBOutlet weak var lblc2: UILabel!
    @IBOutlet weak var lblc3: UILabel!
    @IBOutlet weak var lblc4: UILabel!
    @IBOutlet weak var lblDailyRevenue: UILabel!
    var currentIndex = 0
    var firstJson: [Any] = []
    var secondJson: [Any] = []
    var companies1: [CompanyJson1] = []
    var companies2: [CompanyJson2] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //MARKL - Retrieve JSON Data from remote server
        //Create a default config
        CreateJson1()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Parses the first json
    func ParseFirst(jsonObject: [Any]? ){
        
        guard let json = jsonObject
        else { print("Parse Failed to Unwrap jsonObject."); return }
        for firstLevelItem in json {
            //Try to convert
            guard let object = firstLevelItem as? [String: Any],
            let name = object["name"] as? String,
            let version = object["version"] as? String,
            let company = object["company"] as? String
                else{
                    continue
            }
            // No need for any con inits, we know this has no nul data
         companies1.append(CompanyJson1.init(name: name, version: version, company: company))
         
            
            
        }
        
        CreateJson2()
        print("first \(companies1.count) ")
        
        
        
        
        
        
    }
    // Parses the second jsonData
    func ParseSecond(jsonObject: [Any]?){
        guard let json = jsonObject
            else { print("Parse Failed to Unwrap jsonObject."); return }
        for firstLevelItem in json {
            //Try to convert
            guard let object = firstLevelItem as? [String: Any],
                let catchPhrase = object["catch_phrase"] as? String,
                let colors = object["colors"] as? [[String: Any]]
            
                else{
                    continue
            }
            // Used to store the data to the object
            var colorsKey: [String] = []
            var colorsVal: [String] = []
            for c in colors{
                for k in c {
                    if k.key == "desription" {
                        colorsKey.append((k.value as? String)!)
                    }
                    else {
                        colorsVal.append((k.value as? String)!)
                    }
                   
                    
                }
            }
            // Uses the default init to create the companies 2 objext
            if let revenue =  object["daily_revene"] as? String{
                companies2.append(CompanyJson2.init(catchPhrase: catchPhrase, revenue: revenue, colorsKey: colorsKey, colorsValue: colorsVal))
            }
            // Uses the convinences init incase we have missing data
            else {
                companies2.append(CompanyJson2.init(catcPhrase: catchPhrase, colorsKey: colorsKey, colorsValue: colorsVal))
            }
            
            
            
        }
        print("second \(companies2.count) ")
        update()
        
        
    }
    
    func CreateJson1(){
        //MARKL - Retrieve JSON Data from remote server
        //Create a default config
        let config = URLSessionConfiguration.default
        
        //Create a session
        let session = URLSession(configuration: config)
        
        //Validate the URL to make sure its not a broken link
        if let validURL = URL(string: "https://api.myjson.com/bins/9kj3f"){
            //Creates a task for what ever is found at the URL as a data object
            let task = session.dataTask(with: validURL) { (data, response, error) in
                //If there is an error, bail out of the entire method.
                if let error = error {
                    print("Data ask failed with error \(error.localizedDescription)")
                    return
                }
                //If this runs, we got the information from the URL as a DATA object and can now use it!
                print("Success")
                //Check responce status, validate data
                guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let validData = data
                    else{print("JSONObject creation failed"); return}
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [Any]
                    self.firstJson = jsonObj!
                    self.ParseFirst(jsonObject: self.firstJson)
                }
                catch{
                    print(error.localizedDescription)
                }
            }
            //MUST ALWAYS START THE TASK!!!
            task.resume()
           
    }
       

    }
    func CreateJson2(){
        //MARKL - Retrieve JSON Data from remote server
        //Create a default config
        let config = URLSessionConfiguration.default
        
        //Create a session
        let session = URLSession(configuration: config)
        
        //Validate the URL to make sure its not a broken link
        if let validURL = URL(string: "https://api.myjson.com/bins/1cxa6j"){
            //Creates a task for what ever is found at the URL as a data object
            let task = session.dataTask(with: validURL) { (data, response, error) in
                //If there is an error, bail out of the entire method.
                if let error = error {
                    print("Data ask failed with error \(error.localizedDescription)")
                    return
                }
                //If this runs, we got the information from the URL as a DATA object and can now use it!
                print("Success")
                //Check responce status, validate data
                guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let validData = data
                    else{print("JSONObject creation failed"); return}
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [Any]
                    self.secondJson = jsonObj!
                    self.ParseSecond(jsonObject: self.secondJson)
                }
                catch{
                    print(error.localizedDescription)
                }
            }
            //MUST ALWAYS START THE TASK!!!
            task.resume()
        }
        
        
    }
    
    // Hides the lbls for the colors
    func hideColor(){
        
         self.lblc1.isHidden = true
         self.lblc2.isHidden = true
         self.lblc3.isHidden = true
        self.lblc4.isHidden = true
        self.lblc1Name.isHidden = true
        self.lblc2Name.isHidden = true
        self.lblc3Name.isHidden = true
        self.lblc4Name.isHidden = true
        
    }
    //Updates the index
    
    @IBAction func updateIndex(sender: UIButton){
        
        if sender.tag == -1 && currentIndex == 0{
            currentIndex = companies1.count - 1
        }
        else if sender.tag == 1 && currentIndex == companies1.count - 1{
            currentIndex = 0
        }
        else {
            currentIndex += sender.tag
        }
        update()
    }
    // Updates the story board with the index information, unhiding any color lbls that need to be shown
    func update(){
        DispatchQueue.main.async {
        self.hideColor()
        self.lblVersion.text = self.companies1[self.currentIndex].version
        self.lblName.text = self.companies1[self.currentIndex].name
        self.lblCompany.text = self.companies1[self.currentIndex].company
        self.lblCatchPhrase.text = self.companies2[self.currentIndex].catchPhrase
        self.lblDailyRevenue.text = self.companies2[self.currentIndex].revenue
        
        if self.companies2[self.currentIndex].self.colorsKey.count > 0{
            self.lblc1.text = self.companies2[self.currentIndex].colorsValue[0]
            self.lblc1Name.text = self.companies2[self.currentIndex].colorsKey[0]
            self.lblc1.isHidden = false
            self.lblc1Name.isHidden = false
        }
        if self.companies2[self.currentIndex].self.colorsKey.count > 1{
            self.lblc2.text = self.companies2[self.currentIndex].colorsValue[1]
            self.lblc2Name.text = self.companies2[self.currentIndex].colorsKey[1]
            self.lblc2.isHidden = false
            self.lblc2Name.isHidden = false
            
            }
            
        if self.companies2[self.currentIndex].self.colorsKey.count > 2{
            self.lblc3.text = self.companies2[self.currentIndex].colorsValue[2]
            self.lblc3Name.text = self.companies2[self.currentIndex].colorsKey[2]
            self.lblc3.isHidden = false
            self.lblc3Name.isHidden = false
        }
        if self.companies2[self.currentIndex].self.colorsKey.count > 3{
            self.lblc4.text = self.companies2[self.currentIndex].colorsValue[3]
            self.lblc4Name.text = self.companies2[self.currentIndex].colorsKey[3]
            self.lblc4.isHidden = false
            self.lblc4Name.isHidden = false
        }
        }
        
    }
    
}
