//
//  CompanyJson1.swift
//  AndersonBrandonCE03
//
//  Created by Brandon Anderson on 5/4/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class CompanyJson1{
    // Stored var
    let name: String
    let version: String
    let company: String
    
    //default init
    init(name:String, version: String, company: String) {
        self.name = name
        self.version = version
        self.company = company
    }
}
