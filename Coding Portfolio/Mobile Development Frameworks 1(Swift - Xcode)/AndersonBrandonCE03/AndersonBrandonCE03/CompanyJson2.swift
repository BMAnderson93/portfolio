//
//  CompanyJson2.swift
//  AndersonBrandonCE03
//
//  Created by Brandon Anderson on 5/4/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class CompanyJson2 {
    //Stored vars
    let catchPhrase : String
    let revenue : String!
    let colorsKey : [String]
    let colorsValue : [String]
    
    
   
    
    // default init
    init(catchPhrase: String, revenue: String? = nil, colorsKey: [String], colorsValue: [String]){
        self.catchPhrase = catchPhrase
        self.revenue = revenue
        self.colorsKey = colorsKey
        self.colorsValue = colorsValue
    }
    
    //Con init incase we have some nul data
    convenience init (catcPhrase: String, colorsKey: [String], colorsValue: [String]){
        self.init(catchPhrase: catcPhrase, revenue: "$0.00", colorsKey: colorsKey, colorsValue: colorsValue)
        
    }
   
    
}
