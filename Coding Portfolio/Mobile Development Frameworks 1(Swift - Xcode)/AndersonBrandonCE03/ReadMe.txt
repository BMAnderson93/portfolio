Assignment:
For this assignment, you will be pulling related JSON data from 2 remote locations, then combining that data by mapping it into a single model type for display.
You will then display the relevant data of that type to the user through a simple object browser UI.
The application will step through your list of model objects and correctly populate all UI fields with relevant.
Final turn-in for this coding exercise should be named (lastname)(firstname)CE03.zip
Example �DoeJaneCE03.zip�
JSON Links: 

App Data: These data sets describe an application perhaps from a product manager�s perspective.

Part 1: https://api.myjson.com/bins/9kj3f

Part 2: https://api.myjson.com/bins/1cxa6j

