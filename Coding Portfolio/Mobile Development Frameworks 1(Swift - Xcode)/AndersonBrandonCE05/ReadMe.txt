For this assignment, you will create an application that displays congressional data from the ProPublica Congress API.
Request an API Key, this takes only a moment and is required by ProPublica.
Build the URI: Each API has their own requirements for building the URI, in this case you will be working with the Member Endpoint. The documentation will aid you in supplying the correct version number and the correct number of the house and/or senate (the highest number is the current sitting congress which is the one you should always use).
The ProPublica API requires the use of API requests to be sent as a header each time a request is made. If you would like to test your API call with the header parameters this site can be quite useful, Rest Test Test, click on 'Add Header' to add your header values. Please see the code snippet below for a visual guide on adding the request and creating the task.
Display Parsed data into a TableView of custom cells.
Upon cell selection a DetailView should display the selected legislator�s full name, party, title, state of residence and associated image (the data you download will contain an "id" for each member that you will need for using the images * see the link below
Final turn-in for this coding exercise should be named (lastname)(firstname)CE05.zip
Example �DoeJaneCE05.zip�
 