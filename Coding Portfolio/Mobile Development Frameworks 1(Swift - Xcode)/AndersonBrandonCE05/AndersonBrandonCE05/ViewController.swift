//
//  ViewController.swift
//  AndersonBrandonCE05
//
//  Created by Brandon Anderson on 5/10/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //References to populate the storyboard
    @IBOutlet weak var progressBar: UIActivityIndicatorView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var partyLogo: UIImageView!
    @IBOutlet weak var houseLogo: UIImageView!
    @IBOutlet weak var imageLogo: UIImageView!
    var gov : GovPerson!
    override func viewDidLoad() {
        super.viewDidLoad()

        //If we got a gov we will begin setting the information in the storyboard to match
        if gov != nil{
            
            nameLbl.text = gov.fullName
            stateLbl.text = gov.state
            titleLbl.text = gov.title
            //Sets the extra images used to match the party and if which branch theyre from
            switch gov.party{
            case "R":
                partyLogo.image = #imageLiteral(resourceName: "republican")
            case "D":
                partyLogo.image = #imageLiteral(resourceName: "democrat")
            default:
                print("Nothing Changed")
                
            }
            switch gov.branch{
            case "house":
                houseLogo.image = #imageLiteral(resourceName: "house")
            case "senate":
                houseLogo.image = #imageLiteral(resourceName: "senate")
            default:
                print("Nothing Changed")
                
            }
            
        }
        getImage()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getImage (){
       
        // Gets the Image for the congressman based on their ID
        let url = "https://theunitedstates.io/images/congress/450x550/\(gov.id).jpg"
        if let url = URL(string: url){
            do {
                //Creates a UIImage from the URL created
                let data = try Data(contentsOf: url)
                imageLogo.image = UIImage(data: data)!
            }
            catch{
                //If they dont have a image we will just give them a temp one based on their party
                partyLogo.isHidden = true
                print(error.localizedDescription)
                if gov.party == "R"{
                    imageLogo.image = #imageLiteral(resourceName: "republican")
                }
                else if gov.party == "D"{
                    imageLogo.image = #imageLiteral(resourceName: "democrat")
                }
                else{
                    imageLogo.image = #imageLiteral(resourceName: "IPNY_Logo_3423432")
                }
        }
            //Stops the animation, and it will be hidden after
        progressBar.stopAnimating()
        }
    }

    

}
