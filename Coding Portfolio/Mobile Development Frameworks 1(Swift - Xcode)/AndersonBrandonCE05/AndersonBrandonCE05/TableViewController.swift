//
//  TableViewController.swift
//  AndersonBrandonCE05
//
//  Created by Brandon Anderson on 5/9/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    
    //Vars and references to be used throughout creating our cells
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    var unfilteredGov = [GovPerson]()
    var filteredGovs = [[GovPerson](),[GovPerson](),[GovPerson]()]
    override func viewDidLoad() {
        super.viewDidLoad()

        //Gets all the gov's information from the API
        DownloadJson(atURL: "https://api.propublica.org/congress/v1/115/senate/members.json", branch: "senate")
        DownloadJson(atURL: "https://api.propublica.org/congress/v1/115/house/members.json", branch: "house")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        //Sets the number of sections to be equal to the number of different parties we have
        
        return filteredGovs.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Sets number of rows to be equal to that sections govs
        return filteredGovs[section].count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? GovCell

        // Configure the cell...
        
        else{return tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)}
        // Sets all the information in the custom cell to match that index's gov
        let currentGov = filteredGovs[indexPath.section][indexPath.row]
        cell.nameLbl.text = currentGov.fullName
        cell.stateLbl.text = currentGov.state
        cell.titleLbl.text = currentGov.title
        cell.partyLbl.text = currentGov.party
        switch currentGov.party {
        case "R":
            cell.backgroundColor = UIColor.red
        case "D":
            cell.backgroundColor = UIColor.blue
        default:
            cell.backgroundColor = UIColor.yellow
        }
        return cell
    }
    
    //Mark: - Header Methods
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Creates the section headers
        switch section {
        case 0:
            return "Republican"
        case 1:
            return "Democrat"
            
        case 2:
            return "Independent"
        default:
            return "I've got a bad feeling about this..."
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow{
            // Sends over the selected congressman and updates the title
            let postToSend = filteredGovs[indexPath.section][indexPath.row]
            if let destination = segue.destination as? ViewController{
                destination.gov = postToSend
                self.title = postToSend.fullName
                
                
                
                
            }
        }
    }
    

}
