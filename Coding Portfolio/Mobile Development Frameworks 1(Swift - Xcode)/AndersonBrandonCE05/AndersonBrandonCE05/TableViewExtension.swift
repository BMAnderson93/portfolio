//
//  TableViewExtension.swift
//  AndersonBrandonCE05
//
//  Created by Brandon Anderson on 5/9/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

extension TableViewController{
    
    //Func to download and parse the URL
    func DownloadJson(atURL urlString: String, branch: String){
        self.progressIndicator.isHidden = false
        self.progressIndicator.startAnimating()
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        
        if let validUrl = URL(string: urlString){
            var request = URLRequest(url: validUrl)
            //Gives the URL my API key and gets the information
            request.setValue("7h32YHdKb8IyDVt01IU3KX2YAJbNPaSyY1RqbS3h", forHTTPHeaderField: "X-API-Key")
            
            request.httpMethod = "Get"
            
            let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
                
                //Bail Out on error
                if error != nil { assertionFailure(); return }
                //Check the response, statusCode, and data
                guard let response = response as? HTTPURLResponse,
                    response.statusCode == 200
                    else { assertionFailure(); return
                }
                
                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        // Sets these temp variables to match the format of the information we need to create the object
                        let results = json["results"] as? [[String: Any]]
                        for r in results!{
                            guard let members = r["members"] as? [[String: Any]]
                            else{return}
                            for i in members{
                                guard let fname = i["first_name"] as? String,
                                    let lname = i["last_name"] as? String,
                                    let party = i["party"] as? String,
                                    let title = i["title"] as? String,
                                    let state = i["state"] as? String,
                                    let id = i["id"] as? String
                                
                                    else{return}
                                //Handles the chance of them not having a middle name and Inits the object based on that
                                if let mname = i["middle_name"] as? String{
                                    let newGovPerson = GovPerson.init(fname: fname, mname: mname , lname: lname, title: title, state: state, party: party, id: id, branch: branch)
                                    self.unfilteredGov.append(newGovPerson)
                                }
                                else {
                                    let newGovPerson = GovPerson.init(fname: fname, mname: nil , lname: lname, title: title, state: state, party: party, id: id, branch: branch)
                                    self.unfilteredGov.append(newGovPerson)
                                }
                                
                            }
                            
                        }
                        
                        
                        
                    }
                }
                catch {
                    print(error.localizedDescription)
                    assertionFailure();
                }
                // Reloads the data when were done parsing
                DispatchQueue.main.async {
                    self.filterGovByParty()
                    self.tableView.reloadData()
                }

                
            })
            //MUST BE USED
            task.resume()
        }
    }
    //Filters the goverment parties and stops the progress animation
    func filterGovByParty() {
        
        filteredGovs[0] = unfilteredGov.filter({ $0.party == "R" })
        filteredGovs[1] = unfilteredGov.filter({ $0.party == "D" })
        filteredGovs[2] = unfilteredGov.filter({ $0.party == "I" })
        self.progressIndicator.isHidden = true
        self.progressIndicator.stopAnimating()
        
    }
}
