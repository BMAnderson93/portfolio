//
//  GovCell.swift
//  AndersonBrandonCE05
//
//  Created by Brandon Anderson on 5/10/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class GovCell: UITableViewCell {
    //Vars needed for the cell
    @IBOutlet weak var partyLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
