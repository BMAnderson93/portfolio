//
//  GovPerson.swift
//  AndersonBrandonCE05
//
//  Created by Brandon Anderson on 5/9/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class GovPerson{
    // Var needed to create this object
    let fullName: String
    let party: String
    let title: String
    let state: String
    let id: String
    let branch: String
    
    //Used to create the Object
    init (fname: String, mname: String!, lname: String, title: String, state: String, party: String, id: String, branch: String){
        //Creates the full name based on if they have a middle name or not
        if mname != nil {
        self.fullName = "\(lname), \(String(mname.first!)). \(fname)"
        }
        else {
            self.fullName = "\(lname), \(fname)"
        }
        self.party = party
        self.title = title
        self.state = state
        self.id = id
        self.branch = branch
        
    }
    
}
