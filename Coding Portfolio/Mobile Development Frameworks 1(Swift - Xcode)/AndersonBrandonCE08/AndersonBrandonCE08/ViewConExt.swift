//
//  ViewConExt.swift
//  AndersonBrandonCE08
//
//  Created by Brandon Anderson on 5/17/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation
extension ViewController {
    func ParseJson(){
        if let path = Bundle.main.path(forResource: "zips", ofType: ".json"){
            // Creates the URL using the path we just created
            let url = URL(fileURLWithPath: path)
            
            do {
                // This creates a data object from the url we created
                let data = try Data.init(contentsOf: url)
                // creates the jsonObj from the datafile we created
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)as? [Any]
                // runs the parse function which will handle creating our objects
                // Safely binds jsonObject to jsonObj
                    //Loop through the first level itms in the json obj
                for firstLevelItm in jsonObj! {
                        
                        // Validates that we can unwrap these and if so we will continue to creating a object
                        guard let object = firstLevelItm as? [String: Any],
                            let city = object["city"] as? String,
                            let state = object["state"] as? String,
                            let population = object["pop"] as? Int,
                            let zip = object["_id"] as? String
                            
                            // gets rid of the current obj that failed to be unwrapped and moves to the next
                            else{print("I failed");return}
                        
                        // Create a new city with all the json data
                    self.unfilteredCities.append(City.init(state: state, city: city, population: population, zip: zip))
                    }
                
                
            }
            catch{
                // Prints the error that occured
                print(error.localizedDescription)
            }
        }
    }
}
