//
//  Cities.swift
//  AndersonBrandonCE08
//
//  Created by Brandon Anderson on 5/17/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class City {
    //Local vars needed to build a city
    var state : String
    var city : String
    var population : Int
    var zip : String
    
    //Computed var for displaying citystate
    var cityState : String {
        return "\(city), \(state)"
    }
    
    //Creates the OBJ
    init (state: String, city: String, population: Int, zip: String){
        
        self.state = state
        self.city = city
        self.population = population
        self.zip = zip
        
    }
}
