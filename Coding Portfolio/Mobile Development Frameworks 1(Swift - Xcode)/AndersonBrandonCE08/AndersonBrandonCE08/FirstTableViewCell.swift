//
//  FirstTableViewCell.swift
//  AndersonBrandonCE08
//
//  Created by Brandon Anderson on 5/17/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class FirstTableViewCell: UITableViewCell {
//Class for the first table view cell
    @IBOutlet weak var cityState: UILabel!
    @IBOutlet weak var popCount: UILabel!
    @IBOutlet weak var zipCode: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
