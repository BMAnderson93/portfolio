//
//  ViewController.swift
//  AndersonBrandonCE08
//
//  Created by Brandon Anderson on 5/16/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    
    //Container for the unfiltered cities and a CityHolder OBJ to make passing the data back simpler
    var unfilteredCities : [City] = []
    var returnedCities : CityHolder = CityHolder.init(cities: [])
    override func viewDidLoad() {
        super.viewDidLoad()
        //Parse the json file
        ParseJson()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //Updates and fixed the data table view each time this view loads as well as fixed the navigation controller
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        title = "Displaying \(returnedCities.cities.count) results"
        tableView.reloadData()
       
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //Sets the rows equal to how ever many returned cities we have
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return returnedCities.cities.count
    }
    //Clears and updates the TV
    @IBAction func Clear(){
        returnedCities.cities = []
         title = "Displaying \(returnedCities.cities.count) results"
        tableView.reloadData()
    }
    
    //Configures the TV Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? FirstTableViewCell{
        
        // Configure the cell...
        cell.cityState.text = returnedCities.cities[indexPath.row].cityState
        cell.popCount.text = returnedCities.cities[indexPath.row].population.description
        cell.zipCode.text = returnedCities.cities[indexPath.row].zip.description
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        return cell
    }
    
    //Prepares for the seque by sending over all the cities so we only parse them 1 time
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sVC = segue.destination as? SecondTableViewController{
            self.title = "Results"
            sVC.unfilteredCities = self.unfilteredCities
            sVC.SendBack = self.returnedCities
        }
    }
}

