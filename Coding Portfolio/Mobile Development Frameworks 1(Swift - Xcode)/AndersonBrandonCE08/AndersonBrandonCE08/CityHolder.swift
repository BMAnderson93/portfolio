//
//  CityHolder.swift
//  AndersonBrandonCE08
//
//  Created by Brandon Anderson on 5/17/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

//I use this class so i can easily pass the filtered cities back
class CityHolder {
    var cities : [City]
    
    init (cities: [City]){
        self.cities = cities
    }
}
