//
//  SecondTableViewController.swift
//  AndersonBrandonCE08
//
//  Created by Brandon Anderson on 5/17/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class SecondTableViewController: UITableViewController, UISearchBarDelegate, UISearchResultsUpdating, UISearchControllerDelegate {
    
    
    
    //Use current controller to display results
    var searchController = UISearchController(searchResultsController: nil)
    
    //Objects for saving all the cities, filtering them, and sending them back
    var unfilteredCities : [City] = []
    var filteredCities : [City] = []
    var SendBack : CityHolder?
    override func viewDidLoad() {
        super.viewDidLoad()
        //Used to fix the navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        filteredCities = unfilteredCities
        
        //Setup the search controller
        searchController.dimsBackgroundDuringPresentation = false
        searchController.definesPresentationContext = true
        //To recieve updates to searches here in this table view controller
        searchController.searchResultsUpdater = self
        //Setup the searchbar of the searchcontroller
        searchController.searchBar.scopeButtonTitles = ["All", "MO", "FL"]
        searchController.searchBar.delegate = self
        searchController.searchBar.showsScopeBar = true
        
        //Addd the searchbar as a navigation item
        navigationItem.searchController = searchController

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Sends back the filtered cities and pops the current VC
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        SendBack!.cities = filteredCities
        searchController.searchBar.showsScopeBar = false
       navigationController?.popViewController(animated: true)
        
        
    }
    func updateSearchResults(for searchController: UISearchController) {
        
        //Using the incoming searchController, we need to grab the current searchBar text and scope title selected. Then filter the resaults according to it. Which is why we needed a seperate container to hold that information
        
        //Get the text the user wants
        let searchText = searchController.searchBar.text
        
        //Get the scope title that was selected by the user
        let selectedScope = searchController.searchBar.selectedScopeButtonIndex
        let allScopeTitles = searchController.searchBar.scopeButtonTitles!
        let scopeTitle = allScopeTitles[selectedScope]
        
        
        
        //Filter our content
        
        //Dump our full data set into the array that we will use for filtering
        filteredCities = unfilteredCities
        
        //If the user typed anything we will filter on it
        if searchText != ""{
          
            filteredCities = filteredCities.filter({ (contact) -> Bool in
                return contact.city.lowercased().range(of: searchText!.lowercased()) != nil
            })
        }
        //Then filter again based on the selected scope
        if scopeTitle != "All" {
            filteredCities = filteredCities.filter(
                {
                    
                    $0.state.range(of: scopeTitle) != nil
            })
        }
        tableView.reloadData()
    }
    //UpdatesSearchResults based on our serachController
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearchResults(for: searchController)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    //Setes the rows equal to the total of cities
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filteredCities.count
    }

    
    //Configures each cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? SecondTableViewCell

        // Configure the cell...
        cell?.cityState.text = filteredCities[indexPath.row].cityState
        cell?.zip.text = filteredCities[indexPath.row].zip.description
        cell?.population.text = filteredCities[indexPath.row].population.description

        return cell!
    }
    

    
}

