Assignment: SearchBar
 

Overview: In this lab you will create an application to search and filter a provided JSON file and return those search results to the main view. A UINavigationController will move the user between the two views.

 

Results View (initial view):

This view will contain a UIToolBar and a UITableView.
UIToolBar will contain two buttons, one to search and to clear.
When user taps the search icon they will be taken to the Search View.
When user taps �Clear� any displayed results will be removed.
UITableView:
TableView will initially be empty.
TableView will display search results upon return from Search View.
Search View

This view will contain a UITableView and a UISearchBar.
UITableView:
TableView will initially display all data to be searched.
TableView should update as results are filtered.
TableView should update as scopes change.
UISearchBar
ScopeBar should contain 3 scopes. The first scope should be "All", for the other 2 scopes please choose 2 states, your user will then be able to filter the cities within those 2 states.
When selected scopes display only the data in their scope.
User should be able to search within a selected scope.
Search results are returned to the Results View.
Final turn-in for this coding exercise should be named (lastname)(firstname)CE08.zip
Example �DoeJaneCE08.zip�