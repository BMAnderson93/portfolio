JSON Intermediate Assignment
For this assignment, you will add a JSON file to your project and mapping it for display.
You will then display the relevant data of that type to the user through a simple object browser UI.
The application will step through your list of model objects and correctly populate all UI fields with the relevant information.
On top of the JSON data the UI should also display the following (null data should be handled appropriately):
Number of past employers
Number of responsibilities for each past employer
Number of skills