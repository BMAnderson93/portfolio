//
//  Employer.swift
//  AndersonBrandonCE02
//
//  Created by Brandon Anderson on 5/2/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class Employer {
    
    let companyName: String
    let responsibilities: [String]! //May not exist
    
    
    
    
    // Default init
    init(companyName: String, responsibilities: [String]? = [String]()) {
        self.companyName = companyName
        self.responsibilities = responsibilities
    }
    // Use this if responsibilites is nil
    convenience init(companyName: String, responsibilities: [String?]? = nil){
        if let responsibilities = responsibilities{
            self.init(companyName: companyName, responsibilities: responsibilities)
        }
        else{
            self.init(companyName: companyName, responsibilities: nil)
        }
    }
}
