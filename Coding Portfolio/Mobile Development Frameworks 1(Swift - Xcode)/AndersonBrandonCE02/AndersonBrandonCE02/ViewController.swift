//
//  ViewController.swift
//  AndersonBrandonCE02
//
//  Created by Brandon Anderson on 5/2/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // References to my storyboard
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblMacaddress: UILabel!
    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblSkills: UILabel!
    
    @IBOutlet weak var txtFieldEmployers: UITextView!
    
    @IBOutlet weak var lblSkillsCount: UILabel!
    @IBOutlet weak var lblEmployerCount: UILabel!
    // Uses to hold variables ill need to hold the json data and display it
    var employees = [Employee]()
    var pastJobs: [Employer] = []
    var indexNumber = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let path = Bundle.main.path(forResource: "EmployeeData", ofType: ".json"){
            
            //Create URL with the path
            
            let url = URL(fileURLWithPath: path)
            
            do {
                //Create a data object from our files url
                //After this line executes our file is in binary format inside the data constant
                let data = try Data.init(contentsOf: url)
                
                //Create a json object from the binary data file, then cast it as an array of any type objects
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                
                //At this point we have an array of any objects that represents our json file, we can now parse it
                Parse(jsonOBJ: jsonObj)
                //Updates the form
                update()
             
               
                
                
            }
                
            catch{
                print(error.localizedDescription)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Unwraps the json data
    func Parse(jsonOBJ: [Any]?){
        guard let json = jsonOBJ
            else {print("Failed to unwrap the json file, something went wrong"); return}
        for firstLevelItem in json{
            // stores the data from the json in a usable form
            guard let object = firstLevelItem as? [String: Any],
            let employeename = object["employeename"] as? String,
            let username = object["username"] as? String,
            let macaddress = object["macaddress"] as? String,
            let currenttitle = object["current_title"] as? String,
           
            let skills = object["skills"] as? [Any],
                
            let pastEmployees = object["past_employers"] as? [[String: Any]]
           
            
            
            
                else{continue}
           
            // checks to see if any information is missing from pastemployees and sets it to nil if so
            if pastEmployees.isEmpty != true{
                pastJobs = []
               
                for i in pastEmployees{
                    
                    let responceArray = i["responsibilities"] as! [String]
                    
                    if responceArray.isEmpty != true {
                        let employer = Employer.init(companyName: i["company"] as! String, responsibilities: responceArray)
                        pastJobs.append(employer)
                        
                        
                    }
                    else {
                        let employer = Employer.init(companyName: i["company"]as! String, responsibilities: nil)
                        pastJobs.append(employer)
                        
                    }
                    
                    
                    
                }
                // Checks to see if any information is missing from skills and sets it to nil if so
                if skills.isEmpty != true{
                    let newEmployee  = Employee.init(fullname: employeename, username: username, macaddress: macaddress, currentTitle: currenttitle, skills: (skills as! [String]), empoyers: pastJobs)
                    employees.append(newEmployee)
                }
                else {
                    let newEmployee  = Employee.init(fullname: employeename, username: username, macaddress: macaddress, currentTitle: currenttitle, skills: nil, empoyers: pastJobs)
                    employees.append(newEmployee)
                }
                
                // Creates my employee based on the information we got from the json accounting for any nil information
            }else {
            if skills.isEmpty != true {
                let newEmployee = Employee.init(fullname: employeename, username: username, macaddress: macaddress, currentTitle: currenttitle, skills: (skills as! [String]))
                employees.append(newEmployee)
            }
            else {
                let newEmployee = Employee.init(fullname: employeename, username: username, macaddress: macaddress, currentTitle: currenttitle)
                employees.append(newEmployee)
            }
            }
           
          
        }
        
    }
    // changes index based on what button was pressed
    @IBAction func changeIndex(sender: UIButton){
        if sender.tag == -1 && indexNumber == 0 {
            indexNumber = employees.count - 1
        }
        else if sender.tag == 1 && indexNumber == employees.count - 1 {
            indexNumber = 0
        }
        else {
            indexNumber += sender.tag
        }
        update()
    }
    // used for updating the form based on the index number
    func update(){
        lblName.text = employees[indexNumber].fullname
        lblSkills.text = employees[indexNumber].cleanSkills
        lblJobTitle.text = employees[indexNumber].currentTitle
        lblMacaddress.text = employees[indexNumber].macaddress
        lblUsername.text = employees[indexNumber].username
        txtFieldEmployers.text = employees[indexNumber].cleanEmployees
        lblSkillsCount.text = employees[indexNumber].skillsTotal.description
        lblEmployerCount.text = employees[indexNumber].pastEmployeeTotal.description
    }
}

