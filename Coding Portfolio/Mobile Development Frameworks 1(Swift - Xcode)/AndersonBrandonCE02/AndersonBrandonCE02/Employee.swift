//
//  Employee.swift
//  AndersonBrandonCE02
//
//  Created by Brandon Anderson on 5/2/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class Employee{
    //Stored Properties
    let fullname:  String
    let username:String
    let macaddress: String
    let currentTitle: String
    let skills: [String]!
    let empoyers: [Employer]?
    
    
    
    //Computed Properties
    // Returns the past employee total, sets it to 0 if its nil
    var pastEmployeeTotal: Int{
        if empoyers == nil{
            return 0
        }
        else{
            return empoyers!.count
        }
    }
    var skillsTotal: Int {
        if skills == nil{
            return 0
        }
        else{
            return skills.count
        }
    }
    //Returns a clean string version of all skills, returns none if nil
    var cleanSkills: String{
       var cleanString = ""
        if skills != nil{
        for s in skills{
            cleanString += "\(s), "
        }
        }
        else{
            cleanString = "This person listed no skills  "
        }
        cleanString.remove(at: cleanString.index(before: cleanString.endIndex))
        cleanString.remove(at: cleanString.index(before: cleanString.endIndex))
        return cleanString
    }
    // Returns a clean string of the employers, returns none if nil
    var cleanEmployees: String{
        var cleanEmployee = ""
        var employeeCount = 1
        if empoyers != nil{
            for e in empoyers!{
                if e.responsibilities != nil{
                cleanEmployee += "\(employeeCount)) \(e.companyName) \n Responsibilities (\(e.responsibilities.count)): "
                employeeCount += 1
                
                for r in e.responsibilities{
                    cleanEmployee += "\(r), "
                    
                }
                
            }
              
                else {
                    cleanEmployee += "\(employeeCount)) \(e.companyName) \n Responsibilities (0): No responsibilites were listed!  "
                    employeeCount += 1
                }
                cleanEmployee += "\n"
            }
       
        }
        else{
            cleanEmployee = "This person listed no previous employees!"
        }
        
        return cleanEmployee
    }
    
    //Init
    
    init(fullname: String, username: String, macaddress: String, currentTitle: String, skills: [String]? = nil, empoyers: [Employer]? = nil) {
        self.fullname = fullname
        self.username = username
        self.macaddress = macaddress
        self.currentTitle = currentTitle
        self.skills = skills
        self.empoyers = empoyers
    }
    // Use this init if skills is nil
    convenience init (fullname: String, username: String, macaddress: String, currentTitle: String, empoyers: [Employer]?){
        self.init(fullname: fullname, username: username, macaddress: macaddress, currentTitle: currentTitle, skills: nil, empoyers: empoyers)
    }
    //use this init if employers is nil
    convenience init (fullname: String, username: String, macaddress: String, currentTitle: String,skills: [String]?){
        self.init(fullname: fullname, username: username, macaddress: macaddress, currentTitle: currentTitle, skills: skills, empoyers: nil)
    }
    // USe this init if skills AND employers is nil
    convenience init (fullname: String, username: String, macaddress: String, currentTitle: String){
        
        self.init(fullname: fullname, username: username, macaddress: macaddress, currentTitle: currentTitle, skills: nil, empoyers: nil)
    }
    
    //Methods
}
