Proper use of URLSessions are NOT how to complete this assignment. They are the preferred method of building this functionality. However, our goal is to highlight and utilize Grand Central Dispatch in a very visual way to enhance understanding. URLSessions circumvent our use case which is thread blocking. iOS has actually made it easier for us to perform this particular task without thread blocking by utilizing URLSessions. However, the downloads in this assignment represent any long running task that we might perform. Images are just more interesting than calculating pi to the 20th exponent, or hitting an unusually slow database somewhere.



 

URLSession Regular
URLSession Regular
Regular Download
Regular Download
URLSession Serial
URLSession Serial
Serial Synchronously
Serial Synchronously
Overview: For this lab you will be creating an application that downloads 8 large image files from the internet using no queues, using a serial queue, and using a concurrent queue. You may need to disable the new App Transport Security setting.

 

You will go to Google Images or similar and locate 8 images that are exactly 1080p 16:9 aspect ratio.
Copy the Link address in order to use it in code to download that image.
You will be turning string links into images each time a download button is pressed.
No images should be saved to the device or otherwise exist inside your project.
Copy the links to these images to use in code. You may what to use a url shortener to avoid incredibly ugly code.
These 8 images will be downloaded and assigned to the 8 corresponding UIImageViews.
The PickerView is there only to determine if your UI is locked up or not. It serves no other purpose.
All button logic that follows must be in an elegant and professional format. That means use loops to keep your code pretty.
The buttons function as follows
Clear
Sets all imageView images to nil effectively erasing the images from the app�s memory.
Download Regular
Downloads each image and places it into a UIImageView without the use of Queues or threading of any kind.
Should behave similarly to our Regular Download from lecture.
Download Serial
Downloads each image and places it into a UIImageView using a custom serial queue.
Should behave similarly to our Serial Download from lecture.
Download Concurrent
Downloads each image and places it into a UIImageView using the high priority global concurrent queue.
Should behave similarly to our Concurrent Download from lecture.
Final turn-in for this coding exercise should be named (lastname)(firstname)CE10.zip

Example �DoeJaneCE10.zip�
Below is a simulator running the sample application.