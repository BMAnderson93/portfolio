//
//  ViewController.swift
//  AndersonBrandonCE10
//
//  Created by Brandon Anderson on 5/21/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let img1 = "https://bit.ly/2IDxBiF"
    let img2 = "https://bit.ly/2s7kzyP"
    let img3 = "https://bit.ly/2wZcJg5"
    let img4 = "https://bit.ly/2ICDPen"
    let img5 = "https://bit.ly/2x3ndv6"
    let img6 = "https://bit.ly/2IDyXtL"
    let img7 = "https://bit.ly/2IBfKbZ"
    let img8 = "https://bit.ly/2GDUBrL"
    @IBOutlet weak var imgV1: UIImageView!
    @IBOutlet weak var imgV2: UIImageView!
    @IBOutlet weak var imgV3: UIImageView!
    @IBOutlet weak var imgV4: UIImageView!
    @IBOutlet weak var imgV5: UIImageView!
    @IBOutlet weak var imgV6: UIImageView!
    @IBOutlet weak var imgV7: UIImageView!
    @IBOutlet weak var imgV8: UIImageView!
    
    var imgLinks = [String]()
    var images = [UIImageView]()
    var test = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        imgLinks = [img1,img2,img3,img4,img5,img6,img7,img8,]
        images = [imgV1, imgV2, imgV3, imgV4, imgV5, imgV6, imgV7, imgV8]
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Clears all the images
    @IBAction func clear () {
        for image in images {
            image.image = nil
        }
    }
    //Downloads all the images without a que (will freeze)
    @IBAction func regular (){
        var index = 0
        for _ in images {
            DownloadImage(url: imgLinks[index], index: index)
            index += 1
        }
    }
    //Downloads all the images 1 at a time, wont freeze
    @IBAction func Serial (){
        let serialQueue1 = DispatchQueue(label: "MySerialQueue1", qos: .userInitiated)
        serialQueue1.async {
            var index = 0
            for _ in self.images {
                self.DownloadImage(url: self.imgLinks[index], index: index)
                index += 1
            }
        }
    }
    //Downloads all the images at the same time, wont freeze
    @IBAction func Concurrent() {
        let concurrentQueue = DispatchQueue(label: "MyConcurrentQueue", qos: .userInitiated, attributes: .concurrent)
        
        for i in 0...7 {
            concurrentQueue.async {
                
                self.DownloadImage(url: self.imgLinks[i], index: i)
                
                
            }
            
        }
        
        
    }


}

