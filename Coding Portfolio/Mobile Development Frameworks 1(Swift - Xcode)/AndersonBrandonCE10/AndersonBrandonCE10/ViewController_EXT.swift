//
//  ViewController_EXT.swift
//  AndersonBrandonCE10
//
//  Created by Brandon Anderson on 5/21/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation
import UIKit

extension ViewController {
    //Downloads the image and uploads the main image
    func DownloadImage(url : String, index : Int){
        if let data = try? Data(contentsOf: URL(string:url)!){
            
            
        
        DispatchQueue.main.async {
             self.images[index].image = UIImage(data: data)
        }
        }
        
        
    }
}
