﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AndersonBrandon_Assignment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // Used to add the item to the chosen list
        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Creates my new item
            Item newItem = new Item();
            newItem.name = txtName.Text;
            if (rdoHave.Checked)
            {

                lbHave.Items.Add(newItem);
            }
            else
            {

                lbNeed.Items.Add(newItem);
            }
            // Resets the form
            Clear();

        }

        // Used for resetting the form.
        private void Clear()
        {
            txtName.Clear();
            rdoHave.Checked = true;
        }
        // Used to move items from have to need
        private void btnMoveToNeed_Click(object sender, EventArgs e)
        {
            if(lbHave.SelectedItem != null)
            {
                lbNeed.Items.Add(lbHave.SelectedItem);
                lbHave.Items.Remove(lbHave.SelectedItem);
                lbHave.ClearSelected();
                lbNeed.ClearSelected();
            }
        }
        // Used to move items from need to have
        private void btnMoveToHave_Click(object sender, EventArgs e)
        {
            if (lbNeed.SelectedItem != null)
            {
                lbHave.Items.Add(lbNeed.SelectedItem);
                lbNeed.Items.Remove(lbNeed.SelectedItem);
                lbHave.ClearSelected();
                lbNeed.ClearSelected();
            }
        }
        // Used to Remove item from the list
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(lbHave.SelectedItems.Count != 0)
            {
                lbHave.Items.Remove(lbHave.SelectedItem);
            }
            else if(lbNeed.SelectedItems.Count != 0)
            {
                lbNeed.Items.Remove(lbNeed.SelectedItem);
            }
        }
        // Saves the lists to a txt file
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    using (StreamWriter sw = new StreamWriter(myStream))
                    {
                        sw.WriteLine("Have List");
                        foreach (Item item in lbHave.Items)
                        {
                            sw.WriteLine(item.ToString());

                        }
                        sw.WriteLine("Need List");
                        foreach (Item item in lbNeed.Items)
                        {
                            sw.WriteLine(item.ToString());

                        }
                    }
                    myStream.Close();
                }
            }
        }
        // Exits the application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
