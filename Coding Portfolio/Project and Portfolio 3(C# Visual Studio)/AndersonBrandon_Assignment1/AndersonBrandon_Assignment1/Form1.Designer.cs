﻿namespace AndersonBrandon_Assignment1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbNeed = new System.Windows.Forms.ListBox();
            this.lbHave = new System.Windows.Forms.ListBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnMoveToHave = new System.Windows.Forms.Button();
            this.btnMoveToNeed = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.rdoNeed = new System.Windows.Forms.RadioButton();
            this.rdoHave = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1132, 42);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 38);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbNeed);
            this.groupBox1.Controls.Add(this.lbHave);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.btnMoveToHave);
            this.groupBox1.Controls.Add(this.btnMoveToNeed);
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.rdoNeed);
            this.groupBox1.Controls.Add(this.rdoHave);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(27, 62);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1093, 578);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Grocery List";
            // 
            // lbNeed
            // 
            this.lbNeed.FormattingEnabled = true;
            this.lbNeed.ItemHeight = 25;
            this.lbNeed.Location = new System.Drawing.Point(769, 118);
            this.lbNeed.Name = "lbNeed";
            this.lbNeed.Size = new System.Drawing.Size(308, 479);
            this.lbNeed.TabIndex = 14;
            // 
            // lbHave
            // 
            this.lbHave.FormattingEnabled = true;
            this.lbHave.ItemHeight = 25;
            this.lbHave.Location = new System.Drawing.Point(407, 118);
            this.lbHave.Name = "lbHave";
            this.lbHave.Size = new System.Drawing.Size(308, 479);
            this.lbHave.TabIndex = 13;
            // 
            // btnDelete
            // 
            this.btnDelete.ForeColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(721, 278);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(42, 43);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "X";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnMoveToHave
            // 
            this.btnMoveToHave.Location = new System.Drawing.Point(721, 220);
            this.btnMoveToHave.Name = "btnMoveToHave";
            this.btnMoveToHave.Size = new System.Drawing.Size(42, 43);
            this.btnMoveToHave.TabIndex = 11;
            this.btnMoveToHave.Text = "<";
            this.btnMoveToHave.UseVisualStyleBackColor = true;
            this.btnMoveToHave.Click += new System.EventHandler(this.btnMoveToHave_Click);
            // 
            // btnMoveToNeed
            // 
            this.btnMoveToNeed.Location = new System.Drawing.Point(721, 160);
            this.btnMoveToNeed.Name = "btnMoveToNeed";
            this.btnMoveToNeed.Size = new System.Drawing.Size(42, 43);
            this.btnMoveToNeed.TabIndex = 2;
            this.btnMoveToNeed.Text = ">";
            this.btnMoveToNeed.UseVisualStyleBackColor = true;
            this.btnMoveToNeed.Click += new System.EventHandler(this.btnMoveToNeed_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(64, 260);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(236, 96);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 25);
            this.label3.TabIndex = 9;
            this.label3.Text = "Item Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(40, 118);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(312, 31);
            this.txtName.TabIndex = 8;
            // 
            // rdoNeed
            // 
            this.rdoNeed.AutoSize = true;
            this.rdoNeed.Location = new System.Drawing.Point(258, 174);
            this.rdoNeed.Name = "rdoNeed";
            this.rdoNeed.Size = new System.Drawing.Size(94, 29);
            this.rdoNeed.TabIndex = 7;
            this.rdoNeed.Text = "Need";
            this.rdoNeed.UseVisualStyleBackColor = true;
            // 
            // rdoHave
            // 
            this.rdoHave.AutoSize = true;
            this.rdoHave.Checked = true;
            this.rdoHave.Location = new System.Drawing.Point(40, 174);
            this.rdoHave.Name = "rdoHave";
            this.rdoHave.Size = new System.Drawing.Size(93, 29);
            this.rdoHave.TabIndex = 6;
            this.rdoHave.TabStop = true;
            this.rdoHave.Text = "Have";
            this.rdoHave.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(769, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Need";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(402, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Have";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 665);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnMoveToHave;
        private System.Windows.Forms.Button btnMoveToNeed;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.RadioButton rdoNeed;
        private System.Windows.Forms.RadioButton rdoHave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbNeed;
        private System.Windows.Forms.ListBox lbHave;
    }
}

