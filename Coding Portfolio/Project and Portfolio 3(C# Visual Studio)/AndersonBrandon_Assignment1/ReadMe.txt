Setup:
At this point in the degree, some of you may have become comfortable with C#, and some of you may still feel uneasy about writing code.  Many great software designers and developers did not start off being great at their craft, and most would tell you they had to put in a lot of time and effort to become great.  This is no different than a pro athlete, an accomplished pianist, or a great parent.  All have one thing in common, and that is, they all had to put in time and effort to become great at what they do.  Since your goal is to become a good and hopefully great Mobile Developers, you will need to put in the time and effort to become good, and even more time and effort to become great at what you are striving to be.  There are no secrets, REPETITION IS KEY in becoming a great developer.

In this assignment, you will be utilizing the C# language. This will help to fine tune and enhance the development skills you learned in SDI, ASD and VFW. We will be using Visual Studio to code a series of methods to accomplish common tasks. If you do not have Visual Studio installed, please reach out as early as possible so we can walk you through the process.

You will also use the BitBucket repo that you set up in DVP 1.  The link will be uploaded in a separate activity.

 

INSTRUCTIONS:
The purpose of this assignment is to give you more practice and repetition with the coding skills you learned in SDI, ASD and VFW and enhance them, as well as practice committing code to a repo.  

Failure to include any of the below elements will result in a grade reduction.

 

Exercise 1: GroceryList or PartyList

Create a grocery list or party list, your choice!

 

This list must provide the following:

The list should contain 2 columns; HAVE and NEED.
The ability to enter items into a form that in turn sends that data back to the appropriate list (you will need to use a custom event handler). 
The ability for the user to move an item from HAVE to NEED and from NEED to HAVE.
The ability to delete an individual item from the list.
The ability for the user to save the list locally.
 