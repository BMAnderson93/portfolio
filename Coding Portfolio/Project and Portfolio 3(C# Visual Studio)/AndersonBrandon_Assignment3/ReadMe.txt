Exercise 3: TicTacToe
In this exercise, you will be given a visual framework into which you will inject the logic that will allow two players to complete a game of Tic Tac Toe.  The buttons, menuStrip, and toolStrip have already been added to the Form.  Your job is to take this framework and create a working game.

Logic Required:

File Menu

The File menu contains the following commands:  Load Game, Save Game, and Exit.  You are required to add the logic to allow the user to save and load a game as XML data.  A mechanism should exist that will not allow the user to load anything except TicTacToe data.  The Exit command should gracefully quit the application

All menu commands should have appropriate shortcut keys (CTRL+L, CTRL+S, CTRL+Q)

View Menu

The View menu contains two selections:  Blue and Red.  By default, the Blue command should be selected.  Selecting a different color should change which command is checked and should change the color of the images on any buttons already selected during play.  (That is, any blue X�s and O�s should become red X�s and O�s, the Blue command should be unchecked and the Red command should be checked if the user selects Red on the View menu and vice versa.)

New Game button:

Clicking the New Game button on the toolStrip should allow the players to begin a new game.  It should clear the board without reloading the Form.

Game play:

The user should be able to pick the starter (X or O) from the Select menu.  This means once the user selects X or O, clicking a button will assign the choice to the first button selected.

Once a location on the board is filled, it cannot be overridden.

The turns must alternate; that is, an X or an O cannot be played consecutively.  It must be X, O, X, O or O, X, O, X until the game is complete.

The game will auto-check the board after each play to determine when a user has a legal three-in-a-row and will display a messageBox to indicate who won (X or O).  For example, it might read, �Congratulations X!  You won!�.

The game should also be able to tell when all buttons have been selected and if a stalemate (draw) exists.

