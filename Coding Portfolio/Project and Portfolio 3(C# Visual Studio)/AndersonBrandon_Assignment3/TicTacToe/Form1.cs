﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace TicTacToe
{
    public partial class frmTicTacToe : Form
    {
        // Creates variables used for checking if the game is over, saving, and loading the game
        int turnCount = 0;
        int r1c1;
        int r1c2;
        int r1c3;
        int r2c1;
        int r2c2;
        int r2c3;
        int r3c1;
        int r3c2;
        int r3c3;


        bool xGoesFirst;
        bool canStart;
        // Creates a list of all my buttons
        List<Button> allButtons = new List<Button>();

        public frmTicTacToe()
        {
            foreach (Control btn in Controls)
            {
                if (btn.GetType() == typeof(Button))
                    allButtons.Add((Button)btn);

            }
        
            InitializeComponent();
        }
        // Selects X as the starting move
        private void xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xToolStripMenuItem.Checked = true;
            oToolStripMenuItem.Checked = false;
            xGoesFirst = true;
            canStart = true;
        }
        // Selects o as the starting move
        private void oToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xToolStripMenuItem.Checked = false;
            oToolStripMenuItem.Checked = true;
            xGoesFirst = false;
            canStart = true;
        }
        // Takes the selected space
        private void r1c1button_Click(object sender, EventArgs e)
        {
            MakeMove(r1c1button);
        }
        // Code for actually making the move
        private void MakeMove(Button selectedButton)
        {
           
        
            if (canStart && selectedButton.ImageIndex != 0 && selectedButton.ImageIndex != 1)
            {
                if (turnCount == 0)
                {
                    selectTool.Enabled = false;
                }
                switch (xGoesFirst)
                {
                    case true:
                        {
                            if(turnCount % 2 == 0)
                            {
                                selectedButton.ImageIndex = 1;
                                CheckForWinner(1);
                            }
                            else
                            {
                                selectedButton.ImageIndex = 0;
                                CheckForWinner(0);
                            }
                            turnCount++;
                            break;
                        }
                    case false:
                        {
                            if (turnCount % 2 == 0)
                            {
                                selectedButton.ImageIndex = 0;
                                CheckForWinner(0);
                            }
                            else
                            {
                                selectedButton.ImageIndex = 1;
                                CheckForWinner(1);
                            }
                            turnCount++;
                            break;
                        }

                }
                if(turnCount == 9 && canStart == true)
                {
                    MessageBox.Show("Game over! Cat wins");
                    canStart = false;
                }
            }
        }
        // Code for checking each turn for a winner
        private void CheckForWinner(int player)
        {
            string playerName = "";
            switch (player)
            {
                case 0:
                    {
                        playerName = "O";
                        break;
                    }
                case 1:
                    {
                        playerName = "X";
                        break;
                    }
            }

            if(r1c1button.ImageIndex == player && r1c2button.ImageIndex == player && r1c3button.ImageIndex == player)
            {
                PrintWinner(playerName);
            }
            else if(r2c1button.ImageIndex == player && r2c2button.ImageIndex == player && r2c3button.ImageIndex == player)
            {
                PrintWinner(playerName);
            }
            else if (r3c1button.ImageIndex == player && r3c2button.ImageIndex == player && r3c3button.ImageIndex == player)
            {
                PrintWinner(playerName);
            }
            else if (r1c1button.ImageIndex == player && r2c1button.ImageIndex == player && r3c1button.ImageIndex == player)
            {
                PrintWinner(playerName);
            }
            else if (r1c2button.ImageIndex == player && r2c2button.ImageIndex == player && r3c2button.ImageIndex == player)
            {
                PrintWinner(playerName);
            }
            else if (r1c3button.ImageIndex == player && r2c3button.ImageIndex == player && r3c3button.ImageIndex == player)
            {
                PrintWinner(playerName);
            }
            else if (r1c1button.ImageIndex == player && r2c2button.ImageIndex == player && r3c3button.ImageIndex == player)
            {
                PrintWinner(playerName);
            }
            else if (r1c3button.ImageIndex == player && r2c2button.ImageIndex == player && r3c1button.ImageIndex == player)
            {
                PrintWinner(playerName);
            }
        }
        // Prints out the games winner if its not a tie
        private void PrintWinner(string winner)
        {
            MessageBox.Show(winner + " wins this round!");
            canStart = false;
        }
        // Takes the selected space
        private void r1c2button_Click(object sender, EventArgs e)
        {
            MakeMove(r1c2button);
        }
        // Takes the selected space
        private void r1c3button_Click(object sender, EventArgs e)
        {
            MakeMove(r1c3button);
        }
        // Takes the selected space
        private void r2c1button_Click(object sender, EventArgs e)
        {
            MakeMove(r2c1button);
        }
        // Takes the selected space
        private void r2c2button_Click(object sender, EventArgs e)
        {
            MakeMove(r2c2button);
        }
        // Takes the selected space
        private void r2c3button_Click(object sender, EventArgs e)
        {
            MakeMove(r2c3button);
        }
        // Takes the selected space
        private void r3c1button_Click(object sender, EventArgs e)
        {
            MakeMove(r3c1button);
        }
        // Takes the selected space
        private void r3c2button_Click(object sender, EventArgs e)
        {
            MakeMove(r3c2button);
        }
        // Takes the selected space
        private void r3c3button_Click(object sender, EventArgs e)
        {
            MakeMove(r3c3button);
        }
        // Changes the board to blue
        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            allButtons.Clear();
            blueToolStripMenuItem.Checked = true;
            redToolStripMenuItem.Checked = false;

            foreach (Control btn in Controls)
            {
                if (btn.GetType() == typeof(Button))
                    allButtons.Add((Button)btn);
                   
            }
            foreach(Button btn in allButtons)
            {
                btn.ImageList = blueImages;
            }
        }
        // Changes the board to red
        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            allButtons.Clear();
            blueToolStripMenuItem.Checked = false;
            redToolStripMenuItem.Checked = true;

            foreach (Control btn in Controls)
            {
                if (btn.GetType() == typeof(Button))
                    allButtons.Add((Button)btn);

            }
            foreach (Button btn in allButtons)
            {
                btn.ImageList = redImages;
            }
        }
        // Starts a new game
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            allButtons.Clear();
          

            foreach (Control btn in Controls)
            {
                if (btn.GetType() == typeof(Button))
                    allButtons.Add((Button)btn);

            }
            foreach (Button btn in allButtons)
            {
                btn.ResetText();
                btn.ImageIndex = -1;
                canStart = false;
                selectTool.Enabled = true;
                xToolStripMenuItem.Checked = false;
                oToolStripMenuItem.Checked = false;
                turnCount = 0;
            }
            
        }
        // Exits the application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        // Saves the game as a xml file to be reloaded later
        private void saveGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.DefaultExt = "xml";
            saveFileDialog1.Filter = "XML Files(*.xml)|*.xml";
            saveFileDialog1.FilterIndex = 0;
            

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                XmlWriterSettings settings = new XmlWriterSettings();

                settings.ConformanceLevel = ConformanceLevel.Document;

                settings.Indent = true;
                using (XmlWriter writer = XmlWriter.Create(saveFileDialog1.FileName, settings))
                {
                    AssignNames();
                    writer.WriteStartElement("TicTacToeSave");
                    writer.WriteElementString("r1c1", r1c1.ToString());
                    writer.WriteElementString("r1c2", r1c2.ToString());
                    writer.WriteElementString("r1c3", r1c3.ToString());
                    writer.WriteElementString("r2c1", r2c1.ToString());
                    writer.WriteElementString("r2c2", r2c2.ToString());
                    writer.WriteElementString("r2c3", r2c3.ToString());
                    writer.WriteElementString("r3c1", r3c1.ToString());
                    writer.WriteElementString("r3c2", r3c2.ToString());
                    writer.WriteElementString("r3c3", r3c3.ToString());
                    if (selectTool.Enabled == false)
                    {
                        writer.WriteElementString("EnableSelect", "false");

                    }
                    else
                    {
                        writer.WriteElementString("EnableSelect", "true");
                    }
                    if (xGoesFirst)
                    {
                        writer.WriteElementString("WhoWentFirst", "x");
                    }
                    else
                    {
                        writer.WriteElementString("WhoWentFirst", "o");
                    }
                    if (canStart)
                    {
                        writer.WriteElementString("canStart", "true");
                    }
                    else
                    {
                        writer.WriteElementString("canStart", "false");
                    }
                    if(blueToolStripMenuItem.Checked == true)
                    {
                        writer.WriteElementString("whichColor", "blue");
                    }
                    else
                    {
                        writer.WriteElementString("whichColor", "red");
                    }
                    writer.WriteElementString("TurnCount", turnCount.ToString());
                    writer.WriteEndElement();
                }
                    
            }
        }
        // assigns the variables their names based on what image is in that button, used for saving.
        private void AssignNames()
        {
            r1c1 = r1c1button.ImageIndex;
            r1c2 = r1c2button.ImageIndex;
            r1c3 = r1c3button.ImageIndex;
            r2c1 = r2c1button.ImageIndex;
            r2c2 = r2c2button.ImageIndex;
            r2c3 = r2c3button.ImageIndex;
            r3c1 = r3c1button.ImageIndex;
            r3c2 = r3c2button.ImageIndex;
            r3c3 = r3c3button.ImageIndex;
        }
        // Loads the saved game and insures that it is the correct file type and saved game
        private void loadGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.DefaultExt = "xml";
            openFileDialog1.Filter = "XML Files(*.xml)|*.xml";
            openFileDialog1.FilterIndex = 0;
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                XmlReaderSettings settings = new XmlReaderSettings();

                settings.ConformanceLevel = ConformanceLevel.Document;

                settings.IgnoreComments = true;
                settings.IgnoreWhitespace = true;
                try
                {
                    using (XmlReader reader = XmlReader.Create(openFileDialog1.FileName, settings))
                    {

                        reader.MoveToContent();
                        if (reader.Name != "TicTacToeSave")
                        {
                            MessageBox.Show("You are not using a saved game of tic tac toe!");

                        }
                        else
                        {

                            while (reader.Read())
                            {

                                if (reader.Name == "r1c1")
                                {
                                    r1c1button.ImageIndex = reader.ReadElementContentAsInt();
                                }
                                if (reader.Name == "r1c2")
                                {
                                    r1c2button.ImageIndex = reader.ReadElementContentAsInt();
                                }
                                if (reader.Name == "r1c3")
                                {
                                    r1c3button.ImageIndex = reader.ReadElementContentAsInt();
                                }
                                if (reader.Name == "r2c1")
                                {
                                    r2c1button.ImageIndex = reader.ReadElementContentAsInt();
                                }
                                if (reader.Name == "r2c2")
                                {
                                    r2c2button.ImageIndex = reader.ReadElementContentAsInt();
                                }
                                if (reader.Name == "r2c3")
                                {
                                    r2c3button.ImageIndex = reader.ReadElementContentAsInt();
                                }
                                if (reader.Name == "r3c1")
                                {
                                    r3c1button.ImageIndex = reader.ReadElementContentAsInt();
                                }
                                if (reader.Name == "r3c2")
                                {
                                    r3c2button.ImageIndex = reader.ReadElementContentAsInt();
                                }
                                if (reader.Name == "r3c3")
                                {
                                    r3c3button.ImageIndex = reader.ReadElementContentAsInt();
                                }
                                if (reader.Name == "EnableSelect")
                                {
                                    selectTool.Enabled = reader.ReadElementContentAsBoolean();
                                }
                                if (reader.Name == "WhoWentFirst")
                                {
                                    if (reader.ReadElementContentAsString() == "x")
                                    {
                                        xGoesFirst = true;
                                    }
                                    else
                                    {
                                        xGoesFirst = false;
                                    }
                                }
                                if (reader.Name == "canStart")
                                {
                                    canStart = reader.ReadElementContentAsBoolean();
                                }
                                if (reader.Name == "whichColor")
                                {
                                    if (reader.ReadElementContentAsString() == "red")
                                    {
                                        allButtons.Clear();
                                        blueToolStripMenuItem.Checked = false;
                                        redToolStripMenuItem.Checked = true;

                                        foreach (Control btn in Controls)
                                        {
                                            if (btn.GetType() == typeof(Button))
                                                allButtons.Add((Button)btn);

                                        }
                                        foreach (Button btn in allButtons)
                                        {
                                            btn.ImageList = redImages;
                                        }

                                    }
                                    else
                                    {
                                        allButtons.Clear();
                                        blueToolStripMenuItem.Checked = true;
                                        redToolStripMenuItem.Checked = false;

                                        foreach (Control btn in Controls)
                                        {
                                            if (btn.GetType() == typeof(Button))
                                                allButtons.Add((Button)btn);

                                        }
                                        foreach (Button btn in allButtons)
                                        {
                                            btn.ImageList = blueImages;
                                        }
                                    }
                                }
                                if (reader.Name == "TurnCount")
                                {
                                    turnCount = reader.ReadElementContentAsInt();
                                }


                            }
                        }
                    }

                }
                catch(Exception ex)
                {
                    MessageBox.Show("You must select a tic tac toe save file!");
                }

            }

        }
    }
}
