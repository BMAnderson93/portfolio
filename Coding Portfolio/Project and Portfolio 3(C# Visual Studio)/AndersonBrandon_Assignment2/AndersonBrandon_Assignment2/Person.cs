﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_Assignment2
{
    public class Person
    {
        // Creates all the variables a person will need for this contact form
        public string fName;
        public string lName;
        public string email;
        public string email2;
        public string number;

        // Changes what string is being wrote to the list view
        public override string ToString()
        {
            return fName + " " + lName;
        }
    }
}
