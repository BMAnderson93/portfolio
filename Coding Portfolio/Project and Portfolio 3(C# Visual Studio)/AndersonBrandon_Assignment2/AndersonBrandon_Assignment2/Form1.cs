﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AndersonBrandon_Assignment2
{
    public partial class Form1 : Form
    {
        // Event Handler for sending the contact to the other form
        public event EventHandler<SendContact> GetContact;

        // Assigns the argument for the new event args
        public class SendContact : EventArgs
        {
            public Person newPerson = new Person();

            public SendContact(Person createdPerson)
            {
                this.newPerson = createdPerson;
            }
        }
        public Form1()
        {
            InitializeComponent();
        }

        // Creates the new form and subcribes the custom events
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lvContacts.SelectedItems.Clear();
            Contact newForm = new Contact();
            newForm.SendContact += NewForm_SendContact;
            GetContact += newForm.PopForm;

            newForm.ShowDialog();
            
        }

        // used for subcribing the custom event from the contact form
        private void NewForm_SendContact(object sender, Contact.CreateContact e)
        {
            ListViewItem newItem = new ListViewItem();
            newItem.Text = e.newPerson.ToString();
            newItem.ImageIndex = 0;
            newItem.Tag = e.newPerson;

            if(lvContacts.SelectedItems.Count == 1)
            {
                lvContacts.SelectedItems[0].Remove();
            }           
            lvContacts.Items.Add(newItem);
            lvContacts.Sort();
        }
        // Assigns the contact to the custom event arguments and sends it to the contact form to open and populate it
        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if(lvContacts.SelectedItems[0] != null)
            {
                Contact newForm = new Contact();
                newForm.SendContact += NewForm_SendContact;
                GetContact += newForm.PopForm;
                GetContact(this, new SendContact((Person)lvContacts.SelectedItems[0].Tag));
                newForm.ShowDialog();
            }
        }
        // Removes the selected contact
        private void button1_Click(object sender, EventArgs e)
        {
            if(lvContacts.SelectedItems.Count == 1)
            {
                lvContacts.SelectedItems[0].Remove();
            }
        }
        // Changes the icon view to large
        private void largeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolLarge.Checked = true;
            toolSmall.Checked = false;
       
            lvContacts.View = View.LargeIcon;
        }
        // Changes the icon view to small
        private void toolSmall_Click(object sender, EventArgs e)
        {
            toolLarge.Checked = false;
            toolSmall.Checked = true;

            lvContacts.View = View.SmallIcon;
        }
        // Exits the application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
