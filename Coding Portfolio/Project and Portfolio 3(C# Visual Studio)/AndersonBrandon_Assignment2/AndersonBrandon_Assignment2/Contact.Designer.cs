﻿namespace AndersonBrandon_Assignment2
{
    partial class Contact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.WarningEmail2 = new System.Windows.Forms.Label();
            this.WarningEmail1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEmail2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.WarningEmail = new System.Windows.Forms.Label();
            this.WarningNumber = new System.Windows.Forms.Label();
            this.WarningLastName = new System.Windows.Forms.Label();
            this.lblNameWarning = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.WarningEmail2);
            this.groupBox1.Controls.Add(this.WarningEmail1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtEmail2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.WarningEmail);
            this.groupBox1.Controls.Add(this.WarningNumber);
            this.groupBox1.Controls.Add(this.WarningLastName);
            this.groupBox1.Controls.Add(this.lblNameWarning);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.txtNumber);
            this.groupBox1.Controls.Add(this.txtLName);
            this.groupBox1.Controls.Add(this.txtFName);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(834, 510);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Contact Information";
            // 
            // WarningEmail2
            // 
            this.WarningEmail2.AutoSize = true;
            this.WarningEmail2.Location = new System.Drawing.Point(499, 353);
            this.WarningEmail2.Name = "WarningEmail2";
            this.WarningEmail2.Size = new System.Drawing.Size(0, 25);
            this.WarningEmail2.TabIndex = 17;
            // 
            // WarningEmail1
            // 
            this.WarningEmail1.AutoSize = true;
            this.WarningEmail1.Location = new System.Drawing.Point(219, 353);
            this.WarningEmail1.Name = "WarningEmail1";
            this.WarningEmail1.Size = new System.Drawing.Size(0, 25);
            this.WarningEmail1.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(610, 307);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 25);
            this.label6.TabIndex = 15;
            this.label6.Text = ".com";
            // 
            // txtEmail2
            // 
            this.txtEmail2.Location = new System.Drawing.Point(494, 304);
            this.txtEmail2.Name = "txtEmail2";
            this.txtEmail2.Size = new System.Drawing.Size(108, 31);
            this.txtEmail2.TabIndex = 4;
            this.txtEmail2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(455, 307);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 25);
            this.label5.TabIndex = 13;
            this.label5.Text = "@";
            // 
            // WarningEmail
            // 
            this.WarningEmail.AutoSize = true;
            this.WarningEmail.Location = new System.Drawing.Point(219, 338);
            this.WarningEmail.Name = "WarningEmail";
            this.WarningEmail.Size = new System.Drawing.Size(0, 25);
            this.WarningEmail.TabIndex = 12;
            // 
            // WarningNumber
            // 
            this.WarningNumber.AutoSize = true;
            this.WarningNumber.Location = new System.Drawing.Point(218, 256);
            this.WarningNumber.Name = "WarningNumber";
            this.WarningNumber.Size = new System.Drawing.Size(0, 25);
            this.WarningNumber.TabIndex = 11;
            // 
            // WarningLastName
            // 
            this.WarningLastName.AutoSize = true;
            this.WarningLastName.Location = new System.Drawing.Point(218, 174);
            this.WarningLastName.Name = "WarningLastName";
            this.WarningLastName.Size = new System.Drawing.Size(0, 25);
            this.WarningLastName.TabIndex = 10;
            // 
            // lblNameWarning
            // 
            this.lblNameWarning.AutoSize = true;
            this.lblNameWarning.Location = new System.Drawing.Point(218, 113);
            this.lblNameWarning.Name = "lblNameWarning";
            this.lblNameWarning.Size = new System.Drawing.Size(0, 25);
            this.lblNameWarning.TabIndex = 9;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(329, 405);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(207, 87);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 307);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 25);
            this.label4.TabIndex = 7;
            this.label4.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 25);
            this.label3.TabIndex = 6;
            this.label3.Text = "Phone Number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Last Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "First Name";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(223, 304);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(226, 31);
            this.txtEmail.TabIndex = 3;
            this.txtEmail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail_KeyPress);
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(223, 222);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(414, 31);
            this.txtNumber.TabIndex = 2;
            this.txtNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber_KeyPress);
            // 
            // txtLName
            // 
            this.txtLName.Location = new System.Drawing.Point(223, 140);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(414, 31);
            this.txtLName.TabIndex = 1;
            this.txtLName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLName_KeyPress);
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(223, 67);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(414, 31);
            this.txtFName.TabIndex = 0;
            this.txtFName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFName_KeyPress);
            // 
            // Contact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 547);
            this.Controls.Add(this.groupBox1);
            this.Name = "Contact";
            this.Text = "Contact";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label lblNameWarning;
        private System.Windows.Forms.Label WarningEmail;
        private System.Windows.Forms.Label WarningNumber;
        private System.Windows.Forms.Label WarningLastName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEmail2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label WarningEmail1;
        private System.Windows.Forms.Label WarningEmail2;
    }
}