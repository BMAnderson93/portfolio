﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AndersonBrandon_Assignment2
{
    public partial class Contact : Form
    {
     // Creates the custom event for sending the created contact back to the main form   
        public event EventHandler<CreateContact> SendContact;
        public Contact()
        {
            InitializeComponent();
        }
        // Creates the arguments to be send with the custom event
        public class CreateContact : EventArgs
        {
            public Person newPerson = new Person();

            public CreateContact(Person madePerson)
            {
                this.newPerson = madePerson;
            }
        }

       
        // Validates that each part of the form has the required information and saves if it does
        private void btnSave_Click(object sender, EventArgs e)
        {
            // Creates the timer variable for displaying error messages
            var t = new Timer();
            t.Interval = 3000;
            string blank = "";
            // Saves the form if every part of the form has its content
            if (txtEmail.Text != blank && txtEmail2.Text != blank && txtFName.Text != blank && txtLName.Text != blank && txtNumber.Text != blank)
            {
                Person newPerson = new Person();
                newPerson.lName = txtLName.Text;
                newPerson.fName = txtFName.Text;
                newPerson.number = txtNumber.Text;
                newPerson.email = txtEmail.Text;
                newPerson.email2 = txtEmail2.Text;
                SendContact(this, new CreateContact(newPerson));
                Close();
            }
            // Displays error message
            else if( txtEmail.Text == blank)
            {
                WarningEmail1.Show();
                WarningEmail1.Text = "You can not leave this blank!";


               

                t.Tick += (s, stop) =>
                {
                    WarningEmail1.Hide();
                    t.Stop();
                };
                t.Start();
            }
            // Displays error message
            else if (txtEmail2.Text == blank)
            {
                WarningEmail2.Show();
                WarningEmail2.Text = "You can not leave this blank!";




                t.Tick += (s, stop) =>
                {
                    WarningEmail2.Hide();
                    t.Stop();
                };
                t.Start();
            }
            // Displays error message
            else if (txtFName.Text == blank)
            {
                lblNameWarning.Show();
                lblNameWarning.Text = "You can not leave this blank!";




                t.Tick += (s, stop) =>
                {
                    lblNameWarning.Hide();
                    t.Stop();
                };
                t.Start();
            }
            // Displays error message
            else if (txtLName.Text == blank)
            {
                WarningLastName.Show();
                WarningLastName.Text = "You can not leave this blank!";




                t.Tick += (s, stop) =>
                {
                    WarningLastName.Hide();
                    t.Stop();
                };
                t.Start();
            }
            // Displays error message
            else if (txtNumber.Text == blank)
            {
                WarningNumber.Show();
                WarningNumber.Text = "You can not leave this blank!";




                t.Tick += (s, stop) =>
                {
                    WarningNumber.Hide();
                    t.Stop();
                };
                t.Start();
            }

        }
        // Populates the form from the selected contact in the main form
        public void PopForm(object sender, Form1.SendContact e)
        {
            txtEmail.Text = e.newPerson.email;
            txtEmail2.Text = e.newPerson.email2;
            txtFName.Text = e.newPerson.fName;
            txtLName.Text = e.newPerson.lName;
            txtNumber.Text = e.newPerson.number;
            
        }
        // Checks to make sure the key being pressed is allowed in this part of the form.
        private void txtFName_KeyPress(object sender, KeyPressEventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;
            if (e.KeyChar == '\b')
            {
            }
            // Checks to make sure the key being pressed is allowed in this part of the form.
            else if (Char.IsDigit(e.KeyChar))
            {
                lblNameWarning.Show();
                lblNameWarning.Text = "You may not have a number in your name!";
             
                
                e.Handled = true ;

                t.Tick += (s, stop) =>
                {
                    lblNameWarning.Hide();
                    t.Stop();
                };
                t.Start();



            }
            // Checks to make sure the key being pressed is allowed in this part of the form.
            else if (Char.IsLetter(e.KeyChar) == false)
            {
                lblNameWarning.Show();
                lblNameWarning.Text = "You may not have any special characters in your name!";


              
                t.Tick += (s, stop) =>
                {
                    lblNameWarning.Hide();
                    t.Stop();
                };
                t.Start();
              
                e.Handled = true;
            }
        }
        // Checks to make sure the key being pressed is allowed in this part of the form.
        private void txtLName_KeyPress(object sender, KeyPressEventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;
            if (e.KeyChar == '\b')
            {
            }
            // Checks to make sure the key being pressed is allowed in this part of the form.
            else if (Char.IsDigit(e.KeyChar))
            {
                WarningLastName.Show();
                WarningLastName.Text = "You may not have a number in your name!";


                e.Handled = true;

                t.Tick += (s, stop) =>
                {
                    WarningLastName.Hide();
                    t.Stop();
                };
                t.Start();



            }
            // Checks to make sure the key being pressed is allowed in this part of the form.
            else if (Char.IsLetter(e.KeyChar) == false)
            {
                WarningLastName.Show();
                WarningLastName.Text = "You may not have any special characters in your name!";



                t.Tick += (s, stop) =>
                {
                    WarningLastName.Hide();
                    t.Stop();
                };
                t.Start();

                e.Handled = true;
            }
        }
        // Checks to make sure the key being pressed is allowed in this part of the form.
        private void txtNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;
            if (e.KeyChar == '\b')
            {
            }
            // Checks to make sure the key being pressed is allowed in this part of the form.
            else if (Char.IsLetter(e.KeyChar))
            {
                WarningNumber.Show();
                WarningNumber.Text = "You may not have a letter in your number!";


                e.Handled = true;

                t.Tick += (s, stop) =>
                {
                    WarningNumber.Hide();
                    t.Stop();
                };
                t.Start();



            }
            // Checks to make sure the key being pressed is allowed in this part of the form.
            else if (Char.IsNumber(e.KeyChar) == false)
            {
                WarningNumber.Show();
                WarningNumber.Text = "You may not have any special characters in your phone number!";



                t.Tick += (s, stop) =>
                {
                    WarningNumber.Hide();
                    t.Stop();
                };
                t.Start();

                e.Handled = true;
            }
        }
        // Checks to make sure the key being pressed is allowed in this part of the form.
        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;
            if (e.KeyChar == '\b')
            {

            }
            // Checks to make sure the key being pressed is allowed in this part of the form.
            else if (Char.IsLetterOrDigit(e.KeyChar) != true)
                {
                    WarningEmail1.Show();
                    WarningEmail1.Text = "You may not have any special characters in your email!";



                    t.Tick += (s, stop) =>
                    {
                        WarningEmail1.Hide();
                        t.Stop();
                    };
                    t.Start();

                    e.Handled = true;

                }

        }
        // Checks to make sure the key being pressed is allowed in this part of the form.
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;
            if (e.KeyChar == '\b')
            {

            }
            // Checks to make sure the key being pressed is allowed in this part of the form.
            else if (Char.IsDigit(e.KeyChar) == true)
                {
                    WarningEmail2.Show();
                    WarningEmail2.Text = "You may not have any numbers in this part of your email!";



                    t.Tick += (s, stop) =>
                    {
                        WarningEmail2.Hide();
                        t.Stop();
                    };
                    t.Start();

                    e.Handled = true;

                }
            // Checks to make sure the key being pressed is allowed in this part of the form.
            else if (Char.IsLetter(e.KeyChar) != true)
                {
                    WarningEmail2.Show();
                    WarningEmail2.Text = "You may not have a special character in your email!";


                    e.Handled = true;

                    t.Tick += (s, stop) =>
                    {
                        WarningEmail2.Hide();
                        t.Stop();
                    };
                    t.Start();



                }

        }
    }
}
