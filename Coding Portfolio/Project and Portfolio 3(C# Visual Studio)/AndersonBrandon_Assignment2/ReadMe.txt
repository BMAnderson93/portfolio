Setup:
Just as you did in last week�s code assignment, you will be utilizing the C# language. This will help to fine tune and enhance the development skills you learned in SDI, ASD and VFW. We will be using Visual Studio to code a series of methods to accomplish common tasks. If you do not have Visual Studio installed, please reach out as early as possible so we can walk you through the process.

You will also use the BitBucket repo that you set up in DVP 1. The link will be uploaded in a separate activity.

 

INSTRUCTIONS:
The purpose of this assignment is to give you more practice and repetition with the coding skills you learned in SDI, ASD and VFW and enhance them, as well as practice committing code to a repo.  

Failure to include any of the below elements will result in a grade reduction.

 

Exercise 2: ContactList

Create a contact list.

 

The contact list must provide the following:

The ability to enter the contact information into a form that in turn sends that data back to the contact list (you will need to use a custom event handler). 
The contact list should contain:


First Name
Last Name
Phone Number
Email address
Each of the fields should be validated. (A name should not contain numbers, a phone number should not contain letters and an email should be in the correct format.)  Be sure to include meaningful error messages; check out this resource for a refresher.
Assign an icon to each contact that is saved.
The ability for the user to change the icon size between large (32x32) and small (16x16).
The ability for the user to delete an individual contact.
The ability for the user to edit an individual contact.
The ability for the user to save the contact.