﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Brandon Anderson
 * 06/28/2017
 * MadLibs Style Story Creator*/
namespace Anderson_Brandon_MadLibs
{
    class Program
    {
        static void Main(string[] args)
        {
            // This line is to get the users first input.
            Console.WriteLine("Please enter any name.");
            // This line is to store the users first input.
            string name = Console.ReadLine();
            // This line is to get the users second input.
            Console.WriteLine("Name any body part.");
            // This line is to store the users second input
            string bodyPart = Console.ReadLine();
            // This line is to get the first number needed for the madlib.
            Console.WriteLine("Insert any whole number.");
            // This line is to store the users first number as a string to be converted later.
            string numberOne = Console.ReadLine();
            // This line is to get the users fourth input.
            Console.WriteLine("Whats your favorite place to hang out?");
            // This line is to store the users fourth input.
            string place = Console.ReadLine();
            // This line is to get the users fifth input.
            Console.WriteLine("Whats your favorite cereal?");
            // this line is to store the users fifth input.
            string cerial = Console.ReadLine();
            // This line is to get the second number needed for the madlib.
            Console.WriteLine("Give me another whole number.");
            // This line is to store the users second number as a string to be converted later.
            string numberTwo = Console.ReadLine();
            // This line is to get the final number needed for the madlib.
            Console.WriteLine("One last whole number please.");
            // This line is to store the users final number as a string to be converted later.
            string numberThree = Console.ReadLine();

            // This line is taking the first number stored from the user and converting it from a string to a integer.
            int numberOneParse = int.Parse(numberOne);
            // This line is taking the second number stored from the user and converting it from a string to a integer.
            int numberTwoParse = int.Parse(numberTwo);
            // This line is taking the final number stored from the user and converting it from a string to a integer.
            int numberThreeParse = int.Parse(numberThree);

            // This line is to thank the user for the information they provided and prompt them to press enter to see their story.
            Console.WriteLine("Thanks for that information! Press enter to see your story!");

            // This line is to pause the script and await the users input to display the story.
            Console.ReadKey();

            // This line is taking the users first input and combining it with the first sentence of the story.
            Console.WriteLine("There once was a man named "+ name + ".");
            // This line is taking the users second input and combining it with the second sentence of the story.
            Console.WriteLine("Who had a really large "+ bodyPart + ".");
            /* This line is taking the integer converted from the users first number input and combining it with the third 
             sentence of the story.*/
            Console.WriteLine("He never thought much of it, until one day he accidentally injured " + numberOneParse + " children.");
            // This line is taking the users fourth input and combining it with the fourth sentence of the story.
            Console.WriteLine("After the accident he went to "+ place+ " to have it checked out.");
            // This line is taking the users fifth input and combining it with the fifth sentence of the story.
            Console.WriteLine("He met with a man named "+ cerial + ", which he thought was rather strange.");
            /* This line is taking the integer converted from the users second number input and combining it with the sixth 
            sentence of the story.*/
            Console.WriteLine("The man told him to take "+ numberTwoParse + " pills until the size went down.");
            /* This line is taking the integer converted from the users final number input and combining it with the sixth 
            sentence of the story.*/
            Console.WriteLine("Unfortunately, " + numberThreeParse + " days later he passed away.");
            /* This is the final sentence of the story, taking the users first and second input and combining them with the final 
            sentence of the story.*/
            Console.WriteLine("On his tombstone was written, here lies " + name+ " the man with the largest "+ bodyPart + "." );


        }
    }
}
