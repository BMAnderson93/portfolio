﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_CountFish
{
    /*  Brandon Anderson
        07/18/2017
        Count The Fish
        */
    class Program
    {
        static void Main(string[] args)
        {
            // This line creates my array of fish.
            string[] fish = new string[] {"red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" };
            Start:
            // This line asks the user to pick the color of fish.
            Console.WriteLine("Please pick the color of fish, choose (1) for Red, (2) for Blue, (3) for Green, and (4) for Yellow");
            // This line stores the input.
            int fishNumber;
            // This line validates the users input.
            while (!int.TryParse(Console.ReadLine(), out fishNumber))
            {
                // This line asks the user for a valid input.
                Console.WriteLine("Please enter a valid number.");
            }
            // This line creates a variable of the total fish.
            int totalFish = 0;
            // This line is my first if statement.
            if (fishNumber == 1)
            {
                // This line creates a variable called Red.
                string colorFish = "Red";
                // This line goes through each element in the array.
                for (int i = 0; i < fish.Length; i++)

                {
                    // This line says if the element in the array is the same as the string red, then to do the line of code beneath it.
                    if (fish[i] == "red")
                    {
                        // This line says to add 1 to the total amount of fish.
                        totalFish++;
                    }
                }
                // This line prints the results.
                Console.WriteLine("In the fish tank there are "+totalFish+ " fish of the color "+colorFish+ ".");
            }
            // This line says if the previous if statement is false, then to check and see if this one is true.
            else if (fishNumber == 2)
            {

                // This line creates a variable 
                string colorFish = "Blue";
                // This line goes through each element in the array.
                for (int i = 0; i < fish.Length; i++)
                {
                    // This line says if the element in the array is the same as the string blue, then to do the line of code beneath it.
                    if (fish[i] == "blue")
                    {
                        // This line says to add 1 to the total amount of fish.
                        totalFish++;
                    }
                }
                // This line prints the results.
                Console.WriteLine("In the fish tank there are " + totalFish + " fish of the color " + colorFish + ".");
            }
            // This line says if the previous if statement is false, then to check and see if this one is true.
            else if (fishNumber == 3)
            {

                // This line creates a variable 
                string colorFish = "Green";
                // This line goes through each element in the array.
                for (int i = 0; i < fish.Length; i++)
                {
                    // This line says if the element in the array is the same as the string green, then to do the line of code beneath it.
                    if (fish[i] == "green")
                    {
                        // This line says to add 1 to the total amount of fish.
                        totalFish++;
                    }
                }
                // This line prints the results.
                Console.WriteLine("In the fish tank there are " + totalFish + " fish of the color " + colorFish + ".");
            }
            // This line says if the previous if statement is false, then to check and see if this one is true.
            else if (fishNumber == 4)
            {

                // This line creates a variable 
                string colorFish = "Yellow";
                // This line goes through each element in the array.
                for (int i = 0; i < fish.Length; i++)
                {
                    // This line says if the element in the array is the same as the string yellow, then to do the line of code beneath it.
                    if (fish[i] == "yellow")
                    {
                        // This line says to add 1 to the total amount of fish.
                        totalFish++;
                    }
                }
                // This line prints the results.
                Console.WriteLine("In the fish tank there are " + totalFish + " fish of the color " + colorFish + ".");
            }
            // This line says that if none of the previous if statements are true, then to start the code over again and ask for a valid response.
            else {
                Console.WriteLine("Please enter a valid response");
                goto Start;
            }
            /*• 
             * Test	that	each	color	choice, 1,2,3,	and	4, works	from	your	menu and	gives	you	the	correct	amount	of	fish.
                • Try	typing	in	the	number	6.		Does	your	code	ask	the	user	to	give	a	valid	choice	of	1-4? Yes, 
                my code validates the users response.
                • If	you	add	more	fish	to	the	tank,	does	your	code	still work? Yes, you can add any amount of fish as long as they
                are one of the colors listed.
                • User	Choice	– 1	for	Red
                o Results - “In	the	fish	tank	there	are	2 fish	of	the	color	Red. 
             
             
             
             */

        }
    }
}
