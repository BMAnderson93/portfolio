﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindErrorsFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            //Brandon Anderson
            //Date: 07/21/2017
            //Find The Errors In Functions

            //In this program we will be asking for 2 prices for the user
            //We will ask for the sales tax rate
            //Create a function that will return the price + sales tax
            //Create a function that will add the 2 prices with sales tax together for the total cost

            Console.WriteLine("Hello and welcome to our purchase calculator!\r\nWe will be asking you for 2 item prices and the sales tax rate.\r\n");

            Console.WriteLine("What is the cost of your first item?");
            decimal cost1 = Validate(Console.ReadLine(), "cost of your first item?");


            Console.WriteLine("What is the cost of your second item?");

            decimal cost2 = Validate(Console.ReadLine(), "cost of your second item?");


            Console.WriteLine("What is the sales tax rate %?");

            decimal salesTax = Validate(Console.ReadLine(), "sales tax rate in %?");

            Console.WriteLine("I have all the information I need.\r\nYour first item costs {0}.\r\nYour second item costs {1} and the sales tax is {2}%.", cost1, cost2, salesTax);

            decimal cost1WithTax = AddSalesTax(cost1, salesTax);
            decimal cost2WithTax = AddSalesTax(cost2, salesTax);
          

            decimal grandTotal = TotalCosts(cost1WithTax, cost2WithTax);
           

            Console.WriteLine("\r\n With tax your first item costs $" + cost1WithTax + ".\r\nYour second item costs $" + cost2WithTax + ".");
            Console.WriteLine("\r\nWhich makes the total for your bill $" + grandTotal+ ".");



        }

        public static decimal AddSalesTax(decimal price, decimal tax)
        {

            decimal totalWithTax = price + (price * (tax / 100));
            decimal cost1Rounded = Math.Round(totalWithTax, 2, MidpointRounding.AwayFromZero);
            return cost1Rounded;

        }

        public static decimal TotalCosts(decimal cost1, decimal cost2)
        {
            decimal total = cost1 + cost2;
            decimal grandTotalRounded = Math.Round(total, 2, MidpointRounding.AwayFromZero);
            return total;

        }
        public static decimal Validate(string input, string sentance)
        {
            decimal cost;
            while (!decimal.TryParse(input, out cost))
            {
                Console.WriteLine("Please only type in numbers!\r\nWhat is the " + sentance);

                input = Console.ReadLine();
                

            }
            return cost;
        }

    }
}
