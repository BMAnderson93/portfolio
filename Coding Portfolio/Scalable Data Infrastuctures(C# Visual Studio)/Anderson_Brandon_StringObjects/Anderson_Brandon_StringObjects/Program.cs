﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            
            // This line asks the user for a input to start the program.
            Console.WriteLine("Press enter to begin the program!");
            // This line stores the users input.
            string input = Console.ReadLine();
            // This line says while the input is "" to loop through the program.
            while (input == "")
            {
                Console.Clear();
                // This line asks the user for their email to validate.
                Console.WriteLine("Please enter your email");
                // This line stores the email in a variable.
                string email = Console.ReadLine();
                // This line runs the email through the checker.
                string results = Utilities.Checker(email);
                // This line prints the results.
                Console.WriteLine("The email address of " + email + " is a " + results + " email address!");
                // This line asks the user for a input to continue.
                Console.WriteLine("Press enter to go to the next problem.");
                // This line pauses the program.
                Console.ReadLine();
                // This line clears the console.
                Console.Clear();
                // This line asks the user for their list of items.
                Console.WriteLine("Please enter a list of items using any separator you'd like!");
                // This line stores the input.
                string firstSentence = Console.ReadLine();
                // This line asks what separator they used.
                Console.WriteLine("Please enter the separator you used!");
                // This line stores the input.
                string firstSepUsed = Console.ReadLine();
                // This line asks what separator they'd like to replace the old one with.
                Console.WriteLine("Please enter the new separator you'd like to use!");
                // This line stores the input
                string secondSepUsed = Console.ReadLine();
                // This line runs all of the users input through the custom function.
                string secondSentence = Utilities.Separator(firstSentence, firstSepUsed, secondSepUsed);
                // This line prints the results.
                Console.WriteLine("The original string of " + firstSentence + " with the new separator is " + secondSentence + ".");
                // This line asks the user if they'd like to start over.
                Console.WriteLine("Press enter to start over, or you may exit the client now.");
                // This line changes the variable input based on what the user inputs.
                input = Console.ReadLine();
                // This line clears the console.
                Console.Clear();
            }
            
        }
    }
}
/* Email Tests
o Email	– test@fullsail.com
 Results	- “The	email	address	of	test@fullsail.com	is	a	valid email	address.”
o Email	– test@full@sail.com
 Results	- “The	email	address	of	test@full@sail.com	is	not	a	valid email	
address.”
o Email	– test@full	sail.com
 Results	- “The	email	address	of	test@full	sail.com	is	not	a	valid email	
address.”
o Now	 Email	– bmanderson.@fullsail.com@
 Results	- “The	email	address	of	bmanderson.@fullsail.com@	is	not	a	valid email	
address.”



  Separator Tests
o List	– “1,2,3,4,5”	orginalSeparator	– “,”	newSeparator	– “-”
 Results	– “The	original	string	of	1,2,3,4,5 with	the	new	separator	is	1-2-3-
4-5.”
o List	– “red:	blue:	green:	pink”	orginalSeparator	– “:”	newSeparator	– “,”
 Results	– “The	original	string	red:	blue:	green:	pink	with	the	new	
separator	is	red,	blue,	green,	pink.”
o List	– “one two for”	orginalSeparator	– “ ”	newSeparator	– “,”
 Results	– “The	original	string	 one two for - ","  with the new separator is one,two,for

    */
