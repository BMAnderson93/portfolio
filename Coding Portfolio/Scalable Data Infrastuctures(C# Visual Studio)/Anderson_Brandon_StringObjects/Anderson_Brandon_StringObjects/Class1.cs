﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_StringObjects
{
    // This line creates my new custom class
    class Utilities
    {
        // This line creates my first function.
        public static string Checker(string email)
        {
            // This line creates a new int variable.
            int mPeriodCount = 0;
            // This line creates a new int variable.
            int mAtCount = 0;
            // This line creates a new int variable.
            int mAtLocatiion = email.IndexOf("@");
            // This line creates a new int variable.
            int mPeriodLocation = email.IndexOf(".");
            // This line begins to loop through each letter in the string "email".
            foreach (var letter in email)
            {
                // This line counts the number of @'s in the string.
                if (letter == '@')
                {
                    // This number adds to the total of @'s.
                    mAtCount++;
                }
                // This line counts the number of periods in the string.
                if (letter == '.')
                {
                    // This line adds to the total number of periods.
                    mPeriodCount++;
                }
            }

            {
                // This line creates a bool that is true if the period count is less equal to 0.
                bool noPeriods = mPeriodCount == 0;
                // This line creates a bool that is true if the @ count is not equal to 1
                bool atCountWrong = mAtCount != 1;
                // This line creates a bool that is true if the period is found before the @.
                bool periodLocationWrong = mAtLocatiion > mPeriodLocation;
                // This like is true if any spaces are found in the string.
                bool emailHasSpace = email.Contains(" ");
                // This line says if any of the previous bools are true, then the email in not valid.
                if (emailHasSpace || atCountWrong || periodLocationWrong || noPeriods)
                {
                    return "invalid";
                }
                // This line says if all the bools are false then the email is valid.
                else
                {
                    return "valid";
                }
            }

        }
        // This line creates my second function.
        public static string Separator(string sentence, string firstSep, string secondSep)
        {
            // This line takes the items provided and swaps out the first separator with the second.
            string secondSentence = sentence.Replace(firstSep,secondSep);
            return secondSentence;
        }
    }
}
