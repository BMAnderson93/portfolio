﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, my name is Brandon Anderson and I am in Mobile Development.");
            Console.WriteLine("When im not in school my hobbies are Cosplay, League of Legends, Destiny, and Paintball.");
            Console.WriteLine("If I was trapped on a island and could only have one drink it would be water.");
            Console.WriteLine("If I was allowed only one book I would bring with me Halo the fall of Reach.");
            Console.WriteLine("Finally if I was only allowed one album it would be Phobia by Breaking Benjamin.");
        }
       
    }
}
