Instructions

Using Visual Studio, start a new project, and create a new C# project/solution that will output information to the terminal window. This output should include all of the information outlined in your problem analysis. So, any information about what the code should do can be extrapolated from the first part of this lab.

Remember that all output should be meaningful to a user. If you simply output a list with no context, a user will not know what you are trying to say. Be sure to include enough text to give the output meaning.

The name of the project should be in the following format: LastNameFirstName_Lab1.  You will lose points if your lab does not follow this naming convention.

Find the project folder on your computer and compress the entire folder into a zip file. The folder should be installed along the following path where "your username" is the name you use to log into the Windows virtual machine:

C:\Users\<your username>\Documents\Visual Studio 2015\Projects

Create a zip file of this folder by right-clicking or CTRL+Clicking the folder, selecting Send To from the context menu, and then selecting Compressed (zipped) folder in the secondary menu.  Change the name of the zip file to match the convention noted above and upload this to FSO.

Things to Consider

Look to your problem analysis to verify that you've included all the outputs required by the problem.

If you ask the lab specialist for assistance on the code, he is going to ask to see your problem analysis first. If you didn't do that, he's going to tell you to complete that task before he'll look at your code. So, make sure you're doing the lab in the correct order.