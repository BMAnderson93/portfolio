﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_Conditionals
{
    /*Brandon Anderson
      7/10/17
      Conditionals
     */
    class Program
    {
        static void Main(string[] args)
        { // This is used so that the "goto" function knows to return here.
            Start:
            // Problem #1: Temperature Converter
            // This line asks the user to input the temperature they'd like to convert.
            Console.WriteLine("Please enter the temperature you'd like to convert.");
            // This line stores the input and converts it into a decimal.
            decimal temp =  int.Parse(Console.ReadLine());
            // This line asks if the number provided is in Fahrenheit or Celsius.
            Console.WriteLine("Is this in Fahrenheit or Celsius? (Enter F for Fahrenheit or C for Celsius.)");
            // This line stores the users input.
            string tempScale = Console.ReadLine();
            // This creates a bool variable that is true if the input from the user matches "C".
            bool celsiusCap = tempScale == "C";
            // This creates a bool variable that is true if the input from the user matches "c" which makes the code work regardless of caps.
            bool celsiusLow = tempScale == "c";
            // This is my first possible outcome for my code, which states that if either of the listed variables are true then the commands in that section will run.
            if (celsiusCap || celsiusLow)
            {
            // This line creates a new variable by taking the variable provided and using the formula to convert it to Celsius.
                decimal cConverted = Math.Round ((temp * 1.8m + 32.00m), 1);
            // This line then prints the results, giving the user their temperture converted.
                Console.WriteLine("The temperature is " + cConverted + " degrees Fahrenheit.");
            }
            // This line states that if the previous "if" statement is false, to then run this section of code.
            else {
            // This line creates a new variable by taking the temperature provided, and using the math formula to convert it Celsius. 
                decimal fConverted = Math.Round((temp - 32.00m) / 1.8m);
            // This line then prints the results, giving the user their temperature converted.
                Console.WriteLine("The temperature is " + fConverted + " degrees Celsius.");
                /* Test Values
                   o 32F is	0C
                   o 100C is 212F
                   o 50c, Does a lowercase c make a	difference? No it doesn't, the code has been written to function properly regardless of caps.
                   o 33c is 91.4 */
            }
            // Problem #2: Last Chance for Gas
            // This line asks the user how many gallons their car can hold.
            Console.WriteLine("How many gallons can your tank hold?");
            // This line takes the users input and converts it to a decimal, then stores it in a variable.
            decimal carTank = decimal.Parse(Console.ReadLine());
            // This line asks the user how full their tank currently is.
            Console.WriteLine("How full is your tank? (in %)");
            // This line takes the users input and converts it to a decimal, then stores it in a variable.
            decimal tankFull = decimal.Parse(Console.ReadLine());
            // This line asks the user how many miles to the gallon their car gets.
            Console.WriteLine("How many miles to the gallon does your car get?");
            // This line takes the users input and converts it to a decimal, then stores it in a variable.
            decimal mpg = decimal.Parse(Console.ReadLine());
            // This line creates a new variable by first taking how much gas is left in the tank and dividing it by 100. 
            // It then takes the result and multiplies it by the total gas the tank can hold and the miles to the galon it gets.
            int milesLeft = Convert.ToInt32((tankFull / 100) * carTank * mpg);
            // This line creates a new variable that is true if the miles left is greater than 199.
            bool needGas = milesLeft < 199;
            // This line takes the variable and runs the section below it if the variable is true.
            if (needGas)
            {
                // This line prints out how many more miles they can drive and if it is enough to make it to the next gas station.
                Console.WriteLine("You only have " + milesLeft + " miles you can drive, better stop for gas while you can!"); 
            }
            // This line states that if the previous "if" is untrue, then to run the section of code beneath it.
            else {
                Console.WriteLine("Yes, you can drive " + milesLeft + " more miles and you can make it without stopping for gas!") ;
                /*o Gallons- 20 , Gas Tank = 50% full, MPG- 25  Result –
                 “Yes, you can drive 250 more miles and you can make it without stopping for gas!” 
                 o Gallons- 12 , Gas Tank = 60% full, MPG- 20  Result – 
                 “You only have 144 miles you can drive, better get gas now while you can!”
                 o Gallons- 7 , Gas Tank = 20% full, MPG- 10  Result – 
                 “You only have 14 miles you can drive, better get gas now while you can!”
                */
            }
            // Problem #3: Grade Letter Calculator
            // This line asks the user what their current grade is.
            Console.WriteLine("What is your current grade? (in %)");
            // This line creates a new variable by taking the users input and converting it to a int.
            int grade = int.Parse(Console.ReadLine());
            // This line creates a variable that is true if the grade is > than 89.
            bool gradeA = grade > 89;
            // This line creates a variable that is true if the grade is > than 79.
            bool gradeB = grade > 79;
            // This line creates a variable that is true if the grade is > than 72.
            bool gradeC = grade > 72;
            // This line creates a variable that is true if the grade is > than 69.
            bool gradeD = grade > 69;
            // This line states that if the variable is true, then to run the code beneath it.
            if (gradeA)
            {
            // This line tells the user their grade % and what letter grade that matches to.
                Console.WriteLine("You have a " + grade + "%, which means you earned a(n) A in the class! ");
            }
            // This line says if the previous "if" variable is untrue, to check and see if this variable is true. If the variable is true, it then runs the line of code.
            else if (gradeB)
            {
                // This line tells the user their grade % and what letter grade that matches to.
                Console.WriteLine("You have a " + grade + "%, which means you earned a(n) B in the class! ");
            }
            // This line says if the previous "if" variable is untrue, to check and see if this variable is true. If the variable is true, it then runs the line of code.
            else if (gradeC)
            {
                // This line tells the user their grade % and what letter grade that matches to.
                Console.WriteLine("You have a " + grade + "%, which means you earned a(n) C in the class! ");
            }
            // This line says if the previous "if" variable is untrue, to check and see if this variable is true. If the variable is true, it then runs the line of code.
            else if (gradeD)
            {
                // This line tells the user their grade % and what letter grade that matches to.
                Console.WriteLine("You have a " + grade + "%, which means you earned a(n) D in the class! ");
            }
            // This line says if none of the previous "if" statements are true, then to run this line of code.
            else {
                // This line tells the user their grade % and what letter grade that matches to.
                Console.WriteLine("You have a " + grade + "%, which means you earned a(n) F in the class! ");
                /*o Grade – 92%  Result - “You have a 92%, which means you have earned a(n) A in the class!” 
                  o Grade – 80%  Result - “You have a 80%, which means you have earned a(n) B in the class!” 
                  o Grade – 67%  Result - “You have a 67%, which means you have earned a(n) F in the class!” 
                  o What happens when you type in 120%?  Is this a valid response? Can you re-prompt the user? 
                  If you enter 120% you will recieve the output saying you have a A. The response is not valid due to the grading system going only up to 100%.
                  In this program there is no re-prompt for the user, however you COULD program in two "else if" statements for any number less than 0
                  and any number greater than 100 to prompt the user that their response is not valid and to re enter a correct number.
                  o Grade – 2%  Result - “You have a 2%, which means you have earned a(n) F in the class!” 
                 */
            }

            // Problem #4: Discount Double Check

            // This line asks the user to input the cost of their first item.
            Console.WriteLine("Please enter the cost of the first item. (in $)");
            // This line takes the users input and converts it to a decimal, then stores it as a new variable.
            decimal firstItem = decimal.Parse(Console.ReadLine());
            // This line asks the user to input the cost of their second item.
            Console.WriteLine("Please enter the cost of the second item. (in $)");
            // This line takes the users input and converts it to a decimal, then stores it as a new variable.
            decimal secondItem = decimal.Parse(Console.ReadLine());
            // This line gets the subtotal by adding the two items together.
            decimal subTotal = firstItem + secondItem;
            // This line uses the first possible discount to create a variable to be used later.
            decimal firstDiscountDec = 0.10m;
            // This line uses the second possible discount to create a variable to be used later.
            decimal secondDiscountDec = 0.05m;
            // This line creates a variable that is the total amount due after the discount of 10% is added.
            decimal firstTotalDiscount = Math.Round(subTotal - (subTotal * firstDiscountDec), 2);
            // This line creates a variable that is the total amount due after the discount of 5% is added.
            decimal secondTotalDiscount = Math.Round(subTotal - (subTotal * secondDiscountDec), 2);
            // This line creates a variable that is true if the subtotal is greater than 99.
            bool firstDiscount =  subTotal > 99;
            // This line creates a variable that is true if the subtotal is greater than 49.
            bool secondDiscount = subTotal > 49;
            // This line says that if the variable "firstDiscount" is true, then run the code beneath it.
            if (firstDiscount)
            {
            // This line displays the users total purchase with their discount.
                Console.WriteLine("Your total purchase is $" + firstTotalDiscount + ", which includes your 10% discount.");
            }
            // This line says that if the previous variable is untrue, then check and see if this variable is true. If the variable is true, the code beneath it will run
            else if (secondDiscount)
            {
            // This line displays the users total purchase with their discount.
                Console.WriteLine("Your total purchase is $" + secondTotalDiscount + ", which includes your 5% discount.");
            }
            // This line says that if none of the previous variables are true, then to run the line of code beneath it.
            else {
            // This line displays the users total without a discount if they did not get one.
                Console.WriteLine("Your total purchase is $" + subTotal + ".");
            }
            // This line asks the user to press the enter key if they'd like to restart the program to enter new values.
            Console.WriteLine("Press enter to start over with new values!");
            // This pauses the program and waits for the users input to continue.
            Console.ReadKey();
            // This line tells the program to return to the start of the program.
            goto Start;
            /*o First Item Cost - $ 45.50, Second Item Cost - $75.00, Total - $108.45  Results - “Your total purchase is $108.45, which includes your 10% discount.” 
              o First Item Cost - $ 30.00, Second Item Cost - $25.00, Total - $52.25   Results - “Your total purchase is $52.25, which includes your 5% discount.” 
              o First Item Cost - $ 5.75, Second Item Cost - $12.50, Total - $18.25    Results - “Your total purchase is $18.25.”
              o First Item Cost - $ 34.22, Second Item Cost - $86.44, Total - $120.66  Results - “Your total purchase is $108.59, which includes your 5% discount.”
             
             */
        }
    }
}
