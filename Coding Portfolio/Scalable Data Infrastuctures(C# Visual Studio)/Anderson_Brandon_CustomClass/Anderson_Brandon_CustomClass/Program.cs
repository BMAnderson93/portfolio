﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            // This line Instantiates the class and creates a new object.
            DogShelter kennel = new DogShelter(100,0,0);
            // This line creates a loop that will repeat until i is greater than 6.
            for (int i = 0; i < 6; i++)
            {
                // This line asks if the user wants to add or remove a dog.
                Console.WriteLine("Welcome to the dog kennel! Would you like to remove or add a dog?(Press 1 to remove, or 2 to add.)");
                int input = int.Parse(Console.ReadLine());
                if (input == 1)
                {
                    // This line asks how many dogs they want to remove.
                    Console.WriteLine("How many dogs would you like to remove?");
                    int dogsRemove = int.Parse(Console.ReadLine());
                    // This line changes the current amount of dogs in the shelter to the correct amount after removing the dogs.
                    kennel.mCurrentDogCount = DogShelter.RemoveDog(dogsRemove, kennel.mCurrentDogCount );
                    // This line prints how many dogs are left.
                    Console.WriteLine("There is now " +kennel.mCurrentDogCount+ " dogs left in this kennel.");
                    // This line creates a new variable stating how much room is left.
                    int roomLeft = kennel.mDogCap - kennel.mCurrentDogCount;
                    // This line prints how many dogs it can still hold.
                    Console.WriteLine("We can still hold " + roomLeft  + " more dogs!");
                    // This line pauses the code and aways the users input.
                    Console.WriteLine("Press enter to continue.");
                    Console.ReadLine();
                    // This line clears the console to keep the code print looking clean.
                    Console.Clear();
                    
                }
                else
                {
                    // This line asks the user how many dogs they want to add.
                    Console.WriteLine("How many dogs would you like to add?");
                    int dogsAdd = int.Parse(Console.ReadLine());
                    // This line changes the current count to the correct amount after running the function.
                    kennel.mCurrentDogCount = DogShelter.AddDog(dogsAdd, kennel.mCurrentDogCount, kennel.mDogCap);
                    // This line prints the current amount of dogs in the shelter.
                    Console.WriteLine("There is now " + kennel.mCurrentDogCount + " dogs currently in this kennel.");
                    // This line creates a new variable that is the room left in the kennel.
                    int roomLeft = kennel.mDogCap - kennel.mCurrentDogCount;
                    // This line prints how much room is still left.
                    Console.WriteLine("We can still hold " + roomLeft + " more dogs!");
                    Console.WriteLine("Press enter to continue.");
                    // This line pauses the code.
                    Console.ReadLine();
                    // This line clears the console to keep it looking clean.
                    Console.Clear();
                }

            }
        }
    }
}
