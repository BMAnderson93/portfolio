﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_CustomClass
{   // This line creates my new custom class.
    class DogShelter
    {
        // This line creates a member variable.
        public int mDogCap;
        // This line creates a member variable.
        public int mDogMin = 0;
        // This line creates a member variable.
        public int mCurrentDogCount;
        // This line constructs the function.
        public DogShelter (int _dogCap, int _dogMin, int _currentDogCount)
        {
            mDogCap = _dogCap;
            mDogMin = _dogMin;
            mCurrentDogCount = _currentDogCount;
        }
        
        // This like sets the value for the variable.
        public void SetDogCap(int dogCap)
        {
            this.mDogCap = dogCap;
        }
        // This line gets the value and returns it to the variable.
        public int GetDogCap()
        {
            return this.mDogCap;
        }
        // This like sets the value for the variable.
        public void SetDogMin()
        {
            this.mDogMin = 0;
        }
        // This line gets the value and returns it to the variable.
        public int GetDogMin()
        {
            return this.mDogMin;
        }
        // This like sets the value for the variable.
        public void SetCurrentDogCount(int currentDogCount)
        {
            this.mCurrentDogCount = currentDogCount;
        }
        // This line gets the value and returns it to the variable.
        public int GetCurrentDogCount()
        {
            return this.mCurrentDogCount;
        }
        // This line creates a new method for removing dogs from the kennel.
        public static int RemoveDog(int dogRemove, int currentCount)
        {
            // This line says that if the current count is greater than -1 then you can remove that amount of dogs.
            if (currentCount - dogRemove > -1)
            {
                currentCount = currentCount - dogRemove;
                return currentCount;
            }
            // This line says if the statement above isn't true then you cant remove that many dogs and the amount remains the same.
            else
            {
                Console.WriteLine("Sorry, but there are not enough dogs for you to take that amount.");
                return currentCount;
            }
        }
        // This line creates a method to add dogs to the kennel.
        public static int AddDog(int dogAdd, int currentCount, int dogCap)
        {
            // This line says if the current count + the amount of dogs being added is less than the cap plus 1 then you can add that many dogs.
            if (currentCount + dogAdd < dogCap+1)
            {
                currentCount = currentCount + dogAdd;
                return currentCount;
            }
            // If the previous line isn't true the user is told they can't add that many dogs and the count remains the same.
            else
            {
                Console.WriteLine("Sorry, but there is not enough room to hold that many more dogs.");
                return currentCount;
            }
        }
    }
    
}
