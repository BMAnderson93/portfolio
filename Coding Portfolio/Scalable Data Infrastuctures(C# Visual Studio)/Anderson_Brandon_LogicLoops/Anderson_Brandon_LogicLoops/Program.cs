﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_LogicLoops
{
    // Brandon Anderson
    // 7/13/17
    // Logic & Loops.
    class Program
    {
        static void Main(string[] args)
        // Problem #1 – Logical Operators: Tire Pressure I


        {  // This line asks the user for an input.
            Console.WriteLine("Please enter the pressure of the front left tire.");
            // This line stores the users input.
            int tiresArray00;
            // This line validates the users input.
            while (!int.TryParse(Console.ReadLine(), out tiresArray00)) {
                // This line asks the user for a valid input
                Console.WriteLine("Please enter a valid number.");
            }

            // This line asks tbe user for a input.
            Console.WriteLine("Please enter the pressure of the front right tire.");
            // This line stores the input.
            int tiresArray01;
            // This line validates the users input.
            while (!int.TryParse(Console.ReadLine(), out tiresArray01))
            {
                // This line asks the user for a valid input.
                Console.WriteLine("Please enter a valid number.");
            }
            // This line asks the user for an input.
            Console.WriteLine("Please enter the pressure of the back left tire.");
            // This line validates the users input.
            int tiresArray02;
            while (!int.TryParse(Console.ReadLine(), out tiresArray02))
            {
                // This line asks the user for a valid input.
                Console.WriteLine("Please enter a valid number.");
            }
            // This line asks the user for an input.
            Console.WriteLine("Please enter the pressure of the back right tire.");
            // This line validates the users input.
            int tiresArray03;
            while (!int.TryParse(Console.ReadLine(), out tiresArray03))
            {
                // This line asks the user for a valid input.
                Console.WriteLine("Please enter a valid number.");
            }
            // This line creates a new array.
            int[] tiresArray = new int[] { tiresArray00, tiresArray01, tiresArray02, tiresArray03 };

            // This line begins my if statement.
            if (tiresArray00 == tiresArray01 && tiresArray02 == tiresArray03)
            {
                // This line prints the result.
                Console.WriteLine("Tires pass spec!");
            }
            // This line says what to do if the previous statement is false.
            else
            {
                // This line prints the result.
                Console.WriteLine("Get your tires checked out!");
            }
            /* 
             • Data	Sets	To	Test:
o Front	Left	– 32,	Front	Right	– 32,	Back	Left	-30	Back	Right- 30		- Tires	OK
o Front	Left	– 36,	Front	Right	– 32,	Back	Left	-25	Back	Right- 25		- Check	Tires
o Front	Left	– 31,	Front	Right	– 39,	Back	Left	-20	Back	Right- 12		- Check	Tires
             */
            //Problem #2 – Logical Operators: Movie Ticket Price



            // This line asks the user for their age.
            Console.WriteLine("Please enter your age.");
            // This line validates the users response.
            int age;
            while (!int.TryParse(Console.ReadLine(), out age))
            {
                // This line asks the user for a valid input.
                Console.WriteLine("Please enter a valid number.");
            }
            // This line asks the user for the time of their movie.
            Console.WriteLine("Please enter the time of your movie. (In military time!)");
            // This line validates the users input.
            int movieTime;
            while (!int.TryParse(Console.ReadLine(), out movieTime))
            {
                // This line asks the user for a valid input.
                Console.WriteLine("Please enter a valid number.");
            }
            // This creates a variable to be used later.
            int discountPrice = 7;
            // This line creates a variable to be used later.
            int price = 12;
            // This line begins my if statement.
            if (age > 54 || age < 10 || movieTime == 14 || movieTime == 15 || movieTime == 16 || movieTime == 17)
            {
                // This line prints the results.
                Console.WriteLine("The ticket price is $" + discountPrice + ".");
            }
            // This is my else statement.
            else
            {
                // This line prints the results.
                Console.WriteLine("The ticket price is $" + price + ".");
            }
            /* 
            Data	Sets	To	Test:
o Age	– 57,	Time	– 20,	Ticket	Price	- $7.00
o Age	– 9,	Time	– 20,	Ticket	Price	- $7.00
o Age	– 38,	Time	– 20,	Ticket	Price	- $12.00
o Age	– 25,	Time	– 16,	Ticket	Price	- $7.00
o Age   - 2,    Time    - 15,   Ticket  Price   - $7.00
            */


            // Problem #3 – For Loop: Add Up The Odds or Evens

            // This line is a variable to be used later.
            int addEven = 0;
            // This line is a variable to be used later.
            int addOdd = 0;
            // This line is a variable to be used later.
            int addOddDone = 0;
            // This line is a variable to be used later.
            int addEvenDone = 0;
            // This line is a new array.
            int[] array = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            // This line is used to get the user to enter valid data.
            Start:
            // This line asks the user what numbers they want to add.
            Console.WriteLine("Would you like to see the sum of all odd or even numbers?");
            // This line stores the users input.
            string input = Console.ReadLine();
            // This is my if statement.
            if (input == "even")
                // This line is to get each different element from the array.
            {
                for (addEven = 0; addEven < array.Length; addEven++)
                {
                    // This line checks to see if the element is even.
                    if (array[addEven] % 2 == 0)

                    {
                        // If even the element will then be added to the total.
                        addEvenDone += array[addEven];

                    }


                }
                // This line prints the result.
                Console.WriteLine("The even numbers add up to " +addEvenDone + ".");

            }
            // This line says if the user asks for the odd total to begin this line of code.
            else if (input == "odd")

                // This line gets all the elements from the array.
            {
                for (addOdd = 0; addOdd < array.Length; addOdd++)
                {
                    // This line checks to see if the element is odd
                    if (array[addOdd] % 2 != 0)
                    {
                        // If the element is odd it will be then added to the total.
                        addOddDone += array[addOdd];

                    }


                }
                // This line prints the results.

                Console.WriteLine("The odd numbers add up to " + addOddDone + ".");
            }
            // This line begins my else statement.
            else {
                // This line validates the users input.
                Console.WriteLine("Please enter a valid response!");
                // This line tells the program to allow the user to use valid numbers.
                goto Start;
                /*
                 Data	Sets	To	Test:
o Array:	{1,2,3,4,5,6,7},	Sum	of	Evens	– 12		Sum	of	Odds	– 16
o Array:	{12,13,14,15,16,17},	Sum of Evens	– 42		Sum	of	Odds	– 45
o Array:	{2, 2, 2, 5, 4, 5},	Sum of Evens	– 10		Sum	of	Odds	– 10
                 */

                //Problem #4 – While Loop: Charge It!

            }
            // This line asks the user for their input.
            Console.WriteLine("Please enter your max credit limit.");
            // This line convers the users input to a decimal.
            decimal creditLimit = decimal.Parse (Console.ReadLine());
            // This line creates a variable to be used later.
            decimal total = 0;
            // This line creates a variable to be used later.
            decimal creditLeft = 0;
            // This line begins my while loop.
            while (total < creditLimit) {
               
                {
                    // This line asks the user to input their purchases.
                    Console.WriteLine("How much is your purchase?");
                    // This line stores their input.
                    decimal purchase = decimal.Parse(Console.ReadLine());
                    // This line adds their purchaes to their total.
                    total += purchase;
                    // This line subtracts their purchase from their total.
                    creditLeft = creditLimit - total;
                    // This is my if statement.
                    if (creditLeft >= 0)
                    {
                        // This line prints the results.
                        Console.WriteLine("With your current purchase of $" + purchase + ", you can still spend $" + creditLeft + ".");
                    }
                    // This is me else statement if the previous statement is untrue.
                    else {
                        // This line prints the results.
                        Console.WriteLine("With your last purchase you have reached your credit limit and exceeded it by $" + Math.Abs(creditLeft) + ".");
                    }
                    /*
                 o Credit	Limit	– 20.00
§ Purchase1- 5.00 - You	can	still	spend	$15.00
§ Purchase2- 12.00	- You	can	still	spend	$3.00
§ Purchase3- 7.00	- You	have	exceeded	your	limit	by $4.00.
                 o Credit	Limit	– 100.00
$ Purchase1- 101    - You   have exceeded your limit by $1
                 */
                }

            }
            
        }
    }
}
