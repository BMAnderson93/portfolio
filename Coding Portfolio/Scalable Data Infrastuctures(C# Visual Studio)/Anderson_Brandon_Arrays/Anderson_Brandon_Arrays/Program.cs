﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Brandon Anderson
             7/11/2017
             Arrays
             */

            //Create your own project and call it Lastname_Firstname_Arrays
            //Copy this code inside of this Main section into your Main Section
            //Work through each of the sections

            //Declare and Define The Starting Number Arrays
            int[] firstArray = new int[4] { 4, 20, 60, 150 };
            double[] secondArray = new double[4] { 5, 40.5, 65.4, 145.98 };

            //Find the total of each array and store it in a variable and output to console
            // This line is used to find the 0 element in the third array.
            decimal thirdArray0 = Convert.ToDecimal(firstArray[0] + secondArray[0]);
            // This line is used to find the 1 element in the third array.
            decimal thirdArray1 = Convert.ToDecimal(firstArray[1] + secondArray[1]);
            // This line is used to find the 2 element in the third array.
            decimal thirdArray2 = Convert.ToDecimal(firstArray[2] + secondArray[2]);
            // This line is used to find the 3 element in the third array.
            decimal thirdArray3 = Convert.ToDecimal(firstArray[3] + secondArray[3]);
            // This line is used to find the total of the first array by adding each element of the array together.
            decimal firstArrayTotal = Convert.ToDecimal(firstArray[0] + firstArray[1] + firstArray[2] + firstArray[3]);
            // This line is used to find the total of the second array by adding each element of the array together.
            double secondArrayTotal = Convert.ToDouble(secondArray[0] + secondArray[1] + secondArray[2] + secondArray[3]);
            // This line is used to find the average of the first array by taking the total of the array and dividing it by the length.
            decimal firstArrayAverage = Convert.ToDecimal(firstArrayTotal / firstArray.Length);
            // This line is used to find the average of the second array by taking the total of the array and dividing it by the length.
            decimal secondArrayAverage = Convert.ToDecimal(secondArrayTotal / secondArray.Length);
            // This line prints out the total for the first array.
            Console.WriteLine("The total for the first array is " + firstArrayTotal + ".");
            // This line prints out the total for the second array.
            Console.WriteLine("The total for the second array is " + secondArrayTotal + ".");
            // This line prints out the average of the first array.
            Console.WriteLine("The average for the first array is " + firstArrayAverage + ".");
            // This line prints out the average of the second array.
            Console.WriteLine("The average for the second array is " + secondArrayAverage + ".");
            // This line creates the third array by using the variables were created before by adding the elements of array 1 and 2 together.
            decimal[] thirdArray = new decimal[] { thirdArray0, thirdArray1, thirdArray2, thirdArray3 };
            // This line prints out the entire contents of the third array.
            Console.WriteLine("The third array is " + "[{0}]" + ".", string.Join(", ", thirdArray));




            //Calculate the average of each array and store it in a variable and output to console
            //Just a reminder to check the averages with a calculator as well, to make sure they are correct.



            /*
               Create a 3rd number array.  
               The values of this array will come from the 2 given arrays.
                -You will take the first item in each of the 2 number arrays, add them together and then store this sum inside of the new array.
                -For example Add the index#0 of array 1 to index#0 of array2 and store this inside of your new array at the index#0 spot.
                -Repeat this for each index #.
                -Do not add them by hand, the computer must add them.
                -Do not use the numbers themselves, use the array elements.
                -After you have the completed new array, output this to the Console.
             */





            /*
               Given the array of strings below named MixedUp.  
               You must create a string variable that puts the items in the correct order to make a complete sentence.
                -Use each element in the array, do not re-write the strings themselves.
                -Concatenate them in the correct order to form a sentence.
                -Store this new sentence string inside of a string variable you create.
                -Output this new string variable to the console.
             */

            //Declare and Define The String Array
            string[] MixedUp = new string[] { "but the lighting of a", "Education is not", "fire.", "the filling", "of a bucket," };
            // This line creates a new string by putting the elements of the array in the correct order to create a sentence.
            string unMixed = MixedUp[1] + " " + MixedUp[3] + " " + MixedUp[4] + " " + MixedUp[0] + " " + MixedUp[2];
            // This line prints out the variable.
            Console.WriteLine(unMixed);




        }
    }
}
