﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_Exam
{
    class Program
    {
        static void Main(string[] args)
        {
            Start:
            Console.WriteLine("***Bob's Landscaping Staff***");
            Employee.PrintList();
            Console.WriteLine("Please choose: <hire> <fire> <quit>");
            string input = Console.ReadLine();

            while (input != "hire" && input != "fire" && input != "quit")
            {
                Console.WriteLine("Please enter a valid response!");
                input = Console.ReadLine();
            }
            if (input == "hire")
            {
                Console.WriteLine("Please enter the name of the employee!");
                string newEmployee = Console.ReadLine();
                Employee.AddName(newEmployee);
                Console.Clear();
                goto Start;
            }
            else if (input == "fire")
            {
                if (Employee.mEmployees.Count == 0)
                {
                    Console.WriteLine("Im sorry, but there is no one currently working for you!");
                    goto Start;
                }
                else
                {
                    Console.WriteLine("Please enter the numer of the employee you want to remove!");
                    int employeeNumber = int.Parse(Console.ReadLine());
                    while (employeeNumber < 1 || employeeNumber > Employee.mEmployees.Count)
                    {
                        Console.WriteLine("Please enter a valid employee number!");
                        employeeNumber = int.Parse(Console.ReadLine());
                    }
                    Employee.RemoveName(employeeNumber);
                    Console.Clear();
                    goto Start;
                }
            }
            else { }

                }

            
        }
    }

