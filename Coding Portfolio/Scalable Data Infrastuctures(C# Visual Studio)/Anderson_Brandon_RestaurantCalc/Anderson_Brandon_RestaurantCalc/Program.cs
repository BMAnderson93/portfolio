﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_RestaurantCalc
{/* 
    Brandon Anderson
    06/29/2017
    Tip Calculator
 */
    class Program
    {
        static void Main(string[] args)
            /* I use "Start" so at the end of my program I can return to this line, restarting my program.
            This makes testing multiple numbers much easier, due to not having to close and reopen the program to make sure 
            it provides correct answers.*/
        { Start:
            // I use this line to ask for the price of the first meal.
            Console.WriteLine("Please enter the price of the first guests meal.");
            // This line is for storing the price of the first meal.
            string food1 = Console.ReadLine();
            // I use this line to ask for the price of the second meal.
            Console.WriteLine("Please enter the price of the second guests meal.");
            // This line is for storing the price of the second meal.
            string food2 = Console.ReadLine();
            // I use this line to ask for the price of the third meal.
            Console.WriteLine("Please enter the price of the third guests meal.");
            // This line is for storing the price of the third meal.
            string food3 = Console.ReadLine();
            // I use this line to ask what % of the bill they would like to leave as a tip.
            Console.WriteLine("What tip % would you like to leave based on the quality of service you received?");
            // This is the line for storing the % they would like to leave as a tip.
            string tip = Console.ReadLine();
            // This line converts the price of the first order from a string to a float and saves it as a new variable named "food1Parse"
            float food1Parse = float.Parse(food1);
            // This line converts the price of the second order from a string to a float and saves it as a new variable named "food2Parse"
            float food2Parse = float.Parse(food2);
            // This line converts the price of the third order from a string to a float and saves it as a new variable named "food3Parse"
            float food3Parse = float.Parse(food3);
            // This line converts the tip percentage they'd like to leave from a string to a float and saves it as a new variable named "tipParse"
            float tipParse = float.Parse(tip);
            /* This line takes the floats "food1Parse", "food2Parse", and "food3Parse" adding their total together to give the guests their subtotal
            creating a new variable named "subTotal". */
            float subTotal = food1Parse + food2Parse + food3Parse;
            /* This line is for finding out the total tip they will be leaving by using the order of operations to first take the variable
             "tipParse", and having it divided by 100. After we have done that we take the solution of that equation and multiply it by the variable
             "subTotal". This provides us with the total tip the party will be leaving and saves it in a variable named "tipTotal".*/
            float tipTotal = subTotal * (tipParse / 100);
            /* This line takes the variables "tipTotal" and "subTotal" adding their values together to give the guests their grand total, which is then
            saved in a variable called "totalBill"*/
            float totalBill = tipTotal + subTotal;
            /* This line takes the variable "totalBill" and divided it by 3 in order to evenly seperate the bill for the 3 party members, which
              is then stowed in a variable called "billDivided".*/
            float billDivided = (totalBill / 3);
            // This line is used to take the variable "totalBill" and round it up incase the variable exceeds 2 decimals.
            float billDividedRounded = (float)Math.Round(billDivided, 2);
            // This line takes the variable "subTotal" and uses it to display the parties total bill before the tip is included.
            Console.WriteLine("Your total before applying the tip is $" + subTotal + '.');
            // This line takes the variable "tipTotal" and uses it to display the total tip they will be leaving.
            Console.WriteLine("You will be leaving $" + tipTotal + " as the tip.");
            // This line uses the variable "totalBill" to display the grand total of the entire bill.
            Console.WriteLine("Making your grand total $" + totalBill + '.');
            // This line uses the variable "billDividedRounded" to display the total price each member of the party needs to pay.
            Console.WriteLine("To split your check evenly amongst your party is $" + (billDividedRounded) +" per guest. ");
            // I use this line to ask the user if they'd like to start the program over again with new variable.
            Console.WriteLine("Press enter to add new values!");
            // I use this line to pause the program and stop it from looping until the user indicates they are ready for it to restart by pressing enter
            Console.ReadKey();
            // This line is to tell the program to return to line 15 where "Start" is found.
            goto Start;
        }
    }
}

 /* These are the test values provided.
   Test     #1
• Check	#1	– 10.00
• Check#2	– 15.00
• Check#3	– 25.00
• Tip	%	- 20
• Sub-Total Without Tip	– 50.00
• Total Tip – 10.00
• Grand total   With Tip – 60.00
• Cost per person	– 20.00

    Test     #2
• Check	#1	– 20.25
• Check#2	– 17.75
• Check#3	– 23.90
• Tip	%	- 10
• Sub-Total Without Tip	– 61.90
• Total Tip – 6.19
• Grand total   With Tip – 68.09
• Cost per person	– 22.69666	or rounded 22.70*/