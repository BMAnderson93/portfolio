﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anderson_Brandon_Methods
{
    /* Brandon Anderson
       07/17/17
       Methods
       */
    class Program
    {
        static void Main(string[] args)

        {
            // Problem #1: Painting A Wall
            // This line asks the user for the width.
              Console.WriteLine("What is the width of the wall? (In feet.)");
            // This line stores the input.
              decimal width;
            // This line validates the input.
              while (!decimal.TryParse(Console.ReadLine(), out width))
              {
                  // This line asks the user for a valid input
                  Console.WriteLine("Please enter a valid number.");
              }
              // This line asks for the height of the wall.
              Console.WriteLine("What is the height of the wall? (In feet.)");
            // This line stores the input.
              decimal height;
            // This line validates the input.
              while (!decimal.TryParse(Console.ReadLine(), out height))
              {
                  // This line asks the user for a valid input
                  Console.WriteLine("Please enter a valid number.");
              }
              // This line asks how many coats they wanna give the wall.
              Console.WriteLine("How many coats of paint do you wanna give it?");
            // This line stores the input.
              decimal coats;
            // This line validates the input.
              while (!decimal.TryParse(Console.ReadLine(), out coats))
              {
                  // This line asks the user for a valid input.
                  Console.WriteLine("Please enter a valid number.");
              }
              // This line asks how much surface area 1 gallon.
              Console.WriteLine("How much surface area will 1 gallon of paint cover? (In feet^2.)");
            // This line stores the input.
              decimal gallons;
            // This line validates the input.
              while (!decimal.TryParse(Console.ReadLine(), out gallons))
              {
                  // This line asks the user for a valid input.
                  Console.WriteLine("Please enter a valid number.");
              }
              // This line creates a new variable called gallons needed.
              decimal gallonsNeeded = Painting(width, height, coats, gallons);
            // This line rounds the previous variable and creates a new one.
              decimal gallonsNeededRounded = Math.Round(gallonsNeeded, 2, MidpointRounding.AwayFromZero);
            // Thi line prints the results.
              Console.WriteLine("For " + coats + " coats on that wall, you will need " + gallonsNeededRounded + " gallons of paint.");
            /* Tests
              Width	– 8,	Height	– 10,	Coats	– 2,	Surface	Area	- 300	ft2
              § Results	- “For	2	coats	on	that	wall,	you	will	need	.53	gallons	of	paint.”
              Width	– 30,	Height	– 12.5,	Coats	– 3,	Surface	Area	- 350	ft2
              § Results	- “For	3	coats	on	that	wall,	you	will	need	3.21	gallons	of	paint.”
              Width	– 20,	Height	– 2,	Coats	– 1,	Surface	Area	- 1000	ft2
              § Results	- “For	1	coats	on	that	wall,	you	will	need	.04 	gallons	of	paint.”
               
              
             
             */


            // Problem #2:  Stung!
            // This line asks for the animals weight.
            Console.WriteLine("What is the animals weight in pounds?");
            // This line stores the input.
              decimal weight;
            // This line validates the users input.
              while (!decimal.TryParse(Console.ReadLine(), out weight))
              {
                  // This line asks the user for a valid input
                  Console.WriteLine("Please enter a valid number.");
              }
              // This line creates a new variable called beeStings, and calls the function.
              decimal beeStings = BeeStings(weight);
            // This line prints the results.
              Console.WriteLine("It takes " + beeStings + " to kill this animal.");

            /* • Data	Sets	To	Test	
                o Animal’s	Weight	– 10
                § Results	- “It	takes 90 bee	stings	to	kill	this	animal.”
                o Animal’s	Weight	– 160
                § Results	- “It	takes 1440 bee	stings	to	kill	this	animal.”
                o Animal’s	Weight	– Twenty
                § Results	– Re-prompt	for	number value.	
                o Animal's Weight - 100
                § Results	- “It	takes 900 bee	stings	to	kill	this	animal.”
             
             
             
             
             
             
             
             
             */




            // Problem #3: Reverse it!

            // This line creates the first array.
            string[] test = new string[] { "!", "t","s" ,"e", "T"};
            // This line creates the second array by calling the function.
            string[] rTest = BackArray(test);
            // This line creates a new string variable.
            string ogTest = "";
            // This line creates a new string variable.
            string revTest = "";
            // This line takes the first array and prints each element into a new string.
            for (int i = 0; i < test.Length; i++) {
                ogTest += " " + test[i] + " ";
            }
            // This line takes the second array and prints each element into a new string.
            for (int i = 0; i < rTest.Length; i++)
            {
                revTest += " " + rTest[i] + " ";
            }
            // This line prints the results.
            Console.WriteLine("Your original array was [" + ogTest + "] and now it is reversed as [" + revTest + "].");

            /*§ Data	Sets	To	Test	
            o Initial array	– [“apple”,	“pear”,	“peach”,	“coconut”,	“kiwi”]
            § Results	- Your	original	array	was	[“apple”,	“pear”,	“peach”,	“coconut”,	“kiwi”] 
            and	now	it	is	reversed as	[“kiwi”,	“coconut”,	“peach”,	“pear”,	“apple”]
            o Initial array	– [“red”,	“orange”,	“yellow”,	“green”, “blue”, ”indigo”,	“violet”]
            § Results	- Your	original	array	was	[“red”,	“orange”,	“yellow”,	“green”,“blue”, ”indigo”,	“violet”] 
            and	now	it	is	reversed as	[“violet”,	“indigo”,	“blue”,	“green”,	“yellow”,	“orange”,	“red”]
            o NInitial array - ["!","t","s","e","t"]
            § Results	- Your	original	array	was ["!","t","s","e","t"]
            and	now	it	is	reversed as	["t","e","s","t","!"]
             
             
             
             
             */




        }
        // This is my first function for problem #1.
        public static decimal Painting(decimal w, decimal h, decimal c, decimal g) {
            // This gets the area by taking the variable w and multiplying it by the variable h.
            decimal area = w * h;
            // This takes the area and multiplies it by how many coats the user wants.
            decimal areaCoats = area * c;
            // This gets how many gallons are needed by deviding the total area by how much feet each gallon of paint can cover.
            decimal gallonsNeeded = areaCoats / g;
            // This line returns the gallons needed.
            return gallonsNeeded;
        }
        // This is my second function for problem #2.
        public static decimal BeeStings(decimal p) {
            // This line figures how many total stings an animal can take.
            decimal totalStings = p * 9;
            // This line returns total stings.
            return totalStings;
        }
        // This is my final function for problem #3.
        public static string[] BackArray(string[] s)
        {
            // This line creates a new array that is equal in length to the array s.
            string[] nameReverse = new string[s.Length];
            // This line creates a new int with the value of 0.
            int N = 0;
            // This line creates a new string with no value.
            string newArray = "";
            // This line begins my for statment
            for (int i = s.Length - 1; i > -1; i--)
            {
                // This line makes each elemt of nameReverse = the array s.
                nameReverse[N] = s[i];
                // This line adds 1 to the int N.
                N++;
            }
            // This line returns the array.
            return nameReverse;

        }
       
       
        
       
        }

    }

