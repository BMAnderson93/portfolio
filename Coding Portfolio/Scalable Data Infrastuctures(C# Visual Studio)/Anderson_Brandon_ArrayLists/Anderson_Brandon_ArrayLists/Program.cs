﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Anderson_Brandon_ArrayLists
{
    /*  Brandon Anderson
     *  07/19/2017
     *  Array Lists
     
         
         */
    class Program
    {
        static void Main(string[] args)
        {
            // This calls my function.
            Arrays();
        }
        // This creates my new function.
        public static void Arrays()
        {
            // This creates a new array list called teams.
            ArrayList teams = new ArrayList();
            // This adds a team to my list.
            teams.Add("Cardinals");
            // This adds a team to my list.
            teams.Add("Cubs");
            // This adds a team to my list.
            teams.Add("Yankees");
            // This adds a team to my list.
            teams.Add("Dodgers");
            // This adds a team to my list.
            teams.Add("Rays");
            // This creates a new array list called citied.
            ArrayList cities = new ArrayList();
            // This adds the teams city to my list.
            cities.Add("St. Louis");
            // This adds the teams city to my list.
            cities.Add("Chicago");
            // This adds the teams city to my list.
            cities.Add("New York");
            // This adds the teams city to my list.
            cities.Add("Brooklyn");
            // This adds the teams city to my list.
            cities.Add("Tampa");

            // This line goes through each element in each array.
            for (int i = 0; i < teams.Count; i++)

            {
                // This line combines each matching element in both arrays into a sentance.
                Console.WriteLine("The " +teams[i] + " are from " +cities[i] + ".");
                

               
            }
            // This line removes the element 1 from the team array.
            teams.RemoveAt(1);
            // This line removes the element 2 from the team array.
            teams.RemoveAt(2);
            // This line removes the element 1 from the cities array.
            cities.RemoveAt(1);
            // This line removes the element 2 from the cities array.
            cities.RemoveAt(2);
            // This line adds a new elemnt to the begining of the first array.
            teams.Insert(0, "Angels");
            // This line adds a new elemnt to the begining of the second array.
            cities.Insert(0, "Los Angeles");
            // This line goes through each element in each array.
            for (int i = 0; i < teams.Count; i++)
            {
                // This line combines each matching element in both arrays into a sentance.
                Console.WriteLine("The " + teams[i] + " are from " + cities[i] + ".");



            }

        }
    }
    }

