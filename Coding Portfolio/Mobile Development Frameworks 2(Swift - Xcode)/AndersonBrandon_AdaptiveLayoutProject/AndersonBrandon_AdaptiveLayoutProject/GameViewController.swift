//
//  GameViewController.swift
//  AndersonBrandon_AdaptiveLayoutProject
//
//  Created by Brandon Anderson on 6/8/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit
import AVFoundation

class GameViewController: UIViewController {
    @IBOutlet weak var GameOverIcon: UIImageView!
    
    @IBOutlet weak var Replay: UIImageView!
    @IBOutlet weak var ViewBackGround: UIView!
    @IBOutlet weak var YouWinIcon: UIImageView!
    @IBOutlet weak var RestartButton: UIButton!
    
    @IBOutlet weak var ExitButton: UIButton!
    @IBOutlet weak var minutes: UILabel!
    @IBOutlet weak var seconds: UILabel!
    var allSpaces =  [UIButton]()
    
    
    @IBOutlet weak var r0c1: UIButton!
    
    @IBOutlet weak var r0c2: UIButton!
    
    @IBOutlet weak var r0c3: UIButton!
    
    @IBOutlet weak var r0c4: UIButton!
    
    @IBOutlet weak var r0c5: UIButton!
    
    @IBOutlet weak var r1c1: UIButton!
    
    @IBOutlet weak var r1c2: UIButton!
    
    @IBOutlet weak var r1c3: UIButton!
    
    @IBOutlet weak var r1c4: UIButton!
    
    
    @IBOutlet weak var r1c5: UIButton!
    
    @IBOutlet weak var r2c1: UIButton!
    
    @IBOutlet weak var r2c2: UIButton!
    @IBOutlet weak var r2c3: UIButton!
    
    @IBOutlet weak var r2c4: UIButton!
    
    @IBOutlet weak var r2c5: UIButton!
    
    @IBOutlet weak var r3c1: UIButton!
    
    @IBOutlet weak var r3c2: UIButton!
    
    @IBOutlet weak var r3c3: UIButton!
    
    
    @IBOutlet weak var r3c4: UIButton!
    
    
    @IBOutlet weak var r3c5: UIButton!
    @IBOutlet weak var r4c1: UIButton!
    
    @IBOutlet weak var r4c2: UIButton!
    
    @IBOutlet weak var r4c3: UIButton!
 
    
    @IBOutlet weak var r4c4: UIButton!
    
    
    @IBOutlet weak var r4c5: UIButton!
    
    @IBOutlet weak var r5c1: UIButton!
    
    @IBOutlet weak var r5c2: UIButton!
    
    @IBOutlet weak var r5c3: UIButton!
    
    @IBOutlet weak var r5c4: UIButton!
    
    @IBOutlet weak var r5c5: UIButton!
    var soundEffect :AVAudioPlayer!
    var firstSelected : UIButton!
    var winValue = 0
    var matchesCount = 0
    var tileImages = [UIImage]()
    var tilePieces = [GameTile]()
    var timer = Timer()
    var gameTime: Int!
    var secondsCount = 0
    var minutesCount = 0
    var gameTimeCopy: Int!
    let urlClick = Bundle.main.url(forResource: "selectSound", withExtension: "mp3")
    let urlCorrect = Bundle.main.url(forResource: "correctSound", withExtension: "mp3")
    let urlWin = Bundle.main.url(forResource: "winSound", withExtension: "mp3")
    let urlWrong = Bundle.main.url(forResource: "wrongSound", withExtension: "mp3")
    let urlLose = Bundle.main.url(forResource: "loseSound", withExtension: "mp3")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Creates a copy of my game time so i can reset it once the games over if they wanna play again
       gameTimeCopy = gameTime
        //Hides the end game screen and buttons for starting a new game or exiting
        GameOverIcon.isHidden = true
        Replay.isHidden = true
        ViewBackGround.isHidden = true
        ExitButton.isHidden = true
        ExitButton.isUserInteractionEnabled = false
        YouWinIcon.isHidden = true
        RestartButton.isUserInteractionEnabled = false
        RestartButton.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
     print("Hey this is a Iphone")
     //Sets the spaces for the iphone set up
             allSpaces.append(contentsOf: [r1c1,r1c2,r1c3,r1c4,r2c1,r2c2,r2c3,r2c4,r3c1,r3c2,r3c3,r3c4,r4c1,r4c2,r4c3,r4c4,r5c1,r5c2,r5c3,r5c4])
            winValue = allSpaces.count / 2
            
 
            
            
        case .pad:
            //Sets the spaces for a ipad set up
            allSpaces.append(contentsOf: [r0c1,r0c2,r0c3,r0c4,r0c5,r1c1,r1c2,r1c3,r1c4,r1c5,r2c1,r2c2,r2c3,r2c4,r2c5,r3c1,r3c2,r3c3,r3c4,r3c5,r4c1,r4c2,r4c3,r4c4,r4c5,r5c1,r5c2,r5c3,r5c4,r5c5])
           
           
           
        print("Hey this is a Ipad")
        case .unspecified:
          print("What is this o-o")
        default:
            print("I have no idea what you did")
        }
        //Creates the game board
         CreateBoard()
        //Sets the win value to half the total spaces
        winValue = allSpaces.count / 2
        for i in allSpaces{
            // Goes through each space and shows the hiddem image assigned to it before the game begins
            for t in tilePieces{
                if i.tag == t.buttonTag {
                    i.setBackgroundImage(t.hiddenImage, for: UIControlState.normal)
                    i.isUserInteractionEnabled = false
                }
            }
        }
        //Waits 5 seconds before hiding all the images and begining the game
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            for i in self.allSpaces{
                i.setBackgroundImage(#imageLiteral(resourceName: "Tile"), for: UIControlState.normal)
                i.isUserInteractionEnabled = true
            }
            //Begins the time
            self.runTimer()
        }
        
        
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Runs the time
    func runTimer() {
        //If the user selected no time limit then we will use the count up timer
        if gameTime == nil {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(GameViewController.countUpTimer)), userInfo: nil, repeats: true)
        }
        //Else we will use the time the user asked for
        else {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(GameViewController.countDownTimer)), userInfo: nil, repeats: true)
        }
    }
    //Counts up from 0 resetting the seconds when they reach 60 and adding a minute
    @objc func countUpTimer() {
        secondsCount += 1     //This will decrement(count down)the seconds.
        if secondsCount == 60{
            secondsCount = 0
            minutesCount += 1
            //Updates the minutes label to the user
            minutes.text = minutesCount.description
        }
        //Updates the seconds label to the user
        seconds.text = secondsCount.description //This will update the label.
        
    }
    //Counts down from the given game time
    @objc func countDownTimer(){
       
         seconds.text = gameTime.description
         gameTime! -= 1
        //When time is up we disable all buttons and the game ends in a loss
        if gameTime == -1 {
            for i in self.allSpaces{
                i.isUserInteractionEnabled = false
                
            }
            //Plays the loss sound effect to the user
            do{
                soundEffect = try AVAudioPlayer(contentsOf: urlLose! )
                soundEffect.prepareToPlay()
                soundEffect.play()
            }
                //If any errors occur theyll get printed out
            catch let error as NSError{
                print(error.localizedDescription)
            }
            //Stops the timer
            self.timer.invalidate()
            //Displays the end game screen with the game over icon
            GameOverIcon.isHidden = false
            self.RestartButton.isHidden = false
            self.RestartButton.isUserInteractionEnabled = true
            ViewBackGround.isHidden = false
            ExitButton.isHidden = false
            ExitButton.isUserInteractionEnabled = true
            YouWinIcon.isHidden = true
            Replay.isHidden = false
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   

 
    //Used for handing gameplay selection
    @IBAction func TileSelected(_ sender: Any) {
        do{
            //Plays the click sound effect
        soundEffect = try AVAudioPlayer(contentsOf: urlClick! )
        soundEffect.prepareToPlay()
        soundEffect.play()
        }
        catch let error as NSError{
            print(error.localizedDescription)
        }
        
        //If there is no current first selected button then we will assign it and reveal the hidden image
        if let button = sender as? UIButton{
            if firstSelected == nil {
                for i in tilePieces{
                    if i.buttonTag == button.tag{
                        button.setBackgroundImage(i.hiddenImage, for: UIControlState.normal)
                        button.isUserInteractionEnabled = false
                        firstSelected = button
                    }
                }
                
            }
                //Else if we already have one we will reveal the second selected image and begin to handle game rules
            else {
                for i in tilePieces{
                    if i.buttonTag == button.tag {
                    button.setBackgroundImage(i.hiddenImage, for: UIControlState.normal)
                        //Disables all buttons so the user cant click around while showing the two selected icons
                        for i in allSpaces{
                            i.isUserInteractionEnabled = false
                        }
                        //After showing the tiles for a second we will decide if they match or not
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            
                            //If they match we will get rid of both selected icons
                            if button.currentBackgroundImage == self.firstSelected.currentBackgroundImage{
                                for i in self.allSpaces{
                                    if i.tag == button.tag || i.tag == self.firstSelected.tag {
                                        
                                        i.isUserInteractionEnabled = false
                                        i.isHidden = true
                                       
                                    }
                                }
                                //We will also increment the matches count by 1 and see if they have won
                                self.firstSelected = nil
                                self.matchesCount += 1
                                do{
                                    self.soundEffect = try AVAudioPlayer(contentsOf: self.urlCorrect! )
                                    self.soundEffect.prepareToPlay()
                                    self.soundEffect.play()
                                }
                                catch let error as NSError{
                                    print(error.localizedDescription)
                                }
                                //If theyve won we will play the win sound effect and display the victory end game screen
                                if self.matchesCount == self.winValue {
                                    print("Game over")
                                    do{
                                        self.soundEffect = try AVAudioPlayer(contentsOf: self.urlWin! )
                                        self.soundEffect.prepareToPlay()
                                        self.soundEffect.play()
                                    }
                                    catch let error as NSError{
                                        print(error.localizedDescription)
                                    }
                                    self.GameOverIcon.isHidden = true
                                    self.Replay.isHidden = false
                                    self.timer.invalidate()
                                    self.ViewBackGround.isHidden = false
                                    self.ExitButton.isHidden = false
                                    self.ExitButton.isUserInteractionEnabled = true
                                    self.YouWinIcon.isHidden = false
                                    self.RestartButton.isHidden = false
                                    self.RestartButton.isUserInteractionEnabled = true
                                    
                                }
                                
                            }
                                //If they dont match we'll hide both selected images and they can try again
                            else {
                                for i in self.allSpaces {
                                    
                                    if i.tag == button.tag || i.tag == self.firstSelected.tag {
                                        i.setBackgroundImage(#imageLiteral(resourceName: "Tile"), for: UIControlState.normal)
                                        
                                    }
                                    
                                }
                                do{
                                    self.soundEffect = try AVAudioPlayer(contentsOf: self.urlWrong! )
                                    self.soundEffect.prepareToPlay()
                                    self.soundEffect.play()
                                }
                                catch let error as NSError{
                                    print(error.localizedDescription)
                                }
                                self.firstSelected = nil
                            }
                            for i in self.allSpaces{
                                i.isUserInteractionEnabled = true
                            }
                        }
                        
              
                    }
                }
            }
        }
    }
    
    //Stops the game and goes back
    @IBAction func StopGame(){
    
        self.dismiss(animated: true, completion: nil)
    }
    //Func used for creating the board
    func CreateBoard(){
        //Sets the board by creating new game tiles from the hidden images and the button tags
        var testBoard = 0
        print(tileImages.count)
        for image in tileImages{
            tilePieces.append(GameTile.init(hiddenImage: image, buttonTag: testBoard))
            testBoard += 1
           
        }
    }
   
    //MARK - Restart Game
    @IBAction func restart(){
        //Hides the end game icons and buttons
        GameOverIcon.isHidden = true
        matchesCount = 0
        gameTime = gameTimeCopy
        Replay.isHidden = true
        ViewBackGround.isHidden = true
        ExitButton.isHidden = true
        ExitButton.isUserInteractionEnabled = false
        YouWinIcon.isHidden = true
        self.RestartButton.isUserInteractionEnabled = false
        self.RestartButton.isHidden = true
        self.seconds.text = ""
        self.minutes.text = ""
        
        //Resets our tile pieces to be empty
        tilePieces = [GameTile]()
        //Shuffles the tile images again so we get a new game
        tileImages = Shuffle(Images: tileImages)
        for i in allSpaces{
            i.isUserInteractionEnabled = false
            i.isHidden = false
        }
        //Creates the board
        CreateBoard()
        //Shows all the hidden images again to the user before the game begins
        for i in allSpaces{
            for t in tilePieces{
                if i.tag == t.buttonTag {
                    i.setBackgroundImage(t.hiddenImage, for: UIControlState.normal)
                    i.isUserInteractionEnabled = false
                }
            }
        }
        //After 5 seconds the new game begins
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            for i in self.allSpaces{
                i.setBackgroundImage(#imageLiteral(resourceName: "Tile"), for: UIControlState.normal)
                i.isUserInteractionEnabled = true
            }
            //Resets the timers
            self.secondsCount = 0
            self.minutesCount = 0
            
            self.runTimer()
            
        }
    }
    //Shuffles an array of UIImages and spits it back out
    func Shuffle(Images: [UIImage]) -> [UIImage]{
        var imgCopy = Images
        var shuffledImages = [UIImage]()
        for _ in Images{
            //Goes through the given array and randomly selects 1 image, adds that image to the new array, and then removes it from the old one. This continues until the old array is empty.
            let random = Int(arc4random_uniform(UInt32(imgCopy.count)))
            shuffledImages.append(imgCopy[random])
            imgCopy.remove(at: random)
        }
        return shuffledImages
    }

}
