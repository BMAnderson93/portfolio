Overview
This is an involved project that will take some time to complete properly. Starting early is a must!

For this project you will be creating a simple memory game. If you�re unfamiliar with the game, I�m speaking of the children�s game with cards or tiles turned face down on the table. You may turn over two at a time, if they are a match you may remove them from the board. If not, they get turned facedown. You repeat until all of the cards have been matched and removed. You can find a free, somewhat poorly designed example here. This memory game is an example of complete functionality, but medium to low quality. Shoot for high, portfolio-ready quality.

A secondary purpose of this project is to have a complete, well-made iOS application to put in your portfolio. Put in the time and effort, and make something that you can be proud of!



Assignment Instructions
We�re purposefully not giving many detailed images or video examples for this project. Part of your grade will be the design, aesthetics, and general quality of your app. There is one student example video at the bottom of this activity to help you get an understanding of what is a very solid project. Please don't rip off his design or sound effects. There are plenty of ways to create a theme and feel for this project. Use your imagination!

You are required to use the adaptive layout system and leverage size classes as we have been doing in lecture. This is also a test on how well you can take the the skills that you�ve studied from APL to now and put together a complete project from beginning to end.

With the assignment we've included a set of free icon images below for you to use as your objects. Please use these icons or other icons/images that you hold (or have purchased) commercial rights for.

4.1.1MemoryIconImages.zip
18 MB


Special Requirements:
The minimum number of memory tiles on an iPhone is 20 (4x5 grid)
The minimum number of memory tiles on an iPad is 30 (5x6 grid)
If you have a different # of tiles on iPhone vs. iPad that counts as 1 size class change for the rubric.
When the game starts each tile is revealed to the user for ~5 seconds while being un-interactable. Think of this as a preview and a chance to memorize a few matches.
Images don�t skew, stretch, or squash on any devices/rotations.
Each tile has only 1 match on the board.
When chosen, Tiles reveal their icon.
Tile icons are not visible when they are �face down"
Successful matches are removed from the board.
When the app starts a �Play� button is presented to the user.
Tapping the Play button starts a timer counting up from Zero.
Timer is displayed to the user on screen while playing.
When all matches have been made:
Timer stops counting.
Some sort of simple game over / congratulations message is shown including final timer value and any other info you think might be relevant.
When the game is completed the user can replay the game.


Special Note:

On the rubric the Good column for Size Classes is asking for changes to your UI based on size classes. It is not asking that your UI just works unaltered on other layouts. By default it already should. Think about the color blender project. We made changes to our UI for 2 different size classes in order for our app to work and look better in those formats. The same is required in your project.