Overview
This assignment has multiple parts resulting in 2 projects to turn in. For the first part you will be adding onto the Color Blender lecture in order to complete alterations for the iPad version of the application. The second part will have you create a new temporary project and just turn in separate storyboard files. See that section for details.



Part 1: Assignment Instructions
We�ve Finished up our layouts for iPhones in portrait and landscape. For part 1 of this lab, you will complete the iPad layouts as specified below. The iPhone layouts we completed in lecture SHOULD NOT BE ALTERED by your completion of this lab. The point here is to make changes that don�t break other size class layouts. Remember to uninstall old constraints and install new constraints that fit your needs.




Change large blendViews from 60% to 30% proportional width for iPad Size Classes.
Change smaller blendViews to be 50% of the width of the larger blend views
Install Additional Blended Views in iPad layouts
Rename them with the same pattern as before with d2 and L2 respectively.
Keep the same spacing pattern as other blendedViews
Install the Info View for both iPad Layouts as shown in the example images
Info View to take up 40% of the width
Controls View takes up 60% of the width
All main views have 3pt spaced margins
Add Text �255" to textfields in Helvetica Neue 18 White
Change HexCode text font to Helvetica Neue 25.



Part 2: Assignment Instructions
Create a new project for Part 2, and turn in only the Storyboard files.

Create the layouts below using the adaptive layout techniques we�ve discussed in lecture. For each layout create a new storyboard (they can exist in the same project) and name them accordingly. The images provided are from each storyboard preview. Remember to turn in only the storyboard files.

Look at the requirements and formulate a plan before you begin working on a layout.

Special Requirements
All provided guidelines including colors have been matched exactly
Layout1.storyboard
Notes:

All padding is either 8pt or 1pt where present
Title View is 10% for the overall device height
Title Text is 40pt Bold
Normal Text sizes used include 14pt and 10pt
Colors Used:
White
Light Gray
Dark Gray
Black/White Text



Layout2.storyboard
Notes:

All padding is either 1pt or 3pt where present
Colors Used:
CEDDF2
69A8FF
8DE2F4
A5FF9B
FFB165
Black
