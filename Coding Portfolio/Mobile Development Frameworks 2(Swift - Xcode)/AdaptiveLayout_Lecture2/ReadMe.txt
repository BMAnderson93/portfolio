Overview
This assignment is practice in using your skills with Auto Layout to duplicate a UI layout example as closely as possible. Pay attention to small details! Pretend that a designer has given you these UI mockups from Adobe XD. Your job is to create this UI in Xcode exactly as seen in the examples. The designer has only given you a little bit of information for each layout, you�ll need to constantly do side-by-side comparisons of your work to these example images to get it right.

You�ll be turning in only the storyboard files for each layout, not entire projects. See below for an example on how best to accomplish this.

Assignment Instructions
Follow the guidelines and requirements below.
Create the layouts below using the adaptive layout techniques we�ve discussed in lecture. For each layout create a new storyboard (they can exist in the same project) and name them accordingly. The images provided are from each storyboard preview. Remember to turn in only the storyboard files.

 

Special Requirements

No Errors or Warnings of Any Kind
All constraints match the given values (if given)
Assistant editor preview exactly matches the images given

0:00 / 1:06
Watch closely for an easy way to setup this project and to zip up only storyboard files. ?Remember, you still have to do the assignment below! Don't just turn in empty storyboards.


Layout1.storyboard
Notes:

Center square padding = 5pts

Colors Used:

C7FF78
FFB7A2
FFAEFA
54DDFF
9FA0FF




Layout2.storyboard
Notes:

All padding is 5pt where present
Pink views are 5% of the screen height
Each pink view is 3/4 as wide as the previous view
Colors Used:
FFAEFA
54DDFF




Layout3.storyboard
Notes:

Colors Used:
orange - 255, 125, 20
teal - 79, 157, 164
red- 238, 93, 100
blue - 95, 125, 179
yellow - 255, 255, 33
purple 140, 40, 200
