//
//  DoubleExt.swift
//  AdaptiveLayout_Lecture1
//
//  Created by Brandon Anderson on 5/31/18.
//  Copyright © 2018 JamieBrown. All rights reserved.
//

import Foundation
//Extension for the double class, I use this for cleaning up the values that are doubles
//that have no numbers in any decimal places to not show any
extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
