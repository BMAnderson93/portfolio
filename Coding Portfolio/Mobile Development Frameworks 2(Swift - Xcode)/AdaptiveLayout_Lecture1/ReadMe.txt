Follow the guidelines and requirements below.
No Errors or Warnings of Any Kind in Your Project
Move Table View to the right side. (Proportions remain the same.)
The Title Bar has changed considerably. Figure out a good way to match the example images with what you've learned.
The Bottom Bar's size has been adjusted.
Info View Requirements:
All information relating to each element is displayed in the Info View
Tapping on TableViewCells populates the info panel correctly
Elements that don't have a Melting or Boiling point:
Display "N/A" for those values - (Do not alter the DataSource)