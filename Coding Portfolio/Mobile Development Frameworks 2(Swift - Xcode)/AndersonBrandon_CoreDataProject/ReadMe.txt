Overview
For this project you will be continuing work on your Adaptive Layout project. You will first need to fix all errors, and issues from your first turn-in. If you lost points for it, or I mentioned it in any of your feedback, it should get fixed. Next you will add appropriate controls, views, etc. to allow for users to save a high score. You will accomplish this data persistence with Core Data.



Assignment Instructions
Fix any issues expressed in project feedback from first turn in. Make improvements as necessary to improve the functionality and quality of your application.
Additional Features & Functionality
Add a prompt or additional view that allows user to input their name/initials
Add a new View Controller that will display at minimum the top 5 scores sorted by ascending completion time.
This view will display the name/initials, the time it took to complete, the number of moves the user made, and the date upon which this score was achieved.
Core Data Saving
You�ll need to save out the data for each completed puzzle in order to display it in the high scores view.
You�ll be saving all items displayed in the new tops scores ViewController Including:
User�s name/initials
Time to completion
Number of turns to completion
Timestamp of the date.
All info should be displayed for each high score in the high scores view.