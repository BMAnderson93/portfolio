//
//  HighScores.swift
//  AndersonBrandon_AdaptiveLayoutProject
//
//  Created by Brandon Anderson on 6/22/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation

class HighScoreC{
    var movesC : Int
    var timeC : Int
    var timeDisplayC : String
    var dateC : String
    var nameC : String
    
    
    init(moves : Int, time: Int, timeDisplay : String, date : String, name: String) {
        self.movesC = moves
        self.timeC = time
        self.timeDisplayC = timeDisplay
        self.dateC = date
        self.nameC = name
    }
    
    func DisplayMe() -> String{
        return "\(nameC) - Moves: \(movesC) Time: \(timeDisplayC) Date: \(dateC) "
    }
   
    
}
