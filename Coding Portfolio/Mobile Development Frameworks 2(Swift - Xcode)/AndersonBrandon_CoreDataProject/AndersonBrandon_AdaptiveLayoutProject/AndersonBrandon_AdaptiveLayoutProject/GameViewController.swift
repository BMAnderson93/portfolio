//
//  GameViewController.swift
//  AndersonBrandon_AdaptiveLayoutProject
//
//  Created by Brandon Anderson on 6/8/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

class GameViewController: UIViewController {
    @IBOutlet weak var GameOverIcon: UIImageView!
    
    @IBOutlet weak var HighScoresButton: UIButton!
    @IBOutlet weak var Replay: UIImageView!
    @IBOutlet weak var ViewBackGround: UIView!
    @IBOutlet weak var YouWinIcon: UIImageView!
    @IBOutlet weak var RestartButton: UIButton!
    
    @IBOutlet weak var ExitButton: UIButton!
    @IBOutlet weak var minutes: UILabel!
    @IBOutlet weak var seconds: UILabel!
    var allSpaces =  [UIButton]()
    
    
    @IBOutlet var GameButtonsIpad: [UIButton]!
    @IBOutlet var GameButtons: [UIButton]!

    var soundEffect :AVAudioPlayer!
    var firstSelected : UIButton!
    var winValue = 0
    var matchesCount = 0
    var tileImages = [UIImage]()
    var tilePieces = [GameTile]()
    var timer = Timer()
    var gameTime: Int!
    var secondsCount = 0
    var minutesCount = 0
    var gameTimeCopy: Int!
    let urlClick = Bundle.main.url(forResource: "selectSound", withExtension: "mp3")
    let urlCorrect = Bundle.main.url(forResource: "correctSound", withExtension: "mp3")
    let urlWin = Bundle.main.url(forResource: "winSound", withExtension: "mp3")
    let urlWrong = Bundle.main.url(forResource: "wrongSound", withExtension: "mp3")
    let urlLose = Bundle.main.url(forResource: "loseSound", withExtension: "mp3")
    
    var highScores :[HighScoreC] = []
    var moves = 0
    var timerCount = 0
    // CoreData Required Objects
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //ManagedObjectContext - Our Notepad, we write on the notepad and save it to the device
    
    private var managedContext :NSManagedObjectContext!
    
    //NSEntityDescription - Used to help build the entity by describing the specific Entity from the datamodel
    
    private var entityDescription: NSEntityDescription!
    
    //NSManagedObject - We will use this to represent the High Scores object
    //THIS IS WHERE OUR DATA LIVES everything else is just setup
    
    private var highScore1 :NSManagedObject!
    private var highScore2 :NSManagedObject!
    private var highScore3 :NSManagedObject!
    private var highScore4 :NSManagedObject!
    private var highScore5 :NSManagedObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        managedContext = appDelegate.managedObjectContext
        
        entityDescription = NSEntityDescription.entity(forEntityName: "HighScores", in: managedContext)
        
        //Creates a copy of my game time so i can reset it once the games over if they wanna play again
       gameTimeCopy = gameTime
        //Hides the end game screen and buttons for starting a new game or exiting
        HighScoresButton.isHidden = true
        HighScoresButton.isEnabled = false
        HighScoresButton.isUserInteractionEnabled = false
        GameOverIcon.isHidden = true
        Replay.isHidden = true
        ViewBackGround.isHidden = true
        ExitButton.isHidden = true
        ExitButton.isUserInteractionEnabled = false
        YouWinIcon.isHidden = true
        RestartButton.isUserInteractionEnabled = false
        RestartButton.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
        CheckForScores()
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
     print("Hey this is a Iphone")
     //Sets the spaces for the iphone set up
             allSpaces = GameButtonsIpad
            winValue = allSpaces.count / 2
            
 
            
            
        case .pad:
            //Sets the spaces for a ipad set up
            allSpaces = GameButtons
           
           
           
        print("Hey this is a Ipad")
        case .unspecified:
          print("What is this o-o")
        default:
            print("I have no idea what you did")
        }
        //Creates the game board
         CreateBoard()
        //Sets the win value to half the total spaces
        winValue = allSpaces.count / 2
        for i in allSpaces{
            // Goes through each space and shows the hiddem image assigned to it before the game begins
            for t in tilePieces{
                if i.tag == t.buttonTag {
                    i.setBackgroundImage(t.hiddenImage, for: UIControlState.normal)
                    i.isUserInteractionEnabled = false
                }
            }
        }
        //Waits 5 seconds before hiding all the images and begining the game
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            for i in self.allSpaces{
                i.setBackgroundImage(#imageLiteral(resourceName: "Tile"), for: UIControlState.normal)
                i.isUserInteractionEnabled = true
            }
            //Begins the time
            self.runTimer()
        }
        
        
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Runs the time
    func runTimer() {
        //If the user selected no time limit then we will use the count up timer
        if gameTime == nil {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(GameViewController.countUpTimer)), userInfo: nil, repeats: true)
        }
        //Else we will use the time the user asked for
        else {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(GameViewController.countDownTimer)), userInfo: nil, repeats: true)
        }
    }
    //Counts up from 0 resetting the seconds when they reach 60 and adding a minute
    @objc func countUpTimer() {
        secondsCount += 1     //This will decrement(count down)the seconds.
        if secondsCount == 60{
            secondsCount = 0
            minutesCount += 1
            //Updates the minutes label to the user
            minutes.text = minutesCount.description
        }
        //Updates the seconds label to the user
        seconds.text = secondsCount.description //This will update the label.
        
    }
    //Counts down from the given game time
    @objc func countDownTimer(){
       
         seconds.text = gameTime.description
         gameTime! -= 1
        //When time is up we disable all buttons and the game ends in a loss
        if gameTime == -1 {
            for i in self.allSpaces{
                i.isUserInteractionEnabled = false
                
            }
            //Plays the loss sound effect to the user
            do{
                soundEffect = try AVAudioPlayer(contentsOf: urlLose! )
                soundEffect.prepareToPlay()
                soundEffect.play()
            }
                //If any errors occur theyll get printed out
            catch let error as NSError{
                print(error.localizedDescription)
            }
            //Stops the timer
            self.timer.invalidate()
            //Displays the end game screen with the game over icon
            GameOverIcon.isHidden = false
            self.RestartButton.isHidden = false
            self.RestartButton.isUserInteractionEnabled = true
            ViewBackGround.isHidden = false
            ExitButton.isHidden = false
            ExitButton.isUserInteractionEnabled = true
            YouWinIcon.isHidden = true
            Replay.isHidden = false
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   

 
    //Used for handing gameplay selection
    @IBAction func TileSelected(_ sender: Any) {
        do{
            //Plays the click sound effect
        soundEffect = try AVAudioPlayer(contentsOf: urlClick! )
        soundEffect.prepareToPlay()
        soundEffect.play()
        }
        catch let error as NSError{
            print(error.localizedDescription)
        }
        
        //If there is no current first selected button then we will assign it and reveal the hidden image
        if let button = sender as? UIButton{
            if firstSelected == nil {
                for i in tilePieces{
                    if i.buttonTag == button.tag{
                        button.setBackgroundImage(i.hiddenImage, for: UIControlState.normal)
                        button.isUserInteractionEnabled = false
                        firstSelected = button
                        moves += 1
                        print(moves)
                    }
                }
                
            }
                //Else if we already have one we will reveal the second selected image and begin to handle game rules
            else {
                for i in tilePieces{
                    if i.buttonTag == button.tag {
                    button.setBackgroundImage(i.hiddenImage, for: UIControlState.normal)
                        //Disables all buttons so the user cant click around while showing the two selected icons
                        for i in allSpaces{
                            i.isUserInteractionEnabled = false
                        }
                        //After showing the tiles for a second we will decide if they match or not
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            
                            //If they match we will get rid of both selected icons
                            if button.currentBackgroundImage == self.firstSelected.currentBackgroundImage{
                                for i in self.allSpaces{
                                    if i.tag == button.tag || i.tag == self.firstSelected.tag {
                                        
                                        i.isUserInteractionEnabled = false
                                        i.isHidden = true
                                       
                                    }
                                }
                                //We will also increment the matches count by 1 and see if they have won
                                self.firstSelected = nil
                                self.matchesCount += 1
                                do{
                                    self.soundEffect = try AVAudioPlayer(contentsOf: self.urlCorrect! )
                                    self.soundEffect.prepareToPlay()
                                    self.soundEffect.play()
                                }
                                catch let error as NSError{
                                    print(error.localizedDescription)
                                }
                                //If theyve won we will play the win sound effect and display the victory end game screen
                                if self.matchesCount == self.winValue {
                                    print("Game over")
                                    do{
                                        self.soundEffect = try AVAudioPlayer(contentsOf: self.urlWin! )
                                        self.soundEffect.prepareToPlay()
                                        self.soundEffect.play()
                                    }
                                    catch let error as NSError{
                                        print(error.localizedDescription)
                                    }
                                    self.GameOverIcon.isHidden = true
                                    self.Replay.isHidden = false
                                    self.timer.invalidate()
                                    self.ViewBackGround.isHidden = false
                                    self.ExitButton.isHidden = false
                                    self.ExitButton.isUserInteractionEnabled = true
                                    self.YouWinIcon.isHidden = false
                                    self.RestartButton.isHidden = false
                                    self.RestartButton.isUserInteractionEnabled = true
                                    
                                    let alert = UIAlertController(title: "High Scores", message: "Enter your name or inital below!", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    let action = UIAlertAction(title: "Name Input", style: .default) { (alertAction) in
                                        let textField = alert.textFields![0] as UITextField
                                        var name = ""
                                        if (textField.text?.isEmpty)! {
                                            
                                            name = "NA"
                                            
                                            
                                        }
                                        else{
                                            name = textField.text!
                                        }
                                        let date = Date()
                                        let calendar = Calendar.current
                                        let currentDate = calendar.dateComponents([.year, .month, .day], from: date)
                                        let displayDate = (currentDate.year?.description)! + "/ " + (currentDate.month?.description)! + "/ " + (currentDate.day?.description)!
                                        
                                     
                                        let time = self.secondsCount + (self.minutesCount * 60)
                                        let timeDisplay = self.minutes.text! + ":" + self.seconds.text! 
                                        let insertNumber = self.CheckForHighScore(clicks: self.moves, time: time)
                                        self.highScores.insert(HighScoreC.init(moves: self.moves, time: time, timeDisplay: timeDisplay, date: displayDate, name: name), at: insertNumber)
                                        print(self.highScores.count)
                                        self.highScores.remove(at: self.highScores.count - 1)
                                        print(self.highScores.count)
                                        self.SaveHighScore()
                                        
                                        print(self.highScores[0].movesC.description + self.highScores[0].timeC.description + self.highScores[0].dateC)
                                    }
                                    
                                    alert.addTextField { (textField) in
                                        textField.placeholder = "Enter your name"
                                    }
                                    
                                    alert.addAction(action)
                                    if self.gameTime == nil {
                                        
                                    self.present(alert, animated: true, completion: nil)
                                        self.HighScoresButton.isHidden = false
                                        self.HighScoresButton.isEnabled = true
                                        self.HighScoresButton.isUserInteractionEnabled = true
                                    }
                                   
                                    
                                }
                                
                            }
                                //If they dont match we'll hide both selected images and they can try again
                            else {
                                for i in self.allSpaces {
                                    
                                    if i.tag == button.tag || i.tag == self.firstSelected.tag {
                                        i.setBackgroundImage(#imageLiteral(resourceName: "Tile"), for: UIControlState.normal)
                                        
                                    }
                                    
                                }
                                do{
                                    self.soundEffect = try AVAudioPlayer(contentsOf: self.urlWrong! )
                                    self.soundEffect.prepareToPlay()
                                    self.soundEffect.play()
                                }
                                catch let error as NSError{
                                    print(error.localizedDescription)
                                }
                                self.firstSelected = nil
                            }
                            for i in self.allSpaces{
                                i.isUserInteractionEnabled = true
                            }
                        }
                        
              
                    }
                }
            }
        }
    }
    
    //Stops the game and goes back
    @IBAction func StopGame(){
    
        self.dismiss(animated: true, completion: nil)
    }
    //Func used for creating the board
    func CreateBoard(){
        //Sets the board by creating new game tiles from the hidden images and the button tags
        var testBoard = 0
        print(tileImages.count)
        for image in tileImages{
            tilePieces.append(GameTile.init(hiddenImage: image, buttonTag: testBoard))
            testBoard += 1
           
        }
    }
   
    //MARK - Restart Game
    @IBAction func restart(){
        //Hides the end game icons and buttons
        GameOverIcon.isHidden = true
        HighScoresButton.isHidden = true
        HighScoresButton.isEnabled = false
        HighScoresButton.isUserInteractionEnabled = false
        matchesCount = 0
        gameTime = gameTimeCopy
        Replay.isHidden = true
        ViewBackGround.isHidden = true
        ExitButton.isHidden = true
        ExitButton.isUserInteractionEnabled = false
        YouWinIcon.isHidden = true
        self.RestartButton.isUserInteractionEnabled = false
        self.RestartButton.isHidden = true
        self.seconds.text = ""
        self.minutes.text = ""
        self.moves = 0
        
        //Resets our tile pieces to be empty
        tilePieces = [GameTile]()
        //Shuffles the tile images again so we get a new game
        tileImages = Shuffle(Images: tileImages)
        for i in allSpaces{
            i.isUserInteractionEnabled = false
            i.isHidden = false
        }
        //Creates the board
        CreateBoard()
        //Shows all the hidden images again to the user before the game begins
        for i in allSpaces{
            for t in tilePieces{
                if i.tag == t.buttonTag {
                    i.setBackgroundImage(t.hiddenImage, for: UIControlState.normal)
                    i.isUserInteractionEnabled = false
                }
            }
        }
        //After 5 seconds the new game begins
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            for i in self.allSpaces{
                i.setBackgroundImage(#imageLiteral(resourceName: "Tile"), for: UIControlState.normal)
                i.isUserInteractionEnabled = true
            }
            //Resets the timers
            self.secondsCount = 0
            self.minutesCount = 0
            
            self.runTimer()
            
        }
    }
    //Shuffles an array of UIImages and spits it back out
    func Shuffle(Images: [UIImage]) -> [UIImage]{
        var imgCopy = Images
        var shuffledImages = [UIImage]()
        for _ in Images{
            //Goes through the given array and randomly selects 1 image, adds that image to the new array, and then removes it from the old one. This continues until the old array is empty.
            let random = Int(arc4random_uniform(UInt32(imgCopy.count)))
            shuffledImages.append(imgCopy[random])
            imgCopy.remove(at: random)
        }
        return shuffledImages
    }
    
    func CheckForScores(){
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "HighScores")
        do {
            let results = try managedContext.fetch(fetchRequest)
            print(results.count)
            var temp :[NSManagedObject] = []
            for obj in results{
                print(obj.description)
                temp.append(obj)
                
                
            }
            if temp.count != 0 {
                highScore1 = temp[0]
                highScore2 = temp[1]
                highScore3 = temp[2]
                highScore4 = temp[3]
                highScore5 = temp[4]
            }
           
           
            if highScore1 != nil  && highScore2 != nil && highScore3 != nil && highScore4 != nil && highScore5 != nil{
                print("This isnt our first time doing this")
                let allScores = [highScore1,highScore2,highScore3,highScore4,highScore5]
                print(highScore1.value(forKey: "name")!)
                for score in allScores{
                    let moveS = score?.value(forKey: "turns") as? Int
                    let timeS = score?.value(forKey: "time") as? Int
                    let timeD = score?.value(forKey: "timeDisplay") as? String
                    let dateS = score?.value(forKey: "date") as? String
                    let nameS = score?.value(forKey: "name") as? String
                    highScores.append(HighScoreC.init(moves: moveS!, time: timeS!, timeDisplay: timeD!, date: dateS!, name: nameS!))
                }
                
            }
            else {
                print("We Havent done this yet")
                highScore1 = NSManagedObject(entity: entityDescription, insertInto: managedContext)
                highScore2 = NSManagedObject(entity: entityDescription, insertInto: managedContext)
                highScore3 = NSManagedObject(entity: entityDescription, insertInto: managedContext)
                highScore4 = NSManagedObject(entity: entityDescription, insertInto: managedContext)
                highScore5 = NSManagedObject(entity: entityDescription, insertInto: managedContext)
                let allScores = [highScore1,highScore2,highScore3,highScore4,highScore5]
                
                for score in allScores{
                    score?.setValue("NA", forKey: "name")
                    score?.setValue("NA", forKey: "date")
                    score?.setValue(0, forKey: "time")
                    score?.setValue("0", forKey: "timeDisplay")
                    score?.setValue(0, forKey: "turns")
                }
                appDelegate.saveContext()
                for score in allScores{
                    let moveS = score?.value(forKey: "turns") as? Int
                    let timeS = score?.value(forKey: "time") as? Int
                    let timeD = score?.value(forKey: "timeDisplay") as? String
                    let dateS = score?.value(forKey: "date") as? String
                    let nameS = score?.value(forKey: "name") as? String
                    highScores.append(HighScoreC.init(moves: moveS!, time: timeS!, timeDisplay: timeD!, date: dateS!, name: nameS!))
                }
            }
            
            
        }
        catch{
            assertionFailure()
        }
    }

    func CheckForHighScore(clicks: Int, time: Int) -> Int{
        var counter = 0
        for score in highScores{
            if score.movesC == 0 || clicks < score.movesC{
                 print("We are going to override score number \(counter)")
                return counter
            }
            else if score.movesC == clicks && time < score.timeC{
                 print("We are going to override score number \(counter)")
                return counter
            }
            counter += 1
        }
        print("We will not be changing any of the scores")
        return highScores.count
    }
    func SaveHighScore(){
        
        var counter = 0
        
        for newScore in highScores{
            switch counter {
            case 0:
                print(newScore.nameC)
                highScore1.setValue(newScore.nameC, forKey: "name")
                highScore1.setValue(newScore.dateC, forKey: "date")
                highScore1.setValue(newScore.timeC, forKey: "time")
                highScore1.setValue(newScore.timeDisplayC, forKey: "timeDisplay")
                highScore1.setValue(newScore.movesC, forKey: "turns")
            case 1:
                print(newScore.nameC)
                highScore2.setValue(newScore.nameC, forKey: "name")
                highScore2.setValue(newScore.dateC, forKey: "date")
                highScore2.setValue(newScore.timeC, forKey: "time")
                highScore2.setValue(newScore.timeDisplayC, forKey: "timeDisplay")
                highScore2.setValue(newScore.movesC, forKey: "turns")
            case 2:
                print(newScore.nameC)
                highScore3.setValue(newScore.nameC, forKey: "name")
                highScore3.setValue(newScore.dateC, forKey: "date")
                highScore3.setValue(newScore.timeC, forKey: "time")
                highScore3.setValue(newScore.timeDisplayC, forKey: "timeDisplay")
                highScore3.setValue(newScore.movesC, forKey: "turns")
            case 3:
                print(newScore.nameC)
                highScore4.setValue(newScore.nameC, forKey: "name")
                highScore4.setValue(newScore.dateC, forKey: "date")
                highScore4.setValue(newScore.timeC, forKey: "time")
                highScore4.setValue(newScore.timeDisplayC, forKey: "timeDisplay")
                highScore4.setValue(newScore.movesC, forKey: "turns")
            case 4:
                print(newScore.nameC)
                highScore5.setValue(newScore.nameC, forKey: "name")
                highScore5.setValue(newScore.dateC, forKey: "date")
                highScore5.setValue(newScore.timeC, forKey: "time")
                highScore5.setValue(newScore.timeDisplayC, forKey: "timeDisplay")
                highScore5.setValue(newScore.movesC, forKey: "turns")
            default:
                print("No new high score")
            }
            counter += 1
            
        }
       
        appDelegate.saveContext()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let svc = segue.destination as? HighScoreViewController{
            svc.highScores = self.highScores
        }
    }
}
