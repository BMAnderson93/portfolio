//
//  HighScoreViewController.swift
//  AndersonBrandon_AdaptiveLayoutProject
//
//  Created by Brandon Anderson on 6/22/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class HighScoreViewController: UIViewController {

    var highScores: [HighScoreC] = []
    
    @IBOutlet var HighScores: [UILabel]!
    override func viewDidLoad() {
        super.viewDidLoad()
        CreateLeaderBoard()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func CreateLeaderBoard(){
        var counter = 0
        for score in highScores{
            HighScores[counter].text = score.DisplayMe()
            counter += 1
        }
    }
    
    @IBAction func Close(){
    self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
