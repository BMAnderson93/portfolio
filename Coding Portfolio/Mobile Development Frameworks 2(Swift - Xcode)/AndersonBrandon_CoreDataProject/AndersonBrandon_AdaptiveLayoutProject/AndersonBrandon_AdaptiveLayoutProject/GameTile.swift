//
//  GameTile.swift
//  AndersonBrandon_AdaptiveLayoutProject
//
//  Created by Brandon Anderson on 6/10/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import Foundation
import UIKit

class GameTile{
    //Hiddem image for face down tiles and a tag to match it to a button
    var hiddenImage: UIImage
    var buttonTag: Int
    
    init(hiddenImage: UIImage, buttonTag: Int) {
        self.hiddenImage = hiddenImage
        self.buttonTag = buttonTag
    }
}
