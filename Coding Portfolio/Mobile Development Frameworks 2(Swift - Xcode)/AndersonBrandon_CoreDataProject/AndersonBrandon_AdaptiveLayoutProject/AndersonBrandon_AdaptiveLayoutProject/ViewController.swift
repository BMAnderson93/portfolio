//
//  ViewController.swift
//  AndersonBrandon_AdaptiveLayoutProject
//
//  Created by Brandon Anderson on 6/6/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var Timer: UISegmentedControl!
    @IBOutlet weak var ImagePreview: UIImageView!
    //Creates all of our icons and icon previews for either Ipad or Iphone
    var magicIconsSmall = [#imageLiteral(resourceName: "t0"),#imageLiteral(resourceName: "t0"),#imageLiteral(resourceName: "t1"),#imageLiteral(resourceName: "t1"),#imageLiteral(resourceName: "t2"),#imageLiteral(resourceName: "t2"),#imageLiteral(resourceName: "t3"),#imageLiteral(resourceName: "t3"),#imageLiteral(resourceName: "t4"),#imageLiteral(resourceName: "t4"),#imageLiteral(resourceName: "t5"),#imageLiteral(resourceName: "t5"),#imageLiteral(resourceName: "t6"),#imageLiteral(resourceName: "t6"),#imageLiteral(resourceName: "t7"),#imageLiteral(resourceName: "t7"),#imageLiteral(resourceName: "t8"),#imageLiteral(resourceName: "t8"),#imageLiteral(resourceName: "t10"),#imageLiteral(resourceName: "t10")]
    var largeMagicIcon = [#imageLiteral(resourceName: "t0"),#imageLiteral(resourceName: "t0"),#imageLiteral(resourceName: "t1"),#imageLiteral(resourceName: "t1"),#imageLiteral(resourceName: "t2"),#imageLiteral(resourceName: "t2"),#imageLiteral(resourceName: "t3"),#imageLiteral(resourceName: "t3"),#imageLiteral(resourceName: "t4"),#imageLiteral(resourceName: "t4"),#imageLiteral(resourceName: "t5"),#imageLiteral(resourceName: "t5"),#imageLiteral(resourceName: "t6"),#imageLiteral(resourceName: "t6"),#imageLiteral(resourceName: "t7"),#imageLiteral(resourceName: "t7"),#imageLiteral(resourceName: "t8"),#imageLiteral(resourceName: "t8"),#imageLiteral(resourceName: "t10"),#imageLiteral(resourceName: "t10"),#imageLiteral(resourceName: "t11"),#imageLiteral(resourceName: "t11"),#imageLiteral(resourceName: "t12"),#imageLiteral(resourceName: "t12"),#imageLiteral(resourceName: "t13"),#imageLiteral(resourceName: "t13"),#imageLiteral(resourceName: "t14"),#imageLiteral(resourceName: "t14"),#imageLiteral(resourceName: "t9"),#imageLiteral(resourceName: "t9")]
    var magicIconsSmallPreview = #imageLiteral(resourceName: "SmallMagic")
    var magicIconsLargePreview = #imageLiteral(resourceName: "LargeMagicIcons")
    var musicIconsSmall = [#imageLiteral(resourceName: "m2"),#imageLiteral(resourceName: "m2"),#imageLiteral(resourceName: "m3"),#imageLiteral(resourceName: "m3"),#imageLiteral(resourceName: "m4"),#imageLiteral(resourceName: "m4"),#imageLiteral(resourceName: "m5"),#imageLiteral(resourceName: "m5"),#imageLiteral(resourceName: "m6"),#imageLiteral(resourceName: "m6"),#imageLiteral(resourceName: "m7"),#imageLiteral(resourceName: "m7"),#imageLiteral(resourceName: "m8"),#imageLiteral(resourceName: "m8"),#imageLiteral(resourceName: "m9"),#imageLiteral(resourceName: "m9"),#imageLiteral(resourceName: "m13"),#imageLiteral(resourceName: "m13"),#imageLiteral(resourceName: "m14"),#imageLiteral(resourceName: "m14")]
    var musicIconsLarge = [#imageLiteral(resourceName: "m0"),#imageLiteral(resourceName: "m0"),#imageLiteral(resourceName: "m1"),#imageLiteral(resourceName: "m1"),#imageLiteral(resourceName: "m2"),#imageLiteral(resourceName: "m2"),#imageLiteral(resourceName: "m3"),#imageLiteral(resourceName: "m3"),#imageLiteral(resourceName: "m4"),#imageLiteral(resourceName: "m4"),#imageLiteral(resourceName: "m5"),#imageLiteral(resourceName: "m5"),#imageLiteral(resourceName: "m6"),#imageLiteral(resourceName: "m6"),#imageLiteral(resourceName: "m7"),#imageLiteral(resourceName: "m7"),#imageLiteral(resourceName: "m8"),#imageLiteral(resourceName: "m8"),#imageLiteral(resourceName: "m9"),#imageLiteral(resourceName: "m9"),#imageLiteral(resourceName: "m10"),#imageLiteral(resourceName: "m10"),#imageLiteral(resourceName: "m11"),#imageLiteral(resourceName: "m11"),#imageLiteral(resourceName: "m12"),#imageLiteral(resourceName: "m12"),#imageLiteral(resourceName: "m13"),#imageLiteral(resourceName: "m13"),#imageLiteral(resourceName: "m14"),#imageLiteral(resourceName: "m14")]
    var musicIconsSmallPreview = #imageLiteral(resourceName: "SmallMusicIcons")
    var musicIconsLargePreview = #imageLiteral(resourceName: "LargeMusicIcons")
    var natureIconsSmall = [#imageLiteral(resourceName: "n0"),#imageLiteral(resourceName: "n1"),#imageLiteral(resourceName: "n2"),#imageLiteral(resourceName: "n3"),#imageLiteral(resourceName: "n4"),#imageLiteral(resourceName: "n5"),#imageLiteral(resourceName: "n6"),#imageLiteral(resourceName: "n7"),#imageLiteral(resourceName: "n8"),#imageLiteral(resourceName: "n9"),#imageLiteral(resourceName: "n0"),#imageLiteral(resourceName: "n1"),#imageLiteral(resourceName: "n2"),#imageLiteral(resourceName: "n3"),#imageLiteral(resourceName: "n4"),#imageLiteral(resourceName: "n5"),#imageLiteral(resourceName: "n6"),#imageLiteral(resourceName: "n7"),#imageLiteral(resourceName: "n8"),#imageLiteral(resourceName: "n9")]
    var natureIconsLarge = [#imageLiteral(resourceName: "n0"),#imageLiteral(resourceName: "n1"),#imageLiteral(resourceName: "n2"),#imageLiteral(resourceName: "n3"),#imageLiteral(resourceName: "n4"),#imageLiteral(resourceName: "n5"),#imageLiteral(resourceName: "n6"),#imageLiteral(resourceName: "n7"),#imageLiteral(resourceName: "n8"),#imageLiteral(resourceName: "n9"),#imageLiteral(resourceName: "n0"),#imageLiteral(resourceName: "n1"),#imageLiteral(resourceName: "n2"),#imageLiteral(resourceName: "n3"),#imageLiteral(resourceName: "n4"),#imageLiteral(resourceName: "n5"),#imageLiteral(resourceName: "n6"),#imageLiteral(resourceName: "n7"),#imageLiteral(resourceName: "n8"),#imageLiteral(resourceName: "n9"),#imageLiteral(resourceName: "n10"),#imageLiteral(resourceName: "n10"),#imageLiteral(resourceName: "n11"),#imageLiteral(resourceName: "n11"),#imageLiteral(resourceName: "n12"),#imageLiteral(resourceName: "n12"),#imageLiteral(resourceName: "n13"),#imageLiteral(resourceName: "n13"),#imageLiteral(resourceName: "n14"),#imageLiteral(resourceName: "n14")]
    var natureIconsSmallPreview = #imageLiteral(resourceName: "SmallNatureIcons")
    var natureIconsLargePreview = #imageLiteral(resourceName: "LargeNatureIcons")
    var weaponsIconsSmall = [#imageLiteral(resourceName: "w0"),#imageLiteral(resourceName: "w1"),#imageLiteral(resourceName: "w2"),#imageLiteral(resourceName: "w3"),#imageLiteral(resourceName: "w4"),#imageLiteral(resourceName: "w5"),#imageLiteral(resourceName: "w6"),#imageLiteral(resourceName: "w7"),#imageLiteral(resourceName: "w8"),#imageLiteral(resourceName: "w9"),#imageLiteral(resourceName: "w0"),#imageLiteral(resourceName: "w1"),#imageLiteral(resourceName: "w2"),#imageLiteral(resourceName: "w3"),#imageLiteral(resourceName: "w4"),#imageLiteral(resourceName: "w5"),#imageLiteral(resourceName: "w6"),#imageLiteral(resourceName: "w7"),#imageLiteral(resourceName: "w8"),#imageLiteral(resourceName: "w9")]
    var weaponsIconsLarge = [#imageLiteral(resourceName: "w0"),#imageLiteral(resourceName: "w1"),#imageLiteral(resourceName: "w2"),#imageLiteral(resourceName: "w3"),#imageLiteral(resourceName: "w4"),#imageLiteral(resourceName: "w5"),#imageLiteral(resourceName: "w6"),#imageLiteral(resourceName: "w7"),#imageLiteral(resourceName: "w8"),#imageLiteral(resourceName: "w9"),#imageLiteral(resourceName: "w0"),#imageLiteral(resourceName: "w1"),#imageLiteral(resourceName: "w2"),#imageLiteral(resourceName: "w3"),#imageLiteral(resourceName: "w4"),#imageLiteral(resourceName: "w5"),#imageLiteral(resourceName: "w6"),#imageLiteral(resourceName: "w7"),#imageLiteral(resourceName: "w8"),#imageLiteral(resourceName: "w9"),#imageLiteral(resourceName: "w10"),#imageLiteral(resourceName: "w10"),#imageLiteral(resourceName: "w11"),#imageLiteral(resourceName: "w11"),#imageLiteral(resourceName: "w12"),#imageLiteral(resourceName: "w12"),#imageLiteral(resourceName: "w13"),#imageLiteral(resourceName: "w13"),#imageLiteral(resourceName: "w14"),#imageLiteral(resourceName: "w14")]
    var weaponsIconsSmallPreview = #imageLiteral(resourceName: "Smallweapons")
    var weaponsIconsLargePreview = #imageLiteral(resourceName: "Largeweapons")
    var gameIconsSmall = [#imageLiteral(resourceName: "g0"),#imageLiteral(resourceName: "g0"),#imageLiteral(resourceName: "g1"),#imageLiteral(resourceName: "g1"),#imageLiteral(resourceName: "g2"),#imageLiteral(resourceName: "g2"),#imageLiteral(resourceName: "g3"),#imageLiteral(resourceName: "g3"),#imageLiteral(resourceName: "g4"),#imageLiteral(resourceName: "g4"),#imageLiteral(resourceName: "g5"),#imageLiteral(resourceName: "g5"),#imageLiteral(resourceName: "g6"),#imageLiteral(resourceName: "g6"),#imageLiteral(resourceName: "g7"),#imageLiteral(resourceName: "g7"),#imageLiteral(resourceName: "g8"),#imageLiteral(resourceName: "g8"),#imageLiteral(resourceName: "g9"),#imageLiteral(resourceName: "g9")]
    var gameIconsLarge = [#imageLiteral(resourceName: "g0"),#imageLiteral(resourceName: "g0"),#imageLiteral(resourceName: "g1"),#imageLiteral(resourceName: "g1"),#imageLiteral(resourceName: "g2"),#imageLiteral(resourceName: "g2"),#imageLiteral(resourceName: "g3"),#imageLiteral(resourceName: "g3"),#imageLiteral(resourceName: "g4"),#imageLiteral(resourceName: "g4"),#imageLiteral(resourceName: "g5"),#imageLiteral(resourceName: "g5"),#imageLiteral(resourceName: "g6"),#imageLiteral(resourceName: "g6"),#imageLiteral(resourceName: "g7"),#imageLiteral(resourceName: "g7"),#imageLiteral(resourceName: "g8"),#imageLiteral(resourceName: "g8"),#imageLiteral(resourceName: "g9"),#imageLiteral(resourceName: "g9"),#imageLiteral(resourceName: "g10"),#imageLiteral(resourceName: "g10"),#imageLiteral(resourceName: "g11"),#imageLiteral(resourceName: "g11"),#imageLiteral(resourceName: "g12"),#imageLiteral(resourceName: "g12"),#imageLiteral(resourceName: "g13"),#imageLiteral(resourceName: "g13"),#imageLiteral(resourceName: "g14"),#imageLiteral(resourceName: "g14")]
    var gameIconsSmallPreview = #imageLiteral(resourceName: "SmallGameIcons")
    var gameIconsLargePreview = #imageLiteral(resourceName: "LargeGameIcons")
    
    var soundEffect :AVAudioPlayer!
    
    var allImageSets = [[UIImage]]()
    var displayImageSets = [UIImage]()
    var chosenSet = 0
    
     let urlNext = Bundle.main.url(forResource: "nextSound", withExtension: "mp3")
    
    
    // CoreData Required Objects
    
    //ManagedObjectContext - Our Notepad, we write on the notepad and save it to the device
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            //If were on a iphone then we set our display images and preview images to match the small versions
            displayImageSets += [magicIconsSmallPreview,musicIconsSmallPreview,natureIconsSmallPreview,weaponsIconsSmallPreview,gameIconsSmallPreview]
            allImageSets.append(magicIconsSmall)
            allImageSets.append(musicIconsSmall)
            allImageSets.append(natureIconsSmall)
            allImageSets.append(weaponsIconsSmall)
            allImageSets.append(gameIconsSmall)
            ImagePreview.image = magicIconsSmallPreview
            print("Hey this is a Iphone")
           
        case .pad:
            //If were on a ipad then we use the larger icons
            displayImageSets += [magicIconsLargePreview, musicIconsLargePreview, natureIconsLargePreview, weaponsIconsLargePreview, gameIconsLargePreview ]
            
            allImageSets.append(largeMagicIcon)
            allImageSets.append(musicIconsLarge)
            allImageSets.append(natureIconsLarge)
            allImageSets.append(weaponsIconsLarge)
            allImageSets.append(gameIconsLarge)
            ImagePreview.image = magicIconsLargePreview
            
            print("Hey this is a Ipad")
        case .unspecified:
            print("What is this o-o")
        default:
            print("I have no idea what you did")
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Sends over the game time and the selected image set to begin the game
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let svc = segue.destination as? GameViewController{
            //Shuffles the images before sending
            svc.tileImages = Shuffle(Images: allImageSets[chosenSet])
            switch Timer.selectedSegmentIndex{
            case 1 :
            svc.gameTime = 60
            case 2:
                svc.gameTime = 45
            case 3:
                svc.gameTime = 30
                
           
            default:
                print("Hi")
            }
        }
       
    }
    //Shuffles an array of UIImages and spits it back out
    func Shuffle(Images: [UIImage]) -> [UIImage]{
        var imgCopy = Images
        print(Images.count)
        var shuffledImages = [UIImage]()
        for _ in Images{
            //Goes through the given array and randomly selects 1 image, adds that image to the new array, and then removes it from the old one. This continues until the old array is empty.
            let random = Int(arc4random_uniform(UInt32(imgCopy.count)))
            shuffledImages.append(imgCopy[random])
            imgCopy.remove(at: random)
        }
        print(shuffledImages.count)
        return shuffledImages
    }
   
    //Changes the selected image set
    @IBAction func ChangeSelection(_ sender: Any) {
        do{
            soundEffect = try AVAudioPlayer(contentsOf: urlNext! )
            soundEffect.prepareToPlay()
            soundEffect.play()
        }
        catch let error as NSError{
            print(error.localizedDescription)
        }
        //Switches based of the tag
        if let button = sender as? UIButton{
        switch button.tag  {
        case 1:
            //Goes to the previous image set and to the last image set if were at the begining
            if chosenSet == displayImageSets.count - 1 {
                chosenSet = 0
            }
            else{
                chosenSet += 1
            }
            ImagePreview.image = displayImageSets[chosenSet]
            
        default:
            //Goes to the next image set and to the first image set if were at the end
            if chosenSet == 0 {
                chosenSet = displayImageSets.count - 1
            }
            else{
                chosenSet -= 1
            }
            ImagePreview.image = displayImageSets[chosenSet]
        }
        }
        
    }
    

}

