Overview
You�ll be creating a simple Rock, Paper, Scissors application to be played with other nearby users. Connection and gameplay will occur over a local data connection using Apple's Multipeer Framework. You will be responsible for implementing the game functionality as well as all of the functionality for connecting, sending, and receiving data over the local connection.

Assignment Instructions
Your app will be graded based on connecting and behaving properly with your application running on another device. Read the rubric carefully to understand the full requirements of the assignment. You can test this as you build, by building to 2 different simulators such as the iPhone 8 and iPhone 8 Plus

Special Requirements:

Proper use of Adaptive Layout techniques are utilized.
Your application looks and performs correctly on all devices and layouts 4� and larger.
Game can be replayed while there is a connection
Players can not make a choice until both players hit some version of �Play� each round.
Players are then given 3 seconds in which to choose.
To try and mimic the 1..2..3..SHOOT experience of RPS in real life.
Player choices aren�t revealed until both have locked in a choice or the timer is up.
When choices are revealed a win/lose indication is given to each player.
A score is kept and displayed for each session indicating the number of wins, losses, and ties against the current opponent.
Student Example
The below example is an above average, but not perfect example of this project. His use of space on the landscape layout could have been better, and though not shown there were some small issues with his iPad layout and code as well. This does a great job of highlighting the quality level that you should aim for however.