//
//  ViewController.swift
//  AndersonBrandon_MultipeerConnectivityProject
//
//  Created by Brandon Anderson on 6/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ViewController: UIViewController, MCBrowserViewControllerDelegate,MCSessionDelegate {
  
    
    
    //Session and peer ID
    var peerID:MCPeerID!
    
    var session:MCSession!
    
    var browser:MCBrowserViewController!
    
    var advertiser:MCAdvertiserAssistant!
    
    //Outlets
    @IBOutlet weak var opponentChoiceImage: UIButton!
    @IBOutlet weak var playChoiceImage: UIButton!
    @IBOutlet weak var displayWLT: UILabel!
    @IBOutlet weak var WinLabel: UILabel!
    @IBOutlet weak var LossLabel: UILabel!
    @IBOutlet weak var TieLabel: UILabel!
    @IBOutlet weak var WLTLabel: UILabel!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var countDownLabel: UILabel!
    @IBOutlet weak var rockButton: UIButton!
    @IBOutlet weak var scissorButton: UIButton!
    @IBOutlet weak var paperButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var connectedLabel: UILabel!
    @IBOutlet weak var connectionStatus: UILabel!
  
    
    @IBOutlet weak var waitingOpponentLabel: UILabel!
    @IBOutlet weak var waitingMeLabel: UILabel!
    //Varaibles that allow the game to begin
    var selfReady = false
    var oppenentReady = false
    //Variables for the score
    var wins = 0
    var loss = 0
    var ties = 0
    //Variables for the timer
    var time = 3
    var timer = Timer()
    
    let serviceID = "rps-lobby"
    var chosenFighter = "rock"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Creates our peer id and all the session information then begins advertising
        peerID = MCPeerID(displayName: UIDevice.current.name)
        
        session = MCSession(peer:peerID, securityIdentity: nil, encryptionPreference: MCEncryptionPreference.none)
        
        session.delegate = self
       
        advertiser = MCAdvertiserAssistant(serviceType: serviceID, discoveryInfo: nil, session: session)
        
        advertiser.start()
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        browserViewController.dismiss(animated: true, completion: nil)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        browserViewController.dismiss(animated: true, completion: nil)
    }
    // Delegate methods for MCSession.
  
        
        
        // Remote peer changed state.
        
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState){
        
            DispatchQueue.main.async {
                
              
               //If we have a connection then we know we can enable the play button and allow games to begin
                    if session.connectedPeers.count == 1{
                        
                        self.connectedLabel.isHidden = false
                        self.connectionStatus.isHidden = true
                        self.playButton.isEnabled = true
                        self.playButton.isHidden  = false
                        self.playButton.isUserInteractionEnabled = true
                        self.connectButton.isHidden = true
                        
                        
                    }
                        //Else if we dont then we need to make sure all the game assets are hidden and reset to their original values
                    else {
                        self.connectedLabel.isHidden = true
                        self.connectionStatus.isHidden = false
                        self.playButton.isUserInteractionEnabled = false
                         self.playButton.isEnabled = false
                        self.playButton.isHidden = true
                         self.connectButton.isHidden = false
                        self.scissorButton.isHidden = true
                        self.paperButton.isHidden = true
                        self.rockButton.isHidden = true
                        self.waitingMeLabel.isHidden = true
                        self.waitingOpponentLabel.isHidden = true
                        self.playChoiceImage.isHidden = true
                        self.opponentChoiceImage.isHidden  = true
                        self.WLTLabel.isHidden = true
                        self.displayWLT.isHidden = true
                        self.countDownLabel.isHidden = true
                        self.WinLabel.isHidden = true
                        self.LossLabel.isHidden = true
                        self.TieLabel.isHidden = true
                        self.wins = 0
                        self.loss = 0
                        self.ties = 0
                        self.time = 3
                        self.selfReady = false
                        self.oppenentReady = false
                    }
                
            }
        
        }
        
        
        // Received data from remote peer.
        
        func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID){
           
            // If the sent message is "ready" then we will just update the status of that player
            if let recievedMessage:String = String(data: data, encoding: String.Encoding.utf8){
                if recievedMessage == "ready"{
                    DispatchQueue.main.async {
                        self.oppenentReady = true
                        // if were also ready we will begin the game
                        if self.selfReady  == true{
                            self.enableButtons()
                            self.timer.invalidate()
                            
                            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerFunc), userInfo: nil, repeats: true)
                        }
                        else {
                            self.waitingMeLabel.isHidden = false
                        }
                    }
                    
                  
                }
                    //If the sent message is not ready, then we know its to begin the game
                else {
                    DispatchQueue.main.async {
                        
                    // Checks both fighters and decides who wins based on the rules of rock paper scissors and incremements the correct score by 1
                    if recievedMessage == self.chosenFighter{
                        print("tie")
                        self.ties += 1
                        self.TieLabel.isHidden = false
                    }
                    else if recievedMessage == "rock" && self.chosenFighter == "scissors"{
                        print("enemy wins")
                        self.loss += 1
                        self.LossLabel.isHidden = false
                        
                    }
                    else if recievedMessage == "rock" && self.chosenFighter == "paper"{
                        print("you win")
                        self.wins += 1
                         self.WinLabel.isHidden = false
                    }
                    else if recievedMessage == "scissors" && self.chosenFighter == "rock"{
                         print("you win")
                         self.wins += 1
                         self.WinLabel.isHidden = false
                    }
                    else if recievedMessage == "scissors" && self.chosenFighter == "paper"{
                         print("enemy wins")
                         self.loss += 1
                       self.LossLabel.isHidden = false
                    }
                    else if recievedMessage == "paper" && self.chosenFighter == "rock"{
                         print("enemy wins")
                         self.loss += 1
                        self.LossLabel.isHidden = false
                    }
                    else if recievedMessage == "paper" && self.chosenFighter == "scissors"{
                         print("you win")
                        self.wins += 1
                        self.WinLabel.isHidden = false
                    }
                        //Shows the player chosen fighter and the opponents
                        switch recievedMessage{
                        case "paper":
                            self.opponentChoiceImage.setImage(#imageLiteral(resourceName: "Paper"), for: UIControlState.normal)
                        case "scissors":
                             self.opponentChoiceImage.setImage(#imageLiteral(resourceName: "Scissors"), for: UIControlState.normal)
                        default:
                             self.opponentChoiceImage.setImage(#imageLiteral(resourceName: "Rock"), for: UIControlState.normal)
                        }
                        switch self.chosenFighter{
                        case "paper":
                            self.playChoiceImage.setImage(#imageLiteral(resourceName: "Paper"), for: UIControlState.normal)
                        case "scissors":
                            self.playChoiceImage.setImage(#imageLiteral(resourceName: "Scissors"), for: UIControlState.normal)
                        default:
                            self.playChoiceImage.setImage(#imageLiteral(resourceName: "Rock"), for: UIControlState.normal)
                        }
                        self.opponentChoiceImage.isHidden = false
                        self.playChoiceImage.isHidden = false
                    self.updateScore()
                    }
                    
                }
            }
            
        }
    //Func for handeling the timer
    @objc func timerFunc(){
        countDownLabel.isHidden = false
        countDownLabel.text = time.description
        // Counds down from 3 and at 3 and then resets the timer and disables it. then sends the fighter data
        if time == 0 {
            time = 3
            timer.invalidate()
            disableButtons()
            countDownLabel.isHidden = true
            resetReady()
            
            if let sendFighter = chosenFighter.data(using: String.Encoding.utf8){
            
            do{
                try session.send(sendFighter, toPeers: session.connectedPeers, with: MCSessionSendDataMode.reliable)
            }
            catch{
                print("Error, Couldnt send the data")
            }
            }
            
        }
        else
        {        time -= 1
        }
    }
        
        // Received a byte stream from remote peer.
        
        func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID){
        
        }
        
        
        // Start receiving a resource from remote peer.
        
        func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress){
        
        }
        
        
        // Finished receiving a resource from remote peer and saved the content
        // in a temporary location - the app is responsible for moving the file
        // to a permanent location within its sandbox.
        
        func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?){
        
        }
    //Handles creating the connection and displaying the browser
    @IBAction func ConnectTapped(_ sender: Any) {
       
        browser = MCBrowserViewController(serviceType: serviceID, session: session  )
        browser.delegate = self
        self.present(browser, animated: true, completion: nil)
    }
    
    //Handles starting the game
    @IBAction func playTapped(_ sender: Any){
        if let _ = sender as? UIButton{
            selfReady = true
            //Sends over to the other player that we are ready to begin
            if let sendReady = "ready".data(using: String.Encoding.utf8){
                do{
                   try session.send(sendReady, toPeers: session.connectedPeers, with: MCSessionSendDataMode.reliable)
                }
                catch{
                    print("Error, Couldnt send the data")
                }
                
                DispatchQueue.main.async {
                    
                    //If our oppenent is ready we ccan begin the game
                    if self.oppenentReady  == true{
                        self.enableButtons()
                        self.timer.invalidate()
                        //Starts the timer and runs our timer func
                        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerFunc), userInfo: nil, repeats: true)
                    }
                    else{ // else theyre not ready so well display to the user we're waiting on the opponent
                        self.waitingOpponentLabel.isHidden  = false
                    }
                }
                
            }
        }
    }
    
    //Changes the selected choice of rock paper scissors
    @IBAction func GameSelection(_ sender: Any){
        if let button = sender as? UIButton{
            switch button.tag{
            case 0:
                chosenFighter = "rock"
                
            case 1 :
                chosenFighter = "scissors"
              
                
            default:
                chosenFighter = "paper"
                
            }
           
            
        }
    }
    //Enables the bosses for the game and hides all the other labes and images
    func enableButtons(){
        rockButton.isEnabled  = true
        rockButton.isHidden = false
        
        paperButton.isEnabled  = true
        paperButton.isHidden = false
        
        scissorButton.isEnabled  = true
        scissorButton.isHidden = false
        
        WLTLabel.isHidden = true
        displayWLT.isHidden = true
        WinLabel.isHidden = true
        LossLabel.isHidden = true
        TieLabel.isHidden = true
        waitingMeLabel.isHidden = true
        waitingOpponentLabel.isHidden = true
        
        self.opponentChoiceImage.isHidden = true
        self.playChoiceImage.isHidden = true
        
    }
    //Disables all the buttons, hides waiting, and shows win lose ties
    func disableButtons(){
        print("WeCleared")
        rockButton.isEnabled  = false
        rockButton.isHidden = true
        
        paperButton.isEnabled  = false
        paperButton.isHidden = true
        
        scissorButton.isEnabled  = false
        scissorButton.isHidden = true
        
        waitingMeLabel.isHidden = true
        waitingOpponentLabel.isHidden = true
        
        WLTLabel.isHidden = false
        displayWLT.isHidden = false
    }
    //Resets the ready status for the players
    func resetReady(){
        selfReady = false
        oppenentReady = false
    }
    //Updates the score
    func updateScore(){
        WLTLabel.text = "\(wins)-\(loss)-\(ties)"
    }
}



