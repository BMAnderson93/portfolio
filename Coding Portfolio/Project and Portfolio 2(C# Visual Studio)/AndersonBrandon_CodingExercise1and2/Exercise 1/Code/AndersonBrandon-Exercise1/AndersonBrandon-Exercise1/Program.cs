﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_Exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creates the random variable to be called on at a later time.
            Random random = new Random();
            // Creates my list of coffee brands and populates it with 20 brands.
            List<string> coffeeBrands = new List<string>(new string[] { "Maxwell House", "Folgers", "NesCafe", "Eight o Clock Coffee", "Jacobs", "Tassimo", "Sanka", "Kenco", "Easy Serving Espresso Pod", "Moccona", "Nabob", "Senseo", "High Point", "State House Coffee", "Red Cricle Coffee", "Dunkin Donuts", "Lavazza", "Keurig K Cups", "Seattle's Best", "Breakfast Blend" });
            List<string> coffeeBrandsBackup = new List<string>(new string[] { "Maxwell House", "Folgers", "NesCafe", "Eight o Clock Coffee", "Jacobs", "Tassimo", "Sanka", "Kenco", "Easy Serving Espresso Pod", "Moccona", "Nabob", "Senseo", "High Point", "State House Coffee", "Red Cricle Coffee", "Dunkin Donuts", "Lavazza", "Keurig K Cups", "Seattle's Best", "Breakfast Blend" });
            // Creates my loop for my option menu.
            bool running = true;
            while (running == true)
            {
                Console.Clear();
                Console.WriteLine("Please choose from the following menu options" + System.Environment.NewLine + "1.) Arrange list alphabetically."
                    + System.Environment.NewLine + "2.) Arrange list  in reverse alphabet order" + System.Environment.NewLine + "3.) Delete the entire list randomly." + System.Environment.NewLine + "4.) Exit" + System.Environment.NewLine);
                string input = Console.ReadLine();
                Console.Clear();
                switch (input)
                {
                    // Takes the list and checks if its contents are more than 0, if so it then takes the list and sorts it.
                    case "1":
                        if (coffeeBrands.Count == 0)
                        {
                            coffeeBrands = Refill(coffeeBrandsBackup, coffeeBrands);
                        }
                        else
                            coffeeBrands.Sort();
                        PrintList(coffeeBrands);
                        Console.ReadKey();

                        break;
                    case "2":
                        // Checks if list is greater than 0 and if so it then revereses it by alphabet.
                        if (coffeeBrands.Count == 0)
                        {
                            coffeeBrands = Refill(coffeeBrandsBackup, coffeeBrands);
                        }
                        else
                            coffeeBrands.Sort();
                        coffeeBrands.Reverse();
                        PrintList(coffeeBrands);
                        Console.ReadKey();

                        break;
                        
                    case "3":
                        if (coffeeBrands.Count == 0)
                        {
                            coffeeBrands = Refill(coffeeBrandsBackup, coffeeBrands);
                        }
                        else
                           // Takes the list if its contents are greater than 0 and then creates a loop to go through every item at random and deletes it.
                            while (coffeeBrands.Count > 0)
                        {
                            int n = random.Next(coffeeBrands.Count);
                            Console.WriteLine(coffeeBrands[n] + " has been removed.");
                            coffeeBrands.RemoveAt(n);
                            Console.WriteLine("There is " + coffeeBrands.Count + " coffee brands remaining.");
                        }
                        Console.ReadKey();

                        break;
                    case "4":
                        running = false;
                        break;

                    default:
                        Console.WriteLine("Please make a valid selection!");
                        Console.ReadKey();
                        break;
                }
            }


        }
        // Creates a function for printing off the list to the console.
        static void PrintList(List<string> list)
        {
            for (int i = 0; i < list.Count(); i++)
            {
                Console.WriteLine((i + 1) + " .)" + list[i]);
            }
        }
        // Creates a function for refilling or not reffiling the provided list depending on what the user decides.
        static List<string> Refill(List<string> backup, List<string> current)
        {
            bool running = true;
            // Begins loop for input validation so that the user can not crash the program.
            while (running == true)
            {
                Console.Clear();
                Console.WriteLine("The list is empty, would you like to refill it? (y / n)");
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "y":
                        Console.WriteLine("The list has been refilled.");
                       // Takes the provided list and replaces it with the back up copy.
                        running = false;
                        current = backup;
                        break;


                    case "n":
                        Console.WriteLine("The list has not been refilled.");
                        
                        running = false;
                        break;

                    default:
                        Console.WriteLine("Please enter a valid responce!");
                        Console.ReadKey();
                        break;


                }
            }
            // Returns either the same empty list or the refilled list based on the users choices.
            return current;

        }
    }
}
