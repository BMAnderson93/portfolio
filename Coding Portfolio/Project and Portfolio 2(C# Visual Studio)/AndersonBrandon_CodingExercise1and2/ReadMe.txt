For this assignment, using your current skill set (Visual Studio) and programming knowledge (C#), to practice different programming case studies.



Objectives & Outcomes

After completing this activity, there will be a better understanding of:

How to code more efficiently than before
How to code under tight deadlines
How to apply programming knowledge to new exercises

Level of Effort

This coding assignment should take approximately 120 minutes to complete.

30m Research to find assignment related C# examples, samples, and information pertaining to the coding being done
30m Prep and Delivery in order to be prepared for coding and testing the code until it works properly and there are no bugs
60m Work coding and testing the code until it works
If this activity takes significantly more, or less, time than this estimate, please contact the instructor for guidance

Instructions

For this assignment, complete both (2) programming exercises, in Visual Studio software, using the C# language. Both exercises must be hard/hand coded and not copied from any location, person, etc.  Before starting the assignment, the students will look up what month of the year they are currently in (for the class), and choose the programming topic matter from the list.  Any topic matter used in the assignment that is not in the list, for the current month, will receive a zero for the entire assignment. All code must be �//commented,� in detail, so any reviewer of the code will know what is being attempted with the code.



MONTH

TOPIC

January

Coffee Brands (Ex: Starbucks)

February

Clothing Brands (Ex: Prada)

March

Fruits & Veggies (Ex: Banana)

April

Sports (Ex: Soccer / F�tbol)

May

Fruits & Veggies (Ex: Banana)

June

Sports (Ex: Soccer / F�tbol)

July

Coffee Brands (Ex: Starbucks)

August

Clothing Brands (Ex: Prada)

September

Sports (Ex: Soccer / F�tbol)

October

Coffee Brands (Ex: Starbucks)

November

Clothing Brands (Ex: Prada)

December

Fruits & Veggies (Ex: Banana)




Exercise 1 (Sorting Arrays/Lists):

Create an array/list of 20 Topic Matters (see chart above).
Be sure to NOT alphabetize them in the array/list.
Create interactive code that let�s the user interact with the console and choose the following:
Create code that will alphabetize the array/list (A-Z).
Create code that will alphabetized the array/list backwards (Z-A).
Create code that will randomly choose a name from the array/list, delete it from the array/list, then pick another one, delete it, etc, until there is nothing left in the array.
Example Print Out Of Answer In Console:
Topic From Above (Random Order):
(next line in console) Student Answer (d, z, a, c, etc)
(blank line in console)
Topic From Above (A-Z):  
(next line in console) Student Answer (a, b, c, d, etc)
(blank line in console)
Topic From Above (Z-A):  
(next line in console) Student Answer (z, y, x, w, etc)
(blank line in console)
20 Random Disappearing Topics From Above:
This MUST be in a loop of some sort 
(next line in console) Student Answer (show all 20 strings), next line in console (show 19)...(show 0).  So, this could take up 20 or more lines on the console.
Take a screenshot of the console output/outputs to prove the code works.
Take a screenshot of the code
Save The Exercise


Exercise 2 (Give Clues Until You Get An Answer):

Create a scenario, using the Topic Matter, where the console/computer gives the user a �safe for school� set of clues that lead to a correct �safe for school� answer about the topic matter, where only one answer will work. For example, if the answer is �beach,� a clue could be �this is a place with sand and water.�
The answer must be a one word answer, and the first letter must be a capital letter (Ex:  Florida, Pepsi, John)
The console/computer must check for capital letters.
The console/computer must tell the user if the answer is correct or incorrect and tell it something when those happen as well.  (Ex:  You are correct ! Thank you for answering this question OR This is incorrect, Please try again)
This MUST be in a loop of some sort.  It must continue on until the person gets the right answer.
Keep in mind if you are looking for �Orlando� as the answer, but the user types in �orlando,� the answer is not wrong or incorrect, it just needs a capital letter.  So, be specific in what you tell your users...any mistake can make a user stop using your app or think it is broken.
Also keep in mind, if you are looking for �Miami� as the answer, but the user types in �Tampa,� and you tell them they are wrong, and wait for the answer again and again, that does not help the user get the correct answer...try giving tips, clues, letters to the word, ask the question in a different way, etc.
Take a screenshot of the console output (question and answer).
Take a screenshot of the code.
Save The Exercise

Materials

For this assignment, use Visual Studio on the PC side of the computer and C#.


Reading and Resources

Below is a link to the Microsoft C# Programming Guide

C# MSDN

Grading Criteria

Be sure to review the rubric because it might be looking for something different than what is in the instructions.



Showstoppers

A showstopper is anything that can turn the assignment grade into a zero (0%).  




Deliverables

Turn in one (1) zipped file that contains all images (JPG/PNG) and exercise files (Visual Studio), with the following structure and naming conventions:

LastFirst_CodingExercise1and2.zip
Exercise 1 (Folder)
Contains Visual Studio Exercise Files
Contains Console Output Images
LastFirst_Output1.jpg, LastFirst_Output2.jpg, etc
Contains Code Images
LastFirst_Code1.jpg, LastFirst_Code2.jpg, etc
Exercise 2 (Folder)
Contains Visual Studio Exercise Files
Contains Console Output Images
LastFirst_Output1.jpg, LastFirst_Output2.jpg, etc
Contains Code Images
LastFirst_Code1.jpg, LastFirst_Code2.jpg, etc




*** Don�t forget to save this to the appropriate cloud storage folder you set up in DVP1 & to your BitBucket Repo***






Returning Students

If you have taken this course before, remember, turning in an old assignment, or assignment with the same code, is considered plagiarism.  Therefore, if you are taking the course again, for this assignment, you will need to do ALL OF THE FOLLOWING:

Change the topic matter
Clean up, organize, and make the code more efficient
Add better, clear, more specific code commenting
Use more, better, and/or different OOP principles in your code
Add more interactivity, background colors, font colors, better tips for incorrect answers, etc, to the console output