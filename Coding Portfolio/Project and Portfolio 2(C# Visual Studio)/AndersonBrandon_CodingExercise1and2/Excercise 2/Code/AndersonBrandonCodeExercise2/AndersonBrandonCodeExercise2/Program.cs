﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandonCodeExercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            /* I use this array to hold all of my hints to be used in the loop, I also use the variable total guess to keep track
             of the total guess's to give the user different hints. */
            string[] hints = {"The name was inspired by Moby-Dick", "It was founded in 1971",
                "The Company was founded in Seattle Washington", "The logo has a mermaid in it", "This is a popular place to get coffee",
            "They dont use the regular terms for beverage size's"};
            int totalGuess = 0;
            Console.WriteLine("Welcome to my trivia game! It's your job to guess the keyword to complete this question," +
                " I'll start you off with a hint." + Environment.NewLine+ 
                "This company was founded by two teachers, and a writer!" + Environment.NewLine);
            Console.ReadKey();
           
            bool running = true;
            // Creates a loop to allow the user to continue guessing until they get the question right.
            while (running == true)

            {// Clears the console of all text so not to clutter the screen and prompts the user for their guess.
                Console.Clear();
                Console.WriteLine("Please enter your guess!");
                string input = Console.ReadLine();
                switch (input)
                {
                    // If the users guess is correct this line will inform them and allow them to exit the program.
                    case "Starbucks":
                        {
                            Console.WriteLine("You are correct! Congratulations on solving my trivia question! Press any key to exit.");
                            Console.ReadKey();
                            running = false;
                        }
                        break;

                        // If the user is correct but they need to ajust the capitalization of the first word this line will inform them.

                    case "starbucks":
                        {
                            Console.WriteLine("You're close! Try adding capitalization!");
                            Console.ReadKey();
                        }
                        break;

                    default:
                        {
                            // If the user isn't close to the word this line will give them another clue and allow them to guess again.
                            
                            Console.WriteLine("Your guess was wrong, heres another hint!");
                            // This line prints out the next clue based on the total number of guess's
                            Console.WriteLine(hints[totalGuess]);
                            // This line adds to the users total amount of guess's.
                            totalGuess++;
                            // This line will reset the users guess's to 0 if they exceed the amount of hints available
                            if(totalGuess > (hints.Length - 1))
                            {
                                totalGuess = 0;
                            }
                            Console.ReadKey();
                        }
                        break;
                }
            }
        }
    }
}
