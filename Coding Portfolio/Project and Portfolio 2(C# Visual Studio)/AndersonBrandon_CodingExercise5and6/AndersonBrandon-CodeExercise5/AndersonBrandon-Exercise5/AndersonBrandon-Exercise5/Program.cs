﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_Exercise5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> options = new List<string>{"Rock", "Paper","Scissors","Lizzard","Spock"};

            int turnCount = 0;
            
            int playerWins = 0;
            int computerWins = 0;
            int ties = 0;
            bool running = true;
            bool replayMenu = true;
            // This line displays to the user the name of the game and instructs them on how to begin
            Console.WriteLine("Hello, and welcome to a game of Rock Paper Scissors Lizzard Spock. " + Environment.NewLine + "You will be playing a best of 10 match against the computer, press any key when you're ready to begin!" +
                  Environment.NewLine);
            Console.ReadKey();
            while (running)
            {
                while (turnCount < 10)
                {
                    replayMenu = true;
                    Console.Clear();
                    // These lines display to the user every option they have and asks them to pick what they want this turn
                    Console.WriteLine("Please select the name or number of your choice!");
                    for (int i = 0; i < options.Count(); i++)
                    {
                        Console.WriteLine(i + 1 + "). " + options[i]);
                    }
                    string input = Console.ReadLine().ToLower();
                    // These lines make sure that the user has inputed a valid option
                    while (input != "rock" & input != "paper" & input != "scissors" & input != "lizzard" & input != "spock" & input != "1" & input != "2" & input != "3" & input != "4" & input != "5")
                    {
                        Console.WriteLine("Please enter a correct choice!");
                        input = Console.ReadLine();
                    }
                    switch (input)
                    {
                        // These lines change the users input from a number to a valid option
                        case "1":
                            {
                                input = "Rock";
                                break;
                            }
                        case "2":
                            {
                                input = "Paper";
                                break;
                            }
                        case "3":
                            {
                                input = "Scissors";
                                break;
                            }
                        case "4":
                            {
                                input = "Lizzard";
                                break;
                            }
                        case "5":
                            {
                                input = "Spock";
                                break;
                            }

                        default:
                            {
                                break;
                            }
                    }
                    // This line runs the games rules and picks a winner for the round
                    string whoWins = GameRules(input, options);

                    switch (whoWins)
                    {
                        // These lines add to the winners total points 
                        case "win":
                            {
                                playerWins++;
                                Console.WriteLine("Player Wins!");
                                Console.ReadKey();
                                break;
                            }
                        case "loss":
                            {
                                computerWins++;
                                Console.WriteLine("Computer Wins!");
                                Console.ReadKey();
                                break;
                            }
                        case "tie":
                            {
                                ties++;

                                Console.WriteLine("Tie!");
                                Console.ReadKey();
                                break;
                            }

                        default:
                            {
                                Console.WriteLine(whoWins);
                                Console.WriteLine("error");
                                Console.ReadKey();
                                break;
                            }
                    }
                    // This line keeps tally of how many turns have gone by
                    turnCount++;
                }

                Console.Clear();
                // These lines print out the scores for each players and lets them know who won the game
                Console.WriteLine("The game is over, now I'll show you the score!" +Environment.NewLine+ "Player Wins = " + playerWins + Environment.NewLine +"Computer Wins = " + computerWins + Environment.NewLine + "Ties = " + ties + Environment.NewLine );
                if(playerWins > computerWins)
                {
                    Console.WriteLine("The player wins the game!");
                }
               else if (playerWins < computerWins)
                {
                    Console.WriteLine("The computer wins the game!");
                }
               else if (playerWins == computerWins)
                {
                    Console.WriteLine("The game has ended in a tie!!");
                }
                while (replayMenu)
                {
                    // These lines ask the user if they would like to play again, if they do all tallies are reset to 0
                    Console.WriteLine("Would you like to play again? (Y / N)");
                    string playAgain = Console.ReadLine().ToLower();

                    switch (playAgain)
                    {
                        case "y":
                            {
                                turnCount = 0;
                                playerWins = 0;
                                computerWins = 0;
                                ties = 0;
                                replayMenu = false;
                                break;

                            }
                        case "n":
                            {
                                Environment.Exit(0);
                                replayMenu = false;
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Please enter a correct choice!");
                                playAgain = Console.ReadLine();
                                break;
                            }
                    }
                }

           
            }
        }

        static string GameRules(string playerChoice, List<string> options) 
        {

           // These lines create a random number that the computer chooses for their option
             playerChoice = char.ToUpper(playerChoice.First()) + playerChoice.Substring(1).ToLower();
            Random rnd = new Random();
            int computerChoice = rnd.Next(options.Count());
            string output = "Player has chosen " + playerChoice + Environment.NewLine + "Computer has chosen " + options[computerChoice];
            // These lines handle each possibility of what outcome from the choices could happen and returns who wins.
            if (playerChoice == "Rock" && options[computerChoice] == "Rock")
            {
                Console.WriteLine(output);
                return "tie";
            }
            else if (playerChoice == "Rock" && options[computerChoice] == "Paper")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Rock" && options[computerChoice] == "Scissors")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Rock" && options[computerChoice] == "Lizzard")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Rock" && options[computerChoice] == "Spock")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Paper" && options[computerChoice] == "Rock")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Paper" && options[computerChoice] == "Paper")
            {

                Console.WriteLine(output);
                return "tie";
            }
            else if (playerChoice == "Paper" && options[computerChoice] == "Scissors")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Paper" && options[computerChoice] == "Lizzard")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Paper" && options[computerChoice] == "Spock")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Scissors" && options[computerChoice] == "Rock")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Scissors" && options[computerChoice] == "Paper")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Scissors" && options[computerChoice] == "Scissors")
            {
                Console.WriteLine(output);
                return "tie";
            }
            else if (playerChoice == "Scissors" && options[computerChoice] == "Lizzard")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Scissors" && options[computerChoice] == "Spock")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Lizzard" && options[computerChoice] == "Rock")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Lizzard" && options[computerChoice] == "Paper")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Lizzard" && options[computerChoice] == "Scissors")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Lizzard" && options[computerChoice] == "Lizzard")
            {
                Console.WriteLine(output);
                return "tie";
            }
            else if (playerChoice == "Lizzard" && options[computerChoice] == "Spock")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Spock" && options[computerChoice] == "Rock")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Spock" && options[computerChoice] == "Paper")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Spock" && options[computerChoice] == "Scissors")
            {
                Console.WriteLine(output);
                return "win";
            }
            else if (playerChoice == "Spock" && options[computerChoice] == "Lizzard")
            {
                Console.WriteLine(output);
                return "loss";
            }
            else if (playerChoice == "Spock" && options[computerChoice] == "Spock")
            {
                Console.WriteLine(output);
                return "tie";
            }
            else

                return "error";
        }
    }
}
