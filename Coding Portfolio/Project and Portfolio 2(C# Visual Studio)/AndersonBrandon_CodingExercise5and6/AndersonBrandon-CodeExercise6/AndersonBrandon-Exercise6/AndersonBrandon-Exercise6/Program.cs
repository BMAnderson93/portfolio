﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_Exercise6
{
    class Program
    {
        static void Main(string[] args)
        { // I use these lines to delcare all variables i will be using
            string player1;
            string player2;
            string player3;
            string player4;
            int player1Score = 0;
            int player2Score = 0;
            int player3Score = 0;
            int player4Score = 0;
            int dealOrder = 0;
            bool running = true;
            SortedList<int, string> playerDecks = new SortedList<int, string>();

            Dictionary<string, List<Card>> sortedDecks = new Dictionary<string, List<Card>>();
             //  These lines create my player decks and the main deck populating them with values
            List<Card> player1Deck = new List<Card>();
            List<Card> player2Deck = new List<Card>();
            List<Card> player3Deck = new List<Card>();
            List<Card> player4Deck = new List<Card>();
            List<int> PointValues = new List<int>() {2,3,4,5,6,7,8,9,10,12,12,12,15 };
            List<string> cardName = new List<string> { "2", "3", "4", "5", "6","7", "8", "9", "10", "J", "Q", "K", "A" };
            List<string> cardSuit = new List<string> {"Club", "Spade", "Heart", "Diamond" };
            List<Card> newDeck = new List<Card>();
            List<Card> deck = new List<Card>();
            foreach(string suit in cardSuit)
                // This line creates the deck 
            { 
                for(int i = 0; i < cardName.Count; i++)
                {
                    Card newCard = new Card();
                    newCard.SetPoint(PointValues[i]);
                    newCard.SetNumber(cardName[i]);
                    newCard.SetSuit(suit);
                    deck.Add(newCard);
                }
            }
            // These lines ask the user to enter the player names and makes sure no names are repeated
            Console.WriteLine("Hello, please enter the name for player 1");
            player1 = Console.ReadLine();
            Console.WriteLine("Hello, please enter the name for player 2");
            player2 = Console.ReadLine();
            while (player2 == player1)
            {
                Console.WriteLine("You can not use the same name twice, enter a new name!");
                player2 = Console.ReadLine();
            }
            Console.WriteLine("Hello, please enter the name for player 3");
            player3 = Console.ReadLine();
            while (player3 == player2 || player3 == player1)
            {
                Console.WriteLine("You can not use the same name twice, enter a new name!");
                player3 = Console.ReadLine();
            }
            Console.WriteLine("Hello, please enter the name for player 4");
            player4 = Console.ReadLine();
            while (player4 == player3 || player4 == player2 || player4 == player1)
            {
                Console.WriteLine("You can not use the same name twice, enter a new name!");
                player4 = Console.ReadLine();
            }
            while (running)
            {
                // these lines shuffles the deck 4 times to ensure a good shuffle
                newDeck = Shuffle(deck);
                newDeck = Shuffle(newDeck);
                newDeck = Shuffle(newDeck);
                newDeck = Shuffle(newDeck);

                Console.WriteLine("Press enter to deal the cards!");
                Console.ReadKey();
              
                // These lines deals the cards out evenly to each player
                foreach(Card card in newDeck)
                {
                    if (dealOrder == 0)
                    {
                        player1Deck.Add(card);
                       
                    }
                    else if (dealOrder == 1)
                    {
                        player2Deck.Add(card);
                        
                       

                    }
                    else if (dealOrder == 2)
                    {
                        player3Deck.Add(card);
                       
                    }
                    else if (dealOrder == 3)
                    {
                        player4Deck.Add(card);
                       
                   
                    }
                    dealOrder++;
                    if (dealOrder == 4)
                    {
                        dealOrder = 0;
                    }

                }

                // These lines add the decks to their own dictionaries / lists to use for the winner validation
                sortedDecks.Add(player1, player1Deck);
                sortedDecks.Add(player2, player2Deck);
                sortedDecks.Add(player3, player3Deck);
                sortedDecks.Add(player4, player4Deck);


                player1Score = PointTotal(player1Deck);
                player2Score = PointTotal(player2Deck);
                player3Score = PointTotal(player3Deck);
                player4Score = PointTotal(player4Deck);

                playerDecks.Add(player1Score, player1);
                playerDecks.Add(player2Score, player2);
                playerDecks.Add(player3Score, player3);
                playerDecks.Add(player4Score, player4);

                showWinners(playerDecks, sortedDecks);
                running = false;


            }

        }
        public static void showWinners(SortedList<int, string> scores, Dictionary<string, List<Card>> decks)
        {
            Console.Clear();
            int counter = 0;
            for (int i = 0; i < scores.Count; i++)
            {
                 // these lines take the total points of each deck and displays who the winner is from last to first
                foreach (KeyValuePair<int, string> kvp in scores)
                {if (counter == 0)
                    {
                        Console.WriteLine(kvp.Value + " takes fourth place with the following cards.");
                        foreach (Card card in decks[kvp.Value])
                        {

                            Console.WriteLine(card.GetValue());

                        }
                        Console.WriteLine("Giving them a point value of " + kvp.Key + ".");
                        Console.ReadKey();
                    }
                    else if (counter == 1)
                    {
                        Console.WriteLine(kvp.Value + " takes third place with the following cards.");
                        foreach (Card card in decks[kvp.Value])
                        {

                            Console.WriteLine(card.GetValue());

                        }
                        Console.WriteLine("Giving them a point value of " + kvp.Key + ".");
                        Console.ReadKey();
                    }
                    else if (counter == 2)
                    {
                        Console.WriteLine(kvp.Value + " takes second place with the following cards.");
                        foreach (Card card in decks[kvp.Value])
                        {

                            Console.WriteLine(card.GetValue());

                        }
                        Console.WriteLine("Giving them a point value of " + kvp.Key + ".");
                        Console.ReadKey();
                    }
                    else if (counter == 3)
                    {
                        Console.WriteLine(kvp.Value + " takes first place with the following cards.");
                        foreach (Card card in decks[kvp.Value])
                        {

                            Console.WriteLine(card.GetValue());

                        }
                        Console.WriteLine("Giving them a point value of " + kvp.Key + ".");
                        Console.ReadKey();
                    }
                    counter++;
                    Console.Clear();
                }

            }
        }
        // this line can take a list and shuffle the order of elements
        public static List<Card> Shuffle (List<Card> Deck)
        {
            Random rnd = new Random();
            List<Card> shuffleDeck = new List<Card>();
            int card;
            while(Deck.Count > 0)
            {
               card = rnd.Next(Deck.Count);

                shuffleDeck.Add(Deck[card]);
                Deck.RemoveAt(card);

            }
            return shuffleDeck;

        } 

        // these lines tally up the total points the deck has
        public static int PointTotal (List<Card> deck)
        {

            int totalPoints = 0;
            foreach(Card card in deck)
            {
                totalPoints += card.GetPoint();
            }

            return totalPoints;

        }
    }
}
