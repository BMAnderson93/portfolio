﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_Exercise6
{
    class Card
    { // these are the values the card class will have
        string deckName;
        string cardNumber;
        int pointValue;
        string suit;
        // these are my getter and setters for my card class variables

        public void SetName (string name)
        {
            deckName = name;
        }
        public void SetNumber (string number)
        {
            cardNumber = number;
        }
        public void SetPoint (int number)
        {
            pointValue = number;
        }
        public void SetSuit(string suits)
        {
            suit = suits;
        }
        public string GetName()
        {
            return deckName;
        }
        public string GetValue()
        {
            return cardNumber;
        }
        public int GetPoint()
        {
            return pointValue;
        }
        public string GetSuit()
        {
            return suit;
        }
    }
}
