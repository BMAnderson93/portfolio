For this assignment, using your current skill set (Visual Studio) and programming knowledge (C#), to practice different programming case studies.



Objectives & Outcomes

After completing this activity, there will be a better understanding of:

How to code more efficiently than before
How to code under tight deadlines
How to apply programming knowledge to new exercises

Level of Effort

This coding assignment should take approximately 120 minutes to complete.

30m Research to find assignment related C# examples, samples, and information pertaining to the coding being done
30m Prep and Delivery in order to be prepared for coding and testing the code until it works properly and there are no bugs
60m Work coding and testing the code until it works
If this activity takes significantly more, or less, time than this estimate, please contact the instructor for guidance

Instructions

For this assignment, the student will complete both (2) programming exercises, in Visual Studio software, using the C# language. Both exercises must be hard/hand coded and not copied from any location, person, etc.  Students will also comment their code, in detail, so any reviewer of their code will know what is being attempted with the code.







Exercise 5 (Rock, Paper, Scissors, Lizard, Spock):

Create the game of Rock, Paper, Scissors, Lizard, Spock
The console/computer must ask:
Which do you choose: Paper, Rock, Scissors, Lizard, Spock?
It should accept both lowercase and capital letters (Ex:  Blue and blue), or have a numerical list to choose from
It should state the following after you select your answer
Computer Chooses:  Random Answer (Paper, Rock, Scissors, Lizard, Spock)
It should then state one of the following
Computer Wins!
Tie!
Player Wins!
Your wins, losses, and ties must be recorded/saved. They can be shown each game or at the end of all games, but must be shown at the end of all games.
Play ten (10) games against the computer
When the tenth (10th) game is over, print to the console:
Player Wins:  Recorded Answer
Player/Comp Ties:  Recorded Answer
Player Losses:  Recorded Losses
Be sure to ask if they want to play again, and re-record their wins, ties, and losses.
Take screenshots of the output (all ten games and final results)
Take screenshots of the code
Save The Exercise


Exercise 6 (Card Counting And Points Awarding):

Create an array/list for a deck of cards
2 through A, no jokers
2-10:  Point Value Equals Number
J-K:  Point Value Equals 12
A:  Point Value Equals 15
There will be four (4) players
Have the user input the names
Give out all cards to all players
4 players = 13 cards each
Don�t forget to shuffle the deck.
Deal the cards as if you were playing a real card game...each player gets one card until they are all gone.
Once all the cards are handed out, count each player�s cards, based on the point values above, to see what order they place...first, second, third, fourth
TIP: This might require adding each player�s cards into their own array/list and checking for values.
TIP: Looping this might help as well.
Console Output, from first place to last place, each player�s name, their cards, and their total points. Be sure to actually print out the cards and not the point values of the cards��A� not �15.�
Place:  1st
Player Name
Cards:  1, 2, 3, Etc
Total Value:  TBA
...continue for all players
(after player 4) Total Deck Score:  (this should add up to 420 to be correct)
TIP: Check Your Work/Math. After handing out all cards, your total points should equal 420 points.  If it is anything different, your code is not correct.
TIP:  Run the software a few times, and select a few cards to look at...for example, the Ace.  Make sure it never comes up more than 4 times.
Take screenshots of the output
Take screenshots of the code
Save The Exercise

Materials

For this assignment, students will be using Visual Studio on the PC side of their computer and C#.


Reading and Resources

Below is a link to the Microsoft C# Programming Guide

C# MSDN

Grading Criteria

Be sure to review the rubric because it might be looking for something different than what is in the instructions.



Showstoppers

A showstopper is anything that can turn the assignment grade into a zero (0%).  



Deliverables

Students will turn in one (1) zipped file that contains all images (JPG/PNG) and exercise files (Visual Studio), with the following structure and naming conventions:

LastFirst_CodingExercise5and6.zip
Exercise 5 (Folder)
Contains Visual Studio Exercise Files
Contains Console Output Images
LastFirst_Output1.jpg, LastFirst_Output2.jpg, etc
Contains Code Images
LastFirst_Code1.jpg, LastFirst_Code2.jpg, etc
Exercise 6 (Folder)
Contains Visual Studio Exercise Files
Contains Console Output Images
LastFirst_Output1.jpg, LastFirst_Output2.jpg, etc
Contains Code Images
LastFirst_Code1.jpg, LastFirst_Code2.jpg, etc







*** Don�t forget to save this to the appropriate cloud storage folder you set up in DVP1 & to your BitBucket Repo***





Returning Students

If you have taken this course before, remember, turning in an old assignment, or assignment with the same code, is considered plagiarism.  Therefore, if you are taking the course again, for this assignment, you will need to do ALL OF THE FOLLOWING:

Change the topic matter
Clean up, organize, and make the code more efficient
Add better, clear, more specific code commenting
Use more, better, and/or different OOP principles in your code
Add more interactivity, background colors, font colors, better tips for incorrect answers, etc, to the console output