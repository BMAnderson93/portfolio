For this assignment, using your current skill set (Visual Studio) and programming knowledge (C#), to practice different programming case studies.



Objectives & Outcomes

After completing this activity, there will be a better understanding of:

How to code more efficiently than before
How to code under tight deadlines
How to apply programming knowledge to new exercises

Level of Effort

This coding assignment should take approximately 120 minutes to complete.

30m Research to find assignment related C# examples, samples, and information pertaining to the coding being done
30m Prep and Delivery in order to be prepared for coding and testing the code until it works properly and there are no bugs
60m Work coding and testing the code until it works
If this activity takes significantly more, or less, time than this estimate, please contact the instructor for guidance

Instructions

For this assignment, complete both (2) programming exercises, in Visual Studio software, using the C# language. Both exercises must be hard/hand coded and not copied from any location, person, etc. Comment the code, in detail, so any reviewer of the code will know what is being attempted with the code.





Exercise 7 (Mad Libs Story):

Screen Shot 2016-04-28 at 3.22.35 PM.png
Create the above MadLibs Story (just the text)
Ask the user if they will help you
Ex:  Hello, Can you help me finish a story? If so, I will ask you for a series of nouns, adjectives, etc.
Be sure to check for lowercase and capital letters (Ex:  Yes and yes)
Be sure to tell the user what a noun, adjective, etc is...just in case they do not know.
Ask the user for the blank nouns, adjectives, etc
Ex: Help Me 1/10:  Give me a noun (a noun is�)
Keep track of the answers.
Tip: An array/list might be good for this, so you can loop through it when printing the entire story out.
When all the help is over, say something to the user to let them know you are done
Ex:  Thank you for the help.  The final story is below.
Print to screen the story for the user to read and enjoy.
Be sure to emphasize their words...underlined, all capitals, bold, a different color, etc.
Take a screenshot of the final story with their chosen words
Take a screenshot of all the questions asked to them, with their answers
Take a screenshot of the code
Save The Exercise


Exercise 8 (Make A Smiley Face):

Create one array/list that is a series of numbers...0, 1, 2, 3, etc.
There must be at least 3...one for the background, one for the face, and one for the eyes/mouth
This will be used for your ASCII Character Selection
Pseudo Example: if (array[i] == 2) {  console.write = �$�; }
This array/list will be used to select the ASCII characters in the next array/list.
If you were making a 2D game, this could also be used with a nested loop to get (X, Y) Locations for where to place your tiles/artwork.
Create one array/list that is a series of ASCII Characters�.!@#$%^&*, etc.
There must be at least 2...one for the face, and one for the eyes/mouth
This will be used for your smiley face details.
To help you with the smiley face creation, look at the below array.  It will show you how to lay out the array so you can draw the face in the code easily.
JSON-and-tiles.png

Examples of Final Smiley Faces:
ASCII-happy-face-2.gifEqwwb.png

ASCII-happy-face1.gifDB8850CA5.gif


Create a loop that cycles through your number array/list.
Create a switch statement inside the loop that looks for the array numbers.
Based on the array/list number, print to the console an ASCII character
Pseudo Example: if (array[i] == 2) {  console.write = �$�; }
Take a screenshot of the console output (Smiley Face)
The coding is easy, so the smiley face needs to be extremely detailed.
Maybe even have fun with it...add color, etc.
If it does not look better than one of the examples above, the grade will be a zero (0) for this portion of the assignment.
Take a screenshot of the code
Save The Exercise

Materials

For this assignment, students will be using Visual Studio on the PC side of their computer and C#.


Reading and Resources

Below is a link to the Microsoft C# Programming Guide

C# MSDN

Grading Criteria

Be sure to review the rubric because it might be looking for something different than what is in the instructions.



Showstoppers

A showstopper is anything that can turn the assignment grade into a zero (0%).  



Deliverables

Students will turn in one (1) zipped file that contains all images (JPG/PNG) and exercise files (Visual Studio), with the following structure and naming conventions:

LastFirst_CodingExercise7and8.zip
Exercise 7 (Folder)
Contains Visual Studio Exercise Files
Contains Console Output Images
LastFirst_Output1.jpg, LastFirst_Output2.jpg, etc
Contains Code Images
LastFirst_Code1.jpg, LastFirst_Code2.jpg, etc
Exercise 8 (Folder)
Contains Visual Studio Exercise Files
Contains Console Output Images
LastFirst_Output1.jpg, LastFirst_Output2.jpg, etc
Contains Code Images
LastFirst_Code1.jpg, LastFirst_Code2.jpg, etc






*** Don�t forget to save this to the appropriate cloud storage folder you set up in DVP1 & to your BitBucket Repo***





Returning Students

If you have taken this course before, remember, turning in an old assignment, or assignment with the same code, is considered plagiarism.  Therefore, if you are taking the course again, for this assignment, you will need to do ALL OF THE FOLLOWING:

Change the topic matter
Clean up, organize, and make the code more efficient
Add better, clear, more specific code commenting
Use more, better, and/or different OOP principles in your code
Add more interactivity, background colors, font colors, better tips for incorrect answers, etc, to the console output