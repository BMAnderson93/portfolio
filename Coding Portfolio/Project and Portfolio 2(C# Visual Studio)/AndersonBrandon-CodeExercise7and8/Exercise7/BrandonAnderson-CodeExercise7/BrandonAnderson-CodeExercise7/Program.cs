﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CodeExercise7
{
    class Program
    {
        static void Main(string[] args)
        { // These lines hold my variables for asking the user for words
            string askAdjective = "Can you give me a adjective please? (an adjective is a describing word)";
            string askNoun = "Can you give me a noun? (A noun is a person place or a thing)";
            string askVerbPast = "Can you give me a verb in past tense? (A verb is an action)";
            string askAdverb = "Can you give me a adverb? (An adverb is a word that modifies a verb, adjective, another adverb, determiner, noun phrase, clause, or sentence.)";
            string askVerb = "Can you get me a verb? (A verb is an action)";
            string input;
            List<string> usersInputs = new List<string>();
            bool running = true;

            while (running)
            { // These lines ask the user if theyd like to help make the story and either exits the aplication or begins asking the user for the needed information.
                Console.WriteLine("Hello there! Would you like to help me create a story? (Yes or No)");
                input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "yes":
                        {
                            Console.WriteLine(askAdjective);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askNoun);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askVerbPast);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askAdverb);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askAdjective);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askNoun);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askNoun);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askAdjective);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askVerb);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askAdverb);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askVerbPast);
                            usersInputs.Add(Console.ReadLine());
                            Console.WriteLine(askAdjective);
                            usersInputs.Add(Console.ReadLine());

                            Console.Clear();
                            Console.WriteLine("Thanks for the help! Press enter to see your story!");
                            Console.ReadKey();
                            PrintStory(usersInputs);

                            running = false;
                            break;
                        }

                    case "no":
                        {
                            running = false;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Please enter a valid option!");
                            break;
                        }
                }
                
            }
            Console.WriteLine("Press any key to exit!");
            Console.ReadKey();
            
        }

        static void PrintStory (List<string> userInputs)
        { //  These lines take the users inputs and puts them into the story and displays it for the user to see.
            Console.WriteLine("Today I went to the zoo. I saw a " + userInputs[0] + " " + userInputs[1] + " jumping up and down in its tree." + Environment.NewLine +" He " + userInputs[2]+ " "+ userInputs[3] +" through the large tunnel that led to its " +
                userInputs[4]+ " " + userInputs[5] + "." + Environment.NewLine + " I got some peanuts and passed them through the cage to a gigantic gray " + userInputs[6] + " towering above my head. Feeding that animal made me hungry." + Environment.NewLine + " I went to get a "
                +userInputs[7]  + " scoop of ice cream. It filled my stomach. Afterwards I had to " + userInputs[8] + " " + userInputs[9] + " to catch our bus." + Environment.NewLine + " When I got home I " + userInputs[10] + " my mom for a " + userInputs[11] + " day at the zoo."
                + Environment.NewLine );
        }
    }
}
