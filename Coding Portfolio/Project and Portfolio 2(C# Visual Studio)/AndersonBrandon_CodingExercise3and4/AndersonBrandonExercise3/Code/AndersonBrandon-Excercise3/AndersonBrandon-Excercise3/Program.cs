﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_Excercise3
{
    class Program
    {
        static void Main(string[] args)
        {
          // These lines are used to create all my different classes and naming them   
            List<string> studentsName = new List<string> { "Brandon Anderson", "Dylan Self", "Ashley Balkaran", "Cody Turner", "Nick Dyl"};
            StudentClasses coffeeHistory = new StudentClasses();
            coffeeHistory.SetName("The history of Coffee");
            StudentClasses coffeeScience = new StudentClasses();
            coffeeScience.SetName("The science behind Coffee");
            StudentClasses coffeeSmells = new StudentClasses();
            coffeeSmells.SetName("What gives Coffee its smell");
            StudentClasses coffeeBrands = new StudentClasses();
            coffeeBrands.SetName("The top Coffee brands");
            StudentClasses coffeeMaking = new StudentClasses();
            coffeeMaking.SetName("How to make your own coffee");
// Here I am taking all my classes I made and adding them into a list of classes
            List<StudentClasses> allClasses = new List<StudentClasses> {coffeeHistory, coffeeBrands, coffeeMaking, coffeeScience,
            coffeeSmells};
// Here I am taking each class and populating its student dictionary with people.
            foreach(StudentClasses classes in allClasses)
            {
                classes.AddStudents(studentsName);
            }
            





            bool running = true;
// This loop holds my menu options
            while (running)
            {
                Console.Clear();
                Console.WriteLine("Hello, welcome to the student managing program. Please select an option from the menu below!"
               + Environment.NewLine + "1. View a students grades" + Environment.NewLine + "2. View a students GPA"
                + Environment.NewLine + "3. Edit a students grades" + Environment.NewLine + "4. View a class's GPA"
                 + Environment.NewLine + "5. Exit");

                
                   string input = Console.ReadLine();

                switch (input)
                { // This sesion of code is taking the users input and displaying the users grades who they chose.
                    case "1":
                        {
                            Console.Clear();
                            string studentName = VerifyInList(studentsName);
                            Console.Clear();
                            // This line displays all of the students classes and the grades with them.
                            Console.WriteLine("Here is a list of all the classes that student is taking and their grades!");
                            foreach(StudentClasses classes in allClasses)
                            {
                                float sGrade = classes.GetGrade(studentName);
                                string sLGrade = FindLetter(sGrade);
                                string sCName = classes.GetName();
                                Console.WriteLine(sCName + Environment.NewLine + "%" + sGrade + " = " + sLGrade);
                            }
                           
                          
                            Console.ReadKey();
                            


                            break;
                        }
                    case "2":
                        {// This line takes the users input and displays that individual persons GPA
                            string studentName = VerifyInList(studentsName);
                            List<string> studentGrades = TurnGradeLetterIntoList(allClasses,studentName);
                            double gPA = FindGPA(studentGrades);
                            Console.WriteLine(studentName + " has a GPA of " + gPA);
                            Console.ReadKey();


                            break;
                        }
                        // This option allows the user to select a student and adjust their grade in a specific class
                    case "3":
                        {
                            string studentName = VerifyInList(studentsName);
                            for (int i = 0; i < allClasses.Count(); i++)
                            {

                                string sCName = allClasses[i].GetName();
                                Console.WriteLine(i + 1 + ".) " + sCName);

                            }
                            // These lines get the users option for the class they want to change and updates the students grade
                            Console.WriteLine("Please select the class number you want to change their grade in.");
                            string textInput = Console.ReadLine();
                            int userChoice;
                            while (!int.TryParse(textInput, out userChoice) || userChoice < 1 || userChoice > allClasses.Count)
                            {
                                Console.WriteLine("Please make a valid selection!");
                                Console.ReadKey();
                                Console.Clear();
                                for (int i = 0; i < allClasses.Count(); i++)
                            {

                                string sCName = allClasses[i].GetName();
                                Console.WriteLine(i + 1 + ".) " + sCName);

                            }
                                
                                for (int i = 0; i < allClasses.Count(); i++)
                                {

                                    string sCName = allClasses[i].GetName();
                                    Console.WriteLine(i + 1 + ".) " + sCName);

                                }
                                Console.WriteLine("Please select the class number you want to change their grade in.");
                                textInput = Console.ReadLine();
                            }
                            // This line gets the new grade they want to add for the current student
                            Console.WriteLine("Please enter the new grade now.");
                            string newGrade = Console.ReadLine();
                            float gradePercent;
                            while(!float.TryParse(newGrade, out gradePercent))
                            {
                                Console.WriteLine("Please enter a valid grade!");
                                newGrade = Console.ReadLine();
                                Console.Clear();
                            }
                            allClasses[userChoice - 1].SetGrade(gradePercent, studentName);
                            Console.WriteLine("The students grade has been updated!");
                            Console.ReadKey();
                            break;
                        }
                    case "4":
                        {
                            // These lines get the class from the users selection and displays the GPA for that class.
                            int counter = 1;
                            foreach (StudentClasses currentClass in allClasses)
                            {
                                Console.WriteLine(counter + "). " + currentClass.GetName());
                                counter++;
                            }
                            Console.WriteLine("Please select the number of the class you want to see the GPA of.");
                            string classNumber = Console.ReadLine();
                            int trueClassNumber;
                            while (!int.TryParse(classNumber, out trueClassNumber) || trueClassNumber < 1 || trueClassNumber > allClasses.Count)
                            {
                                Console.WriteLine("Please enter a valid selection!");
                                classNumber = Console.ReadLine();
                            }
                            List<string> classLetters = TurnClassLetterIntoList(allClasses,trueClassNumber -1, studentsName );
                           
                            double classGPA = FindGPA(classLetters);
                            Console.WriteLine("The GPA for this class is " + classGPA + " .");
                            Console.ReadKey();


                            break;
                        }

                    case "5":
                        {
                            running = false;
                            break;
                        }
                    default:
                        { // This line serves ti validate the users input and forces them to select a correct option
                            Console.WriteLine("Please enter a valid choice!");
                            break;
                        }
                }
            }


           



        } // These lines are used for displaying all the students names
        public static void DisplayKeys(List<string> names)
        {
            
            
            foreach(string word in names)
            {
                Console.WriteLine(word);
                

            }
            
        } // These lines are responcible for verifying when the user types a name its an existing student
        public static string VerifyInList (List<string> verifyMe)
        {
            DisplayKeys(verifyMe);
            Console.WriteLine("Please select the name of the student you want.");
            string studentName = Console.ReadLine();
            bool running = true;
            while (running)
            {
                foreach (string name in verifyMe)
                {
                    if (name == studentName)
                    { running = false;
                        
                    }
                    
                    
                        
                    
                }
                if (running == true)
                {
                    Console.WriteLine("Please enter a valid selection!");
                    studentName = Console.ReadLine();
                }
            }
            return studentName;
        }// These lines can take any string of letters and convert them into the GPA.
        public static double FindGPA(List<string> gradeLetter)
        {
            List<int> grades = new List<int>();
            foreach(string grade in gradeLetter)
            {
               if (grade == "A")
                {
                    grades.Add(4);
                }
                else if (grade == "B")
                {
                    grades.Add(3);
                }
                else if (grade == "C")
                {
                    grades.Add(2);
                }
                else if (grade == "D")
                {
                    grades.Add(1);
                }
                else if (grade == "F")
                {
                    grades.Add(0);
                }
            }
            float total = 0f;
            foreach(int value in grades)
            {
                total += value;
            }

            double gPA = total / grades.Count;
         
            gPA = Math.Round(gPA, 2);
            return gPA;
        } // These line will take all of the students grades in floats and change them into a list of strings
        public static List<string> TurnGradeLetterIntoList (List<StudentClasses> students, string studentName)
        {
            List<string> letterGrades = new List<string>();
            foreach(StudentClasses classes in students)
            {
                float grade = classes.GetGrade(studentName);
                letterGrades.Add(FindLetter(grade));
            }
            return letterGrades;
        }
         // These lines take an entire classes grades in floats and changes them into a list of strings
        public static List<string> TurnClassLetterIntoList(List<StudentClasses> allClasses,int choice, List<string> allStudents)
        {
            List<string> studentGrades = new List<string>();
            foreach(string student in allStudents)
            {
                float numberGrade = allClasses[choice].GetGrade(student);
                studentGrades.Add(FindLetter(numberGrade));
              
            }

            
            return studentGrades;
            

        } // These lines takes the users grade and converts it from a float to a letter grade
        public static string FindLetter (float grade)
        {
            string letterGrades;
            if (grade > 89.4)
            {
                letterGrades = "A";
            }
            else if (grade > 79.4)
            {
                letterGrades = "B";
            }
            else if (grade > 72.4)
            {
                letterGrades = "C";

            }
            else if (grade > 69.4)
            {
                letterGrades = "D";
            }
            else
            {
                letterGrades = "F";
            }
            return letterGrades;
        }
        
    }
}
