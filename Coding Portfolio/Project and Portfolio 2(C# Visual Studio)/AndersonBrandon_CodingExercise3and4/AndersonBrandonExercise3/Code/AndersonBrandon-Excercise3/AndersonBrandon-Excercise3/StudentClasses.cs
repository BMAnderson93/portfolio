﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_Excercise3
{
    class StudentClasses

    {
        private string className;
       
        // This line creates a dictionary of students for this specific class
        
        Dictionary<string, float> studentNameGrade = new Dictionary<string, float>();

         //  These lines populates the dictionary with all the students needed.
        public void AddStudents(List<string> studentNames)
        {
            foreach(string name in studentNames)
            {
                studentNameGrade.Add(name, 100);
            }
        }
        
        
       
         // This line returns all students
        public Dictionary<string,float> getAllStudents()
        {
            return studentNameGrade;
        }

        // These lines gets the class name.
        public string GetName()
        {
            return className;
        }
        // These lines gets the students grade
        public float GetGrade(string name)
        {
            return studentNameGrade[name];
        }
        // These lines populate the students with information
        public void PopStudents(List<string> names)
        {
            foreach(string name in names)
            {
                studentNameGrade.Add(name, 100);
               
            }
        }// these lines set the class name
        public void SetName(string name)
        {
            className = name;
        }
        // These lines set the users grades
        public void SetGrade(float grade, string name)
        {
            studentNameGrade[name] = grade;
           
        } 


    }
   

}
