
For this assignment, using your current skill set (Visual Studio) and programming knowledge (C#), to practice different programming case studies.



Objectives & Outcomes

After completing this activity, there will be a better understanding of:

How to code more efficiently than before
How to code under tight deadlines
How to apply programming knowledge to new exercises

Level of Effort

This coding assignment should take approximately 120 minutes to complete.

30m Research to find assignment related C# examples, samples, and information pertaining to the coding being done
30m Prep and Delivery in order to be prepared for coding and testing the code until it works properly and there are no bugs
60m Work coding and testing the code until it works
If this activity takes significantly more, or less, time than this estimate, please contact the instructor for guidance

Instructions

For this assignment, the student will complete both (2) programming exercises, in Visual Studio software, using the C# language. Both exercises must be hard/hand coded and not copied from any location, person, etc.  Before starting the assignment, the students will look up what month of the year they are currently in (for the class), and choose the programming topic matter from the list.  Any topic matter used in the assignment that is not in the list, for the current month, will receive a zero for the entire assignment. Students will also comment their code, in detail, so any reviewer of their code will know what is being attempted with the code.



MONTH

TOPIC

January

Coffee Brands (Ex: Starbucks)

February

Clothing Brands (Ex: Prada)

March

Fruits & Veggies (Ex: Banana)

April

Sports (Ex: Soccer / F�tbol)

May

Fruits & Veggies (Ex: Banana)

June

Sports (Ex: Soccer / F�tbol)

July

Coffee Brands (Ex: Starbucks)

August

Clothing Brands (Ex: Prada)

September

Sports (Ex: Soccer / F�tbol)

October

Coffee Brands (Ex: Starbucks)

November

Clothing Brands (Ex: Prada)

December

Fruits & Veggies (Ex: Banana)





Exercise 3 (Calculating Grades):

Create an Array/List/Dictionary of 5 students
Each student needs a first name and last name
Each student has taken 5 classes, named from the above monthly topic matter
Example:  If the topic is Sports, the class could be �History of Soccer�
Each student has a final grade for each of the 5 classes they have taken.
All grades need to be Doubles/Floats (Ex: 70.00, 85.67, 100.00, etc)
The user will need to see a list of all students and select one to view their classes with numerical grades, and letter grades
The user will need to be able to select a numerical grade to change for the students, which should also update the letter grade with it
The user should be able to see the GPA for each student. The GPA should update when a grade is updated.
The user should be able to see the GPA for each class. The GPA should update when a grade is updated.
Grade Point Average (GPA) is not the average of all the grades, but a point system given to each letter grade, and then added up and averaged
The grades must range from 0-100 points each
Full Sail Grading Chart:
89.5 - 100 = A
79.5 - 89.4 = B
72.5 - 79.4 = C
69.5 - 72.4 = D
0 - 69.4 = F
The �initial� console output might look something like:
Hello Admin,
Select Action:
1. Review Students
2. Review GPA
3. Edit Student
4. Exit
Take screenshots of the console output.
Take screenshots of the code
Save The Exercise


Exercise 4 (Color Dictionary Lookup):

Use the following seven colors:  ROYGBIV
Red, Orange, Yellow, Green, Blue, Indigo, Violet
Have the console/computer ask the user
What is your favorite color:  Red, Orange, Yellow, Green, Blue, Indigo, Violet?
Create a switch statement that checks for the answer.
The computer must allow both capital and lowercase letters (Ex:  Blue and blue), or have a numbered list to select from.
Create a dictionary that has 3 pieces of information about each color
The switch statement will tell the user the facts about the chosen color
(Ex:  Hmmm, did you know <color> is associated with the emotion of anger?)
The console will then ask the user if they would like to learn about another color.
This should loop until there are no more colors to choose from
Take screenshots of the output
Take screenshots of the code
Save The Exercise

Materials

For this assignment, students will be using Visual Studio on the PC side of their computer and C#.


Reading and Resources

Below is a link to the Microsoft C# Programming Guide

C# MSDN

Grading Criteria

Be sure to review the rubric because it might be looking for something different than what is in the instructions.




Showstoppers

A showstopper is anything that can turn the assignment grade into a zero (0%).  



Deliverables

Students will turn in one (1) zipped file that contains all images (JPG/PNG) and exercise files (Visual Studio), with the following structure and naming conventions:

LastFirst_CodingExercise3and4.zip
Exercise 3 (Folder)
Contains Visual Studio Exercise Files
Contains Console Output Images
LastFirst_Output1.jpg, LastFirst_Output2.jpg, etc
Contains Code Images
LastFirst_Code1.jpg, LastFirst_Code2.jpg, etc
Exercise 4 (Folder)
Contains Visual Studio Exercise Files
Contains Console Output Images
LastFirst_Output1.jpg, LastFirst_Output2.jpg, etc
Contains Code Images
LastFirst_Code1.jpg, LastFirst_Code2.jpg, etc





*** Don�t forget to save this to the appropriate cloud storage folder you set up in DVP1 & to your BitBucket Repo***






Returning Students

If you have taken this course before, remember, turning in an old assignment, or assignment with the same code, is considered plagiarism.  Therefore, if you are taking the course again, for this assignment, you will need to do ALL OF THE FOLLOWING:

Change the topic matter
Clean up, organize, and make the code more efficient
Add better, clear, more specific code commenting
Use more, better, and/or different OOP principles in your code
Add more interactivity, background colors, font colors, better tips for incorrect answers, etc, to the console output