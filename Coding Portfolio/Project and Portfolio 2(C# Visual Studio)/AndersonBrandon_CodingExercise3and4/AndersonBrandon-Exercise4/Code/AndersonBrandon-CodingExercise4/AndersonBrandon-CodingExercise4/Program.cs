﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndersonBrandon_CodingExercise4
{
    class Program
    {
        static void Main(string[] args)
        {// These lines create my list of colors and facts about each color.
            List<string> colors = new List<string> {"red","orange","yellow"
            ,"green","blue","indigo","violet"};
            List<string> redColorFacts = new List<string> {"This color travels further than any other!"
                , "If your car is this color, your insurance prices will be higher!", "This is the color of danger!" } ;
            List<string> orangeColorFacts = new List<string> {"The fruit came before the color!",
                "The only word that rymes with it is sporange!", "The sun can appear to be this color!" } ;
            List<string> yellowColorFacts = new List<string> {"This is the color of joy!", "In Japan, yellow is the color of courage!",
            "In egypt it is the color of the morning!"};
            List<string> greenColorFacts = new List<string> {"This is the color of envy!", "Its also the color of money!",
                "Green also represents nature!" } ;
            List<string> blueColorFacts = new List<string> {"Blue is my personal favorite color!", "Blue is the color of the sky.",
            "Blue has a calming effect on people."};
            List<string> indigoColorFacts = new List<string> {"Indigo is a living dye!", "The color reflects great devotion!",
                "The color represents great wisdom!" };
            List<string> violetColorFacts = new List<string> {"It is at the end of the color spectrum!", "It is the color of royalty!",
            "The color can be used to uplift a persons spirit!"};
            // These lines add the colors and their facts to the dictionary
            Dictionary<string, List<string>> colorFacts = new Dictionary<string, List<string>>();
            colorFacts.Add(colors[0], redColorFacts);
            colorFacts.Add(colors[1], orangeColorFacts);
            colorFacts.Add(colors[2], yellowColorFacts);
            colorFacts.Add(colors[3], greenColorFacts);
            colorFacts.Add(colors[4], blueColorFacts);
            colorFacts.Add(colors[5], indigoColorFacts);
            colorFacts.Add(colors[6], violetColorFacts);
            DisplayColorFacts(colorFacts);


          


        }
        public static void DisplayColorFacts(Dictionary<string, List<string>> colorList)
        {
            List<string> colors;
            
            bool running = true;
            colors = colorList.Keys.ToList();
           
            while (running)
            {// This line displays all the colors and asks the user for their choice.
                int counter = 1;
                Console.Clear();
                Console.WriteLine("Hello, please pick a color to hear some facts about!");
                foreach (string color in colors)
                {
                    Console.WriteLine(counter + "). " + color);
                    counter++;
                }
                Console.WriteLine("8). Exit");
                string input = Console.ReadLine().ToLower();
                switch (input)
                {
                    case "red":
                    case "1":
                        {// depending on the users input all these switches display the selected facts about their color.
                            foreach (string facts in colorList["red"])
                            {
                                Console.WriteLine(facts);
                            }
                            Console.WriteLine("Press any key for to return to the menu!");
                            Console.ReadKey();
                            break;
                        }
                    case "orange":
                    case "2":
                        {
                            foreach (string facts in colorList["orange"])
                            {
                                Console.WriteLine(facts);
                            }
                            Console.WriteLine("Press any key for to return to the menu!");
                            Console.ReadKey();
                            break;
                        }
                    case "yellow":
                    case "3":
                        {
                            foreach (string facts in colorList["yellow"])
                            {
                                Console.WriteLine(facts);
                            }
                            Console.WriteLine("Press any key for to return to the menu!");
                            Console.ReadKey();
                            break;
                        }
                    case "green":
                    case "4":
                        {
                            foreach (string facts in colorList["green"])
                            {
                                Console.WriteLine(facts);
                            }
                            Console.WriteLine("Press any key for to return to the menu!");
                            Console.ReadKey();
                            break;
                        }
                    case "blue":
                    case "5":
                        {
                            foreach (string facts in colorList["blue"])
                            {
                                Console.WriteLine(facts);
                            }
                            Console.WriteLine("Press any key for to return to the menu!");
                            Console.ReadKey();
                            break;
                        }
                    case "indigo":
                    case "6":
                        {
                            foreach (string facts in colorList["indigo"])
                            {
                                Console.WriteLine(facts);
                            }
                            Console.WriteLine("Press any key for to return to the menu!");
                            Console.ReadKey();
                            break;
                        }
                    case "violet":
                    case "7":
                        {
                            foreach (string facts in colorList["violet"])
                            {
                                Console.WriteLine(facts);
                            }
                            Console.WriteLine("Press any key for to return to the menu!");
                            Console.ReadKey();
                            break;
                        }// if the user is done learning the facts about these colors this line allows them to exit
                    case "exit":
                    case "8":
                        {
                            running = false;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Please enter a valid choice!");
                            break;
                        }
                }
            }
           
            
        }
    }
}
