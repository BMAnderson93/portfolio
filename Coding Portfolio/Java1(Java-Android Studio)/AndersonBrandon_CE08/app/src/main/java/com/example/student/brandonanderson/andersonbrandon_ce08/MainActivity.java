package com.example.student.brandonanderson.andersonbrandon_ce08;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
// Brandon Anderson

// JAV1 - MDV3810

//  MainActivity.java

public class MainActivity extends AppCompatActivity {

    private EditText textEntry;
    private Toast toast;
    private static final String SAVELIST_KEY = "SAVELIST";
    private static final String SAVEEDIT_KEY = "SAVEEDIT";
    private ArrayAdapter<String> stringArrayAdapter;
    private ArrayList<String> allString = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(textEntry == null){
            setup();
        }

    }
    private void setup(){
        textEntry = (EditText) findViewById(R.id.editText);
        Button inputButton = (Button) findViewById(R.id.button);
        ListView listView = (ListView) findViewById(R.id.list);
        stringArrayAdapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                allString

        );
        listView.setAdapter(stringArrayAdapter);


        inputButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(allString.contains(textEntry.getText().toString()) || textEntry.getText().toString().trim().equals("")  || textEntry.getText().toString()
                        .isEmpty() || allString.contains(textEntry.getText().toString().trim())){

                    if (toast != null){
                        toast.cancel();
                    }
                    toast = Toast.makeText(MainActivity.this, getResources().getString(R.string.error), Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }else{
                    update("",textEntry.getText().toString());
                    stringArrayAdapter.notifyDataSetChanged();


                }
            }
        });
    }
    private void update(String setEntry, String addEntry){
        if (!addEntry.isEmpty()){
            allString.add(addEntry);
        }

        textEntry.setText(setEntry);
        stringArrayAdapter.notifyDataSetChanged();

    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(SAVELIST_KEY, allString);
        outState.putString(SAVEEDIT_KEY, textEntry.getText().toString());

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        allString = savedInstanceState.getStringArrayList(SAVELIST_KEY);
        String input = savedInstanceState.getString(SAVEEDIT_KEY);
        setup();
        update(input, "");
    }
}
