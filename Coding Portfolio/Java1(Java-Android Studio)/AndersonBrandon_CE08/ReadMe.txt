Setup:
You will be creating a new Android Studio project named "LastNameFirstName_CE08" using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top (line 1) of all .java files using the below format.


// First Name Last Name

// JAV1 - Term Number

// File Name (e.g. MainActivity.java)


Grading:
Please refer to the CE:08 tab of this rubric to understand the grading procedures for this assignment. Please also refer to the Extra Information section of this assignment for additional requirements.


Deliverables:
You will compress and upload a file named "LastNameFirstName_CE08.zip" with the following contents:

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.


For this assignment, you'll be making an application that allows the user to enter in string values and display those strings in a list. Additionally, any data currently in the edit field and all data shown in the list should persist between device rotations.


Application UI:

Application should show a single edit field that allows the user to enter in a string value.
Application should show a single button that will allow the user to submit the string entered into the edit field.
Application should show a single ListView that will show all of the strings entered into the edit field.
ListView will use an ArrayAdapter to display a collection of string values.

Functionality:

When the user clicks the submit button, the application should validate the string entered into the edit field to ensure the following conditions are met:
A string was entered into the field. (Length does not equal 0)
String is not entirely whitespace. (Trimmed length does not equal 0)
If a valid string was not entered, a toast should be shown informing the user that the string isn't valid.
If a valid string was entered, it should be added to an ArrayList of strings and the edit field should be cleared out to allow for another string to be entered.
When a string is added to the ArrayList, the ListView should update to show the newly added string along with all other strings in the collection.

State Saving:

When the device rotates, if there is text entered in the edit field then it should be retained and continue to show after the rotation completes.
When the device rotates, all strings being held in the ArrayList should be retained and should show in the ListView after the rotation completes.

Structure / Efficiency / Format:

No warnings (except for Lint) are present when a code inspection is run.
Each Java file contains the comment header described earlier in this document.
All text visible to the user is stored in the appropriate resource file.
All class names should begin with an uppercase letter.
All method and variable names should begin with a lowercase letter.
A custom class name should indicate what object it represents. (e.g. Person, Book, NetworkAsyncTask, BookAdapter) Never start the class name with �my� or �custom.�
A variable name should indicate how they will be used and / or what type they contain.
(e.g. peopleList, userInput, averageNum, outerJsonObj)
A method name should indicate what action they perform and / or what they return.
(e.g. getFirstName, calculateAverage, validateInput)
All accessed views should have an ID that is named to represent what the view will be used for. (e.g. naming the addition button "button_addition")
Code blocks performing the same or similar operations should be moved into a single helper / utility method (pass unique data as arguments).
No wasted memory due to extra or needless variables.
No wasted computation time due to needless operations.
Automatic zero for associating methods with click events in the layout XML.

Extra Information


VERY IMPORTANT GRADING INFORMATION


State saving should be completed using the proper callback methods and the save state bundle.  The application should not use static variables in order to retain data during state changes.  No static variables should be used in this application. You may use static final variables for string keys, but nothing else. Using static variables will result in a zero grade for this assignment.



Below is a brief video demonstration of the completed application. Please read the instructions for the required specifications of the project.


https://youtu.be/eNq00KeJIR0

