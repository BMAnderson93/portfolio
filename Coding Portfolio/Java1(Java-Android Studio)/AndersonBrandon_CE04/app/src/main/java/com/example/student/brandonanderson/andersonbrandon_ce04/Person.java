// Brandon Anderson

// JAV1 - MDV3810

// Person.java

package com.example.student.brandonanderson.andersonbrandon_ce04;



public class Person {

    private final String mFirstName;
    private final String mLastName;
    private final String mBirthday;
    private final Integer mPicture;

    Person(String _FN, String _LN, String _BD, Integer _P ){

        mFirstName =_FN;
        mLastName = _LN;
        mBirthday = _BD;
        mPicture = _P;

    }

    String getName(){return mFirstName + " " + mLastName; }
    String getBDay(){return  mBirthday;}
    Integer getPic(){return mPicture;}


    @Override
    public String toString() {
        return  getName();
    }
}
