// Brandon Anderson

// JAV1 - MDV3810

// MainActivity.java




package com.example.student.brandonanderson.andersonbrandon_ce04;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    // Reference to the current layout's list view
    private ArrayList<Person> allPeople = new ArrayList<>();
    private GridView gridView = null;
    private ListView listView = null;
    private String selectedAdapter = "Array Adapter";
    private Person selectedPerson = null;

    private Context mContext;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createPeople();
        mContext =  this;
        gridView = (GridView) findViewById(R.id.gridView);
        listView = (ListView) findViewById(R.id.listView);
        final Spinner spinnerAdapter = (Spinner) findViewById(R.id.adapterSpinner);
        spinnerAdapter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            selectedAdapter = spinnerAdapter.getSelectedItem().toString();
               updateDisplay();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        final Spinner spinnerView = (Spinner) findViewById(R.id.viewSpinner);
        spinnerView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String selected = spinnerView.getSelectedItem().toString();
                if(selected.equals("List View")){

                    gridView.setVisibility(View.GONE);
                    gridView = null;
                    listView = (ListView) findViewById(R.id.listView);
                    listView.setVisibility(view.VISIBLE);


                }else{

                    gridView = (GridView) findViewById(R.id.gridView);
                    gridView.setVisibility(View.VISIBLE);
                    listView.setVisibility(view.GONE);
                    listView = null;

                }
                updateDisplay();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id) {
                selectedPerson = allPeople.get(position);
                displaySelected(selectedPerson);
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id) {
                selectedPerson = allPeople.get(position);
                displaySelected(selectedPerson);
            }
        });
    }

    private void displaySelected(Person selectedPerson){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setIcon(selectedPerson.getPic());
        builder.setTitle(selectedPerson.getName());
        builder.setMessage(selectedPerson.getBDay());
        builder.setPositiveButton("Cancel", null);
        builder.show();


    }
    private void updateDisplay(){

        if(selectedAdapter.equals("Array Adapter")){
            useArrayAdapter();
        }
        else if (selectedAdapter.equals("Simple Adapter")){
            userSimpleAdapter();
        }
        else if (selectedAdapter.equals("Base Adapter")){
            useCustomAdapterView();
        }



    }
    private void useCustomAdapterView() {

            // Create the adapter
            PersonAdapter aa = new PersonAdapter(this, allPeople);

            // Inform the list view of its new adapter
        if(gridView != null) {
            gridView.setAdapter(aa);
        }else{
            listView.setAdapter(aa);
        }
    }

    private void useArrayAdapter(){


        ArrayAdapter<Person> personArrayAdapter = new ArrayAdapter<>(
                mContext,
                R.layout.support_simple_spinner_dropdown_item,
                allPeople

        );
        if (gridView != null) {
            gridView.setAdapter(personArrayAdapter);
        }else{
            listView.setAdapter(personArrayAdapter);
        }
    }

    private void userSimpleAdapter(){
        final String keyFN = "fullName";
        final String keyBD = "birthDay";

        ArrayList<HashMap<String, String>> dataCollection = new ArrayList<>();


        for (Person person:allPeople) {

            // New hash map for this specific account
            HashMap<String, String> map = new HashMap<>();

            map.put(keyFN, person.getName());
            map.put(keyBD, person.getBDay());


            // Add to our collection
            dataCollection.add(map);

        }

        String[] keys = new String[]{keyFN, keyBD};



        int[] viewIds = new int[]{
                R.id.listview_item_fullName,
                R.id.listview_item_birthday

        };


        SimpleAdapter adapter = new SimpleAdapter(this, dataCollection, R.layout.personsimpleadapter, keys, viewIds);

        if(gridView != null) {
            gridView.setAdapter(adapter);
        }else{
            listView.setAdapter(adapter);
        }
    }


    private void createPeople(){

        allPeople.add(new Person("Aqua","Hearts", "12-13-1996",  R.drawable.aqua));
        allPeople.add(new Person("Axel","Fire", "07-10-1913",  R.drawable.axel));
        allPeople.add(new Person("Kairi","Destiny", "02-29-1939",  R.drawable.kairi));
        allPeople.add(new Person("Namine","Doodlez", "09-12-1330",  R.drawable.namine));
        allPeople.add(new Person("Riku","Dark", "11-11-2011",  R.drawable.riku));
        allPeople.add(new Person("Roxas","Dual", "08-06-2017",  R.drawable.roxas));
        allPeople.add(new Person("Sora","Sky", "06-21-1996",  R.drawable.sora));
        allPeople.add(new Person("Terra","Earth", "04-22-1970",  R.drawable.terra));
        allPeople.add(new Person("Ventus","Sleepy", "01-11-2001",  R.drawable.ventus));
        allPeople.add(new Person("Xion","Who?", "00-00-0000",  R.drawable.xion));


    }
}
