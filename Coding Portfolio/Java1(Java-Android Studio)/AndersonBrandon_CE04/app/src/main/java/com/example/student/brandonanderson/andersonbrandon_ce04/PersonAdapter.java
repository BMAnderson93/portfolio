// Brandon Anderson

// JAV1 - MDV3810

// PersonAdapter.java
package com.example.student.brandonanderson.andersonbrandon_ce04;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PersonAdapter extends BaseAdapter {


    private static final long BASE_ID = 0x01011;

    private Context mContext;


    private ArrayList<Person> mAccounts;


    public PersonAdapter(Context _context, ArrayList<Person> _account){

        mContext = _context;
        mAccounts = _account;
    }


    @Override
    public int getCount(){

        if(mAccounts != null){

            return mAccounts.size();
        }

        return 0;
    }


    @Override
    public Object getItem(int position){

        if(mAccounts != null && position >= 0 || position < mAccounts.size()){

            return mAccounts.get(position);
        }

        return null;
    }



    @Override
    public long getItemId(int position) {

        return BASE_ID + position;

    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder vh = null;

        if (convertView == null){

            convertView = LayoutInflater.from(mContext).inflate(R.layout.personbaseadapter, parent, false);

            vh = new ViewHolder(convertView);
            convertView.setTag(vh);

        }else{

            vh = (ViewHolder)convertView.getTag();
        }

        Person a = (Person)getItem(position);

        if (a != null){

            vh.tv_fn.setText(a.getName());

            vh.tv_bd.setText(a.getBDay());
            Integer pID = a.getPic();
            vh.iv_p.setImageResource(pID);
        }


        return convertView;
    }




    static class ViewHolder{

        private final TextView tv_fn;
        private final TextView tv_bd;
        private final ImageView iv_p;

        public ViewHolder(View _layout){

            tv_fn = (TextView) _layout.findViewById(R.id.listview_item_fullName);
            tv_bd = (TextView) _layout.findViewById(R.id.listview_item_birthday);
            iv_p = (ImageView) _layout.findViewById(R.id.listview_item_picture);


        }
    }
}
