// Brandon Anderson

// JAV1 - 3810

// MainActivity
package com.example.student.brandonanderson.andersonbrandon_ce01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final Character[] unshuffled = {'a','a','b','b','c','c','d','d','e','e','f','f','g','g','h','h'};
    private final Button[] allButton = new Button[16];
    private Button selected = null;
    private Button selected2 = null;
    private Button newGame = null;
    private Integer matches = 0;
    private Integer turnCount = 0;
    private TextView turnLabel = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Places all my buttons into an array for easy handling
        allButton[0] = (Button) findViewById(R.id.btn0);
        allButton[1] = (Button) findViewById(R.id.btn1);
        allButton[2] = (Button) findViewById(R.id.btn2);
        allButton[3] = (Button) findViewById(R.id.btn3);
        allButton[4] = (Button) findViewById(R.id.btn4);
        allButton[5] = (Button) findViewById(R.id.btn5);
        allButton[6] = (Button) findViewById(R.id.btn6);
        allButton[7] = (Button) findViewById(R.id.btn7);
        allButton[8] = (Button) findViewById(R.id.btn8);
        allButton[9] = (Button) findViewById(R.id.btn9);
        allButton[10] = (Button) findViewById(R.id.btn10);
        allButton[11] = (Button) findViewById(R.id.btn11);
        allButton[12] = (Button) findViewById(R.id.btn12);
        allButton[13] = (Button) findViewById(R.id.btn13);
        allButton[14] = (Button) findViewById(R.id.btn14);
        allButton[15] = (Button) findViewById(R.id.btn15);

        newGame = (Button) findViewById(R.id.newGame);
        turnLabel = (TextView) findViewById(R.id.scoreCounter);
        //Assigns the newgame button listener to this
        newGame.setOnClickListener(this);



        //Creates the new game
       newGame();

    }
    @Override
    //Checks to see if we need to restart the game or if were still playing, if were playing we use
    //the slandered rules for matchup to decide how to proceed
    public  void onClick(View v){
        if (matches != 8) {
    if (selected != null && selected2 != null){
        selected.setText("");
        selected2.setText("");
        selected = null;
        selected2 = null  ;
    }
    if (selected == null){

        selected = (Button) v;
        String hiddenLetter = "" + selected.getTag();
        selected.setEnabled(false);
        selected.setText(hiddenLetter);
    }
    else {

        if (selected.getTag() ==  v.getTag()) {

            matches++;


            selected.setVisibility(View.INVISIBLE);
            v.setEnabled(false);

            v.setVisibility(View.INVISIBLE);

            selected = null;
            selected2 = null;


        } else {
            selected2 = (Button) v;
            String hiddenLetter = "" + selected2.getTag();
            selected2.setText(hiddenLetter);
            selected.setEnabled(true);

        }


        turnCount++;
        if (matches == 8) {
            newGame.setEnabled(true);
            newGame.setVisibility(View.VISIBLE);
        }
        turnLabel.setText(String.format("%d",turnCount));
    }


    }
        else{
            matches = 0;
            newGame();
            turnLabel.setText(R.string.defaultLabel_text);
        }


    }

    //We use this to create a new game by shuffeling the characters and replacing them on the board
    //We also reset the board by reshowing all the buttons and hiding the reset button
    private void newGame(){
        //Shuffles the array by turning it into a list then using the collections.shuffle to return
        //new shuffled list we use for creating our board
        List<Character> shuffledArray = new ArrayList<>(Arrays.asList(unshuffled));
        Collections.shuffle(shuffledArray);
       for(int i = 0 ; i < allButton.length ; i++){
            allButton[i].setVisibility(View.VISIBLE);
            allButton[i].setEnabled(true);
            allButton[i].setOnClickListener(this);
            allButton[i].setTag(shuffledArray.get(i));
            allButton[i].setText("");

        }
        newGame.setVisibility(View.INVISIBLE);
        newGame.setEnabled(false);
        turnCount = 0;

        selected = null;
        selected2 = null;
    }
}
