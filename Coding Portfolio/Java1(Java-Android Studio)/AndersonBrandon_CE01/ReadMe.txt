Setup:
You will be creating a new Android Studio project named "LastNameFirstName_CE01" using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top (line 1) of all .java files using the below format.


// First Name Last Name

// JAV1 - Term Number

// The name of the Java file


Grading:
Please refer to the CE:01 tab of this rubric to understand the grading procedures for this assignment.


Deliverables:
You will compress and upload a file named "LastNameFirstName_CE01.zip" with the following contents:

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.


The below described app will be a letter matching game consisting of 16 unmarked buttons with eight matching pairs of letters.  The user will have unlimited guesses of matching each pair of letters.


Application UI:

A TextView to display the number of guesses attempted.
A TextView identifying (a label next to) the guesses attempted TextView.
A button for restarting the game.
This button is not visible to the user at launch of the app or after a restart of the game.
The button becomes visible only when the game has successfully been solved.
Sixteen (16) buttons arranged into four rows and four columns (4x4 arrangement).
Each button will be the same width and height as all other buttons.
When buttons are no longer visible due to a match, the rest of the buttons should not change in size or position.
All buttons should appear with no text by default.
Buttons should only show their value (text label) when selected (further described in the functionality section of this document).
When the app launches or when the user restarts the game, the application should show all 16 buttons in their default state as described above.

Functionality:

When the app launches or when the user restarts the game, the application should generate a set of characters between the letters A through H.
Two different buttons are assigned one of the eight (8) letters� creating a matching pair.
When no buttons are selected or showing a value, clicking a button will�
reveal the letter value (text label) associated with that button.
When one button is selected and showing its value, clicking another button will do one of the following:
If the two (2) selected buttons are�
matches, both buttons are hidden.
not matches, the second button reveals its letter.
Increment the guesses attempted and update the guesses attempted TextView value to show the updated value.
When two buttons are already selected and showing their value, clicking a third button will cause�
the two buttons showing their values to hide their letter values (text labels).
The third button will then show its value.
Once the user has matched all values and all buttons are no longer visible, the restart game button will become visible.
When selected, the guesses attempted will be reset to zero and displayed in the guesses attempted TextView.
The other buttons become visible with their letter values (text labels) hidden.
The restart button becomes hidden.

Structure / Efficiency / Format:

No warnings (except for Lint) are present when a code inspection is run.
Each Java file contains the comment header described earlier in this document.
All text visible to the user is stored in the appropriate resource file.
All class names should begin with an uppercase letter.
All method and variable names should begin with a lowercase letter.
A custom class name should indicate what object it represents. (e.g. Person, Book, NetworkAsyncTask, BookAdapter) Never start the class name with �my� or �custom.�
A variable name should indicate how they will be used and / or what type they contain.
(e.g. peopleList, userInput, averageNum, outerJsonObj)
A method name should indicate what action they perform and / or what they return.
(e.g. getFirstName, calculateAverage, validateInput)
All accessed views should have an ID that is named to represent what the view will be used for. (e.g. naming the addition button "button_addition")
Code blocks performing the same or similar operations should be moved into a single helper / utility method (pass unique data as arguments).
No wasted memory due to extra or needless variables.
No wasted computation time due to needless operations.
Automatic zero for associating methods with click events in the layout XML.

Extra Information


Do NOT use the layout XML to associate a click event with a method. Please implement the appropriate interface to capture the click event.


Below is a brief video demonstration of the completed application. Please read the instructions for the required specifications of the project.


https://youtu.be/uzXwKk4c8W4



