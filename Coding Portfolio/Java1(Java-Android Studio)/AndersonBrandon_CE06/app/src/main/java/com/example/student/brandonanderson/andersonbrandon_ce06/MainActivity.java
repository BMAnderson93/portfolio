package com.example.student.brandonanderson.andersonbrandon_ce06;


import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements TimerAsyncTask.OnFinished {


    private Integer min;
    private Integer sec;
    private Integer totalTime;
    private Button start;
    private Button stop;
    private EditText minTxt;
    private EditText secTxt;
    private TimerAsyncTask timerTask;
    private Toast toast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUp();
    }

    private void setUp(){
        minTxt = (EditText) findViewById(R.id.minText);
        secTxt = (EditText) findViewById(R.id.secText);
        start = (Button) findViewById(R.id.btnStart);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(toast !=null){
                toast.cancel();
                }
                if(!minTxt.getText().toString().isEmpty() && !secTxt.getText().toString().isEmpty()){
                   min = Integer.parseInt(minTxt.getText().toString());
                   sec = Integer.parseInt(secTxt.getText().toString());

                   if(min > 0 || sec > 0){

                       int maxLength = 3;
                       InputFilter[] filterArray = new InputFilter[1];
                       filterArray[0] = new InputFilter.LengthFilter(3);
                       minTxt.setFilters(filterArray);
                       totalTime = (min * 60) + sec;
                       stop.setEnabled(true);
                       start.setEnabled(false);
                       timerTask = new TimerAsyncTask(MainActivity.this, MainActivity.this);

                       timerTask.execute(totalTime.toString());



                   }else{

                       toast = Toast.makeText(MainActivity.this, getResources().getString(R.string.error) , Toast.LENGTH_SHORT);
                       toast.show();

                   }


                }else{
                    toast = Toast.makeText(MainActivity.this, getResources().getString(R.string.error) , Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });
        stop = (Button) findViewById(R.id.btnStop);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Disable the cancel button since it has no purpose until
                // the async task has been started again.

                stop.setEnabled(false);

                // Make sure we have a task to cancel, then cancel
                if(timerTask!=null) {
                    // TODO: Cancel the thread
                    timerTask.cancel(true);
                }

            }
        });
    }
    public void onFinished() {
        // Toggle button functionality

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(2);
        minTxt.setFilters(filterArray);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton(R.string.close, null);
        builder.setTitle(R.string.done);
        builder.show();
        start.setEnabled(true);
        stop.setEnabled(false);
    }

    @Override
    public void onCancelled(Float timePassed) {

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(2);
        minTxt.setFilters(filterArray); minTxt.setMaxLines(2);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton(R.string.close, null);
        builder.setTitle(R.string.timeStopped);
        String msg = getResources().getString(R.string.elapsed) + "\n"+ getResources().getString
                (R.string.hintMin)+" "+(timePassed.intValue() / 60) +" "+getResources().getString
                (R.string.hintSec)+ " " + (int)(Math.floor(timePassed % 60))  ;
        builder.setMessage(msg);
        builder.show();
        minTxt.setText(R.string.finished);
        secTxt.setText(R.string.finished);
        start.setEnabled(true);
        stop.setEnabled(false);
    }

    @Override
    public void onUpdate(Integer mins, Integer secs) {
        minTxt.setText(String.format("%02d", mins));
        secTxt.setText(String.format("%02d", secs));
    }
}
