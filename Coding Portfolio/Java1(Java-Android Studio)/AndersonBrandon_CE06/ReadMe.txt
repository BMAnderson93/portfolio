Setup:
You will be creating a new Android Studio project named "LastNameFirstName_CE06" using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top (line 1) of all .java files using the below format.


// First Name Last Name

// JAV1 - Term Number

// File Name (e.g. MainActivity.java)


Grading:
Please refer to the CE:06 tab of this rubric to understand the grading procedures for this assignment.


Deliverables:
You will compress and upload a file named "LastNameFirstName_CE06.zip" with the following contents:

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.


The below described app will be a countdown timer where the user can input an amount of time and the app should countdown to zero. The user should also be able to interrupt the timer at any point.


Application UI:

Application will contain two edit fields separated by a text field in the middle containing only a colon. Fields should be arranged in a single horizontal row.
Each edit field should use the number keyboard to accept numeric input of no more than two digits.
Application should contain two buttons. One button should be labeled to start a timer and the other should be labeled to stop a timer.

Functionality:

When the start button is clicked, if both edit fields are empty or if the amount of time entered into the fields is zero minutes and zero seconds, a toast should show to inform the user to enter a valid time.
When the start button is clicked, only if valid values are entered into the timer fields, the values entered into the two edit fields should be parsed into minutes and seconds and used to set the initial runtime of the timer.
After parsing the amount of time the timer should run, an AsyncTask should be started to run the timer as described in the AsyncTask section of this assignment.
When the stop button is clicked, if an AsyncTask is running then it should be canceled.
When the stop button is clicked, the two edit fields should be set to read "00".

AsyncTask:

When starting an AsyncTask, the initial running time for the timer should be passed into the AsyncTask using the "Params" generic type and the appropriate AsyncTask method.
Before the AsyncTask starts performing background operations, the appropriate AsyncTask method should be used to show a toast saying the timer has been started.
The AsyncTask should be used to accurately countdown from the passed in starting amount of time and finishing at zero seconds.
While the AsyncTask is running, the UI should be updated to always show the current amount of time remaining through the use of the appropriate AsyncTask method.
When the task finishes executing and counting down to zero, the appropriate AsyncTask method should be used to show an AlertDialog saying that the timer has expired.
If the user cancels the task through the use of the stop button, the appropriate AsyncTask method should be used to show an AlertDialog saying how much time elapsed before the timer was stopped.
If the task is canceled, the UI should not continue to countdown until the user has started a new timer.

Structure / Efficiency / Format:

No warnings (except for Lint) are present when a code inspection is run.
Each Java file contains the comment header described earlier in this document.
All text visible to the user is stored in the appropriate resource file.
All class names should begin with an uppercase letter.
All method and variable names should begin with a lowercase letter.
A custom class name should indicate what object it represents. (e.g. Person, Book, NetworkAsyncTask, BookAdapter) Never start the class name with �my� or �custom.�
A variable name should indicate how they will be used and / or what type they contain.
(e.g. peopleList, userInput, averageNum, outerJsonObj)
A method name should indicate what action they perform and / or what they return.
(e.g. getFirstName, calculateAverage, validateInput)
All accessed views should have an ID that is named to represent what the view will be used for. (e.g. naming the addition button "button_addition")
Code blocks performing the same or similar operations should be moved into a single helper / utility method (pass unique data as arguments).
No wasted memory due to extra or needless variables.
No wasted computation time due to needless operations.
Automatic zero for associating methods with click events in the layout XML.

Extra Information


When running your timer, do not rely on thread sleeps to accurately countdown your time.  Calling Thread.sleep(1000) might not sleep for a full 1,000 milliseconds. As the documentation states, Thread.sleep() can throw an exception and might only sleep for a portion of the time specified. Additionally, even if it does sleep for the full second, the rest of the code in your task takes time to execute as well so you might be spending 1,004 or 1,005 milliseconds. If you take this approach to countdown your time, you will not receive full credit for this assignment.


To more accurately countdown your time, use the current time on the device.  System.currentTimeMillis() will give you the current time on the device in milliseconds.  Store your start time when the task first starts and grab a new system time every so often by using Thread.sleep(500) to space out your calls a bit, but not too much.  Use the difference between the current time and the time you grabbed at the start to determine how much time has passed. Then you can use the difference between the total time your task should run and the difference between the current and start times to determine how much time is remaining.


So for instance, if your timer should run for 10,000 milliseconds and your start time was 1,000,000 milliseconds and the current time is 1,005,000 milliseconds, then 5,000 milliseconds have passed and you have 5,000 milliseconds remaining which means your UI should show five seconds remaining. This might look a bit complicated, but it's just basic subtraction once you understand where the values are coming from.


Below is a brief video demonstration of the completed application. Please read the instructions for the required specifications of the project.


TODO: Youtube Link Here


https://youtu.be/3G4b3ITvw-c