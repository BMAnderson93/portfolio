// Brandon Anderson

// JAV1 - MDV3810

// CustomBaseAdapter.java

package com.example.student.brandonanderson.andersonbrandon_ce05;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomBaseAdapter extends BaseAdapter  {


    private static final long BASE_ID = 0x01011;

    private final Context mContext;


    private final  ArrayList<Person> mAccounts;




    public CustomBaseAdapter(Context _context, ArrayList<Person> _account){

        mContext = _context;
        mAccounts = _account;

    }



    @Override
    public int getCount(){

        if(mAccounts != null){

            return mAccounts.size();
        }

        return 0;
    }


    @Override
    public Object getItem(int position){

        if(mAccounts != null && position >= 0 || position < mAccounts.size()){

            return mAccounts.get(position);
        }

        return null;
    }



    @Override
    public long getItemId(int position) {

        return BASE_ID + position;

    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder vh;

        if (convertView == null){

            convertView = LayoutInflater.from(mContext).inflate(R.layout.custombaseadapter, parent, false);

            vh = new ViewHolder(convertView);
            convertView.setTag(vh);

        }else{

            vh = (ViewHolder)convertView.getTag();
        }

        Person a = (Person)getItem(position);

        if (a != null){

            vh.tv_fn.setText(a.getName());

            if(convertView.getResources().getConfiguration().orientation == 1 ){ vh.tv_id.setText(a.getId().toString());}



        }


        return convertView;
    }




    static class ViewHolder{

        private final TextView tv_fn;
        private final  TextView tv_id;

        private ViewHolder(View _layout) {


            tv_fn = (TextView) _layout.findViewById(R.id.listview_item_fullName);

            tv_id = (TextView) _layout.findViewById(R.id.listview_item_id);


        }

    }
}