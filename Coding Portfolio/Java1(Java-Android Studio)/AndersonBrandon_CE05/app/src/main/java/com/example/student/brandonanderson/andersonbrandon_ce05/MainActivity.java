
// Brandon Anderson

// JAV1 - MDV3810

// MainActivity.java
package com.example.student.brandonanderson.andersonbrandon_ce05;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final ArrayList<Person> allPeople = new ArrayList<>();
    private Person selectedPerson = null;
    private Spinner spinner= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createPeople();

        selectedPerson = allPeople.get(0);

        CustomBaseAdapter ca = new CustomBaseAdapter(this, allPeople);
        if(getResources().getConfiguration().orientation == 1 ){
           setContentView(R.layout.activity_main);
            ListView listView = (ListView) findViewById(R.id.listView);
            listView.setAdapter(ca);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent,
                                        View v, int position, long id) {
                    selectedPerson = allPeople.get(position);
                    displaySelected();
                }
            });
            displaySelected();
        }else {
            setContentView(R.layout.activity_rotated);
            spinner = (Spinner) findViewById(R.id.spinner);

            if (spinner != null) {
                spinner.setAdapter(ca);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        selectedPerson = allPeople.get(spinner.getSelectedItemPosition());
                        displaySelected();
                    }

                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        }

        displaySelected();





    }
    private void displaySelected(){

       TextView tv_ID =  (TextView) findViewById(R.id.person_id);
       TextView tv_FN = (TextView) findViewById(R.id.person_name);
        TextView tv_Date = (TextView) findViewById(R.id.person_date);
        TextView tv_Desc = (TextView) findViewById(R.id.person_desc);
        ImageView imageView = (ImageView) findViewById(R.id.person_pic);

        tv_ID.setText(String.format("%03d",selectedPerson.getId()));
        tv_FN.setText(selectedPerson.getName());
        tv_Date.setText(selectedPerson.getBDay());
        tv_Desc.setText(selectedPerson.getDesc());
        imageView.setImageResource(selectedPerson.getPic());
    }

    private void createPeople(){

        allPeople.add(new Person("Aqua","Hearts", "12-13-1996", R.string.aqua_desc,  R.drawable.aqua,100001));
        allPeople.add(new Person("Kairi","Destiny", "02-29-1939", R.string.kairi_desc, R.drawable.kairi, 100002));
        allPeople.add(new Person("Namine","Doodlez", "09-12-1330",R.string.namine_desc,  R.drawable.namine, 100003));
        allPeople.add(new Person("Riku","Dark", "11-11-2011",R.string.riku_desc,  R.drawable.riku, 100004));
        allPeople.add(new Person("Roxas","Dual", "08-06-2017",R.string.roxas_desc,  R.drawable.roxas, 100005));
        allPeople.add(new Person("Sora","Sky", "06-21-1996",R.string.sora_desc,  R.drawable.sora, 100006));
        allPeople.add(new Person("Terra","Earth", "04-22-1970",R.string.terra_desc,  R.drawable.terra, 100007));
        allPeople.add(new Person("Ventus","Sleepy", "01-11-2001",R.string.ventus_desc,  R.drawable.ventus, 100008));
        allPeople.add(new Person("Axel","Fire", "07-10-1913",R.string.axel_desc,  R.drawable.axel, 100009));
        allPeople.add(new Person("Xion","Who?", "00-00-0000",R.string.xion_desc,  R.drawable.xion, 100010));


    }
}
