// Brandon Anderson

// JAV1 - MDV3810

// Person.java

package com.example.student.brandonanderson.andersonbrandon_ce05;



public class Person {

    private final String mFirstName;
    private final String mLastName;
    private final String mBirthday;
    private final Integer mDesc;
    private final Integer mPicture;
    private final Integer mID;

    Person(String _FN, String _LN, String _BD,Integer _Desc, Integer _P, Integer _ID ){

        mFirstName =_FN;
        mLastName = _LN;
        mDesc = _Desc;
        mID = _ID;
        mBirthday = _BD;
        mPicture = _P;

    }

    String getName(){return mFirstName + " " + mLastName; }
    Integer getDesc(){return mDesc;}
    String getBDay(){return  mBirthday;}
    Integer getPic(){return mPicture;}
    Integer getId(){return mID;}


    @Override
    public String toString() {
        return  getName();
    }
}
