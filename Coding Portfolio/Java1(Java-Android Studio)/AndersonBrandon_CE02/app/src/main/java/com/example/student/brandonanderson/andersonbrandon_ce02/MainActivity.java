// Brandon Anderson

// JAV1 - MDV3810

//  MainActivity.java


package com.example.student.brandonanderson.andersonbrandon_ce02;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {


    private final Random mRan = new Random();
    private int guessCount = 4;
    private int matchCount = 0;
    private static final EditText[] allText = new EditText[4];
    private static final int[] randomNumbers = new int[4];
    private Toast mToast = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        allText[0] = (EditText) findViewById(R.id.num1);
        allText[1] = (EditText) findViewById(R.id.num2);
        allText[2] = (EditText) findViewById(R.id.num3);
        allText[3] = (EditText) findViewById(R.id.num4);
        findViewById(R.id.submit).setOnClickListener(this);
        newGame();



    }

    @Override
    public  void onClick(View v) {

        matchCount = 0;
        if(mToast != null){
            mToast.cancel();
        }

         for(int i = 0; i < allText.length; i++ ){

            //Checks to make sure no field is empty
            if(allText[i].getText().toString().isEmpty()){
                mToast = Toast.makeText(this, R.string.toast_invalid_fields, Toast.LENGTH_SHORT);
                mToast.show();
                return;
            }
            else {
                if(randomNumbers[i] == Integer.parseInt(allText[i].getText().toString())){
                    allText[i].setTextColor(getResources().getColor(R.color.colorGreen));

                    matchCount++;
                }
                else if(randomNumbers[i] > Integer.parseInt(allText[i].getText().toString())){
                    allText[i].setTextColor(getResources().getColor(R.color.colorBlue));

                }
                else if(randomNumbers[i] < Integer.parseInt(allText[i].getText().toString())){
                    allText[i].setTextColor(getResources().getColor(R.color.colorRed));

                }

            }
        }




        guessCount --;
        if(matchCount == 4){

        buildDisplay(true);
        return;
        }
        if(guessCount == 0 ){
        buildDisplay(false);
        return;
        }



        mToast = Toast.makeText(this, String.valueOf(guessCount)+" " + getString(R.string.toast_guess_remain) , Toast.LENGTH_SHORT);
        mToast.show();



    }

    //Builds the display, changing the message whether the user wins or not
    private void buildDisplay(boolean win){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.dialog_title);
        if(win){
            builder.setMessage(R.string.dialog_msgW);
        }else{
        builder.setMessage(R.string.dialog_msgl);}

        builder.setPositiveButton(R.string.dialog_pos_btn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
               newGame();
            }
        });
        builder.show();
    }
    //Starts new game
    private void newGame(){
        guessCount = 4;
        matchCount = 0;
        for(int i = 0 ; i < 4; i++){
            randomNumbers[i] = mRan.nextInt(10);
            allText[i].setText("");
            allText[i].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }


    }


}
