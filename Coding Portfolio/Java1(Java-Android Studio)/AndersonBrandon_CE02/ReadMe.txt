Setup:
You will be creating a new Android Studio project named "LastNameFirstName_CE02" using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top (line 1) of all .java files using the below format.


// First Name Last Name

// JAV1 - Term Number

// File Name (e.g. MainActivity.java)


Grading:
Please refer to the CE:02 tab of this rubric to understand the grading procedures for this assignment.


Deliverables:
You will compress and upload a file named "LastNameFirstName_CE02.zip" with the following contents:

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.


The below described app will be a four number guessing game. The application will generate four random numbers between 0 and 9 (inclusively) and the user will have four attempts at guessing the right combination of numbers. After each guess, the app will let the user know if they were correct or if each number was too high or too low.


Application UI:

Application UI should consist of a single screen that shows all UI elements at once without any scrolling.
UI will contain four edit fields organized into a single row with each edit field taking up an equal amount of space.
All edit fields should use a numeric keyboard and only allow a single numeric character to be entered into the field.
UI will contain a single button that should appear under the edit fields. Button should be labeled to identify it as the button used to submit a guess.

Functionality:

When the app launches or when the user restarts the game, the application should generate four random numbers between 0 and 9 (inclusively).
When the user clicks the submit button, the app should verify that all fields have valid values in them and notify the user with a toast if they don't.
When the user clicks the submit button, if all fields have valid values, the app should color code each entered number as follows:
If the number entered is correct, color the number green.
If the number entered is too low, color the number blue.
If the number entered is too high, color the number red.
When the user clicks the submit button, if all fields have valid values, the user hasn't guessed properly, and the user has guesses remaining, a toast should be shown informing the user of how many guesses they have left.
If the user runs out of guesses, an AlertDialog should be shown to inform the user that they have lost. A single dialog button should be shown which, when clicked, would allow the user to restart the game.
If the user guesses all four numbers correctly in the prescribed number of guesses, an AlertDialog should be shown to inform the user that they have won. A single dialog button should be shown which, when clicked, would allow the user to restart the game.

Structure / Efficiency / Format:

No warnings (except for Lint) are present when a code inspection is run.
Each Java file contains the comment header described earlier in this document.
All text visible to the user is stored in the appropriate resource file.
All class names should begin with an uppercase letter.
All method and variable names should begin with a lowercase letter.
A custom class name should indicate what object it represents. (e.g. Person, Book, NetworkAsyncTask, BookAdapter) Never start the class name with �my� or �custom.�
A variable name should indicate how they will be used and / or what type they contain.
(e.g. peopleList, userInput, averageNum, outerJsonObj)
A method name should indicate what action they perform and / or what they return.
(e.g. getFirstName, calculateAverage, validateInput)
All accessed views should have an ID that is named to represent what the view will be used for. (e.g. naming the addition button "button_addition")
Code blocks performing the same or similar operations should be moved into a single helper / utility method (pass unique data as arguments).
No wasted memory due to extra or needless variables.
No wasted computation time due to needless operations.
Automatic zero for associating methods with click events in the layout XML.

Extra Information


Below is a brief video demonstration of the completed application. Please read the instructions for the required specifications of the project.


https://youtu.be/GFt7WRzx-P8