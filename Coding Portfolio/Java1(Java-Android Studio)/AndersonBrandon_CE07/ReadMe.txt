Setup:
You will be creating a new Android Studio project named "LastNameFirstName_CE07" using the Empty Activity template for this exercise.  Remember to include your name in a comment at the top (line 1) of all .java files using the below format.


// First Name Last Name

// JAV1 - Term Number

// File Name (e.g. MainActivity.java)


Grading:
Please refer to the CE:07 tab of this rubric to understand the grading procedures for this assignment.


Deliverables:
You will compress and upload a file named "LastNameFirstName_CE07.zip" with the following contents:

Be sure to upload the correct project.
Include a signed release APK in the root of the zip.
Entire Android Studio project folder should be submitted.
Delete any folders named "build" to reduce submission size.
Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.


Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.


In this application, you will be pulling data down from the Google Books API and displaying the results in a grid. The purpose of this assignment is to gain experience in pulling data from simpler API before picking your own API for the connected application assignment.


Application UI:

Application should show a single GridView that displays data in two columns.
Grid items should show titles of books and an image of the book.
Images displayed on grid items should come from the Google Books API and should be displayed with the help of a library such as SmartImageView or Picasso.

Networking:

Application should pull down data from the Google Books API using a URLConnection or appropriate subclass.
Application should utilize a stream reading library, such as the Apache commons-io library, to pull data from the connection's InputStream.
NOTE: Use the �jar� version of the lib instead of Maven*.
URLConnection and InputStream used to pull down data should be properly closed and disconnected when finished.
Retrieved data should be parsed using the built-in JSON classes and converted into custom objects that hold only the required data.
The network state should be checked before any connections are made. Make sure to test the application by putting the device into airplane mode.

AsyncTask:

An AsyncTask should be used to facilitate the retrieval of data from the Google Books API.
The appropriate AsyncTask callback should be used to show an indeterminate ProgressBar before the task begins background operations.
The appropriate AsyncTask method should be used to pull down data from the Google Books API on a background thread.
The appropriate AsyncTask callback should be used to hide the shown ProgressBar when the task completes background operations.
The appropriate AsyncTask callback should be used to update the UI with the retrieved data after the task finishes execution.

Structure / Efficiency / Format:

No warnings (except for Lint) are present when a code inspection is run.
Each Java file contains the comment header described earlier in this document.
All text visible to the user is stored in the appropriate resource file.
All class names should begin with an uppercase letter.
All method and variable names should begin with a lowercase letter.
A custom class name should indicate what object it represents. (e.g. Person, Book, NetworkAsyncTask, BookAdapter) Never start the class name with �my� or �custom.�
A variable name should indicate how they will be used and / or what type they contain.
(e.g. peopleList, userInput, averageNum, outerJsonObj)
A method name should indicate what action they perform and / or what they return.
(e.g. getFirstName, calculateAverage, validateInput)
All accessed views should have an ID that is named to represent what the view will be used for. (e.g. naming the addition button "button_addition")
Code blocks performing the same or similar operations should be moved into a single helper / utility method (pass unique data as arguments).
No wasted memory due to extra or needless variables.
No wasted computation time due to needless operations.
Automatic zero for associating methods with click events in the layout XML.

Extra Information


Below is a brief video demonstration of the completed application. Please read the instructions for the required specifications of the project.


https://youtu.be/TUkWK-gxdN0


* The official org.apache.studio commons-io gradle dependency now contains a native library that only works on x86 architecture. Everything works fine when deploying to the emulator; however, it fails when deploying to a physical device (the INSTALL_FAILED_NO_MATCHING_ABIS error is thrown when installing the APK).