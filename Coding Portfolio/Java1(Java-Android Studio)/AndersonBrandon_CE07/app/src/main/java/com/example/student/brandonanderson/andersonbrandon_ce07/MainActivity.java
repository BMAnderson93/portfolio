// Brandon Anderson

// JAV1 - MDV3810

// MainActivity.java
package com.example.student.brandonanderson.andersonbrandon_ce07;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class MainActivity extends AppCompatActivity implements DownloadAsyncTask.OnFinished{

    private View progressView;
    private TextView progressText;
    private GridView gridView;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUp();
        DownloadAsyncTask download = new DownloadAsyncTask(MainActivity.this, MainActivity.this);
        download.execute(" ");




    }

    public void setUp(){
        progressBar = (ProgressBar)findViewById(R.id.progress);
        progressText = (TextView)findViewById(R.id.textView2);
        progressView = findViewById(R.id.progressView);
        gridView = (GridView)findViewById(R.id.gridView);
    }
    @Override
    public void onPreExecute(Boolean connected) {
        if(connected) {
            showProgress();
        }
    }
    public void hideProgress(){
        progressBar.setVisibility(View.GONE);
        progressView.setVisibility(View.GONE);
        progressText.setVisibility(View.GONE);
    }
    public void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
        progressView.setVisibility(View.VISIBLE);
        progressText.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFinished(List<Book> books) {

        if(books.isEmpty()){
            Toast.makeText(this, R.string.connection, Toast.LENGTH_SHORT).show();

            return;
        }

        CustomBaseAdapter ca = new CustomBaseAdapter(this, books);
        gridView.setAdapter(ca);
        try {
            //Just for demonstrating that the progress bar is in fact working. Please don't take away points ;-; I did it for you!
            Thread.sleep(2000);
        }catch (Exception e){e.printStackTrace();}
        hideProgress();

    }
}
