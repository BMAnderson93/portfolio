// Brandon Anderson

// JAV1 - MDV3810

// DownloadAsyncTask.java
package com.example.student.brandonanderson.andersonbrandon_ce07;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DownloadAsyncTask extends AsyncTask<String, Integer, List<Book>> {

    static final private String TAG = "JAV1_DAY7_TASK";




    final private List<Book> allBook = new ArrayList<>();
    final private Context mHostContext;
    final private OnFinished mFinishedInterface;

    // An inner-interface for the user of this class to be notified when the task is done
    interface OnFinished {
        void onFinished(List<Book> books);
        void onPreExecute(Boolean connected);


    }

    DownloadAsyncTask(Context _context, OnFinished _finished) {
        mHostContext = _context;
        mFinishedInterface = _finished;
    }

    // TODO:
    // implement pre execute

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(mFinishedInterface != null){
            mFinishedInterface.onPreExecute(isConnected());

        }


        // Show toast



    }
    // TODO:
    // implement do in background

    @Override
    protected List<Book> doInBackground( String... strings) {
        // process parameter passed to the execute method


        doWebTest();
        return allBook;



    }

    private boolean isConnected(){
        ConnectivityManager mgr = (ConnectivityManager) mHostContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(mgr != null){
            NetworkInfo info = mgr.getActiveNetworkInfo();
            if (info != null){
                return info.isConnected();
            }
        }
        return false;
    }
    private void doWebTest() {

        //
        // NOTE: Getting a connection or network state exception? Check PERMISSIONS in manifest!
        //

        //
        // WARNING: Should be checking the network state and warn the user if not connected!
        //
        if(!isConnected()){
            Log.e(TAG, "User is not connected!" );

            return;
        }


        final String webAddress = "https://www.googleapis.com/books/v1/volumes?q=android";

        JSONObject responseJson = null;

        // HTTP URL connection reference.
        URL url = null;
        // URL reference
        HttpURLConnection connection = null;
        InputStream is = null;

        try {is = new URL( "http://commons.apache.org" ).openStream();


        } catch(Exception e){e.printStackTrace();}finally {
            IOUtils.close(connection);
        }


        try {

            // Create new URL
            url = new URL(webAddress);

            // Open connection

            connection = (HttpURLConnection)url.openConnection();
            // Perform connection operation
            connection.connect();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        // Input stream reference
        try {
            // Error check connection
            // Get the stream
            is = connection.getInputStream();
            // Convert the stream to a string (think about out utils lib)
            responseJson = new JSONObject( IOUtils.toString(is, "UTF-8"));
        }
        catch(Exception e) {
            Log.i(TAG, "WE HAD A EXCEPTION");
            e.printStackTrace();
        }
        finally {
            // If we have a stream, try to close it.
            if(connection != null){
                if(is!=null){
                    try {
                        is.close();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
                connection.disconnect();
            }
        }/**/

        // If we have a connection disconnect it

        // Display result
        buildBook(responseJson);




    }
    private void buildBook(JSONObject passedArray){

        try{JSONArray firstArray = passedArray.getJSONArray("items");

            for (int i = 0; i <firstArray.length() ; i++) {
                JSONObject firstObj = firstArray.getJSONObject(i);
                JSONObject infoObj = firstObj.getJSONObject("volumeInfo");
                JSONObject imgObj = infoObj.getJSONObject("imageLinks");
                allBook.add(new Book(imgObj.getString("thumbnail"), infoObj.getString("title")));




            }
        }

        catch (Exception e){

            e.printStackTrace();
        }

    }


    @Override
    protected void onPostExecute(List<Book> books) {
        if(mFinishedInterface != null){
            mFinishedInterface.onFinished(allBook);

        }
    }
}
