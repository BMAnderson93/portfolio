package com.example.student.brandonanderson.andersonbrandon_ce07;

// Brandon Anderson

// JAV1 - MDV3810

// Book.java
public class Book {
    private String mImgURL;
    private String mBookName;

    public Book(String _ImgURL, String _BookName){
        mBookName = _BookName;
        mImgURL =_ImgURL;

    }

    public String getmBookName() {
        return mBookName;
    }

    public String getmImgURL() {
        return mImgURL;
    }
}
