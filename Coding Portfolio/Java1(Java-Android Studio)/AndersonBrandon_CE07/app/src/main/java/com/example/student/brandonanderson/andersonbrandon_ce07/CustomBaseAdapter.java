// Brandon Anderson

// JAV1 - MDV3810

// CustomBaseAdapter.java

// Brandon Anderson

// JAV1 - MDV3810

// CustomBaseAdapter.java
package com.example.student.brandonanderson.andersonbrandon_ce07;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.student.brandonanderson.andersonbrandon_ce07.Book;
import com.loopj.android.image.SmartImageView;

import java.util.List;

public class CustomBaseAdapter extends BaseAdapter  {


    private static final long BASE_ID = 0x01011;

    private final Context mContext;


    private final  List<Book> mBooks;




    public CustomBaseAdapter(Context _context, List<Book> _books){

        mContext = _context;
        mBooks = _books;

    }



    @Override
    public int getCount(){

        if(mBooks != null){

            return mBooks.size();
        }

        return 0;
    }


    @Override
    public Object getItem(int position){

        if(mBooks != null && position >= 0 || position < mBooks.size()){

            return mBooks.get(position);
        }

        return null;
    }



    @Override
    public long getItemId(int position) {

        return BASE_ID + position;

    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder vh;

        if (convertView == null){

            convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_adapter, parent, false);

            vh = new ViewHolder(convertView);
            convertView.setTag(vh);

        }else{

            vh = (ViewHolder)convertView.getTag();
        }

        Book a = (Book)getItem(position);

        if (a != null){

           vh.tv_Book.setText(a.getmBookName());
           vh.smartImageView.setImageUrl(a.getmImgURL());




        }


        return convertView;
    }




    static class ViewHolder{

        private final TextView tv_Book;
        private final SmartImageView smartImageView;


        private ViewHolder(View _layout) {

            tv_Book = (TextView) _layout.findViewById(R.id.textView);
            smartImageView = (SmartImageView) _layout.findViewById(R.id.smartImageView);


        }

    }
}