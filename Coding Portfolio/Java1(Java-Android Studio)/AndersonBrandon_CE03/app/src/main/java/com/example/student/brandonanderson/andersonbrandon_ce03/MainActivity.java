// Brandon Anderson

// JAV1 - MDV3810

// MainActivity.java

package com.example.student.brandonanderson.andersonbrandon_ce03;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    //ArrayList<String> allStrings = new ArrayList<String>();

    private InputedWordsArrayList<String> allStrings = new InputedWordsArrayList();
    //Console says this can be final but it cant.... weird
    private EditText inputString = null;
    private TextView avgTxt = null;
    private TextView medTxt = null;
    private Toast mToast = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.editTxt_input);
        final NumberPicker numPicker = (NumberPicker) findViewById(R.id.num_picker);
        findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addString(numPicker);

            }

        });

        findViewById(R.id.btn_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeString(numPicker);

            }
        });

        avgTxt = (TextView)findViewById(R.id.txt_view_avg);
        medTxt = (TextView)findViewById(R.id.txt_view_mid);

    }
    private void removeString(final NumberPicker numPicker){


        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.alert_label);
        if (!allStrings.isEmpty()) {

            builder.setMessage(allStrings.get(numPicker.getValue()));


            builder.setPositiveButton(R.string.alert_remove, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    allStrings.remove(numPicker.getValue());

                    if (allStrings.size() == 0) {
                        numPicker.setDisplayedValues(null);
                        avgTxt.setText("");
                        medTxt.setText("");
                    } else {
                        updatePicker(numPicker);
                    }

                }
            });

        }
        else{
            builder.setMessage(R.string.toast_empty_list);
        }
        builder.setNegativeButton(R.string.alert_cancel, null);
        builder.show();
    }
    private void addString(NumberPicker numPicker){

        if(mToast != null){
            mToast.cancel();
        }
        inputString = (EditText) findViewById(R.id.editTxt_input);

        String input = inputString.getText().toString().replace(" ","");
        if( !allStrings.contains(input) && !input.isEmpty()){

            allStrings.add(input);

            updatePicker(numPicker);
            inputString.setText("");


        }
        else if (allStrings.contains(input)){

            mToast = Toast.makeText(this, R.string.toast_word_exits, Toast.LENGTH_SHORT);
            mToast.show();
        }
        else if (input.isEmpty()){

            mToast = Toast.makeText(this, R.string.toast_empty_string, Toast.LENGTH_SHORT);
            mToast.show();
        }



    }

    private  void updatePicker(NumberPicker numPicker){
        numPicker.setDisplayedValues(null);

        numPicker.setMaxValue(allStrings.size() -1);
        numPicker.setMinValue(0);
        numPicker.setDisplayedValues(allStrings.returnStrings());
        updateMath();
    }

  private class InputedWordsArrayList<E> extends ArrayList<E>
    {
        private String[] returnStrings(){
            String[] returnMe = new String[allStrings.size()];

            for(int i = 0; i < allStrings.size(); i++){
                returnMe[i] = allStrings.get(i);

            }
            return  returnMe;
        }
    }
   private void updateMath(){

        Double average = 0.0;
        Double median = 0.0;

        ArrayList<Double> wordValues = new ArrayList<>();

        for (String string: allStrings
             ) {
            wordValues.add( median+string.length());
            average += string.length();
        }
        average = (average / allStrings.size());
        Collections.sort(wordValues);
        if (wordValues.size() % 2 == 0)
            median = (wordValues.get(wordValues.size()/2) + wordValues.get(wordValues.size()/2 - 1))/2;
        else
            median = wordValues.get(wordValues.size()/2);

        avgTxt.setText(String.format("%.2f", average));

        medTxt.setText(String.format("%.2f", median));

    }

}
