CE06:  TreeView and TabControl
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE06" using the C# Windows Forms Application template.

Make sure you include your name, class and term, and code exercise name and number as comments to the top of the each code file of your project.  You will lose 5 points automatically if these comments are missing.
Grading:
Refer to the CE: TreeView and TabControl tab of this rubric to understand the grading for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE06.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Topics: Key Shortcuts, Tree View, Tab Control
Download the example executable and build your application to match all functionality presented in that application. Obviously, there is no need to include the �Demo� labels and text.  The demo executable can be downloaded here.
Images that you can use for this lab are located here.
These are the guidelines used by the grading rubric:
Display Controls(Input and Totals display)
All controls are present (comboBox, numericUpDowns, text boxes, button, and associated labels).
Direction comboBox only allows for the preset inputs to be entered.
Selecting Nowhere for the direction doesn�t allow for adding the trip.
NumericUpDowns increment by the correct amount.
Totals text boxes are not editable.
Totals text boxes are updated with total values of all trips that get added.

Tab Control
Contains two tabs, one for user input, and another for displaying the total values.
Menu Options
The New option has an icon.
The New option has a functional CTRL+N shortcut that is displayed in the menu.
The New option restores controls to their original values.
TreeView
Different icons are used for each component of each trip including differing icons for the different possible directions.
A different image is used for whichever component is selected.
Trip data displayed is based on user input.
Structure is correct with a direction node containing the three other nodes.

Extra Information
Go back through your code and check for the following:
Your application must compile and must not crash when starting up.
All variables and methods are named appropriately.
Input controls are appropriately labeled.
Any information being output to the user should be clear and concise.
Make sure nothing accesses an object that doesn�t exist.
Add comments to your code to indicate that you know what each line does.




