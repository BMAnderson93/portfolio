﻿namespace BrandonAnderson_CE06
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolNew = new System.Windows.Forms.ToolStripMenuItem();
            this.toolExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabLeg = new System.Windows.Forms.TabPage();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMode = new System.Windows.Forms.TextBox();
            this.numHours = new System.Windows.Forms.NumericUpDown();
            this.numMiles = new System.Windows.Forms.NumericUpDown();
            this.drpDirection = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabTotals = new System.Windows.Forms.TabPage();
            this.txtTLegs = new System.Windows.Forms.TextBox();
            this.txtTHours = new System.Windows.Forms.TextBox();
            this.txtTMiles = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabLeg.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMiles)).BeginInit();
            this.tabTotals.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(848, 42);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolNew,
            this.toolExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 38);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolNew
            // 
            this.toolNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.toolNew.Image = ((System.Drawing.Image)(resources.GetObject("toolNew.Image")));
            this.toolNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolNew.Name = "toolNew";
            this.toolNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolNew.Size = new System.Drawing.Size(268, 38);
            this.toolNew.Text = "New";
            this.toolNew.Click += new System.EventHandler(this.toolNew_Click);
            // 
            // toolExit
            // 
            this.toolExit.Name = "toolExit";
            this.toolExit.Size = new System.Drawing.Size(268, 38);
            this.toolExit.Text = "Exit";
            this.toolExit.Click += new System.EventHandler(this.toolExit_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabLeg);
            this.tabControl1.Controls.Add(this.tabTotals);
            this.tabControl1.Location = new System.Drawing.Point(12, 52);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(375, 560);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabLeg
            // 
            this.tabLeg.Controls.Add(this.btnAdd);
            this.tabLeg.Controls.Add(this.groupBox1);
            this.tabLeg.Location = new System.Drawing.Point(8, 39);
            this.tabLeg.Name = "tabLeg";
            this.tabLeg.Padding = new System.Windows.Forms.Padding(3);
            this.tabLeg.Size = new System.Drawing.Size(359, 513);
            this.tabLeg.TabIndex = 0;
            this.tabLeg.Text = "Leg";
            this.tabLeg.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(32, 346);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(294, 125);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMode);
            this.groupBox1.Controls.Add(this.numHours);
            this.groupBox1.Controls.Add(this.numMiles);
            this.groupBox1.Controls.Add(this.drpDirection);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(13, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(334, 282);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Leg";
            // 
            // txtMode
            // 
            this.txtMode.Location = new System.Drawing.Point(122, 212);
            this.txtMode.Name = "txtMode";
            this.txtMode.Size = new System.Drawing.Size(196, 31);
            this.txtMode.TabIndex = 8;
            // 
            // numHours
            // 
            this.numHours.DecimalPlaces = 2;
            this.numHours.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numHours.Location = new System.Drawing.Point(122, 161);
            this.numHours.Name = "numHours";
            this.numHours.Size = new System.Drawing.Size(196, 31);
            this.numHours.TabIndex = 7;
            // 
            // numMiles
            // 
            this.numMiles.DecimalPlaces = 2;
            this.numMiles.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numMiles.Location = new System.Drawing.Point(122, 110);
            this.numMiles.Name = "numMiles";
            this.numMiles.Size = new System.Drawing.Size(196, 31);
            this.numMiles.TabIndex = 6;
            // 
            // drpDirection
            // 
            this.drpDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpDirection.FormattingEnabled = true;
            this.drpDirection.Items.AddRange(new object[] {
            "Nowhere",
            "East",
            "West",
            "North",
            "South"});
            this.drpDirection.Location = new System.Drawing.Point(122, 55);
            this.drpDirection.Name = "drpDirection";
            this.drpDirection.Size = new System.Drawing.Size(196, 33);
            this.drpDirection.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Direction";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Miles";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 25);
            this.label3.TabIndex = 3;
            this.label3.Text = "Hours";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 25);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mode";
            // 
            // tabTotals
            // 
            this.tabTotals.Controls.Add(this.txtTLegs);
            this.tabTotals.Controls.Add(this.txtTHours);
            this.tabTotals.Controls.Add(this.txtTMiles);
            this.tabTotals.Controls.Add(this.label7);
            this.tabTotals.Controls.Add(this.label6);
            this.tabTotals.Controls.Add(this.label5);
            this.tabTotals.Location = new System.Drawing.Point(8, 39);
            this.tabTotals.Name = "tabTotals";
            this.tabTotals.Padding = new System.Windows.Forms.Padding(3);
            this.tabTotals.Size = new System.Drawing.Size(359, 513);
            this.tabTotals.TabIndex = 1;
            this.tabTotals.Text = "Totals";
            this.tabTotals.UseVisualStyleBackColor = true;
            // 
            // txtTLegs
            // 
            this.txtTLegs.Location = new System.Drawing.Point(122, 161);
            this.txtTLegs.Name = "txtTLegs";
            this.txtTLegs.ReadOnly = true;
            this.txtTLegs.Size = new System.Drawing.Size(198, 31);
            this.txtTLegs.TabIndex = 5;
            // 
            // txtTHours
            // 
            this.txtTHours.Location = new System.Drawing.Point(122, 113);
            this.txtTHours.Name = "txtTHours";
            this.txtTHours.ReadOnly = true;
            this.txtTHours.Size = new System.Drawing.Size(198, 31);
            this.txtTHours.TabIndex = 4;
            // 
            // txtTMiles
            // 
            this.txtTMiles.Location = new System.Drawing.Point(122, 65);
            this.txtTMiles.Name = "txtTMiles";
            this.txtTMiles.ReadOnly = true;
            this.txtTMiles.Size = new System.Drawing.Size(198, 31);
            this.txtTMiles.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 25);
            this.label7.TabIndex = 2;
            this.label7.Text = "Legs";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 25);
            this.label6.TabIndex = 1;
            this.label6.Text = "Hours";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 25);
            this.label5.TabIndex = 0;
            this.label5.Text = "Miles";
            // 
            // treeView1
            // 
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(400, 52);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(436, 560);
            this.treeView1.TabIndex = 2;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "gearIcon.png");
            this.imageList1.Images.SetKeyName(1, "rightArrow.jpg");
            this.imageList1.Images.SetKeyName(2, "leftArrow.jpg");
            this.imageList1.Images.SetKeyName(3, "upArrow.jpg");
            this.imageList1.Images.SetKeyName(4, "downArrow.jpg");
            this.imageList1.Images.SetKeyName(5, "document.png");
            this.imageList1.Images.SetKeyName(6, "plusSign.png");
            this.imageList1.Images.SetKeyName(7, "xIcon.png");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 647);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabLeg.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMiles)).EndInit();
            this.tabTotals.ResumeLayout(false);
            this.tabTotals.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolNew;
        private System.Windows.Forms.ToolStripMenuItem toolExit;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabLeg;
        private System.Windows.Forms.TabPage tabTotals;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox drpDirection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtMode;
        private System.Windows.Forms.NumericUpDown numHours;
        private System.Windows.Forms.NumericUpDown numMiles;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTLegs;
        private System.Windows.Forms.TextBox txtTHours;
        private System.Windows.Forms.TextBox txtTMiles;
        private System.Windows.Forms.ImageList imageList1;
    }
}

