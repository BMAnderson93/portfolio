﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE06
{
    // Brandon Anderson
    // VFW
    // C201802
    // TreeView and TabControl
    // CE06
    public partial class Form1 : Form
    {
        // Declares my variables for the counter
        decimal totalHours = 0;
        decimal totalMiles = 0;
        int totalLegs = 0;
        public Form1()
        {
            InitializeComponent();
            Clear();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {// Creates a new travel class based on the input and if they selcted a valid direction adds it to the nodes
            if (drpDirection.SelectedItem.ToString() != "Nowhere")
            {
                Travel newTravel = new Travel();
                newTravel.direction = drpDirection.SelectedItem.ToString();
                newTravel.index = drpDirection.SelectedIndex;
                newTravel.hours = numHours.Value;
                newTravel.miles = numMiles.Value;
                newTravel.mode = txtMode.Text;
                totalHours += newTravel.hours;
                totalMiles += newTravel.miles;
                totalLegs++;

                TreeNode n = new TreeNode();
                n.Text = newTravel.direction;
                n.ImageIndex = newTravel.index;
                n.Tag = newTravel;
                //TreeNode n2 = new TreeNode();
                //n2.Nodes.Add("Miles :" + newTravel.miles);
                //n.Nodes.Add("Hours :" + newTravel.hours);
                //n.Nodes.Add("Mode :" + newTravel.mode);

                for (int i = 5; i <= 7; i++)
                {// based on where I am in the loop adds the correct node to the main node
                    TreeNode n2 = new TreeNode();
                    switch (i)
                    {
                        case 5:
                            {
                                n2.Text = "Miles :" + newTravel.miles;
                                break;
                            }
                        case 6:
                            {
                                n2.Text = "Hours :" + newTravel.hours;
                                break;
                            }
                        case 7:
                            {
                                n2.Text = "Mode :" + newTravel.mode;
                                break;
                            }

                    }
                    // Assigns image inded, I dont need to assign the Selected index due to me putting the required image to the default
                    // location.
                    n2.ImageIndex = i;
                    n.Nodes.Add(n2);
                }

                treeView1.Nodes.Add(n);

            }
        }
        // Clears the users inputs
        private void Clear()
        {
            drpDirection.SelectedIndex = 0;
            numMiles.Value = 0;
            numHours.Value = 0;
            txtMode.Clear();
        }
        // Displays the counts to the user
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (totalLegs != 0)
            {
                txtTHours.Text = totalHours.ToString();
                txtTLegs.Text = totalLegs.ToString();
                txtTMiles.Text = totalMiles.ToString();
            }
            
        }
        // Clears the treeView and resets the counters
        private void toolNew_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Clear();
            tabControl1.SelectedIndex = 0;
            txtTHours.Clear();
            txtTLegs.Clear();
            txtTMiles.Clear();

            totalHours = 0;
            totalLegs = 0;
            totalMiles = 0;
            Clear();
        }
        // Exits
        private void toolExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
