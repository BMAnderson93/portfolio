﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE10
{
    public partial class Form1 : Form
    {
        // Brandon Anderson
        // VFW
        // C201802
        // Research and Development
        // CE10
        // Holds the variables for the event 
        public event EventHandler<ChangeEventArgs> OpenDate;
        List<Event> AllEvents = new List<Event>();
        DateTime today = DateTime.Today;
        bool save = false;
        public Form1()
        {
            // Creates my form and sets the event for when a new date is selected
            InitializeComponent();
            SetImage();
            cal.DateChanged += Cal_DateChanged;
        }
        
        public class ChangeEventArgs : EventArgs
        {
            // Creates my custom event for opening a selected date
            public int selectedTime;
            public int selectedLength;
            public string name;


            public ChangeEventArgs(int time, int length, string name)
            {
                this.selectedLength = length;
                this.selectedTime = time;
                this.name = name;
            }
        }
        // Sets the image and checks to see if the selected date has an event
        private void Cal_DateChanged(object sender, DateRangeEventArgs e)
        {
            SetImage();
            foreach (Event events in AllEvents)
            {
                if (events.dateTime == cal.SelectionRange.Start.ToString())
                {
                    AddEvent addForm1 = new AddEvent();

                    addForm1.SaveEvent += AddForm_SaveEvent;
                    OpenDate += addForm1.DisplayInfo;
                    

                    OpenDate(this, new ChangeEventArgs(events.selectedTime, events.selectedLength, events.name));
                    addForm1.ShowDialog();



                    break;
                    
                }
                else
                {
                    
                }
            }



        }
        // Method called for setting the image
        public void SetImage()
        {
            DateTime test = DateTime.Parse(cal.SelectionRange.Start.ToString());
            int test2 = int.Parse(test.Month.ToString());
            pictureMonth.Image = imageList1.Images[test2];
        }
        // Exits 
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        // Opens the event form
        private void btnAddEvent_Click(object sender, EventArgs e)
        {
            AddEvent addForm = new AddEvent();
            
            addForm.SaveEvent += AddForm_SaveEvent;
            OpenDate += addForm.DisplayInfo;
            addForm.ShowDialog();
        }
        // Adds the event to the list
        private void AddForm_SaveEvent(object sender, AddEvent.ChangeEventArgs e)
        {
            Event newEvent = new Event();
            newEvent.name = e.name;
            newEvent.selectedLength = e.selectedLength;
            newEvent.selectedTime = e.selectedTime;
            newEvent.dateTime = cal.SelectionRange.Start.ToString();
            if (AllEvents.Count == 0)
            {
                AllEvents.Add(newEvent);
            }
            else
            {
                bool foundMatch = false;
                foreach (Event events in AllEvents)
                {
                    if (events.dateTime == cal.SelectionRange.Start.ToString())
                    {
                        SaveOver saveForm = new SaveOver();
                        saveForm.SaveOverFile += SaveForm_SaveOverFile;
                        saveForm.ShowDialog();
                        if (save)
                        {
                            AllEvents.Remove(events);
                            AllEvents.Add(newEvent);
                            save = false;
                            foundMatch = true;

                            break;
                        }
                        
                    }


                }
                if(!foundMatch)
                    {
                    AllEvents.Add(newEvent);
                    
                }
            }
            
        }
        // Saves the file or not based on the bool
        private void SaveForm_SaveOverFile(object sender, SaveOver.ChangeEventArgs e)
        {
            save = e.saveOver;
        }
    }
}
