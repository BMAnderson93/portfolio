﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE10
{
    public partial class SaveOver : Form
    {
        // Brandon Anderson
        // VFW
        // C201802
        // Research and Development
        // CE10
        // Holds the variables for the event 
        public event EventHandler<ChangeEventArgs> SaveOverFile;
        public SaveOver()
        {
            InitializeComponent();
        }
        // Sends back a bool if the user wants to save over the event
        public class ChangeEventArgs : EventArgs
        {
            public bool saveOver;


            public ChangeEventArgs(bool save)
            {
                this.saveOver = save;
            }
        }

        // Sets the bool to true
        private void btnSave_Click(object sender, EventArgs e)
        {
            bool saveThis = true;
            SaveOverFile(this, new ChangeEventArgs(saveThis));
            Close();
        }
        // Closes the form without saving over
        private void btnCan_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
