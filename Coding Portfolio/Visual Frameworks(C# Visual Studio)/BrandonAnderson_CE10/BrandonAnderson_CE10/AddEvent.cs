﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE10
{
    public partial class AddEvent : Form

    {
        // Brandon Anderson
        // VFW
        // C201802
        // Research and Development
        // CE10
        // Holds the variables for the event 
        public event EventHandler<ChangeEventArgs> SaveEvent;

        public AddEvent()
      
        {
            // Sets my combo boxes to their default locations
            InitializeComponent();
            comTime.SelectedIndex = 0;
            comeLength.SelectedIndex = 0;
        }
        public class ChangeEventArgs : EventArgs
        {
            // Creates the custom event for sending the event information back to the main form
            public int selectedTime;
            public int selectedLength;
            public string name;

            
            public ChangeEventArgs(int time,int length, string name)
            {
                this.selectedLength = length;
                this.selectedTime = time;
                this.name = name;
            }
        }
        // Displays the event infor sent from the main form
        public void DisplayInfo(object sender, Form1.ChangeEventArgs e)
        {
            txtName.Text = e.name;
            comeLength.SelectedIndex = e.selectedLength;
            comTime.SelectedIndex = e.selectedTime;
            
            
        }
        // Saves the information and checks if an event on this date already exists
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveEvent != null)
            {
                SaveEvent(this, new ChangeEventArgs(comTime.SelectedIndex, comeLength.SelectedIndex, txtName.Text.ToString()));
                Close();
            }
        }
        // Closes the form without saving anything
        private void btnCan_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
