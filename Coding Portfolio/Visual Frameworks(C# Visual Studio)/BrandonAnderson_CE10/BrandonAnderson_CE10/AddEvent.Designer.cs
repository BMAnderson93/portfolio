﻿namespace BrandonAnderson_CE10
{
    partial class AddEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.comTime = new System.Windows.Forms.ComboBox();
            this.comeLength = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Event Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(81, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Event Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 262);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Event Length";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(230, 84);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(438, 31);
            this.txtName.TabIndex = 3;
            // 
            // comTime
            // 
            this.comTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comTime.FormattingEnabled = true;
            this.comTime.Items.AddRange(new object[] {
            "6:00am",
            "7:00am",
            "8:00am",
            "9:00am",
            "10:00am",
            "11:00am",
            "12:00 pm",
            "1:00pm",
            "2:00pm",
            "3:00pm",
            "4:00pm",
            "5:00pm",
            "6:00pm"});
            this.comTime.Location = new System.Drawing.Point(230, 172);
            this.comTime.Name = "comTime";
            this.comTime.Size = new System.Drawing.Size(438, 33);
            this.comTime.TabIndex = 4;
            // 
            // comeLength
            // 
            this.comeLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comeLength.FormattingEnabled = true;
            this.comeLength.Items.AddRange(new object[] {
            "30 mins",
            "1 hour",
            "1 hour 30 mins",
            "2 hours",
            "2 hours 30 mins",
            "3 hours",
            "ALL DAY"});
            this.comeLength.Location = new System.Drawing.Point(230, 254);
            this.comeLength.Name = "comeLength";
            this.comeLength.Size = new System.Drawing.Size(438, 33);
            this.comeLength.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(448, 354);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(231, 91);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCan
            // 
            this.btnCan.Location = new System.Drawing.Point(105, 354);
            this.btnCan.Name = "btnCan";
            this.btnCan.Size = new System.Drawing.Size(231, 91);
            this.btnCan.TabIndex = 7;
            this.btnCan.Text = "Cancel";
            this.btnCan.UseVisualStyleBackColor = true;
            this.btnCan.Click += new System.EventHandler(this.btnCan_Click);
            // 
            // AddEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 529);
            this.Controls.Add(this.btnCan);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.comeLength);
            this.Controls.Add(this.comTime);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddEvent";
            this.Text = "AddEvent";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.ComboBox comTime;
        private System.Windows.Forms.ComboBox comeLength;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCan;
    }
}