﻿namespace BrandonAnderson_CE10
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupbx = new System.Windows.Forms.GroupBox();
            this.btnAddEvent = new System.Windows.Forms.Button();
            this.cal = new System.Windows.Forms.MonthCalendar();
            this.pictureMonth = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.groupbx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureMonth)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(938, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "01-January.png");
            this.imageList1.Images.SetKeyName(1, "01-January.png");
            this.imageList1.Images.SetKeyName(2, "02-February.png");
            this.imageList1.Images.SetKeyName(3, "03-March.png");
            this.imageList1.Images.SetKeyName(4, "04-April.png");
            this.imageList1.Images.SetKeyName(5, "05-May.png");
            this.imageList1.Images.SetKeyName(6, "06-June.png");
            this.imageList1.Images.SetKeyName(7, "07-July.png");
            this.imageList1.Images.SetKeyName(8, "08-August.png");
            this.imageList1.Images.SetKeyName(9, "09-September.png");
            this.imageList1.Images.SetKeyName(10, "10-October.png");
            this.imageList1.Images.SetKeyName(11, "11-November.png");
            this.imageList1.Images.SetKeyName(12, "12-December.png");
            // 
            // groupbx
            // 
            this.groupbx.Controls.Add(this.btnAddEvent);
            this.groupbx.Controls.Add(this.cal);
            this.groupbx.Controls.Add(this.pictureMonth);
            this.groupbx.Location = new System.Drawing.Point(12, 64);
            this.groupbx.Name = "groupbx";
            this.groupbx.Size = new System.Drawing.Size(914, 613);
            this.groupbx.TabIndex = 1;
            this.groupbx.TabStop = false;
            this.groupbx.Text = "Calendar";
            // 
            // btnAddEvent
            // 
            this.btnAddEvent.Location = new System.Drawing.Point(436, 434);
            this.btnAddEvent.Name = "btnAddEvent";
            this.btnAddEvent.Size = new System.Drawing.Size(255, 109);
            this.btnAddEvent.TabIndex = 2;
            this.btnAddEvent.Text = "Add Event";
            this.btnAddEvent.UseVisualStyleBackColor = true;
            this.btnAddEvent.Click += new System.EventHandler(this.btnAddEvent_Click);
            // 
            // cal
            // 
            this.cal.Location = new System.Drawing.Point(359, 36);
            this.cal.MaxSelectionCount = 1;
            this.cal.Name = "cal";
            this.cal.TabIndex = 1;
            // 
            // pictureMonth
            // 
            this.pictureMonth.Location = new System.Drawing.Point(35, 54);
            this.pictureMonth.Name = "pictureMonth";
            this.pictureMonth.Size = new System.Drawing.Size(240, 204);
            this.pictureMonth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureMonth.TabIndex = 0;
            this.pictureMonth.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 651);
            this.Controls.Add(this.groupbx);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupbx.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureMonth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox groupbx;
        private System.Windows.Forms.Button btnAddEvent;
        private System.Windows.Forms.MonthCalendar cal;
        private System.Windows.Forms.PictureBox pictureMonth;
    }
}

