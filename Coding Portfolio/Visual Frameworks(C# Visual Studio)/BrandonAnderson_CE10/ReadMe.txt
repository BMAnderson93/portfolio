CE10:  Research and Development
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE10" using the C# Windows Forms Application template.

Make sure you include your name, class and term, and code exercise name and number as comments to the top of the each code file of your project.  You will lose 5 points automatically if these comments are missing.
Grading:
Refer to the CE10: Research and Development tab of this rubric to understand the grading for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE10.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Topics: MonthCalendar, PictureBox, ImageList
In both academic and professional settings, you will be called upon to work with aspects of code that you have never used before.  The application you will create for this exercise will use Windows Forms controls we have not discussed in class.  Thus, it is both a research and a development assignment.
For this application, you will implement two controls you haven�t worked with:  a MonthCalendar control and a PictureBox control.  Your form will also include a menuStrip and an imageList.  The menuStrip will contain the Exit command with a corresponding keyboard shortcut.  A button will also be required that will open a second, modal form that allows the user to enter an event name, time, and duration.
The imageList will contain the 12 images, each one corresponding to a month of the year.  You can download a zip file containing images here or you may use your own.  But the imageList must contain 12 images, each being associated with a month of the year.
When the application first opens, the MonthCalendar control will be automatically set to the current month and day.  The PictureBox should display the image associated with the current month.  When the user clicks the Next or Previous arrow buttons on the MonthCalendar control, the image in the PictureBox must change to reflect the new month showing in the calendar.  Keep in mind that if the MonthCalendar control moves from December to January, the image showing needs to revert back to the January image.  Similarly, if the Previous button is pressed and the calendar moves from January to December, the image should reflect the correct month as well.
When the Add Event button is clicked, a new user input form will open as modal that shows the currently selected date and allows the user to enter data about an event that will be assigned to that date.  A textbox should get the name of the event (such as Joe�s Birthday or Dinner Out).  A comboBox should allow the user to select a time between 6:00am and 6:00pm.  Only hour increments are required, but you may do half-hour increments if you wish.  A second comboBox will allow the user to select the duration of the event from 30 mins to 3 hours (in half-hour increments) and including a selection for All Day.  The user must not be able to enter their own data into the comboBox.
The user input form should have two buttons:  Save Event and Cancel.  The Cancel button should close the form without saving the event.  The Save Event button should send the data back to the main form where it will be saved in a List.  When the user clicks to select a date in the MonthCalendar control, the application should check that date against the objects in the List to determine if an event exists for that day.  If one does, a new User Input window should open and be populated with data from the day�s event object.  The user should have the ability to edit the information and save it to the List on the main form.
You will be required to use custom event arguments to send the data from one form to another.  
These are the guidelines used by the grading rubric:
Main Form
PictureBox, MonthCalendar, and ImageList controls exist on the form.
A menuStrip is present on the form.
The menuStrip has a File menu with an Exit command.
The menu command has a keyboard shortcut assigned.
A button exists that when clicked opens the User Input form.
User Input Form
The Date on the User Input form comes from the Main Form.
The form contains a textbox for event name, a comboBox for event time, and a comboBox for event duration.
A Cancel button exists that closes the form.
A Save Event button exists that saves the data to the Main Form.


Events
An event on the MonthCalendar control opens the User Input Form and populates it with data from the List on the Main Form.
An event on the User Input form sends the data back to the Main Form.
Custom event arguments are used to send the data from one form to another.
Extra Information
Go back through your code and check for the following:
Your application must compile and must not crash when starting up.
All variables, controls, and methods are named appropriately.
Input controls are appropriately labeled.
Any information being output to the user should be clear and concise.
Make sure nothing accesses an object that doesn�t exist.
Add comments to your code to indicate that you know what each line does.

