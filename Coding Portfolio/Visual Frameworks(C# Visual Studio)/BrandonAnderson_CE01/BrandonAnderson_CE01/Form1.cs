﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace BrandonAnderson_CE01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        { // Exits the application
            Application.Exit();
        }

        private void btnReset_Click(object sender, EventArgs e)
        { // Clears all the data fields to the default
            txtNameField.Clear();
            checkbxCurrentSmoker.Checked = false;

            dateDOB.Value = DateTime.Now;
        }

        private void toolstripSave_Click(object sender, EventArgs e)
        {
            
            Stream myStream ;
            
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
               
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {// These lines save each of the data fields to strings to be then saved at the selected location
                    string name = txtNameField.Text;
                    string theDate = dateDOB.Value.ToShortDateString();
                    bool isChecked = checkbxCurrentSmoker.Checked;
                    string checkValue ;
                    if (isChecked == true)
                    {
                        checkValue = "true";
                    }
                    else
                        checkValue = "false";
                    // Takes all the strings created above and puts them into 1 array to be then written to the saved file
                    string[] allInfo = new string[] {name,theDate,checkValue };
                    string path = saveFileDialog1.FileName;
                    StreamWriter streamWriter = new StreamWriter(myStream);


                    foreach (string word in allInfo)
                    {
                        streamWriter.WriteLine(word);
                    }
                    streamWriter.Close();


                    myStream.Close();
                }
            }
        }

        private void toolstripLoad_Click(object sender, EventArgs e)
        {

            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            List<string> allInfo = new List<string>();
                            StreamReader streamReader = new StreamReader(myStream);
                            string line;
                            // Reads each line of the selected file and adds them to the created list of strings
                            while ((line = streamReader.ReadLine()) != null)
                            {

                                allInfo.Add(line);
 ;                                
                            }
                            // These lines put each string from the list into the matching data fields
                            txtNameField.Text = allInfo[0];
                            dateDOB.Value = Convert.ToDateTime(allInfo[1]);

                            if (allInfo[2] == "true")
                            {
                                checkbxCurrentSmoker.Checked = true;
                            }
                            else checkbxCurrentSmoker.Checked = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }
    }
}
