﻿namespace BrandonAnderson_CE01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolstripMyFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolstripLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.toolstripSave = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExitApp = new System.Windows.Forms.ToolStripMenuItem();
            this.grpbxUserInfo = new System.Windows.Forms.GroupBox();
            this.lblUserFullName = new System.Windows.Forms.Label();
            this.txtNameField = new System.Windows.Forms.TextBox();
            this.lblCurrentSmoker = new System.Windows.Forms.Label();
            this.checkbxCurrentSmoker = new System.Windows.Forms.CheckBox();
            this.dateDOB = new System.Windows.Forms.DateTimePicker();
            this.lblDOB = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.grpbxUserInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolstripMyFile});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1054, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolstripMyFile
            // 
            this.toolstripMyFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolstripLoad,
            this.toolstripSave,
            this.btnExitApp});
            this.toolstripMyFile.Name = "toolstripMyFile";
            this.toolstripMyFile.Size = new System.Drawing.Size(64, 38);
            this.toolstripMyFile.Text = "File";
            // 
            // toolstripLoad
            // 
            this.toolstripLoad.Name = "toolstripLoad";
            this.toolstripLoad.Size = new System.Drawing.Size(268, 38);
            this.toolstripLoad.Text = "Load";
            this.toolstripLoad.Click += new System.EventHandler(this.toolstripLoad_Click);
            // 
            // toolstripSave
            // 
            this.toolstripSave.Name = "toolstripSave";
            this.toolstripSave.Size = new System.Drawing.Size(268, 38);
            this.toolstripSave.Text = "Save";
            this.toolstripSave.Click += new System.EventHandler(this.toolstripSave_Click);
            // 
            // btnExitApp
            // 
            this.btnExitApp.Name = "btnExitApp";
            this.btnExitApp.Size = new System.Drawing.Size(268, 38);
            this.btnExitApp.Text = "Exit";
            this.btnExitApp.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // grpbxUserInfo
            // 
            this.grpbxUserInfo.Controls.Add(this.btnReset);
            this.grpbxUserInfo.Controls.Add(this.lblDOB);
            this.grpbxUserInfo.Controls.Add(this.dateDOB);
            this.grpbxUserInfo.Controls.Add(this.checkbxCurrentSmoker);
            this.grpbxUserInfo.Controls.Add(this.lblCurrentSmoker);
            this.grpbxUserInfo.Controls.Add(this.txtNameField);
            this.grpbxUserInfo.Controls.Add(this.lblUserFullName);
            this.grpbxUserInfo.Location = new System.Drawing.Point(12, 68);
            this.grpbxUserInfo.Name = "grpbxUserInfo";
            this.grpbxUserInfo.Size = new System.Drawing.Size(1030, 659);
            this.grpbxUserInfo.TabIndex = 1;
            this.grpbxUserInfo.TabStop = false;
            this.grpbxUserInfo.Text = "User Information";
            // 
            // lblUserFullName
            // 
            this.lblUserFullName.AutoSize = true;
            this.lblUserFullName.Location = new System.Drawing.Point(6, 81);
            this.lblUserFullName.Name = "lblUserFullName";
            this.lblUserFullName.Size = new System.Drawing.Size(363, 25);
            this.lblUserFullName.TabIndex = 0;
            this.lblUserFullName.Text = "Please enter your first and last name";
            // 
            // txtNameField
            // 
            this.txtNameField.Location = new System.Drawing.Point(6, 119);
            this.txtNameField.Name = "txtNameField";
            this.txtNameField.Size = new System.Drawing.Size(518, 31);
            this.txtNameField.TabIndex = 1;
            // 
            // lblCurrentSmoker
            // 
            this.lblCurrentSmoker.AutoSize = true;
            this.lblCurrentSmoker.Location = new System.Drawing.Point(6, 177);
            this.lblCurrentSmoker.Name = "lblCurrentSmoker";
            this.lblCurrentSmoker.Size = new System.Drawing.Size(265, 25);
            this.lblCurrentSmoker.TabIndex = 2;
            this.lblCurrentSmoker.Text = "Are you a current smoker?";
            // 
            // checkbxCurrentSmoker
            // 
            this.checkbxCurrentSmoker.AutoSize = true;
            this.checkbxCurrentSmoker.Location = new System.Drawing.Point(11, 225);
            this.checkbxCurrentSmoker.Name = "checkbxCurrentSmoker";
            this.checkbxCurrentSmoker.Size = new System.Drawing.Size(318, 29);
            this.checkbxCurrentSmoker.TabIndex = 2;
            this.checkbxCurrentSmoker.Text = "Yes I am (Leave blank if not)";
            this.checkbxCurrentSmoker.UseVisualStyleBackColor = true;
            // 
            // dateDOB
            // 
            this.dateDOB.Location = new System.Drawing.Point(6, 337);
            this.dateDOB.Name = "dateDOB";
            this.dateDOB.Size = new System.Drawing.Size(406, 31);
            this.dateDOB.TabIndex = 3;
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Location = new System.Drawing.Point(6, 287);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(315, 25);
            this.lblDOB.TabIndex = 5;
            this.lblDOB.Text = "Please select your date of birth!";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(764, 555);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(247, 77);
            this.btnReset.TabIndex = 4;
            this.btnReset.Text = "Reset Data Fields";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 739);
            this.Controls.Add(this.grpbxUserInfo);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grpbxUserInfo.ResumeLayout(false);
            this.grpbxUserInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolstripMyFile;
        private System.Windows.Forms.ToolStripMenuItem toolstripLoad;
        private System.Windows.Forms.ToolStripMenuItem toolstripSave;
        private System.Windows.Forms.ToolStripMenuItem btnExitApp;
        private System.Windows.Forms.GroupBox grpbxUserInfo;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.DateTimePicker dateDOB;
        private System.Windows.Forms.CheckBox checkbxCurrentSmoker;
        private System.Windows.Forms.Label lblCurrentSmoker;
        private System.Windows.Forms.TextBox txtNameField;
        private System.Windows.Forms.Label lblUserFullName;
        private System.Windows.Forms.Button btnReset;
    }
}

