﻿namespace BrandonAnderson_CE07
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.toolSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolNew = new System.Windows.Forms.ToolStripMenuItem();
            this.toolExit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDisc = new System.Windows.Forms.TextBox();
            this.numHours = new System.Windows.Forms.NumericUpDown();
            this.numMonth = new System.Windows.Forms.NumericUpDown();
            this.txtCourseCode = new System.Windows.Forms.TextBox();
            this.txtClassName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.rdoASD = new System.Windows.Forms.RadioButton();
            this.rdoVFW = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdoXMl = new System.Windows.Forms.RadioButton();
            this.rdoJson = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMonth)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(12, 4, 0, 4);
            this.menuStrip1.Size = new System.Drawing.Size(1228, 44);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolLoad,
            this.toolSave,
            this.toolNew,
            this.toolExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolLoad
            // 
            this.toolLoad.Name = "toolLoad";
            this.toolLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.toolLoad.Size = new System.Drawing.Size(268, 38);
            this.toolLoad.Text = "Load";
            this.toolLoad.Click += new System.EventHandler(this.toolLoad_Click);
            // 
            // toolSave
            // 
            this.toolSave.Name = "toolSave";
            this.toolSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.toolSave.Size = new System.Drawing.Size(268, 38);
            this.toolSave.Text = "Save";
            this.toolSave.Click += new System.EventHandler(this.toolSave_Click);
            // 
            // toolNew
            // 
            this.toolNew.Name = "toolNew";
            this.toolNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolNew.Size = new System.Drawing.Size(268, 38);
            this.toolNew.Text = "New";
            this.toolNew.Click += new System.EventHandler(this.toolNew_Click);
            // 
            // toolExit
            // 
            this.toolExit.Name = "toolExit";
            this.toolExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.toolExit.Size = new System.Drawing.Size(268, 38);
            this.toolExit.Text = "Exit";
            this.toolExit.Click += new System.EventHandler(this.toolExit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDisc);
            this.groupBox1.Controls.Add(this.numHours);
            this.groupBox1.Controls.Add(this.numMonth);
            this.groupBox1.Controls.Add(this.txtCourseCode);
            this.groupBox1.Controls.Add(this.txtClassName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnLoad);
            this.groupBox1.Location = new System.Drawing.Point(24, 183);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Size = new System.Drawing.Size(1180, 544);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Class Information";
            // 
            // txtDisc
            // 
            this.txtDisc.Location = new System.Drawing.Point(688, 62);
            this.txtDisc.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtDisc.Multiline = true;
            this.txtDisc.Name = "txtDisc";
            this.txtDisc.ReadOnly = true;
            this.txtDisc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDisc.Size = new System.Drawing.Size(476, 467);
            this.txtDisc.TabIndex = 12;
            // 
            // numHours
            // 
            this.numHours.Location = new System.Drawing.Point(232, 225);
            this.numHours.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.numHours.Name = "numHours";
            this.numHours.ReadOnly = true;
            this.numHours.Size = new System.Drawing.Size(240, 31);
            this.numHours.TabIndex = 11;
            // 
            // numMonth
            // 
            this.numMonth.Location = new System.Drawing.Point(232, 173);
            this.numMonth.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.numMonth.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numMonth.Name = "numMonth";
            this.numMonth.ReadOnly = true;
            this.numMonth.Size = new System.Drawing.Size(240, 31);
            this.numMonth.TabIndex = 10;
            // 
            // txtCourseCode
            // 
            this.txtCourseCode.Location = new System.Drawing.Point(232, 123);
            this.txtCourseCode.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtCourseCode.Name = "txtCourseCode";
            this.txtCourseCode.ReadOnly = true;
            this.txtCourseCode.Size = new System.Drawing.Size(440, 31);
            this.txtCourseCode.TabIndex = 9;
            // 
            // txtClassName
            // 
            this.txtClassName.Location = new System.Drawing.Point(232, 69);
            this.txtClassName.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtClassName.Name = "txtClassName";
            this.txtClassName.ReadOnly = true;
            this.txtClassName.Size = new System.Drawing.Size(440, 31);
            this.txtClassName.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(682, 31);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(195, 25);
            this.label5.TabIndex = 7;
            this.label5.Text = "Course Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 229);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Credit Hours";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 181);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Month";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 129);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Course Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 75);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Class Name";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(132, 325);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(340, 137);
            this.btnLoad.TabIndex = 2;
            this.btnLoad.Text = "Load Class Data";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // rdoASD
            // 
            this.rdoASD.AutoSize = true;
            this.rdoASD.Checked = true;
            this.rdoASD.Location = new System.Drawing.Point(52, 60);
            this.rdoASD.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.rdoASD.Name = "rdoASD";
            this.rdoASD.Size = new System.Drawing.Size(86, 29);
            this.rdoASD.TabIndex = 0;
            this.rdoASD.TabStop = true;
            this.rdoASD.Text = "ASD";
            this.rdoASD.UseVisualStyleBackColor = true;
            // 
            // rdoVFW
            // 
            this.rdoVFW.AutoSize = true;
            this.rdoVFW.Location = new System.Drawing.Point(232, 60);
            this.rdoVFW.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.rdoVFW.Name = "rdoVFW";
            this.rdoVFW.Size = new System.Drawing.Size(90, 29);
            this.rdoVFW.TabIndex = 1;
            this.rdoVFW.Text = "VFW";
            this.rdoVFW.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdoVFW);
            this.groupBox2.Controls.Add(this.rdoASD);
            this.groupBox2.Location = new System.Drawing.Point(24, 67);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox2.Size = new System.Drawing.Size(478, 104);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select The Class";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdoXMl);
            this.groupBox3.Controls.Add(this.rdoJson);
            this.groupBox3.Location = new System.Drawing.Point(712, 67);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox3.Size = new System.Drawing.Size(478, 104);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Select The Data Type";
            // 
            // rdoXMl
            // 
            this.rdoXMl.AutoSize = true;
            this.rdoXMl.Location = new System.Drawing.Point(232, 60);
            this.rdoXMl.Margin = new System.Windows.Forms.Padding(6);
            this.rdoXMl.Name = "rdoXMl";
            this.rdoXMl.Size = new System.Drawing.Size(87, 29);
            this.rdoXMl.TabIndex = 1;
            this.rdoXMl.Text = "XML";
            this.rdoXMl.UseVisualStyleBackColor = true;
            // 
            // rdoJson
            // 
            this.rdoJson.AutoSize = true;
            this.rdoJson.Checked = true;
            this.rdoJson.Location = new System.Drawing.Point(52, 60);
            this.rdoJson.Margin = new System.Windows.Forms.Padding(6);
            this.rdoJson.Name = "rdoJson";
            this.rdoJson.Size = new System.Drawing.Size(99, 29);
            this.rdoJson.TabIndex = 0;
            this.rdoJson.TabStop = true;
            this.rdoJson.Text = "JSON";
            this.rdoJson.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 750);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMonth)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolLoad;
        private System.Windows.Forms.ToolStripMenuItem toolSave;
        private System.Windows.Forms.ToolStripMenuItem toolNew;
        private System.Windows.Forms.ToolStripMenuItem toolExit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtClassName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.RadioButton rdoASD;
        private System.Windows.Forms.RadioButton rdoVFW;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDisc;
        private System.Windows.Forms.NumericUpDown numHours;
        private System.Windows.Forms.NumericUpDown numMonth;
        private System.Windows.Forms.TextBox txtCourseCode;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdoXMl;
        private System.Windows.Forms.RadioButton rdoJson;
    }
}

