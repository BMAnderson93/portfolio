﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;





namespace BrandonAnderson_CE07
{
    // Brandon Anderson
    // VFW
    // C201802
    // JSON and Web Connectivity
    // CE07
    public partial class Form1 : Form
    {
        // Creates the variables for where to get my information
        WebClient apiConnection = new WebClient();
        public string apiVFW = "http://mdv-vfw.com/vfw.json";
        public string apiASD = "http://mdv-vfw.com/asd.json";

        public JObject o;
        public Form1()
        {
            InitializeComponent();
            Clear();
           


        }
        // Clears the input fields
        private void Clear()
        {
            rdoASD.Checked = true;
            txtClassName.Clear();
            txtCourseCode.Clear();
            txtDisc.Clear();
            numHours.Value = 0;
            numMonth.Value = 0;
        }

        // Loads the input fields depending on which class is picked
        private void btnLoad_Click(object sender, EventArgs e)
        {try
            {
                if (rdoASD.Checked)
                {
                    var asdData = apiConnection.DownloadString(apiASD);
                    o = JObject.Parse(asdData);



                }

                else
                {
                    var vfwData = apiConnection.DownloadString(apiVFW);
                    o = JObject.Parse(vfwData);

                }
                Write();

            }
            // Catches the error if there is no internet
            catch (Exception ex)
            {
                MessageBox.Show("You need internet access before doing this!");
            }
           
        }
        // Exits the app
        private void toolExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        // Clears inputs
        private void toolNew_Click(object sender, EventArgs e)
        {
            Clear();
        }
        // Saves the selected class file and serializes it
        private void toolSave_Click(object sender, EventArgs e)
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    StreamWriter streamWriter = new StreamWriter(myStream);
                    JsonWriter writer = new JsonTextWriter(streamWriter);
                    o["class"]["course_description"] = txtDisc.Text;
                    o["class"]["course_name_clean"] = txtClassName.Text;
                    o["class"]["course_code_long"] = txtCourseCode.Text;
                    o["class"]["credit"] = numHours.Value;
                    o["class"]["sequence"] = numMonth.Value;
                    o.WriteTo(writer);
                    writer.Formatting = Formatting.Indented;
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(writer, o);
                    Clear();
                    myStream.Close();
                }
            }
        }
        // Loads the selected file and checks to make sure it matchs the indentifier given
        private void toolLoad_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            StreamReader streamReader = new StreamReader(myStream);

                            JsonReader jsonReader = new JsonTextReader(streamReader);
                            JsonSerializer serializer = new JsonSerializer();
                            o = (JObject)serializer.Deserialize(jsonReader);
                            Write();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("You selected an incorrect file type! Please pick a different one!");
                }

            }
        }
        // Writes the selected class to the form and also makes sure the indentifier is there
        private void Write()
        {
            if (o["class"] != null)
            {
                txtDisc.Text = o["class"]["course_description"].ToString();
                txtClassName.Text = o["class"]["course_name_clean"].ToString();
                txtCourseCode.Text = o["class"]["course_code_long"].ToString();
                numHours.Value = decimal.Parse(o["class"]["credit"].ToString());
                numMonth.Value = decimal.Parse(o["class"]["sequence"].ToString());

                if (txtClassName.Text == "Visual Frameworks")
                {
                    rdoVFW.Checked = true;
                }
                else
                {
                    rdoASD.Checked = true;
                }
            }
            else
            {
                MessageBox.Show("You selected an incorrect file type! Please pick a different one!");
            }
        }
    }
}
