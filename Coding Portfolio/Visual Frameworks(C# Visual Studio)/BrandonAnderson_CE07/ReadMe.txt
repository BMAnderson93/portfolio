CE07:  JSON and Web Connectivity
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE07" using the C# Windows Forms Application template.

Make sure you include your name, class and term, and code exercise name and number as comments to the top of the each code file of your project.  You will lose 5 points automatically if these comments are missing.
Grading:
Refer to the CE07: JSON and Web Connectivity tab of this rubric to understand the grading for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE07.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Topics: Web Services, JSON and JSON.Net, Saving and Loading, Multiline TextBoxes with scroll bars
Students will create an application that can download JSON data from a Web source.  The sources to be used are as follows:
	http://mdv-vfw.com/vfw.json
	http://mdv-vfw.com/asd.json
These should be hard-coded within the project.  Which Web address is used will be determined by a radio button selection made by the user.  You will be required to have a try...catch to ensure that the application does not crash if it cannot connect to the Internet.  This was not demonstrated, so you�ll have to do a little research to make sure it works properly.
You can use code from the examples in order to make your connections.  You will want to view the JSON data so that you know what keys will be available (the keys are the same for both JSON files).  At a minimum you will need to include the following information within the controls on the form:  class selection (ASD or VFW), class name, course code, course description, month (sequence), and credit hours.  You may add additional controls and data, but you must include these.
Once you have downloaded the JSON data, you will select only the required data to populate user input controls on the form.  This will require you to know what data is being downloaded because you will select your controls based on that data.  So, if the data is a number, you should convert it to a number and populate a number control with that data.  Do not use only textboxes to show the data.  The course description should appear in a multiline textbox that contains a vertical scrollbar.  This was not demonstrated in class, so you will need to do some research on textboxes and the properties required to make this work.
The data should save to a text file in a way that will allow it to be loaded back into the form.  But, there should be some mechanism within the file to verify that it is a correct data file before loading it.  In other words, if I try to load a text file that does not include this identifier, the application should indicate this to the user and not load the file.
The Main form should have a File menu that contains the following commands: Load, Save, New, and Exit.  The Load command should allow the user to select a file to load data into the user input controls, the Save command should allow the user to save the current data from the user input controls to a file of their choosing, the New command should clear the form, and the Exit command should close the application.  Each menu command should also have keyboard shortcuts for each command: Load -> CTRL+L, Save -> CTRL+S, New->CTRL+N, Exit->CTRL+Q.
These are the guidelines used by the grading rubric:
Web
The selected Web address is accessible and allows the application to download JSON data.
The Web address used is determined by the user.
The application verifies with a try...catch Web connectivity before trying to download the data.
The data is parsed correctly.
Only the data needed is used to fill the user input controls.
User Input Controls
Controls are included to display the data from the JSON.
A button or some other mechanism exists to allow the user to determine when to download and display the data.
Appropriate controls are used for appropriate data types (such as numericUpDown for numbers).
All controls are cleared to default values when the New command is selected or the keyboard shortcut is used.
File I/O
The data in the user controls is stored to a text file selected using the SaveFileDialog class and opened using the OpenFileDialog class.
The text file also includes an identifier to indicate this is a proper data file for the application.
The data from the file loads into the user controls from the selected file.
The load feature ignores any text file that does not include the identifier to indicate this is a proper data file, but indicates to the user that it is an incorrect file.
Main Form
MenuStrip is used for commands.
Four commands exist on the File menu: Load, Save, New, Exit.
Each command on the File menu has a corresponding keyboard shortcut.

Extra Information
Go back through your code and check for the following:
Your application must compile and must not crash when starting up.
All variables, controls, and methods are named appropriately.
Input controls are appropriately labeled.
Any information being output to the user should be clear and concise.
Make sure nothing accesses an object that doesn�t exist.
Add comments to your code to indicate that you know what each line does.


