CE04:  ListViews
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE04" using the C# Windows Forms Application template.

Make sure you include your name, class and term, and code exercise name and number as comments to the top of the each code file of your project.  You will lose 5 points automatically if these comments are missing.��
Grading:
Refer to the CE: ListViews tab of this rubric to understand the grading for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE04.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Topics: ListView, ImageList
Students will create an application that allows the user to create entities with a dialog window that will be displayed by a ListView in a separate dialog. The main dialog will keep track of how many windows and entities exist.
You will need the same user input setup as usual with a functional Clear button. This user input will be in a popup form separate from the main window. This popup form will also have a toolStrip with an Add button. This Add button will store the current input in an object that will then be stored in a List in the main window. For UI, the main window will have two text displays, one that displays how many user input windows currently exist and one that displays how many objects currently exist that were created by the user input dialogs. The main window will also have the standard File->Exit menu bar option, but in addition to this there will also be a separate List menu with Display and Clear options. The List->Clear option should clear the List of created objects. The List->Display option should open a separate window that can only have one exist at a time and this window will have a ListView for displaying the same objects that are stored in the main window�s List. Double-clicking one of the ListView objects should open a fully functional user input window that is populated with the object�s values. The ListView will use one of the icon views and should be set to fill the window. The only other UI component of this window will be a ToolStrip with a Clear button. This Clear button should clear the List of objects. The main window�s object count and number of objects being shown in the ListView should be the same at all times.��
Follow these guidelines to produce this application:
User Input Window
4 unique user input controls in a group box.
A button that clears all of the user input controls.
A ToolStrip should contain a Button for adding the current input values to a List (not a ListBox) in the main window.
Main Window
Contains a List of objects are added using the user input window.
A Text display should always display how many user input windows currently exist.
A Text display should always display how many objects are currently stored and being displayed in the ListView on the ListView Form.
List->Display displays the ListView Form only if it is not already displayed. If the ListView is already displayed, then there should be a check mark next to the Display option in the menu and a second instance of this window should not be created or displayed.
List->Clear clears the main window�s List as well as the ListView Form�s items in the ListView.
A button that opens a new input window each time it is pressed.
ListView Form
Contents should be the same as the main window�s List whenever the ListView Form is visible/exists.
When an Item is double-clicked in this ListView, a new, fully functional user input window should open with the user input controls populated by the values of the Item the user double-clicked.
A ToolStrip should contain a Clear button with text and an image that, when clicked, clears both the ListView items and the main window�s List.
The ListView should use either the small icon or large icon view.
The ListView must utilize an ImageList for assigning images for the ListViewItems.  You will need to find the images you will use for your ImageList; they should reflect the user input controls in some way so that what is added by the user will determine the image selected for the ListViewItem.


Extra Information
Go back through your code and check for the following:
Your application must compile and must not crash when starting up.
All variables and methods are named appropriately.
Input controls are appropriately labeled.
Any information being output to the user should be clear and concise.
Make sure nothing accesses an object that doesn�t exist.
Add comments to your code to indicate that you know what each line does.  You will lose up to 10 points if the comments are not present.  This is on top of the points you can lose if you do not have name, class and term, and assignment at the top of every code file.
