﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE04
{
    public partial class ListViewForm : Form
    {  // Brandon Anderson
        // VFW
        // C201802
        // ListViews
        // CE04
        // Creates all my custom event handlers for this form
        public event EventHandler imClosing;
        public event EventHandler ClearThatList;
        public event EventHandler SendThisPlayerBack;
       
        public Player testPlayer;
        public ListViewForm()

        {
            
            InitializeComponent();
            Form1 test = new Form1();
           
            
        }

       // Creates my List view and adds the players into it and assigns a image based on their rank

        public void AssaignList(object sender, EventArgs e) {

            lvAllItem.Items.Clear();
            Form1 test = (Form1)sender;
            lvAllItem.LargeImageList = imageRanks;

            foreach (Player p in test.allPlayers)
            {
                ListViewItem item = new ListViewItem();
              
                item.Text = p.ToString();
                item.ImageIndex = p.indexImage;
                item.Tag = p;
              
               
                lvAllItem.Items.Add(item);

            }




        }
        // Lets the other form know its closing
        private void ListViewForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            imClosing(this, new EventArgs());
        }
        // Clears the list

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearThatList(this, new EventArgs());
        }

        // Gets the location of where the user double clicked and sees if it matches an items coridinates, if it does then we will use that item to get its contents.
        private void lvAllItem_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           
                var senderList = (ListView)sender;
                var clickedItem = senderList.HitTest(e.Location).Item;
                if (clickedItem != null)
                {
                Player newPlayer = (Player)clickedItem.Tag;
               

                testPlayer = newPlayer;

                SendThisPlayerBack(this, new EventArgs());
               

            }
            
        }
    }
}
