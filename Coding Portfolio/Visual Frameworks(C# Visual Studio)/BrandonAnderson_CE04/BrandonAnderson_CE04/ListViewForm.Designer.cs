﻿namespace BrandonAnderson_CE04
{
    partial class ListViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListViewForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.lvAllItem = new System.Windows.Forms.ListView();
            this.imageRanks = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnClear});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(585, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnClear
            // 
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(54, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lvAllItem
            // 
            this.lvAllItem.Location = new System.Drawing.Point(12, 40);
            this.lvAllItem.Name = "lvAllItem";
            this.lvAllItem.Size = new System.Drawing.Size(561, 402);
            this.lvAllItem.TabIndex = 1;
            this.lvAllItem.UseCompatibleStateImageBehavior = false;
            
            this.lvAllItem.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvAllItem_MouseDoubleClick);
            // 
            // imageRanks
            // 
            this.imageRanks.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageRanks.ImageStream")));
            this.imageRanks.TransparentColor = System.Drawing.Color.Transparent;
            this.imageRanks.Images.SetKeyName(0, "bronze-rewards-lol.png");
            this.imageRanks.Images.SetKeyName(1, "SilverBadgeSeason2.png");
            this.imageRanks.Images.SetKeyName(2, "GoldBadgeSeason2.png");
            this.imageRanks.Images.SetKeyName(3, "platinum-rewards-lol.png");
            this.imageRanks.Images.SetKeyName(4, "diamondi.png");
            this.imageRanks.Images.SetKeyName(5, "master.png");
            this.imageRanks.Images.SetKeyName(6, "challenger-rewards-lol.png");
            // 
            // ListViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 464);
            this.Controls.Add(this.lvAllItem);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ListViewForm";
            this.Text = "ListViewForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ListViewForm_FormClosed);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnClear;
        private System.Windows.Forms.ListView lvAllItem;
        private System.Windows.Forms.ImageList imageRanks;
    }
}