﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE04
{
   public class Player
    {  // Brandon Anderson
        // VFW
        // C201802
        // ListViews
        // CE04
        public string summonerName;
        public string division;
        public int yearsPlayed;
        public bool lookingForTeam;
        public int indexImage;
        public int selectedIndex;

        public override string ToString()
        {
            return summonerName + " " + division;
        }
    }
}
