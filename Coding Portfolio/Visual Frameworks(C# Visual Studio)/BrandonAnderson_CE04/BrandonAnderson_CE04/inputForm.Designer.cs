﻿namespace BrandonAnderson_CE04
{
    partial class inputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(inputForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolAdd = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.numYearsPlayed = new System.Windows.Forms.NumericUpDown();
            this.comDivision = new System.Windows.Forms.ComboBox();
            this.txtSummoner = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.boolLFT = new System.Windows.Forms.CheckBox();
            this.toolStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numYearsPlayed)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolAdd});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(517, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolAdd
            // 
            this.toolAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolAdd.Image")));
            this.toolAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolAdd.Name = "toolAdd";
            this.toolAdd.Size = new System.Drawing.Size(49, 22);
            this.toolAdd.Text = "Add";
            this.toolAdd.Click += new System.EventHandler(this.toolAdd_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.boolLFT);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.numYearsPlayed);
            this.groupBox1.Controls.Add(this.comDivision);
            this.groupBox1.Controls.Add(this.txtSummoner);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(503, 284);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Player Information";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(338, 214);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(145, 51);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear Selections";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // numYearsPlayed
            // 
            this.numYearsPlayed.Location = new System.Drawing.Point(159, 148);
            this.numYearsPlayed.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numYearsPlayed.Name = "numYearsPlayed";
            this.numYearsPlayed.Size = new System.Drawing.Size(87, 20);
            this.numYearsPlayed.TabIndex = 3;
            // 
            // comDivision
            // 
            this.comDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comDivision.FormattingEnabled = true;
            this.comDivision.Items.AddRange(new object[] {
            "Bronze",
            "Silver",
            "Gold",
            "Platinum",
            "Diamond",
            "Masters",
            "Challenger"});
            this.comDivision.Location = new System.Drawing.Point(159, 105);
            this.comDivision.Name = "comDivision";
            this.comDivision.Size = new System.Drawing.Size(121, 21);
            this.comDivision.TabIndex = 2;
            // 
            // txtSummoner
            // 
            this.txtSummoner.Location = new System.Drawing.Point(159, 59);
            this.txtSummoner.Name = "txtSummoner";
            this.txtSummoner.Size = new System.Drawing.Size(236, 20);
            this.txtSummoner.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Current Division";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Years Played";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Looking For Team";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Summoner Name";
            // 
            // boolLFT
            // 
            this.boolLFT.AutoSize = true;
            this.boolLFT.Location = new System.Drawing.Point(159, 185);
            this.boolLFT.Name = "boolLFT";
            this.boolLFT.Size = new System.Drawing.Size(175, 17);
            this.boolLFT.TabIndex = 6;
            this.boolLFT.Text = "Check if yes (Leave blank if no)";
            this.boolLFT.UseVisualStyleBackColor = true;
            // 
            // inputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 333);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "inputForm";
            this.Text = "inputForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.inputForm_FormClosed);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numYearsPlayed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolAdd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.NumericUpDown numYearsPlayed;
        private System.Windows.Forms.ComboBox comDivision;
        private System.Windows.Forms.TextBox txtSummoner;
        private System.Windows.Forms.CheckBox boolLFT;
    }
}