﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE04
{
    public partial class inputForm : Form

    {  // Brandon Anderson
        // VFW
        // C201802
        // ListViews
        // CE04

            // Creates all the event handlers I will need for this form
        public event EventHandler SendPlayer;
        public event EventHandler InputFormClosing;
        public Player newPlayer;

        public inputForm()
        {
            InitializeComponent();
            Clear();
        }

        // Clears the form
        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
        private void Clear()
        {
            txtSummoner.Clear();
            comDivision.SelectedIndex = 0;
            numYearsPlayed.Value = 0;
            boolLFT.Checked = false;
        }
        // Creates a player from the users inputs and sends it to the main form
        private void toolAdd_Click(object sender, EventArgs e)
        {
            
            newPlayer = new Player();
            newPlayer.summonerName = txtSummoner.Text.ToString() ;
            newPlayer.division = comDivision.SelectedItem.ToString();
            newPlayer.yearsPlayed = (int)numYearsPlayed.Value;
            newPlayer.lookingForTeam = boolLFT.Checked;
            newPlayer.indexImage = comDivision.SelectedIndex;
            newPlayer.selectedIndex = comDivision.SelectedIndex;
            Clear();
            SendPlayer(this, new EventArgs());
           
            
        }
        // Lets the other form know its closing
        private void inputForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            InputFormClosing(this, new EventArgs());
        }
        // Populates the form with given information from another player
        public void ShowThePopForm (Player clickedPlayer)
        {
            
           
            txtSummoner.Text = clickedPlayer.summonerName;
            numYearsPlayed.Value = clickedPlayer.yearsPlayed;
            comDivision.SelectedIndex = clickedPlayer.selectedIndex;
            boolLFT.Checked = clickedPlayer.lookingForTeam;
        }
    }
}
