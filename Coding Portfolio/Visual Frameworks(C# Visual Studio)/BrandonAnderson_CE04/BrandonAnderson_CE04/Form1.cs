﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE04
{
    public partial class Form1 : Form
    {
        // Brandon Anderson
        // VFW
        // C201802
        // ListViews
        // CE04
        public int totalForms = 0;
        public bool displayWindowOpen = false;
        public event EventHandler SendThisForm;
        public List<Player> allPlayers = new List<Player>();
        public Player IHoldThings;
        public ListViewForm fListV;
        public inputForm fInput;
        public Form1()
     
        {
            InitializeComponent();
            UpdateFormCount();
            UpdateListCount();
        
        }
        // Exits the application
        private void toolExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Creates the input window and populates allows all the events from that form to be used.
        private void btnInputWindow_Click(object sender, EventArgs e)
        {
                fInput = new inputForm();
            if (fInput != null)
            {
                fInput.SendPlayer += FInput_SendPlayer;
                fInput.Show();
                fInput.InputFormClosing += FInput_InputFormClosing;
                totalForms++;
                txtWindowCount.Text = totalForms.ToString();
                
            }

           
           


        }
        // When the event is called this will keep track of how many windows are left
        private void FInput_InputFormClosing(object sender, EventArgs e)
        {
            totalForms--;
            txtWindowCount.Text = totalForms.ToString();
        }

        // Uses the custom event  to send a player to the input form
        private void FInput_SendPlayer(object sender, EventArgs e)
        {
            inputForm test = (inputForm)sender;

            string name = test.newPlayer.summonerName;
            allPlayers.Add(test.newPlayer);
            listAllPlayers.Items.Add(test.newPlayer);
            UpdateDisplayedList();
            txtListCount.Text = allPlayers.Count.ToString();
           

        }

        // Displays the list view form and allows all the custom events to be called from it

        private void toolDisplay_Click(object sender, EventArgs e)
        {

            
            fListV = new ListViewForm();
            if(displayWindowOpen == false)
            {
                fListV.Show();
            }
            displayWindowOpen = true;
            toolDisplay.Enabled = false;
            toolDisplay.Checked = true;
            fListV.imClosing += FListV_imClosing;
            fListV.ClearThatList += FListV_ClearThatList;
            fListV.SendThisPlayerBack += FListV_SendThisPlayerBack;
           
            SendThisForm += fListV.AssaignList;
            SendThisForm(this, new EventArgs());
            
        }
        // Subcribes to an event from the list view that sends the player back to the form
        private void FListV_SendThisPlayerBack(object sender, EventArgs e)
        {
            ListViewForm test = (ListViewForm)sender;

            IHoldThings = test.testPlayer;
           
                fInput = new inputForm();
            fInput.InputFormClosing += FInput_InputFormClosing;
            fInput.SendPlayer += FInput_SendPlayer;

            fInput.Show();
            fInput.ShowThePopForm(IHoldThings);
            totalForms++;
            txtWindowCount.Text = totalForms.ToString();
           
           
        }
        // Clears the lists
        private void FListV_ClearThatList(object sender, EventArgs e)
        {
            allPlayers = new List<Player>();
            UpdateListCount();
            UpdateDisplayedList();
            listAllPlayers.Items.Clear();
        }
        // Allows for a new list view window to be opened
        private void FListV_imClosing(object sender, EventArgs e)
        {
            displayWindowOpen = false;
            toolDisplay.Enabled = true;
            toolDisplay.Checked = false;
        }

        // Clears the lists
        private void toolClear_Click(object sender, EventArgs e)
        {
            allPlayers = new List<Player>();
            listAllPlayers.Items.Clear();
            txtListCount.Text = allPlayers.Count.ToString();
            UpdateDisplayedList();
        }

        // Can be called anytime I need to update the form count and the other one manages the list count
        public void UpdateFormCount()
        {
            txtWindowCount.Text = totalForms.ToString();
        }
        public void UpdateListCount()
        {
            txtListCount.Text = allPlayers.Count.ToString();
        }

        // Updates the ListView to always have the correct amount of players inside it
        public void UpdateDisplayedList()
        {

            fListV = new ListViewForm();
            fListV.imClosing += FListV_imClosing;
            fListV.ClearThatList += FListV_ClearThatList;
            fListV.SendThisPlayerBack += FListV_SendThisPlayerBack;

            SendThisForm += fListV.AssaignList;
            SendThisForm(this, new EventArgs());
        }
    }
}
