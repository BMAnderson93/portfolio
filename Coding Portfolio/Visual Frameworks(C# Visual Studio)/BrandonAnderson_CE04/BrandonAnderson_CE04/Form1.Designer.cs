﻿namespace BrandonAnderson_CE04
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolExit = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolDisplay = new System.Windows.Forms.ToolStripMenuItem();
            this.toolClear = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtWindowCount = new System.Windows.Forms.TextBox();
            this.txtListCount = new System.Windows.Forms.TextBox();
            this.btnInputWindow = new System.Windows.Forms.Button();
            this.listAllPlayers = new System.Windows.Forms.ListBox();
            this.menuStrip2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 24);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(738, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.displayToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(738, 24);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolExit
            // 
            this.toolExit.Name = "toolExit";
            this.toolExit.Size = new System.Drawing.Size(152, 22);
            this.toolExit.Text = "Exit";
            this.toolExit.Click += new System.EventHandler(this.toolExit_Click);
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolDisplay,
            this.toolClear});
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.displayToolStripMenuItem.Text = "List";
            // 
            // toolDisplay
            // 
            this.toolDisplay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.toolDisplay.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolDisplay.Name = "toolDisplay";
            this.toolDisplay.Size = new System.Drawing.Size(152, 22);
            this.toolDisplay.Text = "Display";
            this.toolDisplay.Click += new System.EventHandler(this.toolDisplay_Click);
            // 
            // toolClear
            // 
            this.toolClear.Name = "toolClear";
            this.toolClear.Size = new System.Drawing.Size(152, 22);
            this.toolClear.Text = "Clear";
            this.toolClear.Click += new System.EventHandler(this.toolClear_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtListCount);
            this.groupBox1.Controls.Add(this.txtWindowCount);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(714, 52);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Current Form Count";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(419, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Current List Count";
            // 
            // txtWindowCount
            // 
            this.txtWindowCount.Location = new System.Drawing.Point(211, 13);
            this.txtWindowCount.Name = "txtWindowCount";
            this.txtWindowCount.ReadOnly = true;
            this.txtWindowCount.Size = new System.Drawing.Size(100, 20);
            this.txtWindowCount.TabIndex = 2;
            // 
            // txtListCount
            // 
            this.txtListCount.Location = new System.Drawing.Point(558, 13);
            this.txtListCount.Name = "txtListCount";
            this.txtListCount.ReadOnly = true;
            this.txtListCount.Size = new System.Drawing.Size(100, 20);
            this.txtListCount.TabIndex = 3;
            // 
            // btnInputWindow
            // 
            this.btnInputWindow.Location = new System.Drawing.Point(295, 475);
            this.btnInputWindow.Name = "btnInputWindow";
            this.btnInputWindow.Size = new System.Drawing.Size(156, 63);
            this.btnInputWindow.TabIndex = 4;
            this.btnInputWindow.Text = "Input Window";
            this.btnInputWindow.UseVisualStyleBackColor = true;
            this.btnInputWindow.Click += new System.EventHandler(this.btnInputWindow_Click);
            // 
            // listAllPlayers
            // 
            this.listAllPlayers.FormattingEnabled = true;
            this.listAllPlayers.Location = new System.Drawing.Point(18, 94);
            this.listAllPlayers.Name = "listAllPlayers";
            this.listAllPlayers.Size = new System.Drawing.Size(708, 368);
            this.listAllPlayers.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 541);
            this.Controls.Add(this.listAllPlayers);
            this.Controls.Add(this.btnInputWindow);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolExit;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolDisplay;
        private System.Windows.Forms.ToolStripMenuItem toolClear;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtListCount;
        private System.Windows.Forms.TextBox txtWindowCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnInputWindow;
        private System.Windows.Forms.ListBox listAllPlayers;
    }
}

