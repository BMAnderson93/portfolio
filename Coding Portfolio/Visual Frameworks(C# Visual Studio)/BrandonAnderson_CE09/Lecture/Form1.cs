﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
namespace Lecture
{
    public partial class Form1 : Form
    {
        // Brandon Anderson
        // VFW
        // C201802
        // Database Connectivity
        // CE09


        // Creates my variables to be used
        MySqlConnection conn = new MySqlConnection();
        DataTable theData = new DataTable();
        int rowCounter = 0;
        int currentSelection = 1;
        decimal test;
        public Form1()
        {
            InitializeComponent();
            // Builds my connection string
            string connString = BuildConnectionString();
            Connect(connString);
            GetData();
            
        }

        public bool GetData()
        {
            // Gets the data from the table that matches what i need
            string sql = "SELECT title, releaseDate, retailPrice, itemPublisherId FROM item LIMIT 10";

            MySqlDataAdapter adr = new MySqlDataAdapter(sql,conn);
            adr.SelectCommand.CommandType = CommandType.Text;
           
            // Fills the adapter with the data
            adr.Fill(theData);

            int count = theData.Select().Length;
            
            // Sets the form inputs to match
            txtTitle.Text = theData.Rows[0]["title"].ToString();
            if (theData.Rows[rowCounter]["itemPublisherId"] == null)
            {
                numID.Value = 0;
            }
            else
            {
                numID.Value = decimal.Parse(theData.Rows[rowCounter]["itemPublisherId"].ToString());
            }
            numPrice.Value = decimal.Parse(theData.Rows[0]["retailPrice"].ToString());
            calRelease.SetDate( Convert.ToDateTime(theData.Rows[0]["releaseDate"].ToString()));
            lblcurrentNumber.Text = currentSelection.ToString();

            lblcurrentNumber.Text = currentSelection.ToString();
            

            lblCount.Text = count.ToString();

            conn.Close();

            return true;
        }
        // Connects to the server
        public void Connect(string myConnString)
        {
            try
            {
                conn.ConnectionString = myConnString;
                conn.Open();
                

            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1042:
                        MessageBox.Show("Cant resolve the host address.\n" + myConnString);
                        break;
                    case 1045:
                        MessageBox.Show("Invalid user name or password!");
                        break;
                    default:
                        MessageBox.Show(e.ToString() + "\n" + myConnString);
                        break;
                }
            }
        }
        // Builds the connection string
        public string BuildConnectionString()
        {
            string serverIP = "";
            try
            {
                using (StreamReader sr = new StreamReader("C:\\VFW\\connect.txt"))
                {
                    serverIP = sr.ReadToEnd();
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=exampleDatabase;port=8889";
        }

        // Exits the app
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        // Goes to the next item
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (rowCounter < theData.Select().Length -1 )
            {
                rowCounter++;
                currentSelection++;
            }
            UpdateInfo();
        }
        // Displays to form
        public void UpdateInfo()
        {
            if (rowCounter  < theData.Select().Length)
            {
                


                txtTitle.Text = theData.Rows[rowCounter]["title"].ToString();
                if (decimal.TryParse(theData.Rows[rowCounter]["itemPublisherId"].ToString(),out test) == false)
                {
                    numID.Value = 0;
                }
                else
                {
                    numID.Value = decimal.Parse(theData.Rows[rowCounter]["itemPublisherId"].ToString());
                }
                numPrice.Value = decimal.Parse(theData.Rows[rowCounter]["retailPrice"].ToString());
                calRelease.SetDate(Convert.ToDateTime(theData.Rows[rowCounter]["releaseDate"].ToString()));
                lblcurrentNumber.Text = currentSelection.ToString();

            }
        }
        // Goes back 1
        private void btnBack_Click(object sender, EventArgs e)
        {
            if (rowCounter > 0)
            {
                rowCounter--;
                currentSelection--;
            }
            UpdateInfo();
        }
        // Goes to start
        private void btnStart_Click(object sender, EventArgs e)
        {
            rowCounter = 0;
            currentSelection = 1;
            UpdateInfo();

        }
        // Goes to end
        private void btnEnd_Click(object sender, EventArgs e)
        {
            rowCounter = theData.Select().Length -1 ;
            currentSelection = theData.Select().Length;
            UpdateInfo();
        }

        // Saves the entire datatable to a txt file
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    StreamWriter sw = new StreamWriter(myStream);

                   
                        string test = theData.Rows[rowCounter]["title"].ToString() + 
                            theData.Rows[rowCounter]["itemPublisherId"].ToString() +
                               theData.Rows[rowCounter]["retailPrice"].ToString()+
                                  theData.Rows[rowCounter]["releaseDate"].ToString();

                        sw.WriteLine(test);
                    
                    sw.Close();
                    myStream.Close();
                }
            }
        }
    }
}
