CE09:  Database Connectivity
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE09" using the C# Windows Forms Application template.

Make sure you include your name, class and term, and code exercise name and number as comments to the top of the each code file of your project.  You will lose 5 points automatically if these comments are missing.
Grading:
Refer to the CE09: Database Connectivity tab of this rubric to understand the grading for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE09.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Topics: Database Connectivity, Reading Data
You will create an application that can connect to the exampleDatabase on a MySql server.  The connection string should be dynamic using the IP address in the connect.txt file you created on your C:\VFW\ drive.  It will use dbsAdmin as the user ID and password as the password for that user account.  Make sure this user account exists on your MySQL server and has appropriate access to the database.  You will be required to have a try...catch to ensure that the application does not crash if it cannot connect to the MySql server.  The connection string should work when the application is run on the instructor�s computer without having to change any code.
You can start with the code from the code examples, but you will be required to add some significant functionality.  You can select the table and columns you wish to draw from in your application, but the SQL statement should return a recordset with at least 10 but not more than 25 records of at least 4 columns.  You must select the columns within the SELECT statement and not use the * wildcard to return them all.
Once you have loaded the data, you will use the data from the first record to populate user input controls on the form.  This will require you to know what data is being cached because you will select your controls based on that data.  So, if the data is a number, you should convert it to a number and populate a number control with that data.  Do not use only textboxes to show the data.  Therefore, your recordset must include more than just string data.
Your form should have the following additional functionality:
The user must be able to move forward and backward through the recordset
The user should have the ability to jump to the first or last record of the recordset.
A control must show which record you are currently on as well as the total number of records (such as �record X of XX� and this should be record number as human-counted, not index number)
There must be a mechanism to keep the user from moving beyond the first or last record
The Main form should have a File menu that contains the following commands: Save and Exit.  The Save command should allow the user to save the current data from the user input controls to a file of their choosing as properly formatted XML (with the .xml file extension), and the Exit command should close the application.  Each menu command should also have keyboard shortcuts for each command: Save -> CTRL+S, Exit->CTRL+Q.
These are the guidelines used by the grading rubric:
Database Connectivity
The application should connect to a MySql database with a programmatically created connection string
The application verifies with a try...catch server connectivity.
Only the data needed is pulled from the database.
The first record�s data is loaded into controls on the form.
User Input Controls
A button exists that moves forward through each record of the data.
A button exists that moves backward through each record of the data.
A button exists that jumps to the first record of the recordset.
A button exists that jumps to the last record of the recordset.
A control exists to show which record the user on and the total number of records.
Main Form
MenuStrip is used for commands.
Two commands exist on the File menu: Save, Exit.
Clicking Save saves the current record to a properly formatted XML file.
Each command on the File menu has a corresponding keyboard shortcut.



Extra Information
Go back through your code and check for the following:
Your application must compile and must not crash when starting up.
All variables, controls, and methods are named appropriately.
Input controls are appropriately labeled.
Any information being output to the user should be clear and concise.
Make sure nothing accesses an object that doesn�t exist.
Add comments to your code to indicate that you know what each line does.
