﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Xml;






namespace BrandonAnderson_CE07
{
    // Brandon Anderson
    // VFW
    // C201802
    // XML and FIle I/O
    // CE08
    public partial class Form1 : Form
    {
        // Creates the variables for where to get my information
        WebClient apiConnection = new WebClient();
        public string apiVFW = "http://mdv-vfw.com/vfw.json";
        public string apiASD = "http://mdv-vfw.com/asd.json";
        public string apiVFWXML = "http://mdv-vfw.com/vfw.xml";
        public string apiASDXML = "	http://mdv-vfw.com/asd.xml";
        string useThis;




        public JObject o;
        public Form1()
        {
            InitializeComponent();
            Clear();
           


        }
        // Clears the input fields
        private void Clear()
        {
            rdoASD.Checked = true;
            txtClassName.Clear();
            txtCourseCode.Clear();
            txtDisc.Clear();
            numHours.Value = 0;
            numMonth.Value = 0;
        }

        // Loads the input fields depending on which class is picked
        private void btnLoad_Click(object sender, EventArgs e)
        {try
            {
                bool isXML = false;
                
                if (rdoASD.Checked && rdoJson.Checked)
                {
                    var asdData = apiConnection.DownloadString(apiASD);
                    o = JObject.Parse(asdData);



                }
                else if (rdoASD.Checked && rdoXMl.Checked)
                {
                    useThis = apiASDXML;
                    isXML = true;
                    



                }

                else if (!rdoASD.Checked && rdoXMl.Checked)
                {
                    useThis = apiVFWXML;
                    isXML = true;




                }

                else
                {
                    var vfwData = apiConnection.DownloadString(apiVFW);
                    o = JObject.Parse(vfwData);

                }
                if (!isXML)
                    Write();
                else
                    WriteXML();


            }
            // Catches the error if there is no internet
            catch (Exception ex)
            {
                MessageBox.Show("You need internet access before doing this!");
            }
           
        }
        // Exits the app
        private void toolExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        // Clears inputs
        private void toolNew_Click(object sender, EventArgs e)
        {
            Clear();
        }
        // Saves the selected class file and serializes it
        private void toolSave_Click(object sender, EventArgs e)
        {
            if (rdoJson.Checked)
                SaveJson();
            else
                SaveXML();
        }
        // Loads the selected file and checks to make sure it matchs the indentifier given
        private void toolLoad_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string ext = Path.GetExtension(openFileDialog1.FileName);
                    if (ext == ".txt")
                    {
                        rdoJson.Checked = true;
                        if ((myStream = openFileDialog1.OpenFile()) != null)
                        {
                            using (myStream)
                            {
                                StreamReader streamReader = new StreamReader(myStream);

                                JsonReader jsonReader = new JsonTextReader(streamReader);
                                JsonSerializer serializer = new JsonSerializer();
                                o = (JObject)serializer.Deserialize(jsonReader);
                                Write();
                            }
                        }
                    }

                    else
                    {// Reads the File if its XML
                        rdoXMl.Checked = true;
                        XmlReaderSettings settings = new XmlReaderSettings();
                        settings.ConformanceLevel = ConformanceLevel.Document;

                       
                        settings.IgnoreComments = true;
                        settings.IgnoreWhitespace = true;

                        // Validates the selected file is the correct type
                        using (XmlReader reader = XmlReader.Create(openFileDialog1.FileName, settings))
                        {
                          
                            reader.MoveToContent();

                           
                            if (reader.Name != "ClassInformation")
                            {
                                
                                MessageBox.Show("You are not using class information data!");
                                return;
                            }

                           // Prints the loaded XML file to the form
                            while (reader.Read())
                            {
                                
                                if (reader.Name == "CourseName" && reader.IsStartElement())
                                {
                                    txtClassName.Text = reader.ReadElementContentAsString();
                                    if (txtClassName.Text == "Visual Frameworks")
                                    {
                                        rdoVFW.Checked = true;
                                    }
                                    else
                                        rdoASD.Checked = true;
                                }
                                if (reader.Name == "CourseDescription" && reader.IsStartElement())
                                {
                                    txtDisc.Text = reader.ReadElementContentAsString();
                                }
                                if (reader.Name == "CourseCode" && reader.IsStartElement())
                                {
                                    txtCourseCode.Text = reader.ReadElementContentAsString();
                                }
                                if (reader.Name == "CourseHours" && reader.IsStartElement())
                                {
                                    numHours.Value = reader.ReadElementContentAsDecimal();
                                }
                                if (reader.Name == "CourseMonth" && reader.IsStartElement())
                                {
                                    numMonth.Value = reader.ReadElementContentAsDecimal();
                                }
                            }


                            }
                        }

                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("You selected an incorrect file type! Please pick a different one!");
                }

            }
        }
        // Writes the selected class to the form and also makes sure the indentifier is there
        private void Write()
        {
            if (o["class"] != null)
            {
                txtDisc.Text = o["class"]["course_description"].ToString();
                txtClassName.Text = o["class"]["course_name_clean"].ToString();
                txtCourseCode.Text = o["class"]["course_code_long"].ToString();
                numHours.Value = decimal.Parse(o["class"]["credit"].ToString());
                numMonth.Value = decimal.Parse(o["class"]["sequence"].ToString());

                if (txtClassName.Text == "Visual Frameworks")
                {
                    rdoVFW.Checked = true;
                }
                else
                {
                    rdoASD.Checked = true;
                }
            }
            else
            {
                MessageBox.Show("You selected an incorrect file type! Please pick a different one!");
            }
        }
        // Displays the XML to the users form
        private void WriteXML()
        {
            using (XmlReader apiData = XmlReader.Create(useThis))
            {
                while (apiData.Read())
                {
                    rdoXMl.Checked = true;
                    if(apiData.Name == "course_name_clean" && apiData.IsStartElement())
                    {
                        txtClassName.Text = apiData.ReadElementContentAsString();
                       
                    }
                    if (apiData.Name == "course_description" && apiData.IsStartElement())
                    {
                        txtDisc.Text = apiData.ReadElementContentAsString();
                    }
                    if (apiData.Name == "course_code_long" && apiData.IsStartElement())
                    {
                        txtCourseCode.Text = apiData.ReadElementContentAsString();
                    }
                    if(apiData.Name == "sequence" && apiData.IsStartElement())
                    {
                        numMonth.Value = apiData.ReadElementContentAsDecimal();
                    }
                    if(apiData.Name == "credit" && apiData.IsStartElement())
                    {
                        numHours.Value = apiData.ReadElementContentAsDecimal();
                    }
                }
            }
        }

        // Saves the file as a Json
        private void SaveJson()
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.DefaultExt = "txt";
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    StreamWriter streamWriter = new StreamWriter(myStream);
                    JsonWriter writer = new JsonTextWriter(streamWriter);
                    o["class"]["course_description"] = txtDisc.Text;
                    o["class"]["course_name_clean"] = txtClassName.Text;
                    o["class"]["course_code_long"] = txtCourseCode.Text;
                    o["class"]["credit"] = numHours.Value;
                    o["class"]["sequence"] = numMonth.Value;
                    o.WriteTo(writer);
                    writer.Formatting = Newtonsoft.Json.Formatting.Indented;
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(writer, o);
                    Clear();
                    myStream.Close();
                }
            }
        }
        // Saves the file as a XML
        private void SaveXML()
        {
          
            SaveFileDialog dlg = new SaveFileDialog();

       
            dlg.DefaultExt = "xml";
            dlg.Filter = "XML Files (*.xml)|*.xml";
            dlg.FilterIndex = 0;


            if (DialogResult.OK == dlg.ShowDialog())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

               
                settings.Indent = true;
                using (XmlWriter writer = XmlWriter.Create(dlg.FileName, settings))
                {
                    
                    writer.WriteStartElement("ClassInformation");

                
                    writer.WriteElementString("CourseName", txtClassName.Text);
                    writer.WriteElementString("CourseDescription", txtDisc.Text);
                    writer.WriteElementString("CourseCode", txtCourseCode.Text);
                    writer.WriteElementString("CourseHours", numHours.Value.ToString());
                    writer.WriteElementString("CourseMonth", numMonth.Value.ToString());

                  

                    writer.WriteEndElement();
                }
            }
        }
    }
}
