CE08: XML and File I/O
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE08" using the C# Windows Forms Application template.

Make sure you include your name, class and term, and code exercise name and number as comments to the top of the each code file of your project.  You will lose 5 points automatically if these comments are missing.
Grading:
Refer to the CE08: XML and File I/O tab of this rubric to understand the grading for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE08.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Topics: XML, Reading XML, Saving and Loading XML or JSON
Students will create an application that can download XML or JSON data from a Web address.  The Web addresses to be used for XML are as follows:
	http://mdv-vfw.com/vfw.xml
	http://mdv-vfw.com/asd.xml
You should use the Web addresses from CE07 for the JSON data.  These should be hard-coded within the project except for the final extension.  That is, the user selection should determine if .xml or .json is added to the Web address.  You will be required to have a try...catch to ensure that the application does not crash if it cannot connect to the Internet.
You can start with the code from your most recent code exercise, but you will need to add some significant functionality.  As before, at a minimum you will need to include the following information within the controls on the form:  class selection (ASD or VFW), class name, course code, course description, month (sequence), and credit hours.  You may add additional controls and data, but you must include these.
Once you have downloaded the XML data, you will select only the required data to populate user input controls on the form.  This will require you to know what data is being downloaded because you will select your controls based on that data.  So, if the data is a number, you should convert it to a number and populate a number control with that data.  Do not use only textboxes to show the data.  The course description should appear in a multiline textbox that contains a vertical scrollbar.  
To save and load the data, you will have a single Save command and a single Load command.  How the data is saved will be determined by the radio button selected by the user.  Thus, if the user selected the JSON radio button, the data should be saved to a text file with a default .txt extension.  If the user selects the XML radio button, the data should be saved to an XML file with a default .xml file extension.  The data download should not determine how the file is saved.  If a user selected XML to download the data but then selects the JSON radio button, a text file should be created.  If the user selected JSON to download the data but selects the XML radio button afterward, then the data should be saved as XML.  Thus, it will be the radio buttons and not a menu command or other mechanism to determine how the data is saved.
When loading the data, the application will have to determine what file type was selected (.txt or .xml) and read the data correctly to load it into the form�s input controls.  The radio buttons should not dictate what data is loaded.  As before, there should be some mechanism within the Load functionality that will check to see if the file is a correct data file.  If I try to load a text or XML file that does not include this identifier, the application should indicate this to the user and not load the file.
The Main form should have a File menu that contains the following commands: Load, Save, New, and Exit.  The Load command should allow the user to select a file to load data into the user input controls, the Save command should allow the user to save the current data from the user input controls to a file of their choosing, the New command should clear the form, and the Exit command should close the application.  Each menu command should also have keyboard shortcuts for each command: Load -> CTRL+L, Save -> CTRL+S, New->CTRL+N, Exit->CTRL+Q.
These are the guidelines used by the grading rubric:
Web
The selected Web address is accessible and allows the application to download JSON data. 
The Web address used is determined by the user for both class and either JSON or XML data.
The application verifies with a try...catch Web connectivity before trying to download the data.
The data is parsed correctly.
Only the data needed is used to fill the user input controls.


User Input Controls
Controls are included to display the data from the JSON or XML.
A button or some other mechanism exists to allow the user to determine when the data is downloaded and displayed.
Appropriate controls are used for appropriate data types (such as numericUpDown for numbers).
All controls are cleared to default values when the New command is selected or the keyboard shortcut is used.
File I/O
The file type is determined by the selected radio button.
The application can save both XML and text documents.
All data from the user controls is stored to a file file selected using the SaveFileDialog class and opened using the OpenFileDialog class.
The data file also includes an identifier to indicate this is a proper data file for the application.
��The proper file extension is added (.txt or .xml).
The data loads from either the .txt or the .xml file.
Main Form
MenuStrip is used for commands.
Four commands exist on the File menu: Load, Save, New, Exit.
Each command on the File menu has a corresponding keyboard shortcut.

Extra Information
Go back through your code and check for the following:
Your application must compile and must not crash when starting up.
All variables, controls, and methods are named appropriately.
Input controls are appropriately labeled.
Any information being output to the user should be clear and concise.
Make sure nothing accesses an object that doesn�t exist.
Add comments to your code to indicate that you know what each line does.


