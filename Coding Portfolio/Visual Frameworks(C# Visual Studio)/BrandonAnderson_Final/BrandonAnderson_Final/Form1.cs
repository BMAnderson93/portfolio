﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;

namespace BrandonAnderson_Final
{
    // Brandon Anderson
    // VFW
    // C201802
    // Synthesis
    // Final
    public partial class Form1 : Form
    { 
        MySqlConnection conn = new MySqlConnection();
        DataTable theData = new DataTable();

        public Form1()
        {

            InitializeComponent();
            string connString = BuildConnectionString();
            Connect(connString);
            GetData();
            
           

        }
        // Sends the updated information back to the sql server if the server can be reached
        public void SendData()
        {
            try
            {
                if (lvMovies.SelectedItems != null)
                {
                    Movie test = (Movie)lvMovies.SelectedItems[0].Tag;
                    test.dvdName = txtTitle.Text;
                    test.studio = txtStudio.Text;
                    test.rating = txtRating.Text;
                    test.publicRating = numScore.Value;
                    string sql = "UPDATE dvd SET DVD_Title = '" + txtTitle.Text + "' ,Studio = '" + txtStudio.Text
                        + "' ,Rating = '" + txtRating.Text + "' ,publicRating = '" + numScore.Value.ToString() + "' WHERE dvdId = '" + test.indexID + "';";
                   


                    MySqlDataAdapter adr = new MySqlDataAdapter(sql, conn);
                    MySqlCommand sqlCommand = new MySqlCommand(sql);
                    sqlCommand.Connection = conn;

                    if (conn != null)
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    else
                    {
                        conn.Open();
                        sqlCommand.ExecuteNonQuery();
                    }
                    lvMovies.SelectedItems[0].Tag = test;
                    lvMovies.SelectedItems[0].Text = test.ToString();
                    conn.Close();
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Unable to connect to the database!");
            }
        }
        // Gets the data from the SQL server and adds it to a list view
        public bool GetData()
        {
            try
            {
                // Gets the data from the table that matches what i need
                string sql = "SELECT DVD_Title, Studio, Rating, publicRating, dvdId FROM dvd LIMIT 10";

                MySqlDataAdapter adr = new MySqlDataAdapter(sql, conn);
                adr.SelectCommand.CommandType = CommandType.Text;

                // Fills the adapter with the data
                adr.Fill(theData);

                int count = theData.Select().Length;

                // Sets the form inputs to match
                for (int i = 0; i < theData.Select().Length; i++)
                {
                    Movie newMove = new Movie();
                    newMove.dvdName = theData.Rows[i]["DVD_Title"].ToString();
                    newMove.publicRating = decimal.Parse(theData.Rows[i]["publicRating"].ToString());
                    newMove.rating = theData.Rows[i]["Rating"].ToString();
                    newMove.studio = theData.Rows[i]["Studio"].ToString();
                    newMove.indexID = int.Parse(theData.Rows[i]["dvdId"].ToString());

                    ListViewItem lvMovie = new ListViewItem();
                    lvMovie.Text = newMove.ToString();
                    lvMovie.Tag = newMove;
                    lvMovie.ImageIndex = 0;
                    lvMovies.Items.Add(lvMovie);
                }






                conn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to connect to the database!");
            }

            return true;
        }
        // Connects to the sql server
        public void Connect(string myConnString)
        {
            try
            {
                conn.ConnectionString = myConnString;
                conn.Open();


            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1042:
                        MessageBox.Show("Cant resolve the host address.\n" + myConnString);
                        break;
                    case 1045:
                        MessageBox.Show("Invalid user name or password!");
                        break;
                    default:
                        MessageBox.Show(e.ToString() + "\n" + myConnString);
                        break;
                }
            }
        }
        // BUilds the connection string
        public string BuildConnectionString()
        {
            string serverIP = "";
            try
            {
                using (StreamReader sr = new StreamReader("C:\\VFW\\connect.txt"))
                {
                    serverIP = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=exampleDatabase;port=8889";
        }

        // Uses the list view information to populate the controls
        private void lvMovies_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(lvMovies.SelectedItems != null)
            {
                Movie movie = new Movie();
                movie = (Movie)lvMovies.SelectedItems[0].Tag;

                txtRating.Text = movie.rating;
                txtStudio.Text = movie.studio;
                txtTitle.Text = movie.dvdName;
                numScore.Value = movie.publicRating;
            }
        }
        // Event for updating the information
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string connString = BuildConnectionString();
            Connect(connString);
            SendData();
        }
        // Exits
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        // Changes icons to large
        private void largeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolLarge.Checked = true;
            toolSmall.Checked = false;
            lvMovies.View = View.LargeIcon;
        }
        // Changes icons to small
        private void toolSmall_Click(object sender, EventArgs e)
        {
            toolLarge.Checked = false;
            toolSmall.Checked = true;
            lvMovies.View = View.SmallIcon;
        }
    }
}
