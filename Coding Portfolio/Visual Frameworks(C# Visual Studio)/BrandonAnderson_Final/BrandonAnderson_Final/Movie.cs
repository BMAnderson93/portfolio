﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_Final
{
    class Movie
    {// Brandon Anderson
     // VFW
     // C201802
     // Synthesis
     // Final
        public string dvdName;
        public string rating;
        public string studio;
        public decimal publicRating;
        public int indexID;
        public override string ToString()
        {
            return dvdName + " " + publicRating;
        }

    }
}
