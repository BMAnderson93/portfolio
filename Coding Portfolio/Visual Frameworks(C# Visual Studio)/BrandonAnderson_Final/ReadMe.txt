Final:  Synthesis
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_Final" using the C# Windows Forms Application template.

Make sure you include your name, class and term, and code exercise name and number as comments to the top of the each code file of your project.  You will lose 5 points automatically if these comments are missing.
Grading:
Refer to the Final: Synthesis tab of this rubric to understand the grading for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_Final.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Topics: Database Connectivity, Reading Data
You will create an application with a single form that can connect to the exampleDatabase on a MySql server.  The connection string should be dynamic using the IP address in the connect.txt file you created on your C:\ drive.  It will use dbsAdmin as the user ID and password as the password for that user account.  Make sure this user account exists on your MySQL server and has appropriate access to the database.  You will be required to have a try...catch to ensure that the application does not crash if it cannot connect to the MySql server.  The connection string should work when the application is run on the instructor�s computer without having to change any code.
For this application, you will connect to the database, pull data from the database, and populate a ListView with objects created from the data.  The ListView must have ImageLists attached for small and large icons.  You will be required to find the images you wish to use, though it does not have to be more than one image for the large icon and one image for the small icon.  You can select the table and columns you wish to draw from in your application, but the SQL statement should return a recordset with at least 10 but not more than 25 records of at least 4 columns.  You must select the columns within the SELECT statement and not use the * wildcard to return them all.
Once you have loaded the data, you will move through each record and create an object that will then be added to the ListView.  This will require you to know what data is being cached because you will select your controls based on that data.  So, if the data is a number, you should convert it to a number and populate a number control with that data.  Do not use only textboxes to show the data.  Therefore, your recordset must include more than just string data.
Once the data is added to the ListView, if a user double-clicks an item in the ListView, it should populate the user input controls with the data from that object.  The user should then be able to change that data and click a Save button, which will save the data to both the item on the ListView and to the database.
Your form should have the following additional functionality:
The user must be able to select an item in the ListView to populate user input controls.
The user must be able to change the data within an item and save those changes to the ListView item and the database.
The File menu should contain an Exit command with keyboard shortcut.
The View menu should include a command to view large icons and a command to view small icons.  When each is selected, a checkmark should appear to indicate the current view.
These are the guidelines used by the grading rubric:
Database Connectivity
The application should connect to a MySql database with a programmatically created connection string
The application verifies with a try...catch server connectivity.
Only the data needed is pulled from the database.
Data is pulled from the database only once.
ListView
The ListView is populated by the records in the returned recordset.
Selecting an item in the ListView populates user input controls on the form.
Changes made to the data in a user input control can be saved to the selected item on the ListView.
The ListView has ImageLists for large and small icons.
User Input Controls
A user input control exists for each piece of data in a record/item.
The user input controls are appropriate for the data type.
A button exists that saves the data of the currently selected item both to the ListView item and the database.

Main Form
MenuStrip is used for File menu with an Exit command.
MenuStrip also includes a View menu with Large and Small commands.
The Exit command on the File menu has a corresponding keyboard shortcut.


Extra Information
Go back through your code and check for the following:
Your application must compile and must not crash when starting up.
All variables, controls, and methods are named appropriately.
Input controls are appropriately labeled.
Any information being output to the user should be clear and concise.
Make sure nothing accesses an object that doesn�t exist.
Add comments to your code to indicate that you know what each line does.

