﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE05
{
    public class Player
    {// Brandon Anderson
        // VFW
        // C201802
        // Custom Event Arguments
        // CE05
        public string summonerName;
        public string rank;
        public int division;
        public bool isBanned;
        public int IndexNumber;

        public override string ToString()
        {
            return summonerName +" "+ rank;
        }
    }
}
