﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE05
{
    public partial class Form1 : Form
    
    {


        // Brandon Anderson
        // VFW
        // C201802
        // Custom Event Arguments
        // CE05

            // Creates my event handlers to be used later
        public event EventHandler FormNoApply;
        public event EventHandler<SendPlayerDataToInput> SendToInput;
       
        public Form1()
       
        {
            
            InitializeComponent();

           
        }
        // Sends the player information to the input form
        public class SendPlayerDataToInput : EventArgs
        {
            public Player newPlayer = new Player();



            public SendPlayerDataToInput(Player p)
            {

                this.newPlayer = p;





            }
        }

        // Exits the app

        private void toolExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolNew_Click(object sender, EventArgs e)
        {
             

          // Creates my inputform and subcribes all events to methods
           
            InputForm inputForm = new InputForm();
            inputForm.Send += InputForm_Send;
            inputForm.OverRide += InputForm_OverRide;
            FormNoApply += inputForm.DisableApply;
            SendToInput += inputForm.OpenPlayer;
            FormNoApply(this, new EventArgs());

            inputForm.ShowDialog();
            
            

        }
        // Overrides the selected users information with new info
        private void InputForm_OverRide(object sender, InputForm.OverRidePlayerData e)
        {
            lvAllPlayers.SelectedItems[0].Tag = e.newPlayer;
            lvAllPlayers.SelectedItems[0].Text = e.newPlayer.ToString();
            lvAllPlayers.SelectedItems[0].ImageIndex = e.newPlayer.IndexNumber;
        }

        // Adds the player from the input form to the list
        private void InputForm_Send(object sender, InputForm.SendPlayerData e)
        {
            Player np = e.newPlayer;
            ListViewItem lvI = new ListViewItem();
            lvI.Text = np.ToString();
            lvI.ImageIndex = np.IndexNumber;
            lvI.Tag = np;

            lvAllPlayers.Items.Add(lvI);
            toolTxtCount.Text = lvAllPlayers.Items.Count.ToString();
        }


        // Displays the selected player in the new formn with all their information
        private void lvAllPlayers_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            InputForm inputForm = new InputForm();
            inputForm.Send += InputForm_Send;
            inputForm.OverRide += InputForm_OverRide;
            FormNoApply += inputForm.DisableApply;
            SendToInput += inputForm.OpenPlayer;
            SendToInput(this, new SendPlayerDataToInput((Player)lvAllPlayers.SelectedItems[0].Tag));
            
            
            inputForm.ShowDialog();


        }
        // Changes the icons to large
        private void largeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolLarge.Checked = true;
            toolSmall.Checked = false;
            lvAllPlayers.View = View.LargeIcon;
            
        }
        // Changes the icons to small
        private void toolSmall_Click(object sender, EventArgs e)
        {

            toolLarge.Checked = false;
            toolSmall.Checked = true;
            lvAllPlayers.View = View.SmallIcon;
        }

        
    }
}
