﻿namespace BrandonAnderson_CE05
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.boolIsBanned = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numDivision = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSummonerName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rBronze = new System.Windows.Forms.RadioButton();
            this.rSilver = new System.Windows.Forms.RadioButton();
            this.rChallenger = new System.Windows.Forms.RadioButton();
            this.rMasters = new System.Windows.Forms.RadioButton();
            this.rDiamond = new System.Windows.Forms.RadioButton();
            this.rGold = new System.Windows.Forms.RadioButton();
            this.rPlat = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDivision)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnApply);
            this.groupBox1.Controls.Add(this.btnOk);
            this.groupBox1.Controls.Add(this.boolIsBanned);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.numDivision);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtSummonerName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rBronze);
            this.groupBox1.Controls.Add(this.rSilver);
            this.groupBox1.Controls.Add(this.rChallenger);
            this.groupBox1.Controls.Add(this.rMasters);
            this.groupBox1.Controls.Add(this.rDiamond);
            this.groupBox1.Controls.Add(this.rGold);
            this.groupBox1.Controls.Add(this.rPlat);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(335, 257);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Player Information";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(227, 219);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(92, 35);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Cancel";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(122, 219);
            this.btnApply.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(92, 35);
            this.btnApply.TabIndex = 12;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(15, 219);
            this.btnOk.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(92, 35);
            this.btnOk.TabIndex = 11;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // boolIsBanned
            // 
            this.boolIsBanned.AutoSize = true;
            this.boolIsBanned.Location = new System.Drawing.Point(15, 186);
            this.boolIsBanned.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.boolIsBanned.Name = "boolIsBanned";
            this.boolIsBanned.Size = new System.Drawing.Size(282, 17);
            this.boolIsBanned.TabIndex = 10;
            this.boolIsBanned.Text = "Banned Account (Leave blank if not currently banned)";
            this.boolIsBanned.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 154);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Current Division";
            // 
            // numDivision
            // 
            this.numDivision.Location = new System.Drawing.Point(104, 153);
            this.numDivision.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numDivision.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numDivision.Name = "numDivision";
            this.numDivision.Size = new System.Drawing.Size(60, 20);
            this.numDivision.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Summoner Name";
            // 
            // txtSummonerName
            // 
            this.txtSummonerName.Location = new System.Drawing.Point(104, 30);
            this.txtSummonerName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtSummonerName.Name = "txtSummonerName";
            this.txtSummonerName.Size = new System.Drawing.Size(175, 20);
            this.txtSummonerName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 63);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Current Rank";
            // 
            // rBronze
            // 
            this.rBronze.AutoSize = true;
            this.rBronze.Checked = true;
            this.rBronze.Location = new System.Drawing.Point(15, 84);
            this.rBronze.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rBronze.Name = "rBronze";
            this.rBronze.Size = new System.Drawing.Size(58, 17);
            this.rBronze.TabIndex = 2;
            this.rBronze.TabStop = true;
            this.rBronze.Text = "Bronze";
            this.rBronze.UseVisualStyleBackColor = true;
            // 
            // rSilver
            // 
            this.rSilver.AutoSize = true;
            this.rSilver.Location = new System.Drawing.Point(82, 84);
            this.rSilver.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rSilver.Name = "rSilver";
            this.rSilver.Size = new System.Drawing.Size(51, 17);
            this.rSilver.TabIndex = 3;
            this.rSilver.Text = "Silver";
            this.rSilver.UseVisualStyleBackColor = true;
            // 
            // rChallenger
            // 
            this.rChallenger.AutoSize = true;
            this.rChallenger.Location = new System.Drawing.Point(166, 118);
            this.rChallenger.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rChallenger.Name = "rChallenger";
            this.rChallenger.Size = new System.Drawing.Size(75, 17);
            this.rChallenger.TabIndex = 8;
            this.rChallenger.Text = "Challenger";
            this.rChallenger.UseVisualStyleBackColor = true;
            // 
            // rMasters
            // 
            this.rMasters.AutoSize = true;
            this.rMasters.Location = new System.Drawing.Point(82, 118);
            this.rMasters.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rMasters.Name = "rMasters";
            this.rMasters.Size = new System.Drawing.Size(62, 17);
            this.rMasters.TabIndex = 7;
            this.rMasters.Text = "Masters";
            this.rMasters.UseVisualStyleBackColor = true;
            // 
            // rDiamond
            // 
            this.rDiamond.AutoSize = true;
            this.rDiamond.Location = new System.Drawing.Point(15, 118);
            this.rDiamond.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rDiamond.Name = "rDiamond";
            this.rDiamond.Size = new System.Drawing.Size(67, 17);
            this.rDiamond.TabIndex = 6;
            this.rDiamond.Text = "Diamond";
            this.rDiamond.UseVisualStyleBackColor = true;
            // 
            // rGold
            // 
            this.rGold.AutoSize = true;
            this.rGold.Location = new System.Drawing.Point(166, 84);
            this.rGold.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rGold.Name = "rGold";
            this.rGold.Size = new System.Drawing.Size(47, 17);
            this.rGold.TabIndex = 4;
            this.rGold.Text = "Gold";
            this.rGold.UseVisualStyleBackColor = true;
            // 
            // rPlat
            // 
            this.rPlat.AutoSize = true;
            this.rPlat.Location = new System.Drawing.Point(232, 84);
            this.rPlat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rPlat.Name = "rPlat";
            this.rPlat.Size = new System.Drawing.Size(65, 17);
            this.rPlat.TabIndex = 5;
            this.rPlat.Text = "Platinum";
            this.rPlat.UseVisualStyleBackColor = true;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 270);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "InputForm";
            this.Text = "InputForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InputForm_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDivision)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.CheckBox boolIsBanned;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numDivision;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSummonerName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rBronze;
        private System.Windows.Forms.RadioButton rSilver;
        private System.Windows.Forms.RadioButton rChallenger;
        private System.Windows.Forms.RadioButton rMasters;
        private System.Windows.Forms.RadioButton rDiamond;
        private System.Windows.Forms.RadioButton rGold;
        private System.Windows.Forms.RadioButton rPlat;
    }
}