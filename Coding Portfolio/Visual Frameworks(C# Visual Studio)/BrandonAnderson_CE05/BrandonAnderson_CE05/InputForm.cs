﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE05
{
    public partial class InputForm : Form
    {// Brandon Anderson
        // VFW
        // C201802
        // Custom Event Arguments
        // CE05

            // Creates my custom events
        public event EventHandler<SendPlayerData> Send;
        public event EventHandler<OverRidePlayerData> OverRide;
        public InputForm()
        {
            
            InitializeComponent();
            
        }
        // Sends player data back to form 1
        public class SendPlayerData : EventArgs
        {
            public Player newPlayer = new Player();



            public SendPlayerData(string currentSum, string currentRank, int currentDivision, bool currentStatus, int currentIndex)
            {

                this.newPlayer.summonerName = currentSum;
                this.newPlayer.rank = currentRank;
                this.newPlayer.division = currentDivision;
                this.newPlayer.isBanned = currentStatus;
                this.newPlayer.IndexNumber = currentIndex;





            }
        }

        // Creates the event to override the players information
        public class OverRidePlayerData : EventArgs
        {
            public Player newPlayer = new Player();



            public OverRidePlayerData(string currentSum, string currentRank, int currentDivision, bool currentStatus, int currentIndex)
            {

                this.newPlayer.summonerName = currentSum;
                this.newPlayer.rank = currentRank;
                this.newPlayer.division = currentDivision;
                this.newPlayer.isBanned = currentStatus;
                this.newPlayer.IndexNumber = currentIndex;





            }
        }
        // Opens the selected player from form 1
        public void OpenPlayer(object sender, Form1.SendPlayerDataToInput e)
        {
            Player np = e.newPlayer;
            txtSummonerName.Text = np.summonerName;
            int indexSelected = np.IndexNumber;

            switch (indexSelected)
            {
                case 0:
                    {
                        rBronze.Checked = true;
                        break;
                    }
                case 1:
                    {
                        rSilver.Checked = true;
                        break;
                    }
                case 2:
                    {
                        rGold.Checked = true;
                        break;
                    }
                case 3:
                    {
                        rPlat.Checked = true;
                        break;
                    }
                case 4:
                    {
                        rDiamond.Checked = true;
                        break;
                    }
                case 5:
                    {
                        rMasters.Checked = true;  
                        break;
                    }
                case 6:
                    {
                        rChallenger.Checked = true;
                        break;
                    }
                  
            }
            numDivision.Value = np.division;
            boolIsBanned.Checked = np.isBanned;
           

           
        }
        // Clears the inputs
        public void Clear()
        {
            txtSummonerName.Clear();
            rBronze.Checked = true;
            numDivision.Value = 0;
            boolIsBanned.Checked = false;
        }
        // Hides the apply button
        public void DisableApply(object sender, EventArgs e)
        {
            btnApply.ForeColor = Color.Gray;
            btnApply.Enabled = false;
        }

        // Resets the apply button and clears form
        private void InputForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            btnApply.ForeColor = Color.Black;
            btnApply.Enabled = true;
            Clear();
        }
        // Creates the player
        private void btnOk_Click(object sender, EventArgs e)
        {
            string division = "Bronze";
            int index = 0;
            if (rSilver.Checked) {
                division = "Silver";
                index = 1;
            }
            else if (rGold.Checked) {
                division = "Gold";
                index = 2;
            }
            else if (rPlat.Checked) {
                division = "Platinum";
                index = 3;
            }
            else if (rDiamond.Checked) {
                division = "Diamond";
                index = 4;
            }
            else if (rMasters.Checked) {
                division = "Masters";
                index = 5;
            }
            else if (rChallenger.Checked) {
                division = "Challenger";
                index = 6;
            }
            Send(this, new SendPlayerData( txtSummonerName.Text, division, (int)numDivision.Value, boolIsBanned.Checked, index));
           
            Close();

        }
        // Updates the player
        private void btnApply_Click(object sender, EventArgs e)
        {
            string division = "Bronze";
            int index = 0;
            if (rSilver.Checked)
            {
                division = "Silver";
                index = 1;
            }
            else if (rGold.Checked)
            {
                division = "Gold";
                index = 2;
            }
            else if (rPlat.Checked)
            {
                division = "Platinum";
                index = 3;
            }
            else if (rDiamond.Checked)
            {
                division = "Diamond";
                index = 4;
            }
            else if (rMasters.Checked)
            {
                division = "Masters";
                index = 5;
            }
            else if (rChallenger.Checked)
            {
                division = "Challenger";
                index = 6;
            }
            OverRide(this, new OverRidePlayerData(txtSummonerName.Text, division, (int)numDivision.Value, boolIsBanned.Checked, index));
            Close();
        }

        // Closes form without effecting the list
        private void button3_Click(object sender, EventArgs e)
        {
            
            Close();
        }
    }
}
