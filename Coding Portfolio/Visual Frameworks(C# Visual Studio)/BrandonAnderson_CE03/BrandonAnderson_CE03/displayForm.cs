﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE03
{
    // Brandon Anderson
    // VFW
    // C201802
    // Custom Events
    // CE03
    public partial class displayForm : Form
    { // Creates all my custom event handlers
        public event EventHandler emptyList;
        public event EventHandler ChangeToTrue;
        public event EventHandler sendSelected;
        public EventHandler DisableWindow;
        // Creates bools for determining wether the list is open or if i need to clear it.
        public bool isOpen = false;
        public bool clearList = false;

        public displayForm()
        {
            
            InitializeComponent();

            






    }
         // Sends a copy of form 1 to be used
        public void OpenAndSendInfo(object sender, EventArgs e)
        {
            ClearList();
            Form1 test = (Form1)sender;
            foreach(Dog dog in test.allDogs)
            {
                DogsList.Items.Add(dog);
            }
        }
        // Clears the list
        public void ClearList()
        {
            DogsList.Items.Clear();
        }
        // Creates the custom event for when the selected index changes
        private void Dogs_SelectedIndexChanged(object sender, EventArgs e)
        {
            sendSelected(this, new EventArgs());
        }
    
        // Clears the displayed list
        private void toolClear_Click(object sender, EventArgs e)
        {
            emptyList(this, new EventArgs());
        }
        public Dog returnSelectedDog()
        {
            return (Dog)DogsList.SelectedItem;
        }
        // Creates a custom even when the form is closing
        private void displayForm_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            ChangeToTrue(this, new EventArgs());
        }
        // Creates a custom event to handle disabling the window
        private void displayForm_Shown(object sender, EventArgs e)
        {
            DisableWindow(this, new EventArgs());
        }
        // Creates a custom event for changing the bool
        private void displayForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ChangeToTrue(this, new EventArgs());

        }
    }
}
