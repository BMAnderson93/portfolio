﻿namespace BrandonAnderson_CE03
{
    partial class displayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(displayForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolClear = new System.Windows.Forms.ToolStripButton();
            this.DogsList = new System.Windows.Forms.ListBox();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolClear});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(698, 39);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolClear
            // 
            this.toolClear.Image = ((System.Drawing.Image)(resources.GetObject("toolClear.Image")));
            this.toolClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolClear.Name = "toolClear";
            this.toolClear.Size = new System.Drawing.Size(105, 36);
            this.toolClear.Text = "Clear";
            this.toolClear.Click += new System.EventHandler(this.toolClear_Click);
            // 
            // DogsList
            // 
            this.DogsList.FormattingEnabled = true;
            this.DogsList.ItemHeight = 25;
            this.DogsList.Location = new System.Drawing.Point(18, 62);
            this.DogsList.Name = "DogsList";
            this.DogsList.Size = new System.Drawing.Size(668, 454);
            this.DogsList.TabIndex = 1;
            this.DogsList.SelectedIndexChanged += new System.EventHandler(this.Dogs_SelectedIndexChanged);
            // 
            // displayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 545);
            this.Controls.Add(this.DogsList);
            this.Controls.Add(this.toolStrip1);
            this.Name = "displayForm";
            this.Text = "displayForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.displayForm_FormClosing_1);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.displayForm_FormClosed);
            this.Shown += new System.EventHandler(this.displayForm_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ListBox DogsList;
        private System.Windows.Forms.ToolStripButton toolClear;
    }
}