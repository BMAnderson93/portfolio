﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE03
{
    public partial class Form1 : Form

    // Brandon Anderson
    // VFW
    // C201802
    // Custom Events
    // CE03
    { // Creates all my public events and bools to be used in the methods
        public event EventHandler OpenAndSendInfo;
        bool disableTheWindow = false;

        public displayForm test = new displayForm();

       
        public List<Dog> allDogs = new List<Dog>();
        public Form1()
        {
            InitializeComponent();
            
        }
         // Clears the form
        public void Clear()
        {
            txtDogBreed.Text = "";
            chkFixed.Checked = false;
            numPrice.Value = 00.00m;
            dateDOB.ResetText();
        }
        // Adds the dog to the list and displays if the second form is open
        private void btnAddToList_Click(object sender, EventArgs e)
        {
            
            Dog newDog = new Dog();
            newDog.dogBreed = txtDogBreed.Text;
            newDog.isNut = chkFixed.Checked;
            newDog.price = numPrice.Value;
            newDog.dob = dateDOB.Value.ToString();
            allDogs.Add(newDog);
            Clear();

            
              
                OpenAndSendInfo += test.OpenAndSendInfo;


                if (OpenAndSendInfo != null)
                {
                    OpenAndSendInfo(this, new EventArgs());
                }
            


            

        }
        // Displays the second window and populates it for the user
        private void toolDisplay_Click(object sender, EventArgs e)
        {

            disableTheWindow = true;
            displayForm test = new displayForm();
            test.isOpen = true;
            test.emptyList += Test_emptyList;
            test.sendSelected += Test_sendSelected;
            test.ChangeToTrue += Test_ChangeToTrue;
            test.DisableWindow += Test_DisableWindow;  
          

            OpenAndSendInfo += test.OpenAndSendInfo;
            


            if (OpenAndSendInfo != null)
            {
                OpenAndSendInfo(this, new EventArgs());
            }


            
            test.Show();
           
           
        }
         // Changes the view window to either be disabled or enabled based on the bool
        private void Test_ChangeToTrue(object sender, EventArgs e)
        {
            disableTheWindow = false;
            if (disableTheWindow)
            {
                toolDisplay.Checked = true;
                toolDisplay.Enabled = false;
            }
            else
            {
                toolDisplay.Checked = false;
                toolDisplay.Enabled = true;
            }
        }
        // Does the same as above but for a different custom event and parameters
        private void Test_DisableWindow(object sender, EventArgs e)
        {
            if (disableTheWindow)
            {
                toolDisplay.Checked = true;
                toolDisplay.Enabled = false;
            }
            else
            {
                toolDisplay.Checked = false;
                toolDisplay.Enabled = true;
            }
        }
        // Used to display the selected infortmation from window to window
        private void Test_sendSelected(object sender, EventArgs e)
        {
            displayForm test =(displayForm) sender;
            Dog testDog = test.returnSelectedDog();
            if (testDog != null)
            {
                txtDogBreed.Text = testDog.dogBreed;
                numPrice.Value = testDog.price;
                chkFixed.Checked = testDog.isNut;
                dateDOB.Value = Convert.ToDateTime(testDog.dob);
            }
        }
        // Clears the list for the second window
        private void Test_emptyList(object sender, EventArgs e)
        {
            Clear();
            allDogs = new List<Dog>();
            if (OpenAndSendInfo != null)
            {
                OpenAndSendInfo(this, new EventArgs());
            }


        }

        
        // Clears the selected window from the first form and sends the information to the second form
        public void toolClear_Click(object sender, EventArgs e)
        {
            Clear();
            allDogs = new List<Dog>();

            displayForm test = new displayForm();


            OpenAndSendInfo += test.OpenAndSendInfo;


            if (OpenAndSendInfo != null)
            {
                OpenAndSendInfo(this, new EventArgs());
            }

        }
        // Clears the List on the second form
        public void ClearTheList()
        {
            allDogs = new List<Dog>();

            displayForm test = new displayForm();


            OpenAndSendInfo += test.OpenAndSendInfo;


            if (OpenAndSendInfo != null)
            {
                OpenAndSendInfo(this, new EventArgs());
            }
        }
        // Exits the application
        private void toolExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
