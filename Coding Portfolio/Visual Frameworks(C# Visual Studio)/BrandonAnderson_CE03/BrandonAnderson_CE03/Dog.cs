﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE03
{
    // Brandon Anderson
    // VFW
    // C201802
    // Custom Events
    // CE03
    public class Dog
    {
        public string dogBreed;
        public bool isNut;
        public decimal price;
        public string dob;

        public override string ToString()
        {
            return dogBreed + " $" + price;
        }
    }
}
