CE03: Custom Events
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE03" using the C# Windows Forms Application template.

Make sure you include your name, class and term, and code exercise name and number as comments to the top of the each code file of your project.  You will lose 5 points automatically if these comments are missing.
Grading:
Refer to the CE: Custom Events tab of this rubric to understand the grading for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE03.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Topics: ToolStrip, Checked Menu Item, Custom Event Handlers
Students will create an application that allows the user to create entities with the main window. A unique modeless window can be displayed to show all of the stored entities. The data can be cleared from either the modeless window or the main window.
You will need the same style of user input as you had in the previous lab. The Clear button will still need to be present with the same functionality. You will still need a menu bar, but this time File will just have an Exit option and the second top level menu called List will contain a Display command and a Clear command. The Add button for user input should store the input values in a class and store that object in a List (not a ListBox) on the Main form. When the List->Display command in the menu bar is clicked, a modeless custom form should open that contains only a ToolStrip and a ListBox. This ListBox should be populated with the values from the List in the Main window. The ToolStrip should have only a Clear button with both an image and text. When clicked, this Clear button should clear the ListBox items and the main window�s List of items.  Similarly, the List->Clear option in the main window�s menu bar should have the same functionality, clearing the List on the Main window and the ListBox on the second form.
While the ListBox form is open, the Display menu command on the main window should have a check mark next to it.  When the ListBox form is closed, the checkmark should be removed.
When an Item in the ListBox form is selected, the data of the selected item should populate the user input controls on the Main form.
The List of entities stored in the Main window should always be the same as what is shown in the ListBox window. If a new entity is added while the ListBox form is displayed, then that entity should be added to both the main window�s List and the ListBox form�s ListBox.
Follow these guidelines to produce this application:
User Input - Main Window
4 unique user input controls in a group box.
A button that clears all of the user input controls.
A button for adding the current input values to a List (not a ListBox) in the main window.
Adding an object to the List should also clear the input controls.
Menu Strip - Main WIndow
File->Exit closes the application.
List->Display displays the ListBox Form only if it is not already displayed. If the ListBox is already displayed, then there should be a check mark next to the Display option in the menu.
List->Clear clears the main window�s list as well as the ListBox Form�s items.
ListBox - Second Form
Contents should be the same as the main window�s List whenever the ListBox Form is visible/exists.
When an Item is selected in this ListBox the values of that Item should populate the user input controls in the main window.
ToolStrip - Second Form
A single Clear button that has both an Image and Text displaying.
The Clear button should clear both the ListBox items and the main window List.




Custom Events
You will need custom events for this and the way to figure out when you may need them is to think about when you need one Form to react to something happening on a different Form. For example:
When the ListBox in the second Form has an Item selected, the main window�s input fields need to display that Item�s values.
When the second Form�s Clear button is pressed, the main window also needs to clear its List.
When the second Form is closed, the main window needs to uncheck the List->Display option in the menu bar.
There will be other situations where the use of custom events is the best solution specifically when something happens in the main window and the second window needs to react.
Extra Information
Go back through your code and check for the following:
Your application must compile and must not crash when starting up.
All variables and methods are named appropriately.
Input controls are appropriately labeled.
Any information being output to the user should be clear and concise.
Make sure nothing accesses an object that doesn�t exist.
Add comments to your code to indicate that you know what each line does.




