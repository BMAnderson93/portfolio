﻿namespace BrandonAnderson_CE02
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolExit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.statsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolDisplay = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnComp = new System.Windows.Forms.Button();
            this.btnUncomp = new System.Windows.Forms.Button();
            this.chkbxMando = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboxChoreDiff = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numChorePay = new System.Windows.Forms.NumericUpDown();
            this.txtbxChoreName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.bttnDelete = new System.Windows.Forms.Button();
            this.bttnMarkUncomp = new System.Windows.Forms.Button();
            this.bttnMarkComp = new System.Windows.Forms.Button();
            this.listUncomp = new System.Windows.Forms.ListBox();
            this.listComp = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numChorePay)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.statsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1354, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolExit,
            this.toolSave,
            this.toolLoad});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolExit
            // 
            this.toolExit.Name = "toolExit";
            this.toolExit.Size = new System.Drawing.Size(165, 38);
            this.toolExit.Text = "Exit";
            this.toolExit.Click += new System.EventHandler(this.toolExit_Click);
            // 
            // toolSave
            // 
            this.toolSave.Name = "toolSave";
            this.toolSave.Size = new System.Drawing.Size(165, 38);
            this.toolSave.Text = "Save";
            this.toolSave.Click += new System.EventHandler(this.toolSave_Click);
            // 
            // toolLoad
            // 
            this.toolLoad.Name = "toolLoad";
            this.toolLoad.Size = new System.Drawing.Size(165, 38);
            this.toolLoad.Text = "Load";
            this.toolLoad.Click += new System.EventHandler(this.toolLoad_Click);
            // 
            // statsToolStripMenuItem
            // 
            this.statsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolDisplay});
            this.statsToolStripMenuItem.Name = "statsToolStripMenuItem";
            this.statsToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
            this.statsToolStripMenuItem.Text = "Stats";
            // 
            // toolDisplay
            // 
            this.toolDisplay.Name = "toolDisplay";
            this.toolDisplay.Size = new System.Drawing.Size(191, 38);
            this.toolDisplay.Text = "Display";
            this.toolDisplay.Click += new System.EventHandler(this.toolDisplay_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.btnComp);
            this.groupBox1.Controls.Add(this.btnUncomp);
            this.groupBox1.Controls.Add(this.chkbxMando);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboxChoreDiff);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.numChorePay);
            this.groupBox1.Controls.Add(this.txtbxChoreName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(658, 488);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chore Information";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(238, 373);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(189, 71);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "Clear Chore Info";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnComp
            // 
            this.btnComp.Location = new System.Drawing.Point(6, 373);
            this.btnComp.Name = "btnComp";
            this.btnComp.Size = new System.Drawing.Size(189, 71);
            this.btnComp.TabIndex = 8;
            this.btnComp.Text = "Mark Complete";
            this.btnComp.UseVisualStyleBackColor = true;
            this.btnComp.Click += new System.EventHandler(this.btnComp_Click);
            // 
            // btnUncomp
            // 
            this.btnUncomp.Location = new System.Drawing.Point(465, 373);
            this.btnUncomp.Name = "btnUncomp";
            this.btnUncomp.Size = new System.Drawing.Size(189, 71);
            this.btnUncomp.TabIndex = 7;
            this.btnUncomp.Text = "Mark Incomplete";
            this.btnUncomp.UseVisualStyleBackColor = true;
            this.btnUncomp.Click += new System.EventHandler(this.btnUncomp_Click);
            // 
            // chkbxMando
            // 
            this.chkbxMando.AutoSize = true;
            this.chkbxMando.Location = new System.Drawing.Point(187, 273);
            this.chkbxMando.Name = "chkbxMando";
            this.chkbxMando.Size = new System.Drawing.Size(335, 29);
            this.chkbxMando.TabIndex = 6;
            this.chkbxMando.Text = "Manditory? (Leave blank if no)";
            this.chkbxMando.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Chore Difficulty";
            // 
            // comboxChoreDiff
            // 
            this.comboxChoreDiff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboxChoreDiff.FormattingEnabled = true;
            this.comboxChoreDiff.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboxChoreDiff.Location = new System.Drawing.Point(185, 189);
            this.comboxChoreDiff.Name = "comboxChoreDiff";
            this.comboxChoreDiff.Size = new System.Drawing.Size(121, 33);
            this.comboxChoreDiff.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Chore Pay   $";
            // 
            // numChorePay
            // 
            this.numChorePay.Location = new System.Drawing.Point(185, 127);
            this.numChorePay.Name = "numChorePay";
            this.numChorePay.Size = new System.Drawing.Size(152, 31);
            this.numChorePay.TabIndex = 2;
            // 
            // txtbxChoreName
            // 
            this.txtbxChoreName.Location = new System.Drawing.Point(185, 68);
            this.txtbxChoreName.Name = "txtbxChoreName";
            this.txtbxChoreName.Size = new System.Drawing.Size(378, 31);
            this.txtbxChoreName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chore Name";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.bttnDelete);
            this.groupBox2.Controls.Add(this.bttnMarkUncomp);
            this.groupBox2.Controls.Add(this.bttnMarkComp);
            this.groupBox2.Controls.Add(this.listUncomp);
            this.groupBox2.Controls.Add(this.listComp);
            this.groupBox2.Location = new System.Drawing.Point(672, 43);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(683, 790);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 25);
            this.label4.TabIndex = 10;
            this.label4.Text = "Complete";
            // 
            // bttnDelete
            // 
            this.bttnDelete.Location = new System.Drawing.Point(316, 223);
            this.bttnDelete.Name = "bttnDelete";
            this.bttnDelete.Size = new System.Drawing.Size(44, 41);
            this.bttnDelete.TabIndex = 12;
            this.bttnDelete.Text = "X";
            this.bttnDelete.UseVisualStyleBackColor = true;
            this.bttnDelete.Click += new System.EventHandler(this.bttnDelete_Click);
            // 
            // bttnMarkUncomp
            // 
            this.bttnMarkUncomp.Location = new System.Drawing.Point(316, 162);
            this.bttnMarkUncomp.Name = "bttnMarkUncomp";
            this.bttnMarkUncomp.Size = new System.Drawing.Size(44, 41);
            this.bttnMarkUncomp.TabIndex = 11;
            this.bttnMarkUncomp.Text = ">";
            this.bttnMarkUncomp.UseVisualStyleBackColor = true;
            this.bttnMarkUncomp.Click += new System.EventHandler(this.bttnMarkUncomp_Click);
            // 
            // bttnMarkComp
            // 
            this.bttnMarkComp.Location = new System.Drawing.Point(316, 99);
            this.bttnMarkComp.Name = "bttnMarkComp";
            this.bttnMarkComp.Size = new System.Drawing.Size(44, 41);
            this.bttnMarkComp.TabIndex = 10;
            this.bttnMarkComp.Text = "<";
            this.bttnMarkComp.UseVisualStyleBackColor = true;
            this.bttnMarkComp.Click += new System.EventHandler(this.bttnMarkComp_Click);
            // 
            // listUncomp
            // 
            this.listUncomp.FormattingEnabled = true;
            this.listUncomp.ItemHeight = 25;
            this.listUncomp.Location = new System.Drawing.Point(366, 42);
            this.listUncomp.Name = "listUncomp";
            this.listUncomp.Size = new System.Drawing.Size(311, 729);
            this.listUncomp.TabIndex = 4;
            this.listUncomp.Click += new System.EventHandler(this.listUncomp_Click);
            // 
            // listComp
            // 
            this.listComp.FormattingEnabled = true;
            this.listComp.ItemHeight = 25;
            this.listComp.Location = new System.Drawing.Point(6, 42);
            this.listComp.Name = "listComp";
            this.listComp.Size = new System.Drawing.Size(304, 729);
            this.listComp.TabIndex = 3;
            this.listComp.Click += new System.EventHandler(this.listComp_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(361, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 25);
            this.label5.TabIndex = 13;
            this.label5.Text = "Incomplete";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 845);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numChorePay)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolExit;
        private System.Windows.Forms.ToolStripMenuItem toolSave;
        private System.Windows.Forms.ToolStripMenuItem toolLoad;
        private System.Windows.Forms.ToolStripMenuItem statsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolDisplay;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboxChoreDiff;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numChorePay;
        private System.Windows.Forms.TextBox txtbxChoreName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnComp;
        private System.Windows.Forms.Button btnUncomp;
        private System.Windows.Forms.CheckBox chkbxMando;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listUncomp;
        private System.Windows.Forms.ListBox listComp;
        private System.Windows.Forms.Button bttnDelete;
        private System.Windows.Forms.Button bttnMarkUncomp;
        private System.Windows.Forms.Button bttnMarkComp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

