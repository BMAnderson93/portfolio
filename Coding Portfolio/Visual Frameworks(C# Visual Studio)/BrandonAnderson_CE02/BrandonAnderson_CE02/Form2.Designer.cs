﻿namespace BrandonAnderson_CE02
{
    partial class DisplayChoresCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFinishedCount = new System.Windows.Forms.TextBox();
            this.txtUnfinishedCount = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(228, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chores Finished";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(228, 258);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Chores Unfinished";
            // 
            // txtFinishedCount
            // 
            this.txtFinishedCount.Location = new System.Drawing.Point(176, 166);
            this.txtFinishedCount.Name = "txtFinishedCount";
            this.txtFinishedCount.ReadOnly = true;
            this.txtFinishedCount.Size = new System.Drawing.Size(270, 31);
            this.txtFinishedCount.TabIndex = 2;
            // 
            // txtUnfinishedCount
            // 
            this.txtUnfinishedCount.Location = new System.Drawing.Point(176, 307);
            this.txtUnfinishedCount.Name = "txtUnfinishedCount";
            this.txtUnfinishedCount.ReadOnly = true;
            this.txtUnfinishedCount.Size = new System.Drawing.Size(270, 31);
            this.txtUnfinishedCount.TabIndex = 3;
            // 
            // DisplayChoresCount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 485);
            this.Controls.Add(this.txtUnfinishedCount);
            this.Controls.Add(this.txtFinishedCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DisplayChoresCount";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFinishedCount;
        private System.Windows.Forms.TextBox txtUnfinishedCount;
    }
}