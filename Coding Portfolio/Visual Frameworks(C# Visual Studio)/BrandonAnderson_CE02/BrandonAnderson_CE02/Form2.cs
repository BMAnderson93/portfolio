﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandonAnderson_CE02
{
    // Brandon Anderson
    // VFW
    // C201802
    // Passing Data and Multiple Forms
    // CE02
    public partial class DisplayChoresCount : Form
    {
        // Displays the count for each list of chores to the user
        public DisplayChoresCount()
        {
            InitializeComponent();
        }

        public string FinishedCount
        {
            set
            {
                txtFinishedCount.Text = value;
            }
        }
        public string UnFinishedCount
        {
            set
            {
                txtUnfinishedCount.Text = value;
            }
        }
    }
}
