﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandonAnderson_CE02
{
    // Brandon Anderson
    // VFW
    // C201802
    // Passing Data and Multiple Forms
    // CE02
    class Chore
    {
        public string name;
        public string difficulty;
        public float pay;
        public bool isRequired;
        public int sideImOn;

        // Overrides the default string displayed by the consol
        public override string ToString()
        {
            return name + "  $" + pay;
        }

    }
}
