﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace BrandonAnderson_CE02
{
    // Brandon Anderson
    // VFW
    // C201802
    // Passing Data and Multiple Forms
    // CE02
    public partial class Form1 : Form
    {
        public Form1()
        { // Creates my list of chorses to be used later for saving data
            List<Chore> finished = new List<Chore>();
            List<Chore> unFinished = new List<Chore>();
          
            InitializeComponent();
            Clear();
        }

        private void bttnMarkUncomp_Click(object sender, EventArgs e)
        { // Changes the chore from complete to uncomplete
            if (listComp.SelectedItem != null)
            {
                Chore changeMe = (Chore)listComp.SelectedItem;
                changeMe.sideImOn = 2;
                listUncomp.Items.Add(changeMe);
                
                listComp.Items.Remove(listComp.SelectedItem);

            }
        }

        private void bttnMarkComp_Click(object sender, EventArgs e)
        { //  Changes the chore from uncomplete to complete
            if (listUncomp.SelectedItem != null)
            {
                Chore changeMe = (Chore)listUncomp.SelectedItem;
                changeMe.sideImOn = 1;
                listComp.Items.Add(changeMe);
                listUncomp.Items.Remove(listUncomp.SelectedItem);

            }

        }

        private void bttnDelete_Click(object sender, EventArgs e)
        { // Deletes the selected chore
            if(listUncomp.SelectedItem != null)
            {
                listUncomp.Items.Remove(listUncomp.SelectedItem);
            }
            else if (listComp.SelectedItem != null)
            {
                listComp.Items.Remove(listComp.SelectedItem);
            }
        }

        private void Clear()
        { // Clears the fields in the information field
            txtbxChoreName.Text = "";
            numChorePay.Value = 0;
            comboxChoreDiff.SelectedIndex = 0;
            chkbxMando.Checked = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {// Calls the Clear
            Clear();
        }

        private void btnComp_Click(object sender, EventArgs e)
        { //  Adds the chore to the assigned field
            Chore thisChore = new Chore();
            thisChore.name = txtbxChoreName.Text;
            thisChore.difficulty = comboxChoreDiff.Text;
            thisChore.pay = (float)numChorePay.Value;
            thisChore.isRequired = chkbxMando.Checked;
            thisChore.sideImOn = 1;

            listComp.Items.Add(thisChore);
            Clear();

        }

        private void btnUncomp_Click(object sender, EventArgs e)
        {// Adds the chores to the assigned field 
            Chore thisChore = new Chore();
            thisChore.name = txtbxChoreName.Text;
            thisChore.difficulty = comboxChoreDiff.Text;
            thisChore.pay = (float)numChorePay.Value;
            thisChore.isRequired = chkbxMando.Checked;
            thisChore.sideImOn = 2;
            listUncomp.Items.Add(thisChore);
            Clear();

        }

        private void toolDisplay_Click(object sender, EventArgs e)
        {// Opens the new form to show the counts
            DisplayChoresCount theCounts = new DisplayChoresCount();
            theCounts.FinishedCount = listComp.Items.Count.ToString();
            theCounts.UnFinishedCount = listUncomp.Items.Count.ToString();
            theCounts.ShowDialog();
        
        }

        private void toolSave_Click(object sender, EventArgs e)
        { // Saves the users lists to 1 file where selected
            Stream myStream;

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {// These lines save each of the data fields to strings to be then saved at the selected location
                    string chorName;
                    string chorPay;
                    string chorDiff;
                    string chorMando = "false";
                    string whereAmI;
                    List<string> allInfo = new List<string>();
                    for (int i = 0; i < listComp.Items.Count; i++)
                    {
                        Chore tempChore = (Chore)listComp.Items[i];
                        chorName = tempChore.name;
                        chorPay = tempChore.pay.ToString();
                        chorDiff = tempChore.difficulty;
                        if(tempChore.isRequired == true)
                        {
                            chorMando = "true";
                        }
                        whereAmI = tempChore.sideImOn.ToString();
                        allInfo.Add(chorName);
                        allInfo.Add(chorPay);
                        allInfo.Add(chorDiff);
                        allInfo.Add(chorMando);
                        allInfo.Add(whereAmI);



                    }
                    // takes each item and saves it to the folder
                    for (int i = 0; i < listUncomp.Items.Count; i++)
                    {
                        Chore tempChore = (Chore)listUncomp.Items[i];
                        chorName = tempChore.name;
                        chorPay = tempChore.pay.ToString();
                        chorDiff = tempChore.difficulty;
                        if (tempChore.isRequired == true)
                        {
                            chorMando = "true";
                        }
                        whereAmI = tempChore.sideImOn.ToString();
                        allInfo.Add(chorName);
                        allInfo.Add(chorPay);
                        allInfo.Add(chorDiff);
                        allInfo.Add(chorMando);
                        allInfo.Add(whereAmI);



                    }
                    // Takes all the strings created above and puts them into 1 array to be then written to the saved file

                    string path = saveFileDialog1.FileName;
                    StreamWriter streamWriter = new StreamWriter(myStream);


                    foreach (string word in allInfo)
                    {
                        streamWriter.WriteLine(word);
                    }
                    streamWriter.Close();


                    myStream.Close();
                }
            }
            }

        private void toolLoad_Click(object sender, EventArgs e)
        { // Loads from the selected file

            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            listComp.Items.Clear();
            listUncomp.Items.Clear();
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            List<string> allInfo = new List<string>();
                            List<Chore> allChores = new List<Chore>();
                            int counter = 0;
                            StreamReader streamReader = new StreamReader(myStream);
                            string line;
                            // Reads each line of the selected file and adds them to the created list of strings
                            while ((line = streamReader.ReadLine()) != null)
                            {

                                allInfo.Add(line);

                            }
                            Chore tempChore = new Chore();
                           for (int i = 0; i < allInfo.Count(); i++)
                            { // Depending on the counter, this will assign the proper variables from the loaded file
                                
                                if (counter == 0)
                                {
                                    tempChore = new Chore();
                                    tempChore.name = allInfo[i];
                                   
                                }
                                else if(counter == 1)
                                {
                                    tempChore.pay = float.Parse(allInfo[i]);
                                    
                                }
                                else if (counter == 2)
                                {
                                    tempChore.difficulty = allInfo[i];
                                }
                                else if (counter == 3)
                                {
                                    if (allInfo[i] == "true")
                                    {
                                        tempChore.isRequired = true;
                                    }
                                    else if (allInfo[i] == "false")
                                    {
                                        tempChore.isRequired = false;
                                    }
                                   

                                   
                                    
                                }
                                else if (counter == 4)
                                {
                                    tempChore.sideImOn = int.Parse(allInfo[i]);
                                    allChores.Add(tempChore);
                                }
                                // resets the counter back to 0 after each field has been input for each chore class
                                counter++;
                                if(counter == 5)
                                {
                                    counter = 0;
                                }
                                
                            }
                           foreach(Chore job in allChores)
                            { // Takes each chore created and assigns it to the proper sides
                                if (job.sideImOn == 1)
                                {
                                    listComp.Items.Add(job);
                                }
                                else if (job.sideImOn == 2)
                                {
                                    listUncomp.Items.Add(job);
                                }
                            }
                           
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void toolExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        

       
        private void listComp_Click(object sender, EventArgs e)
        { // Displays the selected info for the chor higlighted
            if (listComp.SelectedItem != null)
            {
                Clear();
                Chore tempChore = (Chore)listComp.SelectedItem;
                listUncomp.SelectedIndex = -1;
                txtbxChoreName.Text = tempChore.name;
                numChorePay.Value = (decimal)tempChore.pay;
                comboxChoreDiff.SelectedIndex = int.Parse(tempChore.difficulty);
                chkbxMando.Checked = tempChore.isRequired;
            }

        }

        private void listUncomp_Click(object sender, EventArgs e)
        {
            if (listComp.SelectedItem != null)
            {
                Clear();
                // Displays the selected info for the chor higlighted
                Chore tempChore = (Chore)listUncomp.SelectedItem;
                listComp.SelectedIndex = -1;
                txtbxChoreName.Text = tempChore.name;
                numChorePay.Value = (decimal)tempChore.pay;
                comboxChoreDiff.SelectedIndex = int.Parse(tempChore.difficulty);
                chkbxMando.Checked = tempChore.isRequired;
            }

        }
    }

}
