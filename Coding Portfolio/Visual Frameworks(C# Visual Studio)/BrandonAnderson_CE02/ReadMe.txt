CE02: Passing Data and Multiple Forms
Setup:
You will be creating a new Visual Studio project named "FirstnameLastname_CE02" using the C# Windows Forms Application template.

Make sure you include your name, class and term, and code exercise name and number as comments to the top of the each code file of your project.  You will lose 5 points automatically if these comments are missing.�
Grading:
Refer to the CE: Passing Data and Multiple Forms tab of this rubric to understand the grading for this assignment.
Deliverables:
You will compress and upload a file named "FirstnameLastname_CE02.zip" with the following contents with the following names:
Project - Folder containing your entire project and all files necessary to build the project(csproj, .cs files, App.config, and Properties folder).

Be sure to upload the correct project and all required files the first time as only one submission will be allowed.  No extra time or consideration will be given if the wrong files are uploaded.
Instructions:
Topics: ListBoxes, Multiple forms, Passing data to a form, Passing data between controls
Students will create an application that allows the user to create entities and sort them into one of two groups. The application allows for moving entities between the two groups and removing them as needed. A modal window can be opened to show how many items are in each group. Saving and loading will also be supported in this application.
You will need the same style of user input as you had in lab one, but one additional type of user input control will be required, bringing the total up to 4. The Clear button will still need to be present with the same functionality. You also will still need a menu bar with a File menu containing Save, Load, and Exit as in the previous assignment, and there will be a new top level menu called Stats with a single Display command under it. Your application will have two list boxes, and allow the user to add an object with the current user input values to either list box of their choice. The user is then able to remove individual objects from either list box or move an object from one list box to the other. The Save and Load options will now keep track of what objects are in the list boxes instead of what is currently in the user input controls. The separate Stats menu will have a Display option.  Stats->Display menu option should bring up a modal dialog that displays the number of objects in each of the list boxes.
Follow these guidelines to produce this application:
User Input
4 unique user input controls in a group box.
A button that clears all of the user input controls.
Some combination of controls that allows the user to store the current input in one of two list boxes.
Adding an object to a listbox should also clear the input controls.
List Boxes
Two distinct list boxes should be present.
Selecting an item in one of the list boxes should populate the user input controls with that object�s values.
Objects stored in the list boxes should display a sensible string representation of the object. This string does not need to have every field from the object in it.
The user should have the ability to move a selected item from one listbox to the other.
The user should be able to remove a selected item from either list box.
Multiple selections should be disabled.
Menu Strip
Should contain to separate menus with the following: a File menu with Load,Save,Exit, and a Stats menu with Display.
Exit should close the application.
Save should save the current state of the two list boxes to a file keeping track of every field in the stored objects as well as which box that each object is stored in. This does not need to keep track of if an item was selected.
Load should be able to take what you saved out and populate both list boxes with all of the objects that they had previously.
The Stats->Display option should display a separate modal dialog that simply displays the total number of objects in each of the two list boxes. 
Extra Information
Go back through your code and check for the following:
Your application must compile and must not crash when starting up.
All variables and methods are named appropriately.
Input controls are appropriately labeled.
Any information being output to the user should be clear and concise.
Make sure nothing accesses an object that doesn�t exist.
Add comments to your code to indicate that you know what each line does.
