//
//  ViewController.swift
//  AndersonBrandon_Exercise06
//
//  Created by Brandon Anderson on 4/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
// Creates access to variables from the views and for the views that I will need to reference
    
    @IBOutlet weak var viewColorBox: UIView!
    
   
    
    @IBOutlet weak var lblRed: UILabel!
    @IBOutlet weak var lblBlue: UILabel!
    @IBOutlet weak var lblGreen: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Sets the color, and sliders to match the first view in the second view
        let sVC: ColorViewController = segue.destination as! ColorViewController
        
        if let bgColor = viewColorBox.backgroundColor, let rFloat = lblRed.text, let bFloat = lblBlue.text, let gFloat = lblGreen.text{
            
            sVC.color = bgColor
            sVC.rFloat = Float(rFloat)
            sVC.bFloat = Float(bFloat)
            sVC.gFloat = Float(gFloat)
        }
        
        
    }
    // Sets the labels and color to match the values created in the second view
    @IBAction func returnToFirst(segue: UIStoryboardSegue){
        if let sVC: ColorViewController = segue.source as? ColorViewController{
            lblRed.text = String(sVC.rFloat!)
            lblBlue.text = String(sVC.bFloat!)
            lblGreen.text = String(sVC.gFloat!)
            viewColorBox.backgroundColor = sVC.viewColorBox.backgroundColor!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

