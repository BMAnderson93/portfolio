//
//  ColorViewController.swift
//  AndersonBrandon_Exercise06
//
//  Created by Brandon Anderson on 4/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ColorViewController: UIViewController {

     // Creates access to variables from the views and for the views that I will need to reference
    @IBOutlet weak var viewColorBox: UIView!
    
    var color: UIColor?
    var rFloat: Float?
    var bFloat: Float?
    var gFloat: Float?
    
    @IBOutlet weak var sliderRed: UISlider!
    @IBOutlet weak var sliderBlue: UISlider!
    @IBOutlet weak var sliderGreen: UISlider!
    override func viewDidLoad() {
        super.viewDidLoad()
       // Sets the color values and color to the matching data being passed in.
        viewColorBox.backgroundColor = color
        sliderRed.value = rFloat!
        sliderBlue.value = bFloat!
        sliderGreen.value = gFloat!
       
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function I use for updating the color when adjusted on the sliders
    @IBAction func updateColor(sender: UISlider){
        switch sender.tag {
            case 0:
            rFloat = sender.value
           
            case 1:
            bFloat = sender.value
          
            case 2:
            gFloat = sender.value
           
        default:
            print("I have a bad feeling about this...")
        }
        updateColor()
    }
    
    // Function I use for updating the view color box color
    func updateColor(){
        viewColorBox.backgroundColor = UIColor(red: CGFloat(rFloat!), green:  CGFloat(gFloat!), blue:  CGFloat(bFloat!), alpha: 1.0)
    }
    // Creates the random values for each color and assigns them
    @IBAction func randomValues(){
        rFloat = Float(Float(arc4random()) / Float(UINT32_MAX ))
        bFloat = Float(Float(arc4random()) / Float(UINT32_MAX ))
        gFloat = Float(Float(arc4random()) / Float(UINT32_MAX ))
        print(rFloat!, bFloat!, gFloat!)
        
        sliderRed.value = rFloat!
        sliderBlue.value = bFloat!
        sliderGreen.value = gFloat!
        updateColor()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
