Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

Create a Single View Application and add a new View Controller in your Storyboard. Follow the requirements for each of the individual ViewControllers below.

Part 1: First ViewController

Your first UIViewController must have 1 Button, 3 Labels, and 1 UIView
The UIView must display a UIColor
The UILabels display the RGB values of that color as decimal numbers in the 0.0 -> 1.0 range. One color channel per label.
You do not need to round this value at all, but make sure that the label is large enough and the font size is small enough to display the whole value.
The UIButton brings the user to the Second View Controller
Color Information is received from the Second ViewController and updates the following UIElements:
The UIView displays the color created in the Second View Controller
The 3 UILabels update to display the RGB value of the newly passed back color.
Part 2: Second ViewController

Your Second View Controller must have 1 UIView, 2 UIButtons, and 3 UISliders representing the different color channels(RGB).
Color Information is sent forward and received from the First View Controller and updates the following elements:
The UIView displays the current color passed forward from First ViewController.
It should NOT have a default value that it reverts to; it should always update based on data passed forward.
The UISlider values reflect the current Red, Green, and Blue color values upon entering the Second View Controller
The UISliders modify the color value for the UIView
For instance, sliding the Red slider adds/removes Red value from the UIView's color.
A Randomize Button that randomizes RGB values and updates UIView and slider bars to reflect the current value.
Back Button returns to the First ViewController, passing back the color information to update that object