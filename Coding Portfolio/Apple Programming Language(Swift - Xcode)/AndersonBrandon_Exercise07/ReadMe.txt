Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

Create a new Single-View Application with 2 View Controllers

Part 1: First ViewController

6 TextFields with the UITextFieldDelegate protocol implemented
When the keyboard is up, pressing the "Next" button on that UITextField must advance the user to the next UITextField (You must make the keyboard "return" key say "Next")
When the final TextField is selected, the "Next" button should dismiss the keyboard and say �done� instead of �next�  (You must make the keyboard "return� key say �Done")
There should also be a UILabel which will be populated when coming back from the Second ViewController
A UIButton to segue to the Second View Controller.
If ANY TextField does not have text, display an alert telling the user to fill in all fields and prevent the segue. 
If the segue happens, you should pass the contents of each UITextField to the second ViewController as an array of Strings
Every time a segue to the second ViewController is triggered should be a UNIQUE array; do NOT continue to append new elements to the array so that your array grows to infinite size.
Part 2: Second ViewController

There should be 6 UILabels, each one displaying one of the Strings passed forward from the first ViewController
Two UIButtons
Cancel - Goes back to First View Controller without doing anything
Reduce - Reduces the array of strings into a single string using the built in array.reduce closure method, passes that String back to the first view controller, and then dismisses the Second View Controller.
The UILabel in the first ViewController should display the reduced String that was sent back.