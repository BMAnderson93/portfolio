//
//  DisplayView.swift
//  AndersonBrandon_Exercise07
//
//  Created by Brandon Anderson on 4/16/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class DisplayView: UIViewController {

    // All variables ill need in this view
    @IBOutlet weak var lbl0: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    
    var allInfo: [String] = []
    var newString: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // puts all lbls into an array
        var allLbls = [lbl0,lbl1,lbl2,lbl3,lbl4,lbl5]
        
        // loops through both arrays and assigns values
        for i in 0...5{
            allLbls[i]?.text = allInfo[i]
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Goes back without making any changes
    @IBAction func goBack(){
        dismiss(animated: true, completion: nil)
    }
    // reduces the array to a single string
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
         newString = allInfo.reduce("", { x, y in
           x + "\n \(y) "
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
