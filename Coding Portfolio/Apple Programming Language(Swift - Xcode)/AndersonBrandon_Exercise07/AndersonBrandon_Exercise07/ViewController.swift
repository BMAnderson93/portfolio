//
//  ViewController.swift
//  AndersonBrandon_Exercise07
//
//  Created by Brandon Anderson on 4/16/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate  {

    // all variables ill need
    @IBOutlet weak var txtField0: UITextField!
    @IBOutlet weak var txtField1: UITextField!
    @IBOutlet weak var txtField2: UITextField!
    @IBOutlet weak var txtField3: UITextField!
    @IBOutlet weak var txtField4: UITextField!
    @IBOutlet weak var txtField5: UITextField!
    var stringArray: [String] = []
    
    @IBOutlet weak var lblToUpdate: UILabel!
    
    override func viewDidLoad() {
        // Sets all the correct keyboards and delegates
        super.viewDidLoad()
        txtField0.delegate = self
        txtField0.returnKeyType = UIReturnKeyType.next
        txtField1.delegate = self
        txtField1.returnKeyType = UIReturnKeyType.next
        txtField2.delegate = self
        txtField2.returnKeyType = UIReturnKeyType.next
        txtField3.delegate = self
        txtField3.returnKeyType = UIReturnKeyType.next
        txtField4.delegate = self
        txtField4.returnKeyType = UIReturnKeyType.next
        txtField5.delegate = self
        txtField5.returnKeyType = UIReturnKeyType.done
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    // Checks to make sure all fields are used and if not displays the alert
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        let uiFields: [UITextField] = [txtField0,txtField1,txtField2,txtField3,txtField4,txtField5]
        stringArray = []
        for i in uiFields{
            
            if i.text == ""{
                let alert = UIAlertController(title: "Warning", message: "You must enter ALL fields before going to the next screen!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                }))
                self.present(alert, animated: true, completion: nil)
                return false
            }
            // adds the fields to a array
            stringArray.append(i.text!)
        }
        
        return true
    }
    // sends the string array to the other view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let displayView: DisplayView = segue.destination as! DisplayView
            displayView.allInfo = stringArray
            
            
        
        
    }
    // Unwinds the other form and updates the label if the user hit that option
    @IBAction func returnToMain(segue: UIStoryboardSegue){
        if let displayView: DisplayView = segue.source as? DisplayView{
            lblToUpdate.text = displayView.newString
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Allows the user to go from txtfield to text field
    func textFieldShouldReturn(_ txtField: UITextField) -> Bool{
        // Looks for the next field
        
        if let nextField = txtField.superview?.viewWithTag(txtField.tag + 1) as? UITextField {
           
            nextField.becomeFirstResponder()
            
           
        } else {
            
            // Removes the keyboard
            txtField.resignFirstResponder()
        }
        
        return false
    }
    

}

