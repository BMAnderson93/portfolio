Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

Part 1: Simple Functions
Create Functions with the following specifications

A Function with No Parameters and No Return
Prints any hardcoded string of your choosing
Call this function
 A Function taking in an Int as a Parameter and returns a newly formatted String
Returns a String with the following format: �The Int that was passed into this function was (THE INT GOES HERE)."
Please don�t actually write out (THE INT GOES HERE). That�s where your parameter will go.
Call this function AND print the return (print should not exist within the function)
Part 2: Random Int Array Function

Create a Function with No Parameters and No Return
Create three Ints named greaterThanTen, exactlyTen & lessThanTen defaulted to 0.
Create an array of 10 random Int values named randInt.
Each Int in the array must be randomly set to a value from 1 to 20
After the array contains 10 random Int values, loop through the array and increment greaterThanTen for each value found that is greater than 10, increment lessThanTen for each value found that is less than 10, and exactlyTen for every value found in the array that is exactly equal to 10
You should be looping through the array itself NOT a range of numbers.
After all elements of the array have been checked and the appropriate Ints incremented:
Print the randInt Array object.
Print the total number of less than 10, exactly 10, and greater than 10 values in a single print statement.
Call this function
Part 3: Nested Arrays Function

Create an Array of Arrays of Doubles. (The outside Array, holds Arrays of Doubles as values.)
Minimum of three(3) inner Arrays containing five(5) Doubles each.
Create a function that takes in an Array of Arrays of Doubles as a Parameter and returns a String Object.
Loop through each of the inner arrays and concatenate each element of the inner Arrays into a String object.
Return the String object after adding all elements of the inner arrays
Call this function passing in the Array of Arrays of Doubles created earlier and print the resulting String.