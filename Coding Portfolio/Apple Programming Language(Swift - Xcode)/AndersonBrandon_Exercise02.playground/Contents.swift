//: Playground - noun: a place where people can play

// Brandon Anderson

import UIKit

// Part 1: Simple Functions

func firstFunc(){
    print("Hello!")
}

firstFunc()

func secondFunc(int: Int) -> String{
    return "The Int that was passed into this function was \(int)."
}
print(secondFunc(int: 10))


// Part 2: Random int array function

func thirdFunc(){
    var greaterThanTen = 0
    var exactlyTen = 0
    var lessThanTen = 0
    
    var randInt: [Int] = []
    
    for _ in 0...9 {
        randInt += [Int(arc4random_uniform(20) + 1)]
    }
    
    for i in randInt{
        if i > 10{
            greaterThanTen += 1
        }
        else if i == 10 {
            exactlyTen  += 1
        }
        else {
            lessThanTen  += 1
        }
    }
    print(randInt)
    print("There were \(greaterThanTen) numbers greater than 10, \(exactlyTen) numbers that were exactly 10, and \(lessThanTen) numbers less than 10.")
    
}
thirdFunc()



// Part 3: Nested Array Function

var nestedArray =  [[2.5,2.3,12.4,12.8,4.6], [2.4,3.4,5.7,9.8,2.1], [2.4,3.2,5.4,6.5,7.9]]

func fourthFunc(arrayOfArray: [[Double]]) -> String{
    var arrayString: String = ""
    for i in arrayOfArray{
        for n in i{
            arrayString += "\(n) "
        }
    }
    return arrayString
}

print(fourthFunc(arrayOfArray: nestedArray))






