Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

Random Doubles Function
Function that takes in no parameters and returns a Double.
Returns random Double between 0-100
Doubles should have the possibility to have decimal values. (Function does not just return whole numbers)
*hint* There should be about 4 billion (UINT32_Max) possible values available between 0-100.
Doubles To Int Function
Function that takes in 2 Doubles and returns an Int
The doubles are multiplied and the rounded result is returned.
Decimals of .0-.4 are rounded down. .5-.9 are rounded up.
Should print the values that were passed into it and the rounded result as an Int before returning.
Call the function, using the function from #1 above for each of the parameters
Array Of Strings Function
Function takes in an Array of Strings
Function should implement the following behavior:
Must use a Switch Statement for logic
If 1-3 Strings passed in, reverse each string in the original Array, then print the new Arrau
["zebra", "JAGUAR", "apple"] becomes ["arbez", "RAUGAJ", "elppa"]
If 4-6, Reverse the element order in the original Array, then print the new Array
["zebra", "JAGUAR", "apple", "Blue", "eMerald"] becomes ["eMerald", "Blue", "apple", "JAGUAR", "zebra"]
If >6 prints strings in alphabetical order. (Print Strings, not the Array)
Note: You may find it easier to modify the case of the strings in order to do this so that they are all the same. 
Call this function according to the following: (This exists outside the function declaration and should be called a total of three times)
Create a String Array of these 3 elements: ["zebra", "JAGUAR", "apple"]
Call function passing in the Array
Append these 2 elements to the Array: ["Blue", "eMerald"]
Call function passing in the Array
Append these 2 elements to the Array: ["beads", "BEAR"]
Call function passing in the Array
Large String Function
Function takes in Large String from randomtextgenerator.com returns Tuple(numVowels, numConsonants)
Copy and paste the entire first paragraph from the website into a string variable, and pass the variable into this function.
Count the number of words that start with consonants and start with vowels
Return the Tuple and print the result of the function call. (Does not print in the function)
We're not counting characters, only first character of each word. Do not count spaces as either type.