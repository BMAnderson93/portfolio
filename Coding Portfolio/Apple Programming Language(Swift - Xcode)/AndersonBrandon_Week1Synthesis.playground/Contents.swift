//: Playground - noun: a place where people can play

// Brandon Anderson

import UIKit

var str = "Hello, playground"
// Part 1

func firstFunction() -> Double {
    
    return Double(Double(arc4random()) / Double(UINT32_MAX)) + Double(arc4random_uniform(100))
    
    
    
}
// Part 2
func secondFunction(FirstNumber: Double, SecondNumber: Double) -> Int {
    
    let newInt = Int(round(FirstNumber * SecondNumber))
    
    print("The first number passed in was \(FirstNumber), the second number passed in was \(SecondNumber), the returned int was \(newInt).")
    
    return newInt

}

secondFunction(FirstNumber: firstFunction(), SecondNumber: firstFunction())

// Part 3


func thirdFunction(stringArray: [String]){
    var newArray: [String] = []
    switch stringArray.count {
    case 1...3:
        for i in stringArray{
           newArray += [String(i.reversed())]
            
        }
        print(newArray)
        
    case 4...6:
        newArray += stringArray.reversed()
        print(newArray)
    case 6...Int(UINT32_MAX):
        newArray += stringArray.sorted{ $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        for i in newArray {
            print(i)
        }
    default:
        print("You must of entered an empty array, nothing to do with that.")
    }
}
extension String {
    var wordList: [String] {
        return components(separatedBy: .punctuationCharacters)
            .joined()
            .components(separatedBy: .whitespaces)
    }
}



var testArray =  ["zebra", "JAGUAR", "apple"]
thirdFunction(stringArray: testArray)
testArray +=  ["Blue", "eMerald"]
thirdFunction(stringArray: testArray)
testArray +=  ["beads", "BEAR"]
thirdFunction(stringArray: testArray)

// Part 4


func fourthFunction(LargeString: String) -> (numVowels: Int, numConsonants: Int){
    var vowCount = 0
    var conCount = 0
    let stringArray = LargeString.lowercased().wordList
    
    for i in stringArray {
        switch i.first {
        case "a", "e", "i", "o", "u":
         vowCount += 1
        default:
            conCount += 1
        }
        
    }
    return (vowCount, conCount)
    
}

var randomText = "Supplied directly pleasant we ignorant ecstatic of jointure so if. These spoke house of we. Ask put yet excuse person see change. Do inhabiting no stimulated unpleasing of admiration he. Enquire explain another he in brandon enjoyed be service. Given mrs she first china. Table party no or trees an while it since. On oh celebrated at be announcing dissimilar insipidity. Ham marked engage oppose cousin ask add yet."

var vCount = fourthFunction(LargeString: randomText).numVowels
var cCount = fourthFunction(LargeString: randomText).numConsonants
print("There were a total of \(vCount) words that started with a vowel and \(cCount) that started with a consonant in that paragraph.")










