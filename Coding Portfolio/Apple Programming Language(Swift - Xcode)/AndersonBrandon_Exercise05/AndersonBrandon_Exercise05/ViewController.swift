//
//  ViewController.swift
//  AndersonBrandon_Exercise05
//
//  Created by Brandon Anderson on 4/11/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
// Gives me access to the information in my text fields and allows my to edit them
    @IBOutlet weak var txtValue1: UITextField!
    @IBOutlet weak var txtValue2: UITextField!
    @IBOutlet weak var txtResult: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
// Used for doing what ever formula the user has seleted
    @IBAction func getResult(sender: UIButton){
        // Gets the value of each txt box and converts it to a float
        let numVal1 = getFloats(txt: txtValue1, tag: sender.tag)
        let numVal2 = getFloats(txt: txtValue2, tag: sender.tag)
        var result : Float = 0
        // Checks which tag was selected and does the formula the user picked
        switch sender.tag {
        case 0:
            result = numVal1 + numVal2
        case 1:
            result = numVal1 - numVal2
        case 2:
            result = numVal1 * numVal2
        case 3:
            result = numVal1 / numVal2
        default:
            print("Something went wrong")
        }
        // Checks to see if the number is whole or not and updates the correct variable type
        if result.truncatingRemainder(dividingBy: 1)  == 0 {
            txtResult.text = String(Int(result))
        }
        else{
            txtResult.text = String(result)}
        
       
        
    }
    
    // Used to get the values of the txt fields
    func getFloats(txt: UITextField!, tag: Int) -> Float{
        var numVal: Float = 0
        
        // Checks the value of the txt field and converts it to a float if not empty
        if let string1 = txt.text, let numValTemp = Float(string1){
            numVal = numValTemp
        }
        else {
            // checks the tag and decides if the number needs a default value of 0 or 1 if its nil
            if  tag > 1 {
                numVal = 1
            }
        }
        return numVal
    }
    // Closes the keyboard if the form is touched outside of the buttons or txt fields
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
}

