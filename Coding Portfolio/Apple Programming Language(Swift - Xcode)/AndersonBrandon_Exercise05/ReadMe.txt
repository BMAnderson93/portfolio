Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

You will be creating a Single-View Application with one UIViewController that has the following requirements:

UIElements
There will be 2 UITextFields, 1 UILabel, and 4 UIButtons
User Input:
User can input a number into each textField
You should change the keyboard type so that the NumberPad keyboard is used.
Dismiss the keyboard by tapping on the view.
HINT: If you're not sure where to start, look at the UIResponder, UIViewController, UIView, and the Event Handling Guide for UIKit elements.
There is a single method you can override in your ViewController and a single method you can run inside of that override to perform this.
Functionality:
The four buttons should be labeled for and are used for the following functions:
Add
Subtract
Multiply
Divide
When each of the UIButtons is pressed the proper function is used against the entries in the text fields; for instance, if there is a 2 in the first field and a 5 in the second field and the multiply button is pressed, the number 10 should display.
For division, assume the "first" TextField (top-most or left-most) is the numerator and the "second" TextField is the denominator.
The label should not display the value wrapped in an Optional. ex. �Optional(57)" - it should be a legitimate response
The UILabel should display a whole number when the value is a whole  number and a decimal when the result has a remainder
For instance, if 4 is divided by 2, the result should display 2 with no decimal, but if 3 is divided by 2, the result should display 1.5.
Error Handling:
Any textField left blank should not crash the application and its value should instead be evaluated as either a 0 or a 1
If Addition or Subtraction are selected, a blank/invalid entry should be evaluated as a 0.
If Multiplication or Division are selected, a blank/invalid entry should be evaluated as a 1.
Eg. One Blank Textfield and One TextField with a Value of 50 should result in a 50 when added together.
Eg. One Blank Textfield and One TextField with a Value of 50 should result in a 50 when multiplied together
Eg. Both Blank Textfields should result in a 0 when subtracted together
You should be using Optional Binding or Nil Coalescing to perform this; you should NOT be using extensive nil or conditional checks.
The app should also properly handle the situation when the user manually enters a 0 and the division button is pressed and it should NOT evaluate this zero as a one.