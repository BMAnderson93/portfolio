Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

Download the zip containing a broken project and follow the requirements below.

This project has many errors that must be fixed before you can receive any points. Once you�ve fixed all the compiler errors, there will be runtime errors for you to hunt down and fix. After fixing the errors there is new functionality described below to add to this project. 

Assignment Overview:

First ViewController
Artist buttons will be hooked up to the same IBAction named artistButtonTapped
IBAction grabs the selected artist data using the tag of the button tapped.
Performs the segue to the Second ViewController
Array of Artists don�t need to have Album Data initially (can be empty on start up, NOT nil)
Random Button will need to randomly select an Artist from the array and perform the segue to the Second ViewController
Will need to pass the selectedArtist as a �Reference� to the Second ViewController
This is so that we can change the reference elsewhere (like adding albums to it!) and the information in our first ViewController will update with the changes with no additional work.
Second ViewController
Go back button will bring the user back to the First ViewController correctly
Artist Label will display the name of the selected artist
Artist Image View will display the image of the selected artist
The textView will display the description computed property of the selected artist
The Create Album button will bring the user to the Third ViewController
Third ViewController
8 textFields that allow the user to press the return key to transition to the next textField
When the user presses the return key on the last textField, the software keyboard should dismiss (Cannot use the Cmd + K shortcut to fake this)
If a word is typed into a textField that expects a number (Year Released), it should not crash the application
If a word was typed into the year released field, default the year to 2016
Will display a UIAlertController if the album name, year released, or ALL song fields are empty
Will inform the user that they must fill in the album name, year released, and at least one song with information
Must be dismissible
Will need a button to bring the user back to the Second ViewController, passing back the newly created album
The selected artist will need to append this to the list of albums and display the update in the Second ViewController�s textView
Part 1: Debugging

You must fix All compiler and runtime errors (Commenting or Deleting broken code will not be given credit. You must debug and fix it). You can modify the code to fit your needs.
Each of the three View Controllers must be reachable.
This just means going forward a user can see each of the three View Controllers, it�s okay if going back to previous View Controllers is not functional at this time
Part 2: Additional Functionality

TextField Delegates working correctly and dismisses the keyboard on the last textField
UIAlertController displays when meeting conditions defined above in Overview
All Artist buttons work and Random button works (Shouldn�t crash *randomly*)
Can have text in any of the song fields and will only count those as songs (Eg. If textField 3 is the only one with text, that Album has one song NOT 3 songs, 2 of which are blank.)
Artist information is displayed correctly on Second ViewController including the album release date and all songs listed
Go back button on Second ViewController goes back to the First ViewController properly (aka NOT presenting)
A word being typed into the year field is accounted for as the year 2016
NOTE: You CANNOT get around this requirement by changing the Year field to using the Number Pad, even though that would be better UX.