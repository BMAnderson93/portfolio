//
//  ThirdViewController.swift
//  AOC - FinalExam
//
//  Created by Joshua Shroyer on 4/28/15.
//  Copyright (c) 2014 Full Sail University. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController, UITextFieldDelegate {
  
    var selectedArtist: Artist?
    
    var songs: [String] = []
  @IBOutlet weak var albumName: UITextField!
  @IBOutlet weak var yearReleased: UITextField!
  @IBOutlet weak var song1: UITextField!
  @IBOutlet weak var song2: UITextField!
  @IBOutlet weak var song3: UITextField!
  @IBOutlet weak var song4: UITextField!
  @IBOutlet weak var song5: UITextField!
  @IBOutlet weak var song6: UITextField!
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
    
    @IBAction func cancel(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    // Validates the require information has been entered before allowing the segue
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if albumName.text?.isEmpty == true || yearReleased.text?.isEmpty == true {
           displayAlert()
            return false
            
        }
        else if song1.text?.isEmpty == true && song2.text?.isEmpty == true && song3.text?.isEmpty == true && song4.text?.isEmpty == true && song5.text?.isEmpty == true && song6.text?.isEmpty == true {
            displayAlert()
            return false
        }
            // Checks to see which fields have names
        else {
            if Int(yearReleased.text!) == nil{
                yearReleased.text = "2016"
            }
            if song1.text?.isEmpty == false{
                songs.append(song1.text!)
            }
            if song2.text?.isEmpty == false{
                songs.append(song2.text!)
            }
            if song3.text?.isEmpty == false{
                songs.append(song3.text!)
            }
            if song4.text?.isEmpty == false{
                songs.append(song4.text!)
            }
            if song5.text?.isEmpty == false{
                songs.append(song5.text!)
            }
            if song6.text?.isEmpty == false{
                songs.append(song6.text!)
            }
            
            return true
        }
    }
    // prepares for the rewind by creating the new album and updating the artist
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        
        let newAlbum = Album.init(name: albumName.text!, year: Int(yearReleased.text!)!, songs: songs)
        
        
        selectedArtist?.albums.append(newAlbum)
    }
    // Displays a alert letting them know why they cant save the information
    func displayAlert(){
        let alert = UIAlertController(title: "Error Missing Information", message: "You must fill in the album name, year released, and at least one song with information!", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        
        present(alert, animated: true)
    }
    
    // Allows the user to next to each field and dismisses on last
    func textFieldShouldReturn(_ txtField: UITextField) -> Bool{
        // Looks for the next field
        
        if let nextField = txtField.superview?.viewWithTag(txtField.tag + 1) as? UITextField {
            
            nextField.becomeFirstResponder()
            
            
        } else {
            
            // Removes the keyboard
            txtField.resignFirstResponder()
        }
        
        return false
    }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
