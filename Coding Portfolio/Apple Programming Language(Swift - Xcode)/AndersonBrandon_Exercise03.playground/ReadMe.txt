Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

Intermediate Functions
Create Functions with the following specifications

A function with no parameters that returns a Bool
The Bool is randomly either true or false, with both values having an equal chance to occur
A function with 2 Parameters that returns a String
The two parameters it takes in are a Bool and a String
If the passed in Bool is True returns an all uppercase version of the String
If the passed in Bool is False returns an all lowercase version of the String.
Call the function 3 times and print the result, using the random bool function you created above for one of the parameters and the Strings listed below for the other input parameter
oNe
TwO
tHrEe
NOTE: The Random Bool function should be used 3 different times;  you should NOT pass the same value to the function each time.
A function that takes in no Parameters that returns an Array of Ints
Function that returns an Array of 10 random Ints 1-100.
Ints must be random, not hardcoded.
The Ints should NOT be explicitly added to the array, but rather you should add them via some kind of control flow statement.
Call this function and print the result.
A function that takes in an array of Ints as a parameter an returns a tuple with 3 elements
Takes in an array of integers as the only parameter
Returns a Tuple containing the smallest, largest, and median values found in the array. Note: You can NOT use the min() or max() methods.
This function should properly work for ANY sized array - NOT just an array of 10 integers. For instance, if an array of 5 elements or 17 elements is passed in, those should also properly calculate the values.
Call this function and print out to the console "The largest value found was (largeValueHere), the smallest value found was (smallValueHere), and the median value found was (medianValueHere)"
Please don't actually write out (largeValueHere), (smallValueHere), or (medianValueHere), this is where the values go.
You should pass in the array generated from Part 3 above.
A function that takes in an array of Ints parameter and no return
The function uses a switch Statement with the following different cases to check against the individual elements of the array
Case 1: 1-33
Case 2: 34-66
Case 3: 67-100
Out of range/error
The function should track the number of times each case gets hit
At the end print the exact number of times each case of the switch got hit.
Example output: �The first case was hit (A) times, the second case was hit (B) times, and the third case was hit (C) times."
The function should be called, passing in an array from Function 3 above.