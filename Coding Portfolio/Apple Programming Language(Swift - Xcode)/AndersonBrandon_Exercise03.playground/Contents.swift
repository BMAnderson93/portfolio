//: Playground - noun: a place where people can play
// Brandon Anderson
import UIKit

var str = "Hello, playground"

// Part 1


func functionOne() -> (Bool) {
    
    var bool = false
    
    
    if arc4random_uniform(10) % 2 == 0 {
        bool = true
    }
   
    
    return bool
    
}
functionOne()
// Part 2


func functionTwo(bool: Bool, string: String) -> String{
    
    if bool{
    return string.uppercased()
    }
    else{
    return string.lowercased()
    }
}
var strArray = ["oNe", "TwO","tHrEe"]
for i in 0...2 {
    print(functionTwo(bool: functionOne(), string: strArray[i]))
}

// Part 3


func functionThree() -> [Int]{
    var intArray: [Int] = []
    for _ in 1...10{
        intArray += [Int(arc4random_uniform(100) + 1)]
    }
    return intArray
}
var functionThreeRandomInts = functionThree()
print(functionThreeRandomInts)

// Part 4

func functionFour(intArray: [Int]) -> (smallInt: Int, largeInt: Int, medDouble: Double) {
    
   var sortedArray =  intArray.sorted()
   
    let smallestInt = sortedArray[0]
    let largestInt = sortedArray[sortedArray.count - 1]
    var medianDouble = 0.0
    if Int(sortedArray.count) % 2 != 0 {
         medianDouble = Double(sortedArray[Int(sortedArray.count - 1) / 2])
        
    }
    else{
       
        medianDouble = (Double(sortedArray[Int((sortedArray.count / 2 - 1) )]) + Double(sortedArray[Int(sortedArray.count / 2)])) / 2
       
        
    }
    
    return (smallestInt, largestInt, medianDouble)
    
}
var allInfo = functionFour(intArray: functionThreeRandomInts)
print("The largest value found was \(allInfo.largeInt), the smallest value found was \(allInfo.smallInt), and the median value found was \(allInfo.medDouble)")



// Part 5

func functionFive(array: [Int]){
    var case1 = 0
    var case2 = 0
    var case3 = 0
    var case4 = 0
    
    for i in array {
        
        switch i {
        case 1...33:
            case1 += 1
        case 34...66:
            case2 += 1
        case 67...100 :
            case3 += 1
        default :
            case4 += 1
        }
    }
    print("The first case was hit \(case1) times, the second case was hit \(case2) times, and the third cas was hit \(case3) times, with \(case4) errors occuring.")
    
}

functionFive(array: functionThreeRandomInts)





