//
//  SummonerProfile.swift
//  AndersonBrandon_Exercise08
//
//  Created by Brandon Anderson on 4/18/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit
class SummonerProfile {
    // Creates a static variable to be used by the class
    static let ranks = ["Unranked","Bronze","Silver","Gold","Platinum","Diamond","Masters","Challenger"]
    // Creates the blue prints for building a profile
    var SummonerName: String
    var SummonerAccountLevel: Int
    var SummonerRank: Int
    var accountWins: Int
    var accountLosses: Int
    var kills: Int
    var deaths: Int
    var winsForPromotion: Int
    
    // gets the KDA based on 2 vars from the class
    var kda: Float {
        if deaths == 0{
            print("Deaths were 0")
            return Float(kills)
            
        }
        
        return Float(kills) / Float(deaths)
    }
    // gets the win ratio from 2 vars from the class
    var winPercentage: Float{
       
        
        if accountWins == 0 {
            return 0
        }
        else if accountLosses == 0 && accountWins == 0{
            return 0
        }
        else if accountLosses == 0 {
            return 1
        }
        else{
            
            return Float(accountWins) / (Float(accountWins) + Float(accountLosses))
            
        }
    }
    
    
    // used to simulate a game
    func playAMatch(sender: ViewController){
        // Sets random kills and deaths the player had this game
         let killsThisGame = Int(arc4random_uniform(11))
         let deathsThisGame = Int(arc4random_uniform(11))
        // updates the tallies
        kills += killsThisGame
        deaths += deathsThisGame
        // var used for telling the user wether they won or lost
        var string: String
        // used to randomly decide if the user wins his or her game, and keeps the tallies
        switch arc4random_uniform(2) {
        case 1:
            string = "You won!"
            accountWins += 1
            winsForPromotion += 1
        default:
            string = "You lost!"
            accountLosses += 1
            winsForPromotion -= 1
        }
            
    // Creates the alert that informs the user of the match details
        let alert = UIAlertController(title: "Match was played!", message: "\(string) \n \(killsThisGame) Kills \(deathsThisGame) Deaths", preferredStyle: .alert)
       
        alert.addAction(UIAlertAction(title: "Okay!", style: .default, handler: nil))
        
        
        sender.present(alert, animated: true)
        
        // Decides if the user should or can promote based on his rank and wins
        if SummonerRank != 7{
        if winsForPromotion == 20 {
            let alert = UIAlertController(title: "Promotion Goal Met", message: "Congratulations! You were promoted!", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yay", style: .default, handler: nil))
            sender.present(alert, animated: true)
            SummonerRank += 1
            winsForPromotion = 0
        }
        }
        
        // Decides if the user should or can demote based on his rank and wins
        if SummonerRank != 1{
            print(SummonerRank)
            print(winsForPromotion)
        if winsForPromotion == -5{
            let alert = UIAlertController(title: "Demoted", message: "Oh no! You were demoted!", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Boo", style: .default, handler: nil))
            sender.present(alert, animated: true)
            SummonerRank -= 1
            winsForPromotion = 0
        }
        }
      
        
        
    }
    
    // Does the placements for the user and decides where he or she will start the season (you can only get placed as high as platinum)
    func DoPlacements(sender: ViewController) {
        
        
        var placed: Int = 0
        switch arc4random_uniform(4) {
        
        
        case 0:
            placed = 1
        case 1:
            placed = 2
        case 2:
            placed = 3
        case 3:
            placed = 4
       
            
        default:
            print("The number messed up")
        }
        
        // Lets the user know where he or she ended up
        
        let alert = UIAlertController(title: "Placements Finished", message: "Congratulations, you were placed \(SummonerProfile.ranks[placed])", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Thanks!", style: .default, handler: nil))
        sender.present(alert, animated: true)

        // updates the users rank
        self.SummonerRank = placed
        
        
    }
    
    // creates a summoner with default values
    init (){
        SummonerName = "Default Summoner"
        SummonerAccountLevel = 0
        SummonerRank = 0
        accountWins = 0
        accountLosses = 0
        kills = 0
        deaths = 0
        winsForPromotion = 0
    }
    // Creates a summoner with what ever values the user decides
    init(SummonerName: String , SummonerAccountLevel: Int ,SummonerRank:Int ,accountWins:Int ,accountLosses: Int,kills: Int,deaths: Int,winsForPromotion:Int ){
         self.SummonerName = SummonerName
         self.SummonerAccountLevel = SummonerAccountLevel
         self.SummonerRank = SummonerRank
         self.accountWins = accountWins
         self.accountLosses = accountLosses
         self.kills = kills
         self.deaths = deaths
         self.winsForPromotion = winsForPromotion
    }
    
    
    
}
