//
//  EditViewController.swift
//  AndersonBrandon_Exercise08
//
//  Created by Brandon Anderson on 4/18/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class EditViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
   
    // Creates variables i will need
    @IBOutlet weak var rankPicker: UIPickerView!
    @IBOutlet weak var summonerNameText: UITextField!
   
    @IBOutlet weak var summonerLevelInt: UITextField!
    @IBOutlet weak var winsInt: UITextField!
    @IBOutlet weak var lossesInt: UITextField!
    @IBOutlet weak var killsInt: UITextField!
    @IBOutlet weak var deathsInt: UITextField!
    @IBOutlet weak var winsNeededInt: UITextField!
    
    var summoner: SummonerProfile?
    let ranks = SummonerProfile.ranks
    var rankInt = 0
    var stringRank: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Checks if the passed in summoner was already created, if they were then its values will populate the form for editing, if not we will allow the user to create their own information from scratch
        if summoner != nil{
        summonerNameText.text = summoner!.SummonerName
        summonerLevelInt.text = String(summoner!.SummonerAccountLevel)
        winsInt.text = String(summoner!.accountWins)
        lossesInt.text = String(summoner!.accountLosses)
        killsInt.text = String(summoner!.kills)
        deathsInt.text = String(summoner!.deaths)
        winsNeededInt.text = String(summoner!.winsForPromotion)
        rankPicker.selectRow(summoner!.SummonerRank, inComponent: 0, animated: true)
        }
       
        
        
        
        

        // Do any additional setup after loading the view.
    }
    // Goes back without changing anything
    @IBAction func cancel(){
     self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // sets the needed functions to use the picker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ranks[row]
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ranks.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        rankInt = row
    }
    
    // allows the user to close the keyboard by tapping outside
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    // validates the information on the fields are allowed and either stops the form from saving or sends the information back
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        print("The unwind happend")
        let arrayLbls = [summonerNameText, summonerLevelInt,winsInt, lossesInt,  killsInt, deathsInt, winsNeededInt]
        
        for i in arrayLbls{
            if i?.text == ""{
            displayWarning(warning: "All fields are required!")
                return false}
            if i !=  summonerNameText {
                if validInt(string: (i?.text!)!) == false{
                    displayWarning(warning: "You must only use a number in rows that ask for them!")
                    return false
            }
                else if Int((i?.text)!)! < 0 && i != winsNeededInt  {
                    displayWarning(warning: "You can only have a negative number in your wins twords promo field! ")
                    return false
                }
                else if i == winsNeededInt  {
                    if Int((i?.text)!)! > 19 || Int((i?.text)!)! < -4{
                        displayWarning(warning: "You can not be  more than 19 wins, or less than -4 wins away from a series!")
                        return false
                    }
                }
                
        }
            
        
       
    }
        
        // if the referenced summoner is not nil, we will update its values here.
        if summoner != nil{
        summoner!.SummonerName = summonerNameText.text!
        summoner!.SummonerAccountLevel = Int(summonerLevelInt.text!)!
        summoner!.accountWins = Int(winsInt.text!)!
        summoner!.accountLosses = Int(lossesInt.text!)!
        summoner!.kills = Int(killsInt.text!)!
        summoner!.deaths = Int(deathsInt.text!)!
        summoner!.SummonerRank = rankInt
       
      
            summoner?.winsForPromotion = Int(winsNeededInt.text!)!
        
           }
        
     return true
    }
    // Figures out the wins that are passed in
    func figureWins() -> Int{
        if Int(winsNeededInt.text!)! < 20 {
            return Int(winsNeededInt.text!)!
        }
        else {
            return 20 - Int(winsNeededInt.text!)!
        }
    }
    // Validates what evers passed in is a int
    func validInt(string: String) -> Bool {
        return Int(string) != nil
    }
    // Displays the alert to the used with what ever message is sent in
    func displayWarning(warning: String){
        let alert = UIAlertController(title: "Warning", message: warning, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }

    
   
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

