//
//  ViewController.swift
//  AndersonBrandon_Exercise08
//
//  Created by Brandon Anderson on 4/18/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // References to the variables I will be using
    @IBOutlet weak var doPlacementsBtn: UIButton!
    @IBOutlet weak var rankImage: UIImageView!
    @IBOutlet weak var playMatchBtn: UIButton!
    
    @IBOutlet weak var summonerRank: UILabel!
    @IBOutlet weak var summonerNameLbl: UILabel!
    @IBOutlet weak var summonerAccountLevelLbl: UILabel!
    @IBOutlet weak var percentWinLbl: UILabel!
    @IBOutlet weak var kdaLbl: UILabel!
    
    @IBOutlet weak var winLbl: UILabel!
    @IBOutlet weak var lossLbl: UILabel!
    
    @IBOutlet weak var killLbl: UILabel!
    @IBOutlet weak var deathLbl: UILabel!
    @IBOutlet weak var winsTilPromoLbl: UILabel!
    @IBOutlet weak var lossTilDemoLbl: UILabel!
    
    let percWinBar = CAShapeLayer()
    let kda = CAShapeLayer()
    var defaultSummonerProfile: SummonerProfile?
    // Used for positioning my circles for the percentage bars
    var position1 = CGPoint(x: 93.75, y: 333.5)
    var position2 = CGPoint(x: 281.25, y: 333.5)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        // makes the circles based off the given locations
        makeCircle(newCircle: percWinBar, position: position1)
        makeCircle(newCircle: kda, position: position2)
        
        
    }
    // function that creates the circles and removes the infil leaving only the stroke to be used as a percent bar
    func makeCircle(newCircle: CAShapeLayer, position: CGPoint){
        let greyBar = CAShapeLayer()
        let path = UIBezierPath(arcCenter: position, radius: 80, startAngle: -CGFloat.pi / 2, endAngle: CGFloat.pi * 2, clockwise: true)
        greyBar.path = path.cgPath
        greyBar.strokeColor = UIColor.gray.cgColor
        greyBar.lineWidth = 5
        greyBar.fillColor = UIColor.clear.cgColor
        view.layer.addSublayer(greyBar)
        
        
        
        
        newCircle.path = path.cgPath
        newCircle.strokeColor = UIColor.cyan.cgColor
        newCircle.lineWidth = 5
        newCircle.lineCap = kCALineCapSquare
        newCircle.fillColor = UIColor.clear.cgColor
        newCircle.strokeEnd = 0
        view.layer.addSublayer(newCircle)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Completes the series for the users account
    @IBAction func completeSeries(){
        if defaultSummonerProfile?.SummonerRank == 0 {
           
            
            defaultSummonerProfile?.DoPlacements(sender: self)
            update(summoner: defaultSummonerProfile!)
        }
    // Tells the user they cant do their series because they either dont have an account or already are ranked
        else {
            let alert = UIAlertController(title: "Warning", message: "You can only do your promos when you're unranked & have an account!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            
            present(alert, animated: true)
        }
        
       
    }
    // sets the player to default values
    @IBAction func setPlayerToDefault(){
    defaultSummonerProfile = SummonerProfile()
        update(summoner: defaultSummonerProfile!)
    }
    // Sends a reference of the player class to the other form for editing or creating
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if let sVC: EditViewController = segue.destination as? EditViewController{
            
            
                
                sVC.summoner = defaultSummonerProfile
            
           
        }
        
    }
    // Plays a match for the user
    @IBAction func playAMatch(){
        if  defaultSummonerProfile?.SummonerRank != 0  && defaultSummonerProfile != nil{
            defaultSummonerProfile?.playAMatch(sender: self)
            if defaultSummonerProfile != nil{
                update(summoner: defaultSummonerProfile!)}
        }
            // Tells the user they cant play a match because they're unranked
        else {
            let alert = UIAlertController(title: "Warning", message: "You can only play a match when you have a rank! Do your promos!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            
            present(alert, animated: true)
        }
         
    }
    // Updates the profile and all the fields
    // Creates the animation the win ratio wll use
    func update(summoner: SummonerProfile){
        let percAnimation = CABasicAnimation(keyPath: "strokeEnd")
        percAnimation.toValue = summoner.winPercentage * 0.8
        percAnimation.duration = 1
        percAnimation.fillMode = kCAFillModeForwards
        percAnimation.isRemovedOnCompletion = false
        percAnimation.fromValue = 0.00
       // creates the animation the kda will use
        let percAnimation2 = CABasicAnimation(keyPath: "strokeEnd")
        percAnimation2.toValue = summoner.kda * 0.8
        percAnimation2.duration = 1
        percAnimation2.fillMode = kCAFillModeForwards
        percAnimation2.isRemovedOnCompletion = false
        percAnimation2.fromValue = 0.00
        // sets the labels to their percentage values
        percentWinLbl.text = String(format: "%.2f", summoner.winPercentage)
        kdaLbl.text = String(format: "%.2f", summoner.kda)
        
        
        // sets the color of the font and percentage bar to match the value. (IE reds bad, yellows even, greens good)
        if defaultSummonerProfile?.winPercentage == 0.5{
            percentWinLbl.textColor = UIColor.yellow
            percWinBar.strokeColor = UIColor.yellow.cgColor
        }
        else if defaultSummonerProfile!.winPercentage < 0.5{
            percentWinLbl.textColor = UIColor.red
            percWinBar.strokeColor = UIColor.red.cgColor
        }
            
        else {
            percentWinLbl.textColor = UIColor.green
            percWinBar.strokeColor = UIColor.green.cgColor
        }
        
        if defaultSummonerProfile?.kda == 1{
            kdaLbl.textColor = UIColor.yellow
            kda.strokeColor = UIColor.yellow.cgColor
        }
        else if defaultSummonerProfile!.kda < 1{
            kdaLbl.textColor = UIColor.red
            kda.strokeColor = UIColor.red.cgColor
        }
            
        else {
            kdaLbl.textColor = UIColor.green
            kda.strokeColor = UIColor.green.cgColor
        }
        // Adds the animations created earlier to the circles
        
        percWinBar.add(percAnimation, forKey: "Test")
        kda.add(percAnimation2, forKey: "Test")
        
        // updates the image used and the fields with the class information
        rankImage.image = UIImage(named: "\(summoner.SummonerRank)")
        summonerRank.text = SummonerProfile.ranks[summoner.SummonerRank]
        summonerNameLbl.text = summoner.SummonerName
        summonerAccountLevelLbl.text = String(summoner.SummonerAccountLevel)
        winsTilPromoLbl.text = String(20 - summoner.winsForPromotion)
        var lossesTilDemo =  5 + summoner.winsForPromotion
        if lossesTilDemo < 0 {
            lossesTilDemo = lossesTilDemo * -1
        }
        lossTilDemoLbl.text = String(lossesTilDemo)
        winLbl.text = String(summoner.accountWins)
        lossLbl.text = String(summoner.accountLosses)
        killLbl.text = String(summoner.kills)
        deathLbl.text = String(summoner.deaths)
        
        // decides if the user can premote or demote based on current rank
        if summoner.SummonerRank == 7{
            winsTilPromoLbl.text = "∞"
        }
        else if summoner.SummonerRank == 1 {
            lossTilDemoLbl.text = "∞"
        }
        
        
    }
    // Gets the created information if needed to create a summoner if the user wants to input their own information, this also can just update the user if they already had one created
    @IBAction func returnToMain(segue: UIStoryboardSegue){
        if defaultSummonerProfile == nil {
            if let sVC: EditViewController = segue.source as? EditViewController{
                defaultSummonerProfile = SummonerProfile(SummonerName: sVC.summonerNameText.text!, SummonerAccountLevel: Int(sVC.summonerLevelInt.text!)!, SummonerRank: sVC.rankInt, accountWins: Int(sVC.winsInt.text!)!, accountLosses: Int(sVC.lossesInt.text!)!, kills: Int(sVC.killsInt.text!)!, deaths: Int(sVC.deathsInt.text!)!, winsForPromotion: sVC.figureWins())}
        }
        update(summoner: defaultSummonerProfile!)
        
    }
    


}

