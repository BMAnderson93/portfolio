Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

In this exercise you will be creating a custom base class. In the first View you will be displaying the data of the class to the user. In the second View you will allow the user to modify that data.

Pick a generic concept for your base class. For instance the base class might be plant, animal, phone, etc. These are fairly generic concepts with lots of specialized types deriving from each. You may NOT use a Person() or a Vehicle() for your base class. If you do, you will receive a zero, as these concepts are covered in class and in the research videos.

Part 1: Custom Base Class
You will create a custom class in its own file that meets all of the following requirements:

3 Stored Properties, all of different types.
2 Computed Properties 
Will be displayed via UILabels in the first ViewController
These computed properties should derive from your stored properties - as in, they should base their values off of stored property values
2 Instance Methods 
Instance methods should perform some useful functionality pertaining to your class and its objects.
Eg. If I had a Car object, don't print out the sum of 2 + 2. That's not useful.
2 Initializers 
A Default Initializer
One that allows the user to set ALL stored properties
Even if you have more stored properties than are required, ALL of them must be included in this initializer
Part 2: First ViewController

The First ViewController includes an object based off of the Class created above.
The ViewController displays ALL of the properties of the object via UILabels. 
Each stored property should be displayed
Each computed property should be displayed
Has a button for each of the object's Instance Method (2 Buttons)
Executes the respective Instance Method
Has a button to Edit the Class Properties
There should be a segue to the second ViewController
The class object should be passed to the second ViewController by reference
HINT: This does NOT mean passing the individual properties of the class object.
After the second view controller is built, this view controller should update based on changes saved in the second view controller.
Part 3: Second ViewController

There should be X number of input fields in this view controller, where X equals the number of stored properties for the class object
By default, these fields should include the object properties passed from the first view controller
The user should be able to modify each stored property via appropriate custom input.
This includes appropriate keyboard types based on the required data.
e.g. if you have a Bool property, you should use something like a UISwitch to control it, rather than forcing the user to type in True or False
There should be a button to save these changes and return to the previous view controller.
Appropriate error handling should exist for blank/invalid data - the object should not be saved and there should be an alert if the data is invalid
There should be a button to return to the previous view controller without making changes