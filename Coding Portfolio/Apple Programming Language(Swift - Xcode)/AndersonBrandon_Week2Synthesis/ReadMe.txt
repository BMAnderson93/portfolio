Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

For this project you will be creating an application that allows a user to Create and View info about countries. Your app will consist of three(3) ViewControllers each with a specific task.

Each Country will be located on one of the 6 inhabited continents of Earth. So, for instance, if I wanted to create Germany in my application, Germany will be associated with the continent of Europe. Each country must belong to a continent.

You DO NOT have to fill in the information for every country on Earth. The user of your application will be filling in country data. You will provide them a way to do so which includes setting up 6 continents which will contain a list of countries. Each country will consist of a name and a population. All of your data must reside in one variable which will be a Dictionary of Continent Names for Keys. The values for this dictionary will be Dictionaries with country names for Keys and Country populations for Values. In simpler terms: A list of Continents contains lists of countries and their associated populations.

All of the population data displayed for continents and countries must be derived from data that the user has created. If I create Country A with a pop. of 10 and country B with a pop. of 20 both on the continent of Asia. Asia's current total pop is 30. By default, the entire dictionary should be empty; the user of the application will populate all of the data.

Part 1: Data Model

A data model should exist that describes all of the countries in the world as a series of dictionaries that exist within a larger dictionary of continents - a dictionary of dictionaries, if you will. "Data model" just refers to the way that you model your data; you don't need a separate class or anything like that. It's merely the way that data is handled in your application.
The data model can and should be empty by default
Only the relevant continent or country information should be passed forward and backwards from the various ViewControllers; there should be no need to pass the entire world dictionary to each view controller
The data model is NOT a separate file but rather exists within the existing ViewController(s)


Part 2: First ViewController

A Button for each populated continent.
When tapped, takes the user to Second View Controller, to display info for the chosen Continent.
A Label to display the entire World's number of countries and total population as a whole.


e.g. "There are X countries in the entire world, with a total population of Y people."
This data is dynamically updated based on the current state of the dictionary at all times.
Part 3: Second ViewController

A UILabel at the top with the name of the Selected Continent
Will display the selected Continent's information in a UITextView:
Displays total number of countries and total population of the continent currently. 
Name & population of each country associated with the continent.
e.g. "The United States has a population of 300000000 people."
Two navigational buttons


An "Add Country" Button that takes the user to Third View Controller
A "Go Back" Button that takes the user back to First View Controller
Part 4: Third ViewController

UI necessary for creating a new country using input for: 
Country Name and Country Population 
An �Add� Button that adds the current info to the current continent in the form of a new country.
After the new country has been added, the user is taken back to the Second View Controller to see the update to the selected Continent.
If one or both fields do not have valid data, the segue should be prevented and an appropriate alert displayed to the user about the error
There should be a �Cancel� or "Back" Button that segues back to the Second View Controller without attempting to add any new data to the Dictionary.
If the keyboard blocks the navigation buttons and cannot be dismissed, this will be evaluated as a crash in the application.
