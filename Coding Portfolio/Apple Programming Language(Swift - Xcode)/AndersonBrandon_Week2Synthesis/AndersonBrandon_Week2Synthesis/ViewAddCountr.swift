//
//  ViewAddCountr.swift
//  AndersonBrandon_Week2Synthesis
//
//  Created by Brandon Anderson on 4/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewAddCountr: UIViewController {

    // variables that will be used throughout this view
    @IBOutlet weak var viewLeft: UILabel!
    @IBOutlet weak var stringName: UITextField!
    @IBOutlet weak var stringCount: UITextField!
    var totalCount = 0
    var sentDic:[String:Int?] = [:]
    var maxPopulation = 9999999999
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(Int.max / 6)
        // Displays how much more room the current continent can hold
        viewLeft.text = "This continent has room for \(maxPopulation - totalCount) more people!"
        

        // Do any additional setup after loading the view.
    }
    // disables keyboard when clicking outside of the text fields
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    // Used when unwinding this view
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        
        
        // Checks to make sure both fields have something in them
        if stringName.text! == "" || stringCount.text! == ""{
            
            
            displayWarning(warning: "ALL fields are required to be entered")
            return false
        }
        // Checks if the given key already exists
        else if sentDic[stringName.text!] != nil {
             displayWarning(warning: "The country you entered already exists! Please create a new country!")
            return false
        }
        // Makes sure a number is given for the population
        else if validInt(string: stringCount.text!) == false{
            displayWarning(warning: "You must only use a number for the population")
            return false
        }
        // Makes sure the population size doesnt go over the max allowed
        else if totalCount + Int(stringCount.text!)! > maxPopulation {
            displayWarning(warning: "Sorry, but you have you have exeeded the population limit of this continent, either add less people or select a new continent!")
            return false
        }
        
        
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // goes back to the previous form without saving anything
    @IBAction func goBack(){
        dismiss(animated: true, completion: nil)}
    
    // function for displaying a warning to the user depending on what that warning is
    func displayWarning(warning: String){
        let alert = UIAlertController(title: "Warning", message: warning, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    // validates the int is really a int
    func validInt(string: String) -> Bool {
        return Int(string) != nil
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
