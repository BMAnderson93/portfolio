//
//  ViewCountr.swift
//  AndersonBrandon_Week2Synthesis
//
//  Created by Brandon Anderson on 4/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewCountr: UIViewController {

    // Creates my variables and references I will be using in this view
    var chosenCont: String?
    var sentDic:[String:Int] = [:]
    var countryCount = 0
    var totalContPop = 0
    
    
    @IBOutlet weak var txtViewCount: UITextView!
    @IBOutlet weak var lblCont: UILabel!
    @IBOutlet weak var lblTotalPrintOut: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // sets the values of the variables to match the dicitionary being sent in to the form
        lblCont.text = chosenCont
        countryCount = getTotals().0
        totalContPop = getTotals().1
        // loops through the sent dictionary and adds any countries to the view
        
        for i in sentDic{
         txtViewCount.text! += "\(i.key) \n  Population \(i.value) \n"
        }
        // updates the count displayed to the user
        updateCount()   

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Prepares the next form to be loaded in
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Validates the right form is being loaded
        if "toAdd" == segue.identifier{
            
            let viewAdd: ViewAddCountr = segue.destination as! ViewAddCountr
            // updates the variables the next form will need from this one
            viewAdd.sentDic = sentDic
            viewAdd.totalCount = getTotals().1
        }
        }
    
    
    
    // unwinds the viewaddcountr view back to this one
    @IBAction func returnToView(segue: UIStoryboardSegue){
       // validates the right form is coming in
        if let editView: ViewAddCountr = segue.source as? ViewAddCountr{
            
            // creates temp variables for updating the dicitionary
            let tempString: String =  editView.stringName.text!
            let tempNumber: Int = Int(editView.stringCount.text!)!
            
            // updates the dicitionary based on the temp variables and if the key already exists
            sentDic.merge(zip([tempString], [tempNumber])) { (_, new) in new }
            
            // updates the country count and the total cont count
            countryCount = getTotals().0
            totalContPop = getTotals().1
            
            // adds the new country to the view
            txtViewCount.text! += "\(tempString) \n  Population \(tempNumber) \n"
                updateCount()}
            
        
    
    }
    // updates the counts displayed to the user
    func updateCount(){
        
        lblTotalPrintOut.text = "There are a total of \(countryCount) countries on the continent of \(chosenCont!), with a total population of \(totalContPop)."
       
    }
    // Gets the totals for the country count and the total pop count
    func getTotals() -> (Int, Int) {
        let countryCount = sentDic.count
        var totalContPop = 0
        for i in sentDic{
            totalContPop += i.value
        }
        return (countryCount, totalContPop)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
