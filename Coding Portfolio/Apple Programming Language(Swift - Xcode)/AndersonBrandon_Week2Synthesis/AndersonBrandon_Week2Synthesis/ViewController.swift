//
//  ViewController.swift
//  AndersonBrandon_Week2Synthesis
//
//  Created by Brandon Anderson on 4/13/18.
//  Copyright © 2018 Brandon Anderson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // Variables used for editing and assigning values later
    @IBOutlet weak var UpdateFeed: UILabel!
    
    var WorldsDic: [String: [String: Int]?] = ["Africa":[:],"Asia":[:],"Australia":[:],"Europe":[:],"North America":[:],"South America":[:]]
    var allConti = ["Africa","Asia","Australia","Europe","North America","South America"]
    var i = 0
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateFeed()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Prepares the view to be changed to another
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
        // Validates the correct seque
        if "toView" == segue.identifier{
            let viewContr: ViewCountr = segue.destination as! ViewCountr
            // sets the variables needed for the next form
            viewContr.chosenCont = allConti[i]
            viewContr.sentDic = WorldsDic[allConti[i]]!!
            
        }
       
            
        
        
    }
    // Checks which country was picked by the user and assigns the tag
    @IBAction func contPicked(sender: UIButton){
        i = sender.tag
        
        
        // begins the segue
         performSegue(withIdentifier: "toView", sender: sender)
        
        
    }
    
    // unwides the previous form to this one
    @IBAction func returnToSelect(segue: UIStoryboardSegue){
        // validates the right form came
        if let editCountr: ViewCountr = segue.source as? ViewCountr{
            // updates the dictionary with the new information and then updates the feed
            WorldsDic[allConti[i]] = editCountr.sentDic
            updateFeed()
        }
    }

    // Used for updating feed
    func updateFeed(){
    var totCountr = 0
    var worldPop = 0
        // loops through the entire dictionary and checks for how many countries exist and their population
        for i in WorldsDic.keys {
           
            for c in WorldsDic[i]!!{
                totCountr += 1
                worldPop += c.value
            }
        }
        // Updates the text field with the proper information for the user
    UpdateFeed.text = "There are \(totCountr) countries in the world with a total population of \(worldPop)."
    }
    
    

}

