Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

Part 1:  Variable Declarations

Declare a variable of each of the following types in the method requested, providing a value to each of them in the declaration unless otherwise noted:
Explicitly declare a constant Bool
Implicitly declare an array of Strings
Explicitly declare a Float
Implicitly declare an EMPTY array of Ints
Explicitly declare a constant UInt32
Implicitly declare an array of arrays of Doubles
Part 2: String Concatenation

Create two String variables called cityName and stateName with default values that equal the name of your hometown and the state in which it is located. If you are an international student, stateName can equal the name of your country, if you prefer.
Create a third String variable called �hometown� that concatenates the two Strings into a single String WITH a comma in between the cityName and stateName
Create an Int variable called �population" that is equal to the most recent population estimate for your hometown
Print to the console in the following format EXACTLY: �I live in (HOMETOWN), which has (POPULATION) people that live there. "
You must replace (HOMETOWN) and (POPULATION) with the variables that you�ve created.
Part 3: Arrays, Conditionals, and Loops

Declare an empty array of Strings called arrayOfStrings
Using a loop, add the phrase �Swift_(Number)� to the array 5 times, replacing (Number) with the number of times the loop has run up to that point. For instance, the second time the loop runs, it should add �Swift_2� to the array.
After the 5 elements are added to the array, use a conditional statement to see if there are EXACTLY 5 elements in the array
If there are exactly 5 elements in the array, loop through each element and print the value of each individual String to the console
Otherwise, for all other counts, it should print �Here is my array: (arrayOfStrings). I must have added the wrong number of elements to it."
Part 4: Random Values
Create a variable for each of the values defined below and print them out to the console. Make new variables. Do not use any of the declared variables from Part 1. 

A Random Int with a minimum possible value of 0 and a maximum possible value of 1000
A Random Int with a minimum possible value of 0 and a maximum possible value of UINT32_MAX (*hint* The random function that takes in zero parameters does this for you. )
A Random Int with a minimum possible value of 9 and a maximum possible value of 31
A Random Double with a minimum value of 0.0 and a maximum value of 1.0
NOTE: You should NOT use drand48() for this unless you are absolutely certain how it works. Instead, think about this like a percentage where you are trying to get a percentage of a maximum value, in this case UINT32_MAX.
Part 5: Documentation Scavenger Hunt - Arrays and Strings

For this part, you will be searching through the Apple iOS Documentation to find information. This is a daily and integral part of every professional developer�s life, and a skill which is best practiced early and often. Documentation is made so that us programmers can look stuff up to see what something is, how it behaves, and how to use it properly. Documentation is a guide to the SDK (Software Development Kit). Development in general and even just the iOS SDK are much to large for even experienced programmers to memorize every little detail. And, if you develop your skill set to include the ability to effectively seek out information in documentation, you won�t have to. We are starting off with documentation of very common use concepts like arrays. Odds are you will have most of this memorized by accident because you will use it so frequently. But something like a UIPickerControllerDelegate you may not use every day, and knowing your way around the documentation will enable you to create bigger and better programs not bound only by what you have memorized.

Seek the answers in the Apple iOS Documentation, The Swift Programming Guide, or the documentation included within Xcode in order to complete the following section in your playground file. Include a comment above each indicating exactly where you found the information in the documentation, along with a link to that location. Please note that answers from sources outside of official Apple documentation will be considered incorrect.

Create an Array of Strings with default values reflecting:
 The names of each of the instance methods that Array objects can utilize.
Remember - "instance method" is another way to say "function that can be run against an object of this type."
Create an Array of Strings with default values reflecting:
The names of each of the properties that all Array objects contain.
Remember - "property" is another way to say "variable that describes an object of this type."
Create a block comment and in it answer the following questions
What collection types other than Arrays are available in the Swift Standard Library currently?
What does the Substring structure do and why would you use it? (DO NOT JUST COPY AND PASTE THE EXPLANATION; YOU MUST RESTATE THIS IN YOUR OWN WORDS)
Which property on a String can you use to tell you whether a String has characters in it or if it is empty?
The components(separatedBy:) method on a String has two possible types for the input parameter that separatedBy: can use. What types are those?