//: Playground - noun: a place where people can play
// Brandon Anderson

import UIKit

// Part 1: Variable Declarations

let firstBool: Bool = true
var firstArrayStrings = ["s1","s2","s3","s4"]
var firstFloat: Float = 0.00
var firstArrayInt = [Int] ()
let firstUInt32: UInt32 = 1;
var firstArrayofArrayDub = [[Double]]()


// Part 2: String Concatenation

var cityName: String = "Waynesville"
var stateName: String = "Missouri"
var hometown: String = cityName + ", \(stateName)"
var population: Int = 5314
print("I live in \(hometown), which has \(population) people that live there.")

// Part 3: Arrays, Conditionals, and Loops

var arrayOfStrings: [String] = []

for i in 1...5{
    arrayOfStrings.append("Swift_\(i)")
}
if arrayOfStrings.count == 5{
    for value in arrayOfStrings{
        print(value)
    }
}
else {
    print("Here is my array: \(arrayOfStrings). I must have added the wrong number of elements to it")
}

// Part 4: Random Values

var firstRandomInt = arc4random_uniform(1001)
var secondRandomInt = arc4random()
var thirdRandomInt = arc4random_uniform(23) + 9
var fourthRandomInt = Double(arc4random()) / Double(UINT32_MAX)





// Part 5: Documentation Scavenger Hunt - Arrays and Strings

// Found on the apple developer library https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/CollectionTypes.html#//apple_ref/doc/uid/TP40014097-CH8-ID105

var instanceMethodsArray: [String] = ["append","insert","remove","removeLast","removeFirst","removeAll","enumerated"]

// Found on the apple developer library https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/CollectionTypes.html#//apple_ref/doc/uid/TP40014097-CH8-ID105

var propertiesArray: [String] = ["count", "isEmpty"]

/*
 Found in the Collection types section in the apple developer library
 https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/CollectionTypes.html#//apple_ref/doc/uid/TP40014097-CH8-ID105
 
 1. What collection types other than Arrays are available in the Swift Standard Library currently?
 
 Answer - Sets and Dictionaries
 
 Found in the substring documentation from the apple developer page
 https://developer.apple.com/documentation/swift/substring
 
 2. What does the Substring structure do and why would you use it? (DO NOT JUST COPY AND PASTE THE EXPLANATION; YOU MUST RESTATE THIS IN YOUR OWN WORDS)
 
 Answer - A substring allows you to take a section or part of another string and use it by itself without having to create an entirely new variable. This could be used
 for quickly displaying only the key parts of a string to the user rather than displaying the original message in its entirety
 
 Found in string documentation from the apple developer page
 https://developer.apple.com/documentation/swift/string
 
 3. Which property on a String can you use to tell you whether a String has characters in it or if it is empty?
 
 A - isEmpty
 
 
 Found in the apple developer nsstring documentation.
 https://developer.apple.com/documentation/foundation/nsstring
 4. The components(separatedBy:) method on a String has two possible types for the input parameter that separatedBy: can use. What types are those?
 
A - string and a CharacterSet
 */











