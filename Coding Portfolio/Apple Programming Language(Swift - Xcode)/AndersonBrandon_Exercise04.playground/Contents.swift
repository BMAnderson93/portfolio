//: Playground - noun: a place where people can play
// Brandon Anderson

import UIKit

var str = "Hello, playground"

// Part 1
var firstDic = ["FK": true, "SK" : true, "TK": false]
var secDic: [String : Bool] = [:]
var thirdDic: [String: Int?] = ["FK": 1, "SK" : 2, "TK": 3]
var fourthDic:[String: (Int, String, Bool)] = [:]
var fifthDic = ["FK": ["FS","SS","TS"], "SK": ["FS","SS","TS"], "TK": ["FS","SS","TS"]]

// Part 2
var firstOp: Double? = 3.5
var secondOp: String? = nil
var thirdOp: (Int, String, String?, Bool)? = (1,"Hi","Hello", true)
var fourthOp: [[[Int?]]?] = [[[nil]]]

// Part 3
var keysToSearch = ["Key_00", "Key_01", "Orange", "Key_153", "Key_05", "Key_10", "Key_25", "Key_15", "Portal", "Key_20"]

var sixDic: [String: Int] = [:]

for i in 0...24{
    if i < 10{
        sixDic["Key_0\(i)"] = Int(arc4random_uniform(101))}
    else{
    sixDic["Key_\(i)"] = Int(arc4random_uniform(101))}
}
for k in keysToSearch{
if let oldValue = sixDic.updateValue(0, forKey: k)    {
    if (oldValue % 2 != 0){
        sixDic.removeValue(forKey: k)
        print("The previous value assoiciated with \(k) was \(oldValue), but since it was odd, it has been removed from the dictionary.")
    }
    else {
        print("The previous value assoiciated with \(k) was \(oldValue), but now its value is 0.")}
} else {
    print("Key \(k) was not found.")
}
}

