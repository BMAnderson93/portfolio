Instructions:
Follow the guidelines/requirements described below in the video, text, and/or images.

Part 1: Dictionary Declarations
Each Dictionary or Array must be declared with at least 3 entries unless otherwise noted. If the types of something aren�t specified, you may use what you like.

Create a Dictionary using Strings for the Key and Bools for the Value.
Create a Dictionary using Strings for the Key and Doubles for the Value. : Declare this as an Empty Dictionary
Create a Dictionary using Strings for the Key and Optional Ints for the Value.
Create a Dictionary using Strings for the Key and Tuples with 3 different types for the Value. : Declare this as an Empty Dictionary
Create a Dictionary using Strings for the Key and Arrays of Strings for the Value.
Part 2: Optional Declarations

Declare an Optional Double and give it a value in the declaration.
Declare an Optional String with no default value.
Declare an Optional Tuple with 4 elements: Int, String, Optional String, and Bool and give it a value in the declaration
Declare an Array of Optional Arrays of Optional Ints with no default value.
Part 3: Random Values Dictionary

Declare an Array named keysToSearch with the following value:
["Key_00", "Key_01", �Orange", "Key_153", "Key_05", "Key_10", "Key_25", "Key_15", �Portal", "Key_20"]
Create a Dictionary of String keys / Int values
The Int values must be a random number between 0 and 100
The dictionary must contain a minimum of 25 entries.
Auto Generate Key names in the format of "Key_##" where ## starts at 00 and counts up sequentially 01, 02, 03, �, 24 for each entry in the dictionary.
The Int values should be added to the dictionary using the Subscript method (NOT update Dictionary method)
You should be using some sort of control flow to programmatically create this dictionary.
Loop through the keysToSearch Array
Check each element in the keysToSearch array to see if it exists in the dictionary that you created using Optional Binding
If the Key is found and its value is an even
Update the value of the key to 0 and print out the previous value associated with the key.
The value should be updated using the updateDictionary method (NOT Subscript)
If the Key is found and its value is an odd number
Remove that entry from the dictionary whist printing out the previous value for the key
If the Key is not found, print out "Key \(thekeyname) not found"